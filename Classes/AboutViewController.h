//
//  AboutViewController.h
//  FairPrice
//
//  Created by Tridip Sarkar on 26/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface AboutViewController : UIViewController  
{
	UIScrollView *aboutScroll;
	UIImageView *aboutImage;
	UIBarButtonItem *closeBtn;
	
  
}

@property(nonatomic,strong) UIBarButtonItem *closeBtn;
-(void)animateView:(id)FlipView WithTransition:(UIViewAnimationTransition)transition;
-(void)createBody;
-(void)createAboutText;
-(UILabel*)createLabelWithText:(NSString*)strText withYPos:(CGFloat)y withHeight:(CGFloat)h textIsBold:(BOOL)flg;

-(UIScrollView *)CreateScrollViewWithFrame:(CGRect)frame
                               ContentSize:(CGSize)size
						   VscrollVisibled:(BOOL)visible
						   HscrollVisibled:(BOOL)Visible
								WillBounce:(BOOL)no;

@end
