//
//  TAVVideoViewController.h
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface TAVVideoViewController : BaseViewController<UIWebViewDelegate>

@property (nonatomic,strong)UIWebView *wvTipsVideo;

-(void)createWebView;

@end
