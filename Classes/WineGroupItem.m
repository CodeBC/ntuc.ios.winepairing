//
//  WineGroupItem.m
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import "WineGroupItem.h"


@implementation WineGroupItem

@dynamic wineID;
@dynamic groupTitle;
@dynamic wineTypeDesc;
@dynamic foodNames;
@dynamic winePrice;
@dynamic wineName;
@dynamic wineRating;
@dynamic wineCountry;
@dynamic wineImage;
@dynamic groupID;

-(void)requestFinished:(ASIHTTPRequest *)request{
    //    NSLog(@"requestResults = %@",[request responseData]);
    if ([request.username isEqualToString:@"WineImage"])
    {
        self.wineImage = [request responseData];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOAD" object:nil userInfo:nil];
}
-(void)requestFailed:(ASIHTTPRequest *)request
{
    if ([request.username isEqualToString:@"WineImage"])
    {
        self.wineImage = nil;
    }
}


@end
