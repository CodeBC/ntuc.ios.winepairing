//
//  TAVVideoViewController.m
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FairPriceAppDelegate.h"
#import "DataModel.h"
#import "Constants.h"
#import "TAVVideoViewController.h"
#import "TipsVideos.h"
#import "TipsVideoItem.h"

@interface TAVVideoViewController ()

@end

@implementation TAVVideoViewController

@synthesize wvTipsVideo;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void)viewDidLoad
{
    [super viewDidLoad];
    [self createWebView];
    
    
}

- (void)viewDidUnload{
    [super viewDidUnload];
}
-(void)createNavigationMenu{
    [self setTitleText:self.title];
    [super createNavigationMenu];
}

-(void)createWebView{
    wvTipsVideo = [[UIWebView alloc] initWithFrame:CGRectMake(10, 90, 300, 220)];
    wvTipsVideo.delegate = self;
    wvTipsVideo.autoresizesSubviews = YES;
    wvTipsVideo.autoresizingMask=(UIViewAutoresizingFlexibleHeight | 
                                    UIViewAutoresizingFlexibleWidth);
    wvTipsVideo.backgroundColor = [UIColor blackColor];
    wvTipsVideo.scrollView.bounces = NO;
    wvTipsVideo.scrollView.showsVerticalScrollIndicator = NO;
    
    TipsVideoItem *tipsVideoItem = (appDelegate.dataModel.arrTipsVideoItem)[0];
    
    NSString *tipsDesc = [@"http://www.youtube.com/embed/" stringByAppendingString:[tipsVideoItem contentDesc]];
    NSLog(@"Video URL Link = %@",tipsDesc);
    [wvTipsVideo loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:tipsDesc]]];

    [self.view addSubview:wvTipsVideo];
}

-(void)createDescView{
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
