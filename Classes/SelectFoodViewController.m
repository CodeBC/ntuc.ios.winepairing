//
//  SelectFoodViewController.m
//  FairPrice
//
//  Created by niladri.schatterjee on 10/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "SelectFoodViewController.h"
#import "WineResultViewController.h"
#import "AddDishWineViewController.h"
#import "CustomCell.h"
#import "Constants.h"
#import "DishItem.h"
#import "DataModel.h"
#import "FairPriceAppDelegate.h"
#import "UIImage+Retina4.h"

@implementation SelectFoodViewController
@synthesize txtSearch,tblWineForFood,celIndex,arrDishItem,arrTemp,searchResultLabel;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

- (void)viewDidLoad 
{
    NSLog(@"SelectFoodViewController");
    [super viewDidLoad];
	[self setArrDishItem:appDelegate.dataModel.arrDishItem];
	self.arrTemp=[NSMutableArray array];
	[self.arrTemp addObjectsFromArray:self.arrDishItem];
	
	self.searchResultLabel=[[UILabel alloc] init];
	[self.searchResultLabel setFrame:CGRectMake(110,172,100,25)];
	[self.searchResultLabel setBackgroundColor:[UIColor clearColor]];
	[self.searchResultLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:24]];
	[self.searchResultLabel setTextColor:[UIColor whiteColor]];
	[self.searchResultLabel setTextAlignment:UITextAlignmentCenter];
	[self.view addSubview:self.searchResultLabel];
    
	//[self createSearchBar];
	[self createTable];
	//[self.tabBarView setImage:[UIImage imageNamed:@"tabsF.png"]];
	//[self createIntro];
	
}

-(void)viewWillAppear:(BOOL)animated
{
	CustomCell *cell=(CustomCell*)[tblWineForFood cellForRowAtIndexPath:celIndex];
	[cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
	cell.lblFoodName.textColor = [UIColor whiteColor];
	[tblWineForFood reloadData];
}

-(void)createNavigationMenu{    
    [self setTitleText:self.title];
    [super createNavigationMenu];
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/
-(void)createSearchBar
{
    UIImageView* aView = [[UIImageView alloc]initWithFrame:CGRectMake(10.0, START_Y+75.0, 300.0, 45.0)];
    //	[aView setImage:[UIImage imageNamed:@"searchBg.png"]];
	[aView setUserInteractionEnabled:YES];
	
	UIImageView* srchLogo = [[UIImageView alloc]initWithFrame:CGRectMake(20.0, 14.0, 14.0, 15.0)];
	[srchLogo setImage:[UIImage imageNamed:@"icon_search.png"]];
	[aView addSubview:srchLogo];
	
	UITextField* searchText = [[UITextField alloc]initWithFrame:CGRectMake(40.0, 8.0, 210.0, 25.0)];
	//searchText.backgroundColor=[UIColor redColor];
    searchText.placeholder = @"SEARCH";
    [searchText setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:18]];
    [searchText setTextColor:[UIColor whiteColor]];
	[searchText addTarget:self action:@selector(doSearch:) forControlEvents:UIControlEventEditingChanged];
	[searchText setBorderStyle:UITextBorderStyleNone];
	searchText.returnKeyType = UIReturnKeySearch ;
	searchText.delegate = self;
    
	[self setTxtSearch:searchText];
	[aView addSubview:searchText];
	
	UIButton *srchButton = [[UIButton alloc] initWithFrame:CGRectMake(260.0, 11.0, 19.0, 19.0)];
	[srchButton setBackgroundImage:[UIImage imageNamed:@"icon_searchClose.png"] forState:UIControlStateNormal];
	[srchButton addTarget:self action:@selector(clickSBarButton:) forControlEvents:UIControlEventTouchUpInside];
	[aView addSubview:srchButton];
	
	[self.view addSubview:aView];

}	
#pragma mark-
#pragma mark Text Field delegate
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}

-(void)doSearch:(id) sender
{
	UITextField* textField = sender;
	if([textField.text isEqualToString:@""]||textField.text==nil)
	{
		[self.arrDishItem removeAllObjects];
		[self.arrDishItem addObjectsFromArray:self.arrTemp];
		[self.tblWineForFood reloadData];
		
		return;
	}
    [self.arrDishItem removeAllObjects];
	NSInteger counter = 0;
    
	for(DishItem *anItem in self.arrTemp)
	{
		@autoreleasepool {
			NSString* str  = [NSString stringWithFormat:@"%@",anItem.dishTitle];//,[anItem exName][anItem details],
			
			BOOL isFound = ([str rangeOfString:textField.text options:NSCaseInsensitiveSearch].location != NSNotFound);
			if(isFound)
			{
				[self.arrDishItem addObject:anItem];
				counter++; 
			}
		}
		
    }
	
	NSLog(@"Searched %@ Found %d" , textField.text,counter);
	if(counter==0)
	{ 
		[searchResultLabel setText:[NSString stringWithFormat:@"No result!"]];
	}
	else {
		
		[searchResultLabel setText:@""];
	}
	
	[self.tblWineForFood reloadData];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	
	NSLog(@"Searching %@ " , textField.text);
	NSRange rangeOfFirstCharacter=[string rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
	if([[textField text] length]==0 && rangeOfFirstCharacter.location!=NSNotFound)
		return NO;
	
	
	return YES;
	
}


-(void)clickSBarButton:(id)sender
{
	[txtSearch setText:@""];
	[searchResultLabel setText:@""];
	[self doSearch:txtSearch];
}

-(void)createTable
{
	UITableView *aTable = [[UITableView alloc]initWithFrame:CGRectMake(0.0, START_Y+100.0, 320.0, 230.0)];
	
	[aTable setBackgroundColor:[UIColor clearColor]];
	[aTable setAllowsSelection:YES];
	[aTable setDelegate:self];
	[aTable setDataSource:self];
    aTable.showsVerticalScrollIndicator = NO;
	[self setTblWineForFood:aTable];
	[self.view addSubview:tblWineForFood];
    
    /*
     
     settings for button at the end of table
     
     */
	
//	UIButton *addDish = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 460.0-TABBARHEIGHT-45.0, 320.0, 45.0)];
//    
//    [addDish setBackgroundImage:[UIImage imageNamed:@"but_add_your_dish.png"] forState:UIControlStateNormal];
//	[addDish setBackgroundImage:[UIImage imageNamed:@"but_add_your_dish.png"] forState:UIControlStateHighlighted];
//    
//	[addDish addTarget:self action:@selector(clickAddDish:) forControlEvents:UIControlEventTouchUpInside];
//	
//	[self.view addSubview:addDish];
	
}
-(void)clickAddDish:(id)sender
{
	AddDishWineViewController* addDishView= [[AddDishWineViewController alloc] init];//WithNibName:@"AddDishViewController" bundle:[NSBundle mainBundle]];
	addDishView.requestType=FOOD;
	[self.navigationController pushViewController:addDishView	animated:YES];
	
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 40.0;
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 	[self.arrDishItem count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	
	CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell==nil) {
		cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
	
	//	//NewsData* anInfo = [[appDelegate.dataModel arrNews] objectAtIndex:[indexPath row]];
	//	
	//	NewsData* anInfo = [arrTemp objectAtIndex:[indexPath row]];
	DishItem *dishItem=arrDishItem[indexPath.row];
	
	[cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
	//[cell.ivIcon setImage:[UIImage imageNamed:@"icon_curries.png"]];
	[cell.lblFoodName setText:[dishItem.dishTitle uppercaseString]];
	cell.lblFoodName.textColor = [UIColor whiteColor];
	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
	
//    if (groupDish.dishIcon1) 
//	{
//		UIImage *dishIcon=[UIImage imageWithData:groupDish.dishIcon1];
//		
//		CGFloat iconWidth=dishIcon.size.width;
//		CGFloat iconHeight=dishIcon.size.height;
//		CGRect contentRect = cell.contentView.bounds;
//		CGFloat contentHeight=contentRect.size.height;
//        
//        //		[cell.ivIcon setFrame:CGRectMake(60,(contentHeight-iconHeight)/2,iconWidth,iconHeight)];
//        [cell.ivIcon setFrame:CGRectMake(28.0,(contentHeight-iconHeight)/2,iconWidth,iconHeight)];
//		[cell.ivIcon setImage:dishIcon];
//	}
//	
//    //	[cell.ivBackImage setImage:[UIImage imageNamed:@"fairPriceBest_btn.png"]];
//    [cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
//	
//	[cell.lblFoodName setText:groupDish.dishTitle];
//    cell.lblFoodName.textColor = [UIColor whiteColor];
//	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//	cell.selectionStyle = UITableViewCellSelectionStyleNone;
//	cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{  

	
	[self cellAdjustForIndexPath:indexPath isDeleteImage:YES];
	[self setCelIndex:(NSIndexPath *)indexPath];
	CustomCell *cell=(CustomCell*)[tblWineForFood cellForRowAtIndexPath:(NSIndexPath *)indexPath];
//	[cell.ivBackImage setImage:[UIImage imageNamed:@"redBg.png"]];
	[cell.lblFoodName setTextColor:[UIColor whiteColor]];
    
	
	DishItem *dishItem=arrDishItem[indexPath.row];
	NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
	paramDict[@"DishID"] = dishItem.dishID;

	
	[self startIndicator];
	[appDelegate.dataModel startRequestWithParameters:REQUEST_DISH_INFO WithParams:paramDict];
	[self stopIndicator];
    NSLog(@"data model arrDishInfo %D",[appDelegate.dataModel.arrDishInfo count]);
	if ([appDelegate.dataModel.arrDishInfo count]>0)
	{
		WineResultViewController* wineResultView= [[WineResultViewController alloc] init];
        [wineResultView setTitle:cell.lblFoodName.text];
		[self.navigationController pushViewController:wineResultView	animated:YES];
		[self stopIndicator];
	}
	else
	{
		
		[cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
		cell.lblFoodName.textColor = [UIColor whiteColor];	
		[self cellAdjustForIndexPath:indexPath isDeleteImage:NO];
	}
	
	
}


-(void)cellAdjustForIndexPath:(NSIndexPath*)indexPath isDeleteImage:(BOOL)flag
{
	int row = indexPath.row;
	int sec = indexPath.section;
	if (row >0) {
		NSIndexPath* indexP = [NSIndexPath indexPathForRow:row-1 inSection:sec];
		CustomCell *aCell=(CustomCell*)[tblWineForFood cellForRowAtIndexPath:(NSIndexPath *)indexP];
		if(flag)
			[aCell.ivBackImage setImage:nil];
		else 
			[aCell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
		[aCell.lblFoodName setTextColor:[UIColor whiteColor]];
		
	}
	
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
	
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end
