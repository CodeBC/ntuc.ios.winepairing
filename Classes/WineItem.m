//
//  WineItem.m
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import "WineItem.h"


@implementation WineItem

@dynamic wineID;
@dynamic wineName;
@dynamic winePrice;
@dynamic winePriceValue;
@dynamic wineTaste;
@dynamic wineDesc;
@dynamic wineExpert;
@dynamic wineType;
@dynamic wineTypeValue;
@dynamic wineImage;
@dynamic wineRating;
@dynamic wineCountry;
@dynamic wineCountryValue;
@dynamic wineTypeID;
@dynamic wineTypeName;

- (void)requestFinished:(ASIHTTPRequest *)request
{
    NSLog(@"YAHOO");
    if ([request.username isEqualToString:@"WineImage"])
    {
        //       NSLog(@"requestDATA = %@",[request  responseData]);
        self.wineImage = [request responseData];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOAD" object:nil userInfo:nil];
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    if ([request.username isEqualToString:@"WineImage"])
    {
        self.wineImage = nil;
    }
}

@end
