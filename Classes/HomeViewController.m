    //
//  HomeViewController.m
//  FairPrice
//
//  Created by Satish Kumar on 10/7/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import "HomeViewController.h"
#import "WineForFoodViewController.h"
#import "FoodForWineViewController.h"
#import "TipsAndVidViewController.h"
#import "JustWineClubViewController.h"
#import "OrderWineDirectViewController.h"
#import "DataModel.h"
#import "FairPriceAppDelegate.h"
#import "Constants.h"

#import "WineListSearchViewController.h"
#import "WineListResultViewController.h"
#import "WineListAllResultViewController.h"
#import "UIImage+Retina4.h"

@implementation HomeViewController

@synthesize menuButton1,menuButton2,menuButton3,menuButton4,menuButton5,menuButton6,menuButton7;
@synthesize imageLogo1,imageLogo2,imageLogo3,imageLogo4,imageLogo5,imageLogo6,imageLogo7;
@synthesize appLink;

-(void)viewWillAppear:(BOOL)animated
{  
    NSLog(@"WELCOME TO THE HOME VIEW");
    
    if(!self.navigationController.navigationBarHidden){
        [self.navigationController setNavigationBarHidden:YES];
    }
    
    if ([menuButton1 isHighlighted])
    {
        [imageLogo1  setImage:[UIImage imageNamed:@"ico_food_with_wine.png"]];
    }
    else {
        [imageLogo1 setImage:[UIImage imageNamed:@"ico_food_with_wine.png"]];
    }

    
    if ([menuButton2 isHighlighted])
    {   
        [imageLogo2 setImage:[UIImage imageNamed:@"ico_wine_with_food.png"]];
        
    }
    else {
        [imageLogo2  setImage:[UIImage imageNamed:@"ico_wine_with_food.png"]];
    }
	
	if ([menuButton3 isHighlighted])
	{   
		[imageLogo3 setImage:[UIImage imageNamed:@"ico_list_all_wines.png"]];
		
	}
	else {
		[imageLogo3  setImage:[UIImage imageNamed:@"ico_list_all_wines.png"]];
	}
    
    if ([menuButton4 isHighlighted])
	{   
		[imageLogo4 setImage:[UIImage imageNamed:@"ico_tips_n_videos.png"]];
		
	}
	else {
		[imageLogo4  setImage:[UIImage imageNamed:@"ico_tips_n_videos.png"]];
	}
    
    if ([menuButton5 isHighlighted])
	{   
		[imageLogo5 setImage:[UIImage imageNamed:@"ico_join_wine_club.png"]];
		
	}
	else {
		[imageLogo5  setImage:[UIImage imageNamed:@"ico_join_wine_club.png"]];
	}
    
    if ([menuButton6 isHighlighted])
	{   
		[imageLogo6 setImage:[UIImage imageNamed:@"ico_order_wine.png"]];
		
	}
	else {
		[imageLogo6  setImage:[UIImage imageNamed:@"ico_order_wine.png"]];
	}
    
    if ([menuButton7 isHighlighted])
	{   
		[imageLogo7 setImage:[UIImage imageNamed:@"ico_share_link.png"]];
		
	}
	else {
		[imageLogo7  setImage:[UIImage imageNamed:@"ico_share_link.png"]];
	}
    
    
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
   // [super viewDidLoad];
	NSLog(@"AT LEAST I GOT HERE");
    [self.view setMultipleTouchEnabled:NO];
    appDelegate=(FairPriceAppDelegate*)[[UIApplication sharedApplication] delegate];
    self.dataModel = ((FairPriceAppDelegate*)[[UIApplication sharedApplication] delegate]).dataModel;
	[self.navigationController setNavigationBarHidden:YES];
	self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.1/255 green:46.3/255 blue:28.2/255 alpha:0.2];
    [self createBackground];
    [self createNavigationMenu];
	[self setTabIndex:1];
    
    self.title = @"Wine Pairing";
    
    [self createBackground];
    [self createNavigationMenu];
	[self createIntro];
	[self createMenu];
//	[self createImage];
	[self hideBackButton];
    
    
    //Set up POST Request
    NSString *postString = [NSString stringWithFormat:@"client_platform=I"];
    NSLog(@"App Check JSON Request: %@",postString);
    
    
    NSMutableURLRequest *theRequest=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"https://balancedfbapps.com/ntuc/modules/client/_ext_check_app_version.php"]
                                                            cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                        timeoutInterval:60.0];
    [theRequest setHTTPMethod:@"POST"];
    NSData *body = [postString dataUsingEncoding:NSUTF8StringEncoding];
    [theRequest setHTTPBody:body];
    NSLog(@"the REQUEST is %@",theRequest);
    clientCheckConnection =[[NSURLConnection alloc] initWithRequest:theRequest delegate:self];
    
    if (clientCheckConnection) {
        // Create the NSMutableData to hold the received data.
        // receivedData is an instance variable declared elsewhere.
        receivedData = [NSMutableData data];
    }
    
    else {
        NSLog(@"Error starting version check connection.");
        
    }
}

/*
 
 UIImageView for navigation on top of the screen
 
 */
-(void)createNavigationMenu{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y, 300.0, 17.0)];
    [imgView setImage:[UIImage imageNamed: @"img_ntuc_logo_white.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imgView];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+27, 300.0, 34.5)];
    [imgView setImage:[UIImage imageNamed:@"wine_finder_logo.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imgView];
    
//    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.25, 300.0, 1.0)];
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.0, 300.0, 1.0)];
    [imgView setImage:[UIImage imageNamed:@"nav_bar_line.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view bringSubviewToFront:imgView];
    [self.view addSubview:imgView];
}

-(void)createBackground{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    [imgView setImage: [UIImage imageNamed:@"home_bg"]];
    [self.view addSubview:imgView];
}

-(void)createIntro
{
    
	UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, START_Y+80.0,300.0, 25.0)];
	[lbl setText:@"A world first, it's the only app that pairs both"];
	lbl.textAlignment = UITextAlignmentCenter;
	lbl.backgroundColor=[UIColor clearColor];
    lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
	[self.view addSubview:lbl];
	
	lbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, START_Y+95.0,300.0, 25.0)];
	[lbl setText:@"Western and Asian food with wine."];
	lbl.textAlignment = UITextAlignmentCenter;
	lbl.backgroundColor=[UIColor clearColor];
	lbl.textColor = [UIColor whiteColor];
    lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
	[self.view addSubview:lbl];
    
//	UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, START_Y,300.0, 25.0)];
//	[lbl setText:@"Welcome to the"];
//	lbl.textAlignment = UITextAlignmentCenter;
//	lbl.backgroundColor=[UIColor clearColor];
//	lbl.textColor = UIColorFromHex(0x452801);
//	lbl.font = [UIFont boldSystemFontOfSize:18.0];
//	[self.view addSubview:lbl];
//	[lbl release];
//	
//	lbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, START_Y+25.0,300.0, 25.0)];
//	[lbl setText:@"FairPrice wine cellar"];
//	lbl.textAlignment = UITextAlignmentCenter;
//	lbl.backgroundColor=[UIColor clearColor];
//	lbl.textColor = UIColorFromHex(0x452801);
//	lbl.font = [UIFont boldSystemFontOfSize:18.0];
//	[self.view addSubview:lbl];
//	[lbl release];
//	
//	lbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, START_Y+60.0,300.0, 20.0)];
//	[lbl setText:@"Simply select your preferences"];
//	lbl.textAlignment = UITextAlignmentCenter;
//	lbl.backgroundColor=[UIColor clearColor];
//	lbl.textColor = UIColorFromHex(0x452801);
//	lbl.font = [UIFont systemFontOfSize:15.0];
//	[self.view addSubview:lbl];
//	[lbl release];
//
//	
//	lbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, START_Y+80.0,300.0, 20.0)];
//	[lbl setText:@"and you'll be toasting with your"];
//	lbl.textAlignment = UITextAlignmentCenter;
//	lbl.backgroundColor=[UIColor clearColor];
//	lbl.textColor = UIColorFromHex(0x452801);
//	lbl.font = [UIFont systemFontOfSize:15.0];
//	[self.view addSubview:lbl];
//	[lbl release];
//
//	
//	lbl = [[UILabel alloc] initWithFrame:CGRectMake(10.0, START_Y+100.0,300.0, 20.0)];
//	[lbl setText:@"favourite wine in no time."];
//	lbl.textAlignment = UITextAlignmentCenter;
//	lbl.backgroundColor=[UIColor clearColor];
//	lbl.textColor = UIColorFromHex(0x452801);
//	lbl.font = [UIFont systemFontOfSize:15.0];
//	[self.view addSubview:lbl];
//	[lbl release];
	
}

-(void)createMenu{
    
    menuButton1 = [[UIButton alloc] initWithFrame:CGRectMake(10.0 , START_Y+140.0, 300.0, 40.0)];
	[menuButton1 setTitle:@"       PAIR FOOD WITH WINE" forState:UIControlStateNormal];
	[menuButton1 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
	[menuButton1 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [menuButton1.titleLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:18]];

	[menuButton1 setBackgroundImage:[UIImage imageNamed:@"cell_bg.png"] forState:UIControlStateNormal];
//	[menuButton1 setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
	[menuButton1 setExclusiveTouch:YES];
	[menuButton1 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    imageLogo1 = [[UIImageView alloc]initWithFrame:CGRectMake(76.0, 10.0, 13.0, 25.0)];
	[menuButton1 addSubview:imageLogo1];
	
	[self.view addSubview:menuButton1];	
	[menuButton1 setTag:1];
    
    
    
    menuButton2 = [[UIButton alloc] initWithFrame:CGRectMake(10.0 , START_Y+180.0, 300.0, 40.0)];
	[menuButton2 setTitle:@"       PAIR WINE WITH FOOD" forState:UIControlStateNormal];
	[menuButton2 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
	[menuButton2 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [menuButton2.titleLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:18]];
    
	[menuButton2 setBackgroundImage:[UIImage imageNamed:@"cell_bg.png"] forState:UIControlStateNormal];
//	[menuButton2 setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
	[menuButton2 setExclusiveTouch:YES];
	[menuButton2 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    imageLogo2 = [[UIImageView alloc]initWithFrame:CGRectMake(76.0, 10.0, 16.0, 24.0)];
	[menuButton2 addSubview:imageLogo2];
	
	[self.view addSubview:menuButton2];	
	[menuButton2 setTag:2];
    
    
    
    menuButton3 = [[UIButton alloc] initWithFrame:CGRectMake(10.0 , START_Y+220.0, 300.0, 40.0)];
	[menuButton3 setTitle:@"       LIST ALL WINES" forState:UIControlStateNormal];
	[menuButton3 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
	[menuButton3 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [menuButton3.titleLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:18]];
    
	[menuButton3 setBackgroundImage:[UIImage imageNamed:@"cell_bg.png"] forState:UIControlStateNormal];
//	[menuButton3 setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
	[menuButton3 setExclusiveTouch:YES];
	[menuButton3 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    imageLogo3 = [[UIImageView alloc]initWithFrame:CGRectMake(76.0, 10.0, 16, 16)];
    imageLogo3.contentMode = UIViewContentModeScaleAspectFit;
	[menuButton3 addSubview:imageLogo3];
	
	[self.view addSubview:menuButton3];	
	[menuButton3 setTag:3];
    
    
    menuButton4 = [[UIButton alloc] initWithFrame:CGRectMake(10.0 , START_Y+260.0, 300.0, 40.0)];
	[menuButton4 setTitle:@"       TIPS & VIDEOS" forState:UIControlStateNormal];
	[menuButton4 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
	[menuButton4 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [menuButton4.titleLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:18]];
    
	[menuButton4 setBackgroundImage:[UIImage imageNamed:@"cell_bg.png"] forState:UIControlStateNormal];
//	[menuButton4 setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
	[menuButton4 setExclusiveTouch:YES];
	[menuButton4 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    imageLogo4 = [[UIImageView alloc]initWithFrame:CGRectMake(76.0, 10.0, 16, 16)];
    imageLogo4.contentMode = UIViewContentModeScaleAspectFit;
	[menuButton4 addSubview:imageLogo4];
	
	[self.view addSubview:menuButton4];	
	[menuButton4 setTag:4];
    
    menuButton5 = [[UIButton alloc] initWithFrame:CGRectMake(10.0 , START_Y+300.0, 300.0, 40.0)];
	[menuButton5 setTitle:@"       JOIN JUST WINE CLUB" forState:UIControlStateNormal];
	[menuButton5 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
	[menuButton5 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [menuButton5.titleLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:18]];
    
	[menuButton5 setBackgroundImage:[UIImage imageNamed:@"cell_bg.png"] forState:UIControlStateNormal];
//	[menuButton5 setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
	[menuButton5 setExclusiveTouch:YES];
	[menuButton5 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    imageLogo5 = [[UIImageView alloc]initWithFrame:CGRectMake(76.0, 10.0, 16, 16)];
    imageLogo5.contentMode = UIViewContentModeScaleAspectFit;
	[menuButton5 addSubview:imageLogo5];
	
	[self.view addSubview:menuButton5];	
	[menuButton5 setTag:5];
    
    menuButton6 = [[UIButton alloc] initWithFrame:CGRectMake(10.0 , START_Y+340.0, 300.0, 40.0)];
	[menuButton6 setTitle:@"       ORDER IN BULK" forState:UIControlStateNormal];
	[menuButton6 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
	[menuButton6 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [menuButton6.titleLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:18]];
    
	[menuButton6 setBackgroundImage:[UIImage imageNamed:@"cell_bg.png"] forState:UIControlStateNormal];
//	[menuButton6 setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
	[menuButton6 setExclusiveTouch:YES];
	[menuButton6 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    imageLogo6 = [[UIImageView alloc]initWithFrame:CGRectMake(76.0, 10.0, 16, 17.5)];
    imageLogo6.contentMode = UIViewContentModeScaleAspectFit;
	[menuButton6 addSubview:imageLogo6];
	
	[self.view addSubview:menuButton6];	
	[menuButton6 setTag:6];
    
    menuButton7 = [[UIButton alloc] initWithFrame:CGRectMake(10.0 , START_Y+380.0, 300.0, 40.0)];
	[menuButton7 setTitle:@"       SHARE" forState:UIControlStateNormal];
	[menuButton7 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
	[menuButton7 setTitleColor:[UIColor lightGrayColor] forState:UIControlStateHighlighted];
    
    [menuButton7.titleLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:18]];
    
	[menuButton7 setBackgroundImage:[UIImage imageNamed:@"cell_bg.png"] forState:UIControlStateNormal];
//	[menuButton7 setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
	[menuButton7 setExclusiveTouch:YES];
	[menuButton7 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    imageLogo7 = [[UIImageView alloc]initWithFrame:CGRectMake(76.0, 10.0, 22.0, 17.5)];
    imageLogo7.contentMode = UIViewContentModeScaleAspectFit;
	[menuButton7 addSubview:imageLogo7];
	
	[self.view addSubview:menuButton7];	
	[menuButton7 setTag:7];
    
//    /*
//     
//     Settings for Wine for Food on Home Page
//     
//     */
//	 menuButton1 = [[UIButton alloc] initWithFrame:CGRectMake(10.0 , START_Y+140.0, 300.0, 40.0)];
//	[menuButton1 setTitle:@"        Wine for Food" forState:UIControlStateNormal];
//	[menuButton1 setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
//	[menuButton1 setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//	[menuButton1.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
//	[menuButton1 setBackgroundImage:[UIImage imageNamed:@"fairPriceBest_btn.png"] forState:UIControlStateNormal];
//	[menuButton1 setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
//	[menuButton1 setExclusiveTouch:YES];
//	[menuButton1 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
//	 imageLogo1 = [[UIImageView alloc]initWithFrame:CGRectMake(76.0, 10.0, 25.0, 15.0)];
//	[menuButton1 addSubview:imageLogo1];
//	[imageLogo1 release];
//	
//	[self.view addSubview:menuButton1];	
//	[menuButton1 setTag:1];
//	[menuButton1 release];
//    
//    /*
//     
//     Settings for Food for Wine on Home Page
//     
//     */
//    
//	menuButton2 = [[UIButton alloc] initWithFrame:CGRectMake(10.0 , START_Y+175.0, 300.0, 40.0)];
//	[menuButton2 setTitle:@"        Food for Wine" forState:UIControlStateNormal];
//	[menuButton2 setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
//	[menuButton2 setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//	[menuButton2.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
//	[menuButton2 setBackgroundImage:[UIImage imageNamed:@"fairPriceBest_btn.png"] forState:UIControlStateNormal];
//	[menuButton2 setBackgroundImage:[UIImage imageNamed:@"blueBg.png"] forState:UIControlStateHighlighted];
//    
//	[menuButton2 setExclusiveTouch:YES];
//    imageLogo2 = [[UIImageView alloc]initWithFrame:CGRectMake(85.0, 10.0, 7.0, 24.0)];
//	[imageLogo2 setImage:[UIImage imageNamed:@"icon_wine.png"]];
//	[menuButton2 addSubview:imageLogo2];
//	[imageLogo2 release];
//	
//	[menuButton2 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
//	[self.view addSubview:menuButton2];
//	[menuButton2 setTag:2];
//	[menuButton2 release];
//
//    /*
//     
//     Settings for Wine List on Home Page
//     
//     */
//	
//	menuButton3 = [[UIButton alloc] initWithFrame:CGRectMake(10.0 , START_Y+212.0, 300.0, 40.0)];
//	[menuButton3 setTitle:@"Wine List" forState:UIControlStateNormal];
//	[menuButton3 setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
//	[menuButton3 setTitleColor:[UIColor whiteColor] forState:UIControlStateHighlighted];
//	[menuButton3.titleLabel setFont:[UIFont boldSystemFontOfSize:18]];
//	[menuButton3 setBackgroundImage:[UIImage imageNamed:@"fairPriceBest_btn.png"] forState:UIControlStateNormal];
//	[menuButton3 setBackgroundImage:[UIImage imageNamed:@"greenBg.png"] forState:UIControlStateHighlighted];
//	[menuButton3 setExclusiveTouch:YES];
//	imageLogo3 = [[UIImageView alloc]initWithFrame:CGRectMake(85.0, 10.0, 10.0, 23.0)];
//	[imageLogo3 setImage:[UIImage imageNamed:@"icon_list.png"]];
//	[menuButton3 addSubview:imageLogo3];
//	[imageLogo3 release];
//	
//	[menuButton3 addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
//	[self.view addSubview:menuButton3];
//	[menuButton3 setTag:3];
//	[menuButton3 release];
//	
	
}


-(void)clickBtn:(id)sender
{
	int tag = [sender tag];
	//[sender setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	if (tag == 1) 
	{
		if ([sender isHighlighted])
		{
			[imageLogo1  setImage:[UIImage imageNamed:@"ico_food_with_wine.png"]];
		}
		else {
			[imageLogo1 setImage:[UIImage imageNamed:@"ico_food_with_wine.png"]];
		}
        
		[self startIndicator];
		[appDelegate.dataModel startRequestWithParameters:REQUEST_GROUP_DISH WithParams:nil];
		[self stopIndicator];
        
		if ([appDelegate.dataModel.arrGroupDish count] > 0) 
		{
            
			WineForFoodViewController* wineFoodView= [[WineForFoodViewController alloc] init ];
            [self.navigationController pushViewController:wineFoodView	animated:YES];
		}
		else {
			[imageLogo1 setImage:[UIImage imageNamed:@"ico_food_with_wine.png"]];
		}
	}
	else if (tag == 2) 
	{   
		if ([sender isHighlighted])
		{  
			[imageLogo2 setImage:[UIImage imageNamed:@"ico_wine_with_food.png"]];
			
		}
		else {
			[imageLogo2  setImage:[UIImage imageNamed:@"ico_wine_with_food.png"]];
		}
		
		[self startIndicator];
		[appDelegate.dataModel startRequestWithParameters:REQUEST_WINETYPE WithParams:nil];
		[self stopIndicator];
		if ([appDelegate.dataModel.arrWineType count] > 0)
		{
			FoodForWineViewController *foodForWineView=[[FoodForWineViewController alloc]init];
			[self.navigationController pushViewController:foodForWineView animated:YES];
		}
		else {
			[imageLogo2  setImage:[UIImage imageNamed:@"ico_wine_with_food.png"]];
		}
	}
	else if (tag == 3) 
	{ 
		if ([menuButton3 isHighlighted])
		{   
			[imageLogo3 setImage:[UIImage imageNamed:@"ico_list_all_wines.png"]];
			
		}
		else {
			[imageLogo3  setImage:[UIImage imageNamed:@"ico_list_all_wines.png"]];
		}
		
		
		[self startIndicator];
        [appDelegate.dataModel startRequestWithParameters:REQUEST_ALL_WINES WithParams:nil];
//		[appDelegate.dataModel loadSearchFilter];
		[self stopIndicator];
		if ([appDelegate.dataModel.arrWine count] > 0)
		{
            
            WineListAllResultViewController* wineResultView= [[WineListAllResultViewController alloc]init];
            wineResultView.requestFromPage=WINELIST;
            [self.navigationController pushViewController:wineResultView	animated:YES];
            
//		  WineListSearchViewController* wineSearchView= [[WineListSearchViewController alloc] init];
//		  [self.navigationController pushViewController:wineSearchView	animated:YES];
//		  [wineSearchView release];
		}
		else {
			[imageLogo3  setImage:[UIImage imageNamed:@"ico_list_all_wines.png"]];
		}
	}
    else if (tag == 4) 
	{ 
		if ([menuButton4 isHighlighted])
		{   
			[imageLogo4 setImage:[UIImage imageNamed:@"ico_tips_n_videos.png"]];
			
		}
		else {
			[imageLogo4  setImage:[UIImage imageNamed:@"ico_tips_n_videos.png"]];
		}
		
		
		[self startIndicator];
		[appDelegate.dataModel startRequestWithParameters:REQUEST_GET_ALL_TIPS WithParams:nil];
        [appDelegate.dataModel startRequestWithParameters:REQUEST_GET_ALL_TIPS_VIDEOS WithParams:nil];
		[self stopIndicator];        
        
		if ([appDelegate.dataModel.arrTipsArticles count] > 0 && [appDelegate.dataModel.arrTipsVideos count])
		{
            TipsAndVidViewController *tipsView = [[TipsAndVidViewController alloc] init];
            [self.navigationController pushViewController:tipsView	animated:YES];
		}
//		else {
//			[imageLogo4  setImage:[UIImage imageNamed:@"ico_tips_n_videos.png"]];
//		}
	}
    else if (tag == 5) 
	{ 
		if ([menuButton5 isHighlighted])
		{   
			[imageLogo5 setImage:[UIImage imageNamed:@"ico_join_wine_club.png"]];
			
		}
		else {
			[imageLogo5  setImage:[UIImage imageNamed:@"ico_join_wine_club.png"]];
		}
		
//		[self startIndicator];
//		[appDelegate.dataModel startRequestWithParameters:REQUEST_SEARCH_FILTER WithParams:nil];
//		[self stopIndicator];
        
//		if ([appDelegate.dataModel.arrCountry count] > 0)
//		{
            JustWineClubViewController *justWineClubView = [[JustWineClubViewController alloc] init];
            [self.navigationController pushViewController:justWineClubView	animated:YES];
//		}
//		else {
//			[imageLogo5  setImage:[UIImage imageNamed:@"ico_join_wine_club.png"]];
//		}
	}
    else if (tag == 6) 
	{ 
		if ([menuButton6 isHighlighted])
		{   
			[imageLogo6 setImage:[UIImage imageNamed:@"ico_order_wine.png"]];
			
		}
		else {
			[imageLogo6  setImage:[UIImage imageNamed:@"ico_order_wine.png"]];
		}
		
		
//		[self startIndicator];
//		[appDelegate.dataModel startRequestWithParameters:REQUEST_SEARCH_FILTER WithParams:nil];
//		[self stopIndicator];
        
//		if ([appDelegate.dataModel.arrCountry count] > 0)
//		{
            OrderWineDirectViewController *orderWineView = [[OrderWineDirectViewController alloc] init];
            [self.navigationController pushViewController:orderWineView	animated:YES];
//		}
//		else {
//			[imageLogo6  setImage:[UIImage imageNamed:@"ico_order_wine.png"]];
//		}
	}
    else if (tag == 7) 
	{ 
		if ([menuButton7 isHighlighted]){   
			[imageLogo7 setImage:[UIImage imageNamed:@"ico_share_link.png"]];
		}
		else {
			[imageLogo7  setImage:[UIImage imageNamed:@"ico_share_link.png"]];
		}
        
        UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Twitter",@"Email", nil];
        
        [actionSheet showInView:self.view];
		
		
//		[self startIndicator];
//		[appDelegate.dataModel startRequestWithParameters:REQUEST_SEARCH_FILTER WithParams:nil];
//		[self stopIndicator];
//		if ([appDelegate.dataModel.arrCountry count] > 0)
//		{
//            OrderWineDirectViewController *orderWineView = [[OrderWineDirectViewController alloc] init];
//            [self.navigationController pushViewController:orderWineView	animated:YES];
//            [orderWineView release];
//		}
//		else {
//			[imageLogo7  setImage:[UIImage imageNamed:@"ico_share.png"]];
//		}
	}
	
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Overriden to allow any orientation.
    return NO;
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark - Unused Methods

-(void)createImage
{   
	UIButton *fairPriceBtn=[UIButton buttonWithType:UIButtonTypeCustom];
	fairPriceBtn.frame=CGRectMake(50.0, START_Y+280.0, 78.0, 26.0);
	[fairPriceBtn setBackgroundColor:[UIColor clearColor]];
	[fairPriceBtn setImage:[UIImage imageNamed:@"fairPriceLogo.png"] forState:UIControlStateNormal];
	[fairPriceBtn addTarget:self action:@selector(clickfairPriceBtn:) forControlEvents:UIControlEventTouchUpInside];
	[fairPriceBtn setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
	fairPriceBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
	[self.view addSubview:fairPriceBtn];
	
	
	UIButton *justwineBtn=[UIButton buttonWithType:UIButtonTypeCustom];
	justwineBtn.frame=CGRectMake(180.0,START_Y+280.0, 78.0, 26.0);
	[justwineBtn setBackgroundColor:[UIColor clearColor]];
	[justwineBtn setImage:[UIImage imageNamed:@"justwine.png"] forState:UIControlStateNormal];
	[justwineBtn addTarget:self action:@selector(clickjustwineBtn:) forControlEvents:UIControlEventTouchUpInside];
	[justwineBtn setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
	justwineBtn.titleLabel.font = [UIFont systemFontOfSize:13.0];
	[self.view addSubview:justwineBtn];
    
}

/*
 
 Launch NTUC Fairprice in safari ( User will leave application )
 
 */
-(void)clickfairPriceBtn:(id)sender
{
	
	[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.fairprice.com.sg/"]];
}
/*
 
 Launch Just Wine Club in safari ( User will leave application )
 
 */
-(void)clickjustwineBtn:(id)sender
{
	
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.fairprice.com.sg/webapp/wcs/stores/servlet/JWCHomePageView?storeId=90001&catalogId=10052&langId=-1&homePage=Y"]];	
	
	
}

#pragma mark - NSURLConnection Delegate
- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    // This method is called when the server has determined that it
    // has enough information to create the NSURLResponse.
    
    // It can be called multiple times, for example in the case of a
    // redirect, so each time we reset the data.
    
    NSLog(@"Connected to server: %@", [(NSHTTPURLResponse*)response allHeaderFields]);
    [receivedData setLength:0];
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    // Append the new data to allocated connections
    NSLog(@"Updating Data");
    [receivedData appendData:data];
    
}

- (void)connection:(NSURLConnection *)connection
  didFailWithError:(NSError *)error
{
    
    // Output any Errors
    NSLog(@"Connection failed! Error - %@ %@",
          [error localizedDescription],
          [error userInfo][NSURLErrorFailingURLStringErrorKey]);
    
    
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    
    NSLog(@"Client check received %d bytes of data",[receivedData length]);
    NSString *receivedStr = [[NSString alloc ]initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSLog(@"%@",receivedStr);
    
    NSError *jsonError;
    NSDictionary *appJSONData =
    [NSJSONSerialization JSONObjectWithData:receivedData
                                    options:NSJSONReadingMutableLeaves
                                      error:&jsonError];
    
    if (appJSONData) {
        NSLog(@"Version Check JSON Sucess: %@",appJSONData);
        if ([appJSONData[@"status"] intValue] == 1) {
            NSLog(@"API Status: OK");
            if ([[[NSBundle mainBundle] infoDictionary][@"CFBundleShortVersionString"] doubleValue] < [appJSONData[@"version"] doubleValue]) {
                
                UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Update available" message:@"Would you like to update to a newer version of the app?" delegate:self cancelButtonTitle:@"No"otherButtonTitles:@"Yes", nil ];
                [alertView show];

                self.appLink = appJSONData[@"link"];
            }
            
        }
        
    }
    else {
        // Inspect the contents of jsonError
        NSLog(@"App Check Failed: %@", jsonError);
    }
    

}

#pragma mark - UIAlertView Delegate

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSLog(@"%@", self.appLink);
    if (buttonIndex == 1) {

        [[UIApplication sharedApplication] openURL: [NSURL URLWithString:self.appLink]];
    }
}


@end
