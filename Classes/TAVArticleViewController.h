//
//  TAVArticleViewController.h
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface TAVArticleViewController : BaseViewController<UIWebViewDelegate>

@property (nonatomic,strong)UIWebView *wvTipsArticle;

-(void)createWebView;

@end
