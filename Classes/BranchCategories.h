//
//  BranchCategories.h
//  FairPrice
//
//  Created by Zayar on 11/17/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface BranchCategories : NSManagedObject

@property (nonatomic, retain) NSString * b_cate_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * color;

@end
