//
//  TipsVideoItem.h
//  FairPrice
//
//  Created by ddseah on 12/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TipsVideoItem : NSObject

@property (nonatomic, strong) NSString *contentTitle;
@property (nonatomic, strong) NSString *contentDesc;

@end
