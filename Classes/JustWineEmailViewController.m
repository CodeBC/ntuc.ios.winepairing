//
//  JustWineEmailViewController.m
//  FairPrice
//
//  Created by David Seah on 18/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FairPriceAppDelegate.h"
#import "DataModel.h"
#import "JustWineEmailViewController.h"
#import "Constants.h"

@interface JustWineEmailViewController ()

@end

@implementation JustWineEmailViewController

@synthesize txtField;

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createBody];
    [self createField];
    [self createSubmitButton];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.txtField;
}

-(void)createBody{
    UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(20.0, START_Y+40.0,280.0, 125.0)];
    
    //	[lbl setText:@"Buy this wine at your nearest FairPrice Xtra or FairPrice Finest. While stocks last."];
	[lbl setText:@"To receive a copy of the Just Wine membership application form, \nPlease fill in your email address below."];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.backgroundColor = [UIColor clearColor];
	lbl.textColor = [UIColor whiteColor];
	lbl.numberOfLines = 0;
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:13];
	[self.view addSubview:lbl];
}

-(void)createField{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(20.0, START_Y+140.0, 189.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_long.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view bringSubviewToFront:imgView];
    [self.view addSubview:imgView];
    
    self.txtField = [[UITextField alloc]initWithFrame:CGRectMake(23.0, START_Y+141.0, 186.0, 23.0)];
    //    txtField3.placeholder = @"Placeholding";
    [txtField setText:@""];
    [txtField setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField setTextColor:[UIColor whiteColor]];
	[txtField setBorderStyle:UITextBorderStyleNone];
	txtField.returnKeyType = UIReturnKeyDone ;
	txtField.delegate = self;
    
    [self.view addSubview:txtField];
}

-(void)createSubmitButton{
    UIButton *fairPriceButton = [[UIButton alloc] initWithFrame:CGRectMake(20.0 , START_Y+180.0, 180.0, 40.0)];
	fairPriceButton.tag=1;
	[fairPriceButton addTarget:self action:@selector(onClickSubmit:) forControlEvents:UIControlEventTouchUpInside];
	[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"but_send_membership.png"] forState:UIControlStateNormal];
	[self.view addSubview:fairPriceButton];
}

-(BOOL)validateEmail:(NSString *)candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex]; 
	
    return [emailTest evaluateWithObject:candidate];
}

-(IBAction)onClickSubmit:(id)sender{
    if([self validateEmail:txtField.text]){
        
        NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
        paramDict[@"Email"] = txtField.text;
        
        [self startIndicator];
        [appDelegate.dataModel startRequestWithParameters:REQUEST_SEND_EMAIL WithParams:paramDict];
		[self stopIndicator];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }else{
        [self showAlertWithMessage:@"Invalid E-mail address"];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField resignFirstResponder];
    return NO;
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
