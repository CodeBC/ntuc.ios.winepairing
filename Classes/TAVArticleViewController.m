//
//  TAVArticleViewController.m
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FairPriceAppDelegate.h"
#import "DataModel.h"
#import "TAVArticleViewController.h"
#import "TipsArticleItem.h"
#import "Constants.h"

@interface TAVArticleViewController ()

@end

@implementation TAVArticleViewController

@synthesize wvTipsArticle;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad{
    [super viewDidLoad];
    [self createWebView];

}

- (void)viewDidUnload{
    [super viewDidUnload];
}
-(void)createNavigationMenu{
    [self setTitleText:self.title];
    [super createNavigationMenu];
}

-(void)createWebView{
    
    wvTipsArticle = [[UIWebView alloc] initWithFrame:CGRectMake(0, 90, 320, 300)];
    wvTipsArticle.delegate = self;
    wvTipsArticle.autoresizesSubviews = YES;
    wvTipsArticle.autoresizingMask=(UIViewAutoresizingFlexibleHeight | 
                               UIViewAutoresizingFlexibleWidth);
    wvTipsArticle.opaque = NO;
    wvTipsArticle.backgroundColor = [UIColor clearColor];
    wvTipsArticle.scrollView.bounces = NO;
    wvTipsArticle.scrollView.showsVerticalScrollIndicator = NO;

    NSString *tipsDesc = [(appDelegate.dataModel.arrTipsArticleItem)[0] contentDesc];
    
//    [wvTipsArticle loadHTMLString:tipsDesc baseURL:nil];
    [wvTipsArticle loadHTMLString:[NSString stringWithFormat:@"<html><body> <font face=\"EuphemiaUCAS\" size=\"2\" color='#ffffff'> %@ </font></body></html>",tipsDesc] baseURL:nil];
    [self.view addSubview:wvTipsArticle];
    
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
