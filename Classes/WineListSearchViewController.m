//
//  WineListSearchViewController.m
//  FairPrice
//
//  Created by niladri.schatterjee on 10/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "WineListSearchViewController.h"
#import "WineListResultViewController.h"
#import "WineListAllResultViewController.h"
#import "AddDishWineViewController.h"
#import "DataModel.h"
#import "FairPriceAppDelegate.h"
#import "Country.h"
#import "PriceRange.h"
#import "WineGroupType.h"
#import "UIImage+Retina4.h"
@implementation WineListSearchViewController
@synthesize strPickerValue,picker_View,lbl1,arrLbl,selIndex,arrWineType,arrCountry,arrRating,arrPrice,btDone,priceIndex,countryIndex,wineIndex,pickerid,requestFromPage;//aView;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    self.title = @"FILTER";
    [super viewDidLoad];
    NSLog(@"WineListSearchViewController");
	arrLbl = [[NSMutableArray alloc]initWithCapacity:4];

	//[self createSearchBar];
	[self createMenu];
    //	[self addWine];
    [self createDoneButton];
//	[self setTitle:@"Wine List"];
	
	Country *countryData=[[Country alloc]init];
	PriceRange *priceData = [[PriceRange alloc]init];
    WineGroupType * wineGroupData = [[WineGroupType alloc]init];
	
	[countryData setCountryCode:@"All"];
	[countryData setCountryName:@"All"];
	
	[priceData setTextValue:@"All"];
	[priceData setValueValue:@"All"];
    
    [wineGroupData setWineConfigKey:@"All"];
    [wineGroupData setWineConfigValue:@"All"];
	
    for(WineGroupType *wgt in appDelegate.dataModel.arrWineGroupType ){
        NSLog(@"appDelegate.arrwineGroupType = %@",wgt.wineConfigValue);        
    }

	self.arrWineType = [[NSMutableArray alloc] initWithCapacity:2];
	[self.arrWineType addObject:wineGroupData];
	[self.arrWineType addObjectsFromArray:appDelegate.dataModel.arrWineGroupType];
    
	self.arrCountry = [[NSMutableArray alloc] initWithCapacity:2];
	[self.arrCountry addObject:countryData];
	[self.arrCountry addObjectsFromArray:appDelegate.dataModel.arrCountry];
    
    self.arrPrice = [[NSMutableArray alloc]initWithCapacity:2];
    [self.arrPrice addObject:priceData];
    [self addPrice];
    
	[self setArrRating:[NSMutableArray array]];
	[self.arrRating addObject:@"All"];
	
	for(NSInteger i=0;i<5;i++){   
		[self.arrRating addObject:[NSString stringWithFormat:@"%d",i+1]];
	}
    
//    count=[appDelegate.dataModel.arrPriceRange count];
//	PriceRange *range;
//	
//	for(int i=0;i<count;i++)
//	{
//		range=[appDelegate.dataModel.arrPriceRange objectAtIndex:i];
//		
//		
//		if ([range.minPrice caseInsensitiveCompare:@"x"]==NSOrderedSame)
//		{
//			[self.arrPrice addObject:[NSString stringWithFormat:@"< S$%@",range.maxPrice]];
//		}
//	    else if([range.maxPrice caseInsensitiveCompare:@"x"]==NSOrderedSame)
//		{
//			[self.arrPrice addObject:[NSString stringWithFormat:@"> S$%@",range.minPrice]];		 
//	    }
//		else if([range.maxPrice caseInsensitiveCompare:@"All"]==NSOrderedSame && [range.minPrice caseInsensitiveCompare:@"All"]==NSOrderedSame)
//		{
//			[self.arrPrice addObject:[NSString stringWithFormat:@"%@",range.minPrice]];		 
//	    }
//		
//	    else
//		{
//			[self.arrPrice addObject:[NSString stringWithFormat:@"S$%@ - S$%@",range.minPrice,range.maxPrice]];
//		}
//	}
//self.arrPrice =[[NSMutableArray  alloc] initWithObjects:@"$19.90",@"$20.99",@"$25.99",nil];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([[self.navigationController viewControllers] count]==1)
	{
		[self hideBackButton];
	}
}

-(void)createNavigationMenu{  
    [self setTitleText:self.title];
    [super createNavigationMenu];
}

-(void)createMenu
{
	UIImageView* aView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0 , START_Y+120.0, 300.0, 40.0)];
	[aView setImage:[UIImage imageNamed:@"cell_bg.png"]];
	[aView setUserInteractionEnabled:YES];
	
	UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(30.0, 10.0, 100.0, 20.0)];
    lbl.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:20];
	[lbl setBackgroundColor:[UIColor clearColor]];
	[lbl setText:@"TYPE"];
	[lbl setTextColor:[UIColor whiteColor]];
	[aView addSubview:lbl];
	
	UILabel* lblType = [[UILabel alloc]initWithFrame:CGRectMake(140.0, 10.0, 100.0, 20.0)];
    lblType.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:20];
	[lblType setBackgroundColor:[UIColor clearColor]];
	lblType.textAlignment = UITextAlignmentRight;
	[lblType setText:@"All"];
	[arrLbl addObject:lblType];
	[lblType setTextColor:[UIColor whiteColor]];
	[aView addSubview:lblType];
	
	UIImageView *downarrow=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_downArrow.png"]];
	downarrow.frame=CGRectMake(225.0-25,10, 14,9);
	
	UIButton *btn  = [UIButton buttonWithType:UIButtonTypeCustom];			
	btn.frame=CGRectMake(50.0, 5.0, 225.0, 30.0);
	[btn setTag:1];
	[btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
	[btn addSubview:downarrow];
	[aView addSubview:btn];
	
	//[btn release];
	[self.view addSubview:aView];
    /////////////////////////////////////////////////////////////////////////////
    aView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0 , START_Y+80.0, 300.0, 40.0)];
	[aView setImage:[UIImage imageNamed:@"cell_bg.png"]];
	[aView setUserInteractionEnabled:YES];
	
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(30.0, 10.0, 100.0, 20.0)];
    lbl.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:20];
	[lbl setBackgroundColor:[UIColor clearColor]];
	[lbl setText:@"COUNTRY"];
	[lbl setTextColor:[UIColor whiteColor]];
	[aView addSubview:lbl];
	
	UILabel* lblCountry = [[UILabel alloc]initWithFrame:CGRectMake(140.0, 10.0, 100.0, 20.0)];
    lblCountry.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:20];
	[lblCountry setBackgroundColor:[UIColor clearColor]];
	lblCountry.textAlignment = UITextAlignmentRight;
	[lblCountry setText:@"All"];
	[arrLbl addObject:lblCountry];
	[lblCountry setTextColor:[UIColor whiteColor]];
	[aView addSubview:lblCountry];
	
	
	downarrow=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_downArrow.png"]];
	downarrow.frame=CGRectMake(225.0-25,10, 14,9);
	btn  = [UIButton buttonWithType:UIButtonTypeCustom];			
	btn.frame=CGRectMake(50.0, 5.0, 225.0, 30.0);
	//btn.backgroundColor=[UIColor redColor];
	//btn  = [[UIButton alloc]initWithFrame:CGRectMake(50.0, 5.0, 200.0, 30.0)];
	//[btn setBackgroundImage:[UIImage imageNamed:@"icon_downArrow.png"] forState:UIControlStateNormal];
	//[btn setBackgroundImage:[UIImage imageNamed:@"icon_downArrow_hover.png"] forState:UIControlStateHighlighted];
	[btn setTag:2];
	[btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
	[btn addSubview:downarrow];
	[aView addSubview:btn];
	//[btn release];
	
	[self.view addSubview:aView];
	
    //////////////////////////////////////////////////////////////////////////////////	
	aView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0 , START_Y+160.0, 300.0, 40.0)];
	[aView setImage:[UIImage imageNamed:@"cell_bg.png"]];
	[aView setUserInteractionEnabled:YES];
	
	lbl = [[UILabel alloc]initWithFrame:CGRectMake(30.0, 10.0, 100.0, 20.0)];
    lbl.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:20];
	[lbl setBackgroundColor:[UIColor clearColor]];
	[lbl setText:@"RATING"];
	[lbl setTextColor:[UIColor whiteColor]];
	[aView addSubview:lbl];
	
	UILabel* lblRating = [[UILabel alloc]initWithFrame:CGRectMake(140.0, 10.0, 100.0, 20.0)];
    lblRating.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:20];
	[lblRating setBackgroundColor:[UIColor clearColor]];
	lblRating.textAlignment = UITextAlignmentRight;
	[lblRating setText:@"All"];
	[arrLbl addObject:lblRating];
	[lblRating setTextColor:[UIColor whiteColor]];
	[aView addSubview:lblRating];
	
	
	downarrow=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_downArrow.png"]];
	downarrow.frame=CGRectMake(225.0-25,10, 14,9);
	btn  = [UIButton buttonWithType:UIButtonTypeCustom];			
	btn.frame=CGRectMake(50.0, 5.0, 225.0, 30.0);
	//btn.backgroundColor=[UIColor redColor];
	//btn  = [[UIButton alloc]initWithFrame:CGRectMake(50.0, 5.0, 200.0, 30.0)];
	//[btn setBackgroundImage:[UIImage imageNamed:@"icon_downArrow.png"] forState:UIControlStateNormal];
	//[btn setBackgroundImage:[UIImage imageNamed:@"icon_downArrow_hover.png"] forState:UIControlStateHighlighted];
	[btn setTag:3];
	[btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
	[btn addSubview:downarrow];
	[aView addSubview:btn];
	//[btn release];
    
	//[self.view addSubview:aView];
	
    /////////////////////////////////////////////////////////////////////////////
	
	aView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0 , START_Y+160.0, 300.0, 40.0)];
	[aView setImage:[UIImage imageNamed:@"cell_bg.png"]];
	[aView setUserInteractionEnabled:YES];
	
	lbl = [[UILabel alloc]initWithFrame:CGRectMake(30.0, 10.0, 100.0, 20.0)];
    lbl.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:20];
	[lbl setBackgroundColor:[UIColor clearColor]];
	[lbl setText:@"PRICE"];
	[lbl setTextColor:[UIColor whiteColor]];
	[aView addSubview:lbl];
	
	UILabel* lblPrice = [[UILabel alloc]initWithFrame:CGRectMake(140.0, 10.0, 100.0, 20.0)];
    lblPrice.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:20];
	[lblPrice setBackgroundColor:[UIColor clearColor]];
	lblPrice.textAlignment = UITextAlignmentRight;
	[lblPrice setText:@"All"];
	[arrLbl addObject:lblPrice];
	[lblPrice setTextColor:[UIColor whiteColor]];
	[aView addSubview:lblPrice];
	
	
	downarrow=[[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon_downArrow.png"]];
	downarrow.frame=CGRectMake(225.0-25,10, 14,9);
	btn  = [UIButton buttonWithType:UIButtonTypeCustom];			
	btn.frame=CGRectMake(50.0, 5.0, 225.0, 30.0);
	//btn.backgroundColor=[UIColor redColor];
	//[btn setBackgroundImage:[UIImage imageNamed:@"icon_downArrow.png"] forState:UIControlStateNormal];
	//[btn setBackgroundImage:[UIImage imageNamed:@"icon_downArrow_hover.png"] forState:UIControlStateHighlighted];
	[btn setTag:4];
	[btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
	[btn addSubview:downarrow];
	[aView addSubview:btn];
	//[btn release];
	
	[self.view addSubview:aView];
	
	//////////////////////////////////////////////////
    //	UIButton* btnSearch = [[UIButton alloc] initWithFrame:CGRectMake(113.0,START_Y+220.0, 99.0, 35.0)];
    //	[btnSearch addTarget:self action:@selector(clickSearchBtn:) forControlEvents:UIControlEventTouchUpInside];
    //	[btnSearch setBackgroundImage:[UIImage imageNamed:@"searchBtn.png"] forState:UIControlStateNormal];
    //	[self.view addSubview:btnSearch];
    //	[btnSearch release];
	
	NSString* titleActionSheet=@"";
	titleActionSheet=[titleActionSheet stringByAppendingString:@"\n\n\n\n\n\n\n\n\n\n\n\n\n"];
	
	pickerActionsheet=[[UIActionSheet alloc] initWithTitle:titleActionSheet delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
	pickerActionsheet.actionSheetStyle=UIActionSheetStyleBlackOpaque;
	
	UIButton *saveButton=[[UIButton alloc]init];
	saveButton.frame=CGRectMake(250,5,62, 30);
	[saveButton setBackgroundColor:[UIColor clearColor]];
	[saveButton setBackgroundImage:[UIImage imageNamed:@"doneBtn.png"] forState:UIControlStateNormal];
	saveButton.tag = 1;
	saveButton.showsTouchWhenHighlighted=YES;
	[saveButton addTarget:self action:@selector(handelDoneEvent:) forControlEvents:UIControlEventTouchUpInside];
	[pickerActionsheet addSubview:saveButton];
	
	
}
-(void)clickSearchBtn:(id)sender{   

	PriceRange *rangeParam = (self.arrPrice)[self.priceIndex]; 
    Country *countryParam = (self.arrCountry)[self.countryIndex];
    WineGroupType *wineGroupParam = (self.arrWineType)[self.wineIndex];
    
	NSLog(@"self.arrLbl count = %d",[self.arrLbl count]);
    
//	NSString *wineType=[[self.arrLbl objectAtIndex:0] text];
	NSString *rating=[(self.arrLbl)[2] text];
//	NSString *countryTitle=[[self.arrLbl objectAtIndex:1] text];
    
    NSLog(@"countryParam = %@",countryParam.countryCode);
    NSLog(@"wineGroupParam = %@",wineGroupParam.wineConfigKey);
    NSLog(@"rangeParam = %@",rangeParam.valueValue);
    NSLog(@"rating = %@",rating);
    
	NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
	paramDict[@"WineType"] = wineGroupParam.wineConfigKey;
    paramDict[@"PriceRange"] = rangeParam.valueValue;
	paramDict[@"Rating"] = rating;
	paramDict[@"CountryTitle"] = countryParam.countryCode;
    
	[self startIndicator];
	[appDelegate.dataModel startRequestWithParameters:REQUEST_SEARCH_RESULT WithParams:paramDict];
	[self stopIndicator];
    
	if ([appDelegate.dataModel.arrWine count]>0) 
	{
		WineListAllResultViewController* wineResultView= [[WineListAllResultViewController alloc]init];
		wineResultView.requestFromPage=WINELIST;
		[self.navigationController pushViewController:wineResultView	animated:YES];
	}
	
	
}
-(void)clickBtn:(id)sender{   
	[self showPickerWithTag:[sender tag]];
	
	[self setSelIndex:[sender tag]];
	
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.4];
	
	switch ([sender tag]) 
	{
		case 3:
			self.view.center=CGPointMake(self.view.center.x,self.view.center.y-35);
			break;
		case 4:	
			self.view.center=CGPointMake(self.view.center.x,self.view.center.y-70);
			break;
            
	}
    
	[UIView commitAnimations];
	//[self createPickerView:[sender tag]];
	
	
	;//int tag = [sender tag];
	
}	
#pragma mark-
#pragma mark Picker View Methods
-(void)handelDoneEvent:(id)sender{ 
	//NSInteger aTag=[sender tag];
	NSString *valueStr = nil;
	int row=[picker_View selectedRowInComponent:0];
	//if(aTag==1)
    NSLog(@"row id %d and pickerid %d and arrPrice count %d",row,pickerid,arrPrice.count);
	if(pickerid==1)
	{
        WineGroupType *wineGroupData =arrWineType[row];
		valueStr=wineGroupData.wineConfigValue;
//        valueStr=[arrWineType objectAtIndex:row];
    }
    else if(pickerid==2)       //if(aTag==2)	
    { 
		Country *countryData=arrCountry[row];	
		valueStr=countryData.countryName;
	}
	else if(pickerid==3)        //(aTag==3)
	{
		valueStr=arrRating[row];
	}else if(pickerid==4)        //if (aTag==4)
	{  
        PriceRange *priceData = arrPrice[row];
        valueStr=priceData.textValue;
	}
	self.strPickerValue = valueStr;
	NSLog(@"Price Index:%d",self.priceIndex);
    NSLog(@"country Index:%d",self.countryIndex);
    NSLog(@"wineGroup Index:%d",self.wineIndex);
	NSLog(@"Selected: %@ Array Count:%d:Tag:%d",strPickerValue,[arrLbl count],selIndex);
	
    //aTag = aTag-1;
	[(self.arrLbl)[selIndex-1] setText:strPickerValue];
	
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.4];
	
	switch (pickerid) 
	{
		case 3:
			self.view.center=CGPointMake(self.view.center.x,self.view.center.y+35);
			break;
		case 4:	
			self.view.center=CGPointMake(self.view.center.x,self.view.center.y+70);
			break;
	}
	
	[UIView commitAnimations];
	[UIView commitAnimations];
	
	[pickerActionsheet dismissWithClickedButtonIndex:0 animated:YES];
    
    [picker_View removeFromSuperview];
	//[[[sender superview] superview] removeFromSuperview];
}


-(void)createPickerView:(int)index
{
	UIView* parentView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, 320, 480)];
    //	[parentView setBackgroundColor:[UIColor blackColor]];
    //	[parentView setAlpha:0.6];
	UIView* aView=[[UIView alloc]initWithFrame:CGRectMake(0.0, 180.0, 320,500 )];//250
	[aView setBackgroundColor:[UIColor blackColor]];
	
	
	UIPickerView *aPickerView=[[UIPickerView alloc] initWithFrame:CGRectMake(0, 35.0, 100, 235)];
	aPickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	
	//arrayNo = [[NSMutableArray alloc] initWithObjects:@" Speaker",@" Title",@" Topic",nil];
	
	
	aPickerView.delegate=self;
    aPickerView.dataSource=self;
//	[aPickerView setDataSource:self];
	aPickerView.showsSelectionIndicator = YES;
	aPickerView.tag=index;
	if (picker_View) {
		[picker_View.superview removeFromSuperview];
	}
	[self setPicker_View:aPickerView];
	
	self.btDone = [UIButton buttonWithType:UIButtonTypeCustom];
	btDone.frame = CGRectMake(240, 3.0, 80.0, 30.0);
	btDone.tag=index;
	[btDone  setImage:[UIImage imageNamed:@"doneBtn.png"] forState:UIControlStateNormal];
	[btDone  setImage:[UIImage imageNamed:@"doneBtn.png"] forState:UIControlStateHighlighted];
	//[btDone setTitle:@"Done" forState:UIControlStateNormal];
	//[btDone setBackgroundColor:[UIColor blackColor]];//WithPatternImage:[UIImage imageNamed:@"bottom_bg.png"]]];
	[aView addSubview:btDone];
	[btDone addTarget:self action:@selector(handelDoneEvent:) forControlEvents:UIControlEventTouchUpInside];
	[btDone setShowsTouchWhenHighlighted:YES];
	
	[picker_View selectRow:1 inComponent:0 animated:NO];
	[aView addSubview:picker_View];
	[parentView addSubview:aView];
	[self.view addSubview:parentView];
	//[self.view addSubview:pickerView];
	
}


-(void)showPickerWithTag:(int)index{
	[self setPickerid:index];
	self.picker_View= [[UIPickerView alloc] initWithFrame:CGRectZero];
	self.picker_View.delegate = self;
	//picker_View.tag=index;
	self.picker_View.frame =CGRectMake(5,40 ,310,216);
	[self.picker_View selectRow:1 inComponent:0 animated:YES];
	
	self.picker_View.showsSelectionIndicator =YES;
	[pickerActionsheet addSubview:self.picker_View];
	[self.picker_View setDataSource:self];
    [pickerActionsheet showInView:self.view];
}


#pragma mark-
#pragma mark Picker View delegates
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;{
	
	return 1;
	
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{  
	NSString* valueForArray=@"";
	//if(picker_View.tag==1)
	if(pickerid==1)	{
        WineGroupType *wineGroupData =arrWineType[row];
		valueForArray=wineGroupData.wineConfigValue;
        self.wineIndex = row;
    }else if(pickerid==2){
		Country *countryData=arrCountry[row];
		valueForArray=countryData.countryName;
        self.countryIndex = row;
	}else if(pickerid==3)	{
        valueForArray=arrRating[row];
    }
	else  if(pickerid==4){
        PriceRange *priceData =arrPrice[row];
		valueForArray=priceData.textValue;
        self.priceIndex = row;
    }
	//[picker_View removeFromSuperview];
	NSLog(@"Selected %@",valueForArray);
	//mlabel.text= [arrayNo objectAtIndex:row];
	[self setStrPickerValue:valueForArray];
	//[btShow setTitle:valueForArray forState:UIControlStateNormal];
	
}
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{   
	NSInteger rowCount=0;
	
	//if(picker_View.tag==1)
	if(pickerid==1)
        rowCount=[arrWineType count];
	else if(pickerid==2)          //if(picker_View.tag==2)
        rowCount=[arrCountry count];
	else if(pickerid==3){         //if(picker_View.tag==3)
        rowCount=[arrRating count];
        NSLog(@"arrRating = %d",[arrRating count]);
    }else if(pickerid==4){          //if(picker_View.tag==4)
        rowCount=[arrPrice count];
    NSLog(@"arrPrice count = %d",[arrPrice count]);
    }
    
	return rowCount;			 
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{ 
    NSString *item=@"";
    //if(picker_View.tag==1)
	if(pickerid==1){
        WineGroupType *wineGroupData =arrWineType[row];
        item=wineGroupData.wineConfigValue;
    }
	else if(pickerid==2)          //if(picker_View.tag==2)
	{ 
        Country *countryData=arrCountry[row];
		item=countryData.countryName;
        NSLog(@"itemPriceRow = %d",row);
        NSLog(@"itemPrice = %@",item);
	}
	else if(pickerid==3)              //if(picker_View.tag==3)
		item=arrRating[row];
	else  if(pickerid==4){             //if(picker_View.tag==4)
        PriceRange *priceData = arrPrice[row];
        item = priceData.textValue;
        NSLog(@"itemPriceRow = %d",row);
        NSLog(@"itemPrice = %@",item);
	}
    
	return item;
}

#pragma mark-

-(void)dismissActionSheet:(id)sender{
	[picker_View removeFromSuperview];
}

-(void)addWine
{
	UIButton *addDish = [[UIButton alloc] initWithFrame:CGRectMake(10.0, START_Y+280.0, 300.0, 50.0)];
	[addDish setBackgroundImage:[UIImage imageNamed:@"addBg-Area.png"] forState:UIControlStateNormal];
	[addDish setBackgroundImage:[UIImage imageNamed:@"addBg-Area.png"] forState:UIControlStateHighlighted];
	[addDish addTarget:self action:@selector(clickAddWine:) forControlEvents:UIControlEventTouchUpInside];
	[addDish setTitle:@"  Add Your Wine" forState:UIControlStateNormal];
	[addDish.titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
	[addDish setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
	
	UIImageView* imgAdd = [[UIImageView alloc]initWithFrame:CGRectMake(60.0, 16.0, 17.0, 17.0)];
	[imgAdd setImage:[UIImage imageNamed:@"icon_Add.png"]];
	[addDish addSubview:imgAdd];
	
	[self.view addSubview:addDish];
	
}

-(void)addPrice{
    PriceRange *range1=[[PriceRange alloc]init];
    PriceRange *range2=[[PriceRange alloc]init];
    PriceRange *range3=[[PriceRange alloc]init];
    PriceRange *range4=[[PriceRange alloc]init];
    PriceRange *range5=[[PriceRange alloc]init];
    PriceRange *range6=[[PriceRange alloc]init];
    
    range1.textValue=@"<=$20";
    range1.valueValue=@"0-20";
    [self.arrPrice addObject:range1];
    
    range2.textValue=@"$20.01-$40";
    range2.valueValue=@"20.01-40";
    [self.arrPrice addObject:range2];
    
    range3.textValue=@"$40.01-$60";
    range3.valueValue=@"40.01-60";
    [self.arrPrice addObject:range3];
    
    range4.textValue=@"$60.01-$80";
    range4.valueValue=@"60.01-80";
    [self.arrPrice addObject:range4];
    
    range5.textValue=@"$80.01-$100";
    range5.valueValue=@"80.01-100";
    [self.arrPrice addObject:range5];
    
    range6.textValue=@">$100";
    range6.valueValue=@"100-";
    [self.arrPrice addObject:range6];
    
    for(id arrPricing in arrPrice){
        NSLog(@"arrPrice = %@",[(PriceRange *)arrPricing textValue]);
    }

}

-(void)addRating{

}

-(void)createDoneButton{
    UIButton *addDish = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 460.0-TABBARHEIGHT-45.0, 320.0, 45.0)];
    
    [addDish setBackgroundImage:[UIImage imageNamed:@"but_done.png"] forState:UIControlStateNormal];
	[addDish setBackgroundImage:[UIImage imageNamed:@"but_done.png"] forState:UIControlStateHighlighted];
    
	[addDish addTarget:self action:@selector(clickSearchBtn:) forControlEvents:UIControlEventTouchUpInside];
	
	[self.view addSubview:addDish];
}
-(void)clickAddWine:(id)sender
{
	AddDishWineViewController* addDishView= [[AddDishWineViewController alloc] init ];
	addDishView.requestType=WINE;
	[self.navigationController pushViewController:addDishView	animated:YES];
	
}
-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}

-(void)clickSBarButton:(id)sender
{
	//[txtSearch setText:@""];
    //	[searchResultLabel setText:@""];
    //	[self doSearch:txtSearch];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end
