//
//  TitleView.m
//  FairPrice
//
//  Created by ddseah on 15/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "TitleView.h"

@implementation TitleView

@synthesize titleLabel;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 300.0, 33.0)];
    }
    return self;
}


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
//    titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, 0.0, 300.0, 33.0)];
//    titleLabel.text = @"PAIR FOOD WITH WINE";
    titleLabel.textColor = [UIColor redColor];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithName:@"Tommaso" size:26.0];
    titleLabel.textAlignment = UITextAlignmentCenter;
    [self addSubview:titleLabel];
    
    CGSize size = [titleLabel.text sizeWithFont:[UIFont fontWithName:@"Tommaso" size:26.0] constrainedToSize:CGSizeMake(300.0, 34.5)];
//    NSLog(@"size = %f / %f",size.width, size.height);
    float padding = 0.0;
    if(size.width != 0){
        padding = 5.0;
    }
    
    float sizeX1 = ((320.0-size.width)/2) - padding;
    float sizeX2 = sizeX1 +size.width + (padding*2);
    float midY = 14.5;

    
//    [[UIColor clearColor] setFill];
//    UIRectFill(rect);
//  sizeX1 = (320-size.width)/2
//  sizeX2 = sizeX1+size.width

    CGContextRef c = UIGraphicsGetCurrentContext();
    CGFloat colorCode[4] = {0.6f, 0.6f, 0.6f, 1.0f};
    CGContextSetStrokeColor(c, colorCode);
    CGContextBeginPath(c);
    CGContextMoveToPoint(c, 0.0f, midY);
    CGContextAddLineToPoint(c, sizeX1, midY);
    CGContextStrokePath(c);
    
    CGContextBeginPath(c);
    CGContextMoveToPoint(c, sizeX2, midY);
    CGContextAddLineToPoint(c, 320.0, midY);
    CGContextStrokePath(c);
}



@end
