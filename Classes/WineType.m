//
//  WineType.m
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import "WineType.h"


@implementation WineType

@dynamic wineTitle;
@dynamic wineTypeId;
@dynamic wineIcon1;
@dynamic wineIcon2;

- (void)requestFinished:(ASIHTTPRequest *)request
{
    if([request.username isEqualToString:@"TypeIcon1"])
    {
        self.wineIcon1 = [request responseData];
    }
    else if ([request.username isEqualToString:@"TypeIcon2"])
    {
        self.wineIcon2 = [request responseData];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"WINETYPE" object:nil userInfo:nil];
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    if([request.username isEqualToString:@"TypeIcon1"])
    {
        self.wineIcon1 = nil;
    }
    else if ([request.username isEqualToString:@"TypeIcon2"])
    {
        self.wineIcon2 = nil;
    }
}


@end
