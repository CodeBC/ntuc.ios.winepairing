//
//  FoodCell.h
//  FairPrice
//
//  Created by Tridip Sarkar on 30/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface FoodCell : UITableViewCell 
{
	UILabel* lblFoodName;
	UIImageView* ivBackImage;
	UIImageView* ivIcon;	

}

@property (nonatomic,strong)UILabel* lblFoodName;
@property (nonatomic,strong)UIImageView* ivBackImage;
@property (nonatomic,strong)UIImageView* ivIcon;


@end
