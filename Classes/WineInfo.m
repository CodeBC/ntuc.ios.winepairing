//
//  WineInfo.m
//  FairPrice
//
//  Created by Tridip Sarkar on 29/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import "WineInfo.h"


@implementation WineInfo
@synthesize wineTitle,wineDescription,arrFoodToGo,wineGroupID,firstWineID;

-(id)init
{
if(self = [super init]) 
{  
	self.wineTitle=@"";
    self.wineDescription=@"";
    self.arrFoodToGo=[NSMutableArray array];
    self.wineGroupID=@"";
    self.firstWineID=@"";
  

}

return self;

}

- (void)dealloc 
{  
	if(self.arrFoodToGo!=nil)
	  self.arrFoodToGo;
}


@end
