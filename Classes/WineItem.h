//
//  WineItem.h
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface WineItem : NSManagedObject

@property (nonatomic, retain) NSString * wineID;
@property (nonatomic, retain) NSString * wineName;
@property (nonatomic, retain) NSString * winePrice;
@property (nonatomic, retain) NSString * winePriceValue;
@property (nonatomic, retain) NSString * wineTaste;
@property (nonatomic, retain) NSString * wineDesc;
@property (nonatomic, retain) NSString * wineExpert;
@property (nonatomic, retain) NSString * wineType;
@property (nonatomic, retain) NSString * wineTypeValue;
@property (nonatomic, retain) NSData * wineImage;
@property (nonatomic, retain) NSString * wineRating;
@property (nonatomic, retain) NSString * wineCountry;
@property (nonatomic, retain) NSString * wineCountryValue;
@property (nonatomic, retain) NSString * wineTypeID;
@property (nonatomic, retain) NSString * wineTypeName;

@end
