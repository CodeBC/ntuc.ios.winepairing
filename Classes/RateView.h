//
//  RateView.h
//  CustomView
//
//  Created by Ray Wenderlich on 7/30/10.
//  Copyright 2010 Ray Wenderlich. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RateView;

@protocol RateViewDelegate
- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating;
@end

@interface RateView : UIView

@property (nonatomic, strong) UIImage *notSelectedImage;
@property (nonatomic, strong) UIImage *halfSelectedImage;
@property (nonatomic, strong) UIImage *fullSelectedImage;
@property (nonatomic, strong) NSMutableArray *imageViews;
@property (nonatomic, assign) float rating;
@property (nonatomic, assign) BOOL editable;
@property (nonatomic, assign) int maxRating;
@property (nonatomic, weak) id <RateViewDelegate> delegate;
@property (nonatomic, assign) int leftMargin;
@property (nonatomic, assign) int midMargin;
@property (nonatomic, assign) CGSize minImageSize;

//@interface RateView : UIView {
//    UIImage *_notSelectedImage;
//    UIImage *_halfSelectedImage;
//    UIImage *_fullSelectedImage;
//    float _rating;
//    BOOL _editable;
//    NSMutableArray *_imageViews;
//    int _maxRating;
//    int _midMargin;
//    int _leftMargin;
//    CGSize _minImageSize;
//    id <RateViewDelegate> _delegate;
//}
//
//@property (retain) UIImage *notSelectedImage;
//@property (retain) UIImage *halfSelectedImage;
//@property (retain) UIImage *fullSelectedImage;
//@property  float rating;
//@property  BOOL editable;
//@property  int maxRating;
//@property (assign) id <RateViewDelegate> delegate;
//@property  int leftMargin;

@end