//
//  AddDishViewController.m
//  FairPrice
//
//  Created by niladri.schatterjee on 10/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "AddDishWineViewController.h"
#import "DataModel.h"
#import "FairPriceAppDelegate.h"
#import "Utility.h"

@implementation AddDishWineViewController
@synthesize txtName,txtEmail,txtFoodSugession,requestType,titleStr,descriptionStr,suggestionStr,suggestionPrompt;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	
    if(requestType==FOOD)
	{	self.titleStr=@"Add Dish";
		self.descriptionStr=@"Eager to know what goes well \n with your favourite dish but can't \n find it here? Suggest one and \n we'll find a wine match for you.";
		self.suggestionStr=@"Food Suggestion/s";
		self.suggestionPrompt=@"Please enter food suggestion/s";
		
	}
	else if(requestType==WINE)
	{
		self.titleStr=@"Add Wine";
		self.descriptionStr=@"Eager to know what goes well \n with your favourite wine but can't \n find it here? Suggest one and \n we'll find a dish match for you.";
		self.suggestionStr=@"Wine Suggestion/s";
		self.suggestionPrompt=@"Please enter wine suggestion/s";

	}
	[self createAddView];
	[self setTitle:self.titleStr];
}

-(void)createAddView
{
	NSString* str =self.descriptionStr;
	UILabel* aLable = [[UILabel alloc]initWithFrame:CGRectMake(10.0, START_Y+75.0, 300.0, 100.0)];
	[aLable setBackgroundColor:[UIColor clearColor]];
	[aLable setNumberOfLines:0];
	aLable.textColor = [UIColor whiteColor];
	aLable.font = [UIFont systemFontOfSize:18.0];
	aLable.textAlignment = NSTextAlignmentCenter;
    
	[aLable setText:str];
	[self.view addSubview:aLable];
	
	/*UIImageView* aView = [[UIImageView alloc]initWithFrame:CGRectMake(40.0, START_Y+185.0, 237.0, 40.0)];
	[aView setImage:[UIImage imageNamed:@"home_field.png"]];
	[aView setUserInteractionEnabled:YES];
	
	UITextField* inputText = [[UITextField alloc]initWithFrame:CGRectMake(12.0, 9.0, 210.0, 30.0)];
	inputText.backgroundColor=[UIColor clearColor];
	[inputText setBorderStyle:UITextBorderStyleNone];
	inputText.returnKeyType = UIReturnKeyDone;
	inputText.placeholder=@"Name";
	inputText.textAlignment=UITextAlignmentCenter;
	inputText.delegate = self;

	[self setTxtName:inputText];
	[aView addSubview:inputText];
	[self.view addSubview:aView];
	
	
	
//	aView = [[UIImageView alloc]initWithFrame:CGRectMake(40.0, START_Y+235.0, 237.0, 40.0)];
//	[aView setImage:[UIImage imageNamed:@"home_field.png"]];
//	[aView setUserInteractionEnabled:YES];
	
	inputText = [[UITextField alloc]initWithFrame:CGRectMake(12.0, 9.0, 210.0, 30.0)];
	inputText.backgroundColor=[UIColor clearColor];
	[inputText setBorderStyle:UITextBorderStyleNone];
	inputText.placeholder=@"Email";
	inputText.keyboardType=UIKeyboardTypeEmailAddress;
	inputText.textAlignment=UITextAlignmentCenter;
	inputText.returnKeyType = UIReturnKeyDone ;
	inputText.delegate = self;

	[self setTxtEmail:inputText];
	[aView addSubview:inputText];
	//[self.view addSubview:aView];
	
	aView = [[UIImageView alloc]initWithFrame:CGRectMake(40.0, START_Y+285.0, 237.0, 40.0)];
	[aView setImage:[UIImage imageNamed:@"home_field.png"]];
	[aView setUserInteractionEnabled:YES];
	
	inputText = [[UITextField alloc]initWithFrame:CGRectMake(12.0, 9.0, 210.0, 30.0)];
	inputText.backgroundColor=[UIColor clearColor];
	[inputText setBorderStyle:UITextBorderStyleNone];
	inputText.returnKeyType = UIReturnKeyDone ;
	inputText.placeholder=self.suggestionStr;
	inputText.textAlignment=UITextAlignmentCenter;
	inputText.delegate = self;
	
	[self setTxtFoodSugession:inputText];
	[aView addSubview:inputText];
	[self.view addSubview:aView];*/
	UITextField* inputText = [[UITextField alloc]initWithFrame:CGRectMake(40.0, START_Y+185.0, 230.0, 30.0)];
	inputText.backgroundColor=[UIColor clearColor];
	[inputText setBorderStyle:UITextBorderStyleNone];
	inputText.returnKeyType = UIReturnKeyDone;
	//inputText.placeholder=@"Name";
	inputText.textAlignment=NSTextAlignmentCenter;
    inputText.textColor = [UIColor whiteColor];
	inputText.delegate = self;

    if ([inputText respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor grayColor];
        inputText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Name" attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
	[self.view addSubview:inputText];
    [Utility makeBorder:inputText andWidth:1 andColor:[UIColor whiteColor]];
    
    inputText = [[UITextField alloc]initWithFrame:CGRectMake(40.0, START_Y+235.0, 230.0, 30.0)];
	inputText.backgroundColor=[UIColor clearColor];
	[inputText setBorderStyle:UITextBorderStyleNone];
	//inputText.placeholder=@"Email";
    if ([inputText respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor grayColor];
        inputText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
	inputText.keyboardType=UIKeyboardTypeEmailAddress;
	inputText.textAlignment=NSTextAlignmentCenter;
    inputText.textColor = [UIColor whiteColor];
	inputText.returnKeyType = UIReturnKeyDone ;
	inputText.delegate = self;
    [self.view addSubview:inputText];
    [Utility makeBorder:inputText andWidth:1 andColor:[UIColor whiteColor]];
    
    inputText = [[UITextField alloc]initWithFrame:CGRectMake(40.0, START_Y+285.0, 230.0, 30.0)];
	inputText.backgroundColor=[UIColor clearColor];
	[inputText setBorderStyle:UITextBorderStyleNone];
	inputText.returnKeyType = UIReturnKeyDone ;
	//inputText.placeholder=self.suggestionStr;
    if ([inputText respondsToSelector:@selector(setAttributedPlaceholder:)]) {
        UIColor *color = [UIColor grayColor];
        inputText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:self.suggestionStr attributes:@{NSForegroundColorAttributeName: color}];
    } else {
        NSLog(@"Cannot set placeholder text's color, because deployment target is earlier than iOS 6.0");
        // TODO: Add fall-back code to set placeholder color.
    }
	inputText.textAlignment=NSTextAlignmentCenter;
    inputText.textColor = [UIColor whiteColor];
	inputText.delegate = self;
	[self.view addSubview:inputText];
	[Utility makeBorder:inputText andWidth:1 andColor:[UIColor whiteColor]];
	
	UIButton* btnSubmit = [[UIButton alloc] initWithFrame:CGRectMake(113.0 , START_Y+340.0, 99.0, 35.0)];
	[btnSubmit setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    //[btnSubmit setTitle:@"Submit" forState:normal];
	[btnSubmit addTarget:self action:@selector(clickSubmit:) forControlEvents:UIControlEventTouchUpInside];
	//[btnSubmit setBackgroundImage:[UIImage imageNamed:@"btnSubmit.png"] forState:UIControlStateNormal];
	//[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
    //[btnSubmit setBackgroundImage:[UIImage imageNamed:@"blank_small.png"] forState:normal];
    [btnSubmit setBackgroundImage:[UIImage imageNamed:@"but_submit.png"] forState:UIControlStateNormal];
	[self.view addSubview:btnSubmit];
	
	//[Utility makeBorder:inputText andWidth:1 andColor:[UIColor whiteColor]];
}

-(void)clickBtn:(id)sender
{
	;
}
-(void)clickSubmit:(id)sender
{
	BOOL result=NO;
	self.txtName.text=[self trim:txtName.text];
	if([self.txtName.text length]==0)
	{ 
		[self showAlertWithMessage:@"Please enter Name"];
		return;
		
	}
	else
	{
		
		result=YES;
		
	}
	
	
	
	
	self.txtEmail.text=[self trim:txtEmail.text];
	if([self.txtEmail.text length]==0)
	{
		[self showAlertWithMessage:@"Please enter email id"];
		return;
		
	}
	else if([self validateEmail:txtEmail.text])
	{
		
		result=YES;
		
	}
	else
	{ 
		[self showAlertWithMessage:@"Please enter valid email id"];
		return;
	}
	
	self.txtFoodSugession.text=[self trim:self.txtFoodSugession.text];
	
	if([self.txtFoodSugession.text length]==0)
	{
		[self showAlertWithMessage:self.suggestionPrompt];
		return;
	}
	else 
	{
		result=YES;
	}
	
	if(result)
	{   
		NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
		if(requestType==FOOD)		
		{ 
		
		   
		   paramDict[@"Name"] = self.txtName.text;
		   paramDict[@"Email"] = self.txtEmail.text; 
		   paramDict[@"Suggestion"] = self.txtFoodSugession.text;
		   paramDict[@"Type"] = @"D"; 
		
		  		  			
		
		  	
		}
		else if(requestType==WINE)
		{
			
			
			paramDict[@"Name"] = self.txtName.text;
			paramDict[@"Email"] = self.txtEmail.text; 
			paramDict[@"Suggestion"] = self.txtFoodSugession.text;
			paramDict[@"Type"] = @"W"; 
			
			
			
			
			
		}
		[self startIndicator];
		[appDelegate.dataModel startRequestWithParameters:REQUEST_ADDDISH WithParams:paramDict];
		[self stopIndicator];
		
		UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"FairPrice" message:appDelegate.dataModel.responseStr delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
		alert.tag=2;
		[alert show];
			
		
	}
	
	
}

#pragma mark-
#pragma mark UIAlertView delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex 
{
	
	if((buttonIndex == 0) && (alertView.tag==2))
	{
		self.txtName.text=@"";
		self.txtEmail.text=@"";
		self.txtFoodSugession.text=@"";
		
	}
		
	
}
#pragma mark-
#pragma mark textField Delegate
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{  
	/*UIView *tempview=self.view;          //[[textField superview] superview];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:.6];
	[tempview setCenter:CGPointMake(tempview.center.x,tempview.center.y-90)];
	[UIView commitAnimations];*/
	return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
	/*UIView *tempview=self.view;      //[[textField superview] superview];
	[UIView beginAnimations:nil context:NULL];
	[UIView setAnimationDuration:0.6];
	[tempview setCenter:CGPointMake(tempview.center.x,tempview.center.y+90)];
	[UIView commitAnimations];*/
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{   
	
	if ([string length] == 0)
		return YES;	
	if (textField == txtName) 
	{
		
		if([[textField text] length] > 30)
		{
			return NO;
		}
		NSRange rangeOfFirstCharacter=[string rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@":;,'?~!@#$%^&*()+<>\"\\/[]{}|"]];
		
		if(rangeOfFirstCharacter.location!=NSNotFound)
		{   	
			return NO;
		}
		return YES;
	}
	else if(textField==txtEmail)
	{	
		if([[textField text] length] > 30)
			return NO;
		return YES;
	}
	else if(textField==txtFoodSugession)
	{
		if([[textField text] length] > 70)
			return NO;
		NSRange rangeOfFirstCharacter=[string rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@":'?~!@#$%^&*()+<>\"\\/[]{}|"]];
		
		if(rangeOfFirstCharacter.location!=NSNotFound)
		{   	
			return NO;
		}
		return YES;
		
		
	}
    
	return YES;
}

#pragma mark-


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}


- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end
