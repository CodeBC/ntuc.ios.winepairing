//
//  FoodCell.m
//  FairPrice
//
//  Created by Tridip Sarkar on 30/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import "FoodCell.h"
#import "Constants.h"


@implementation FoodCell
@synthesize lblFoodName,ivBackImage,ivIcon;
- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
		lblFoodName = [[UILabel alloc] init];
		lblFoodName.textAlignment = UITextAlignmentLeft;
//		lblFoodName.font = [UIFont boldSystemFontOfSize:15.0];
        lblFoodName.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:18];
//		lblFoodName.textColor = UIColorFromHex(0x452801);
        lblFoodName.textColor = [UIColor whiteColor];
		lblFoodName.numberOfLines = 0 ;
		lblFoodName.backgroundColor = [UIColor clearColor];
		
		ivBackImage = [[UIImageView alloc] init];
		ivIcon = [[UIImageView alloc] init];
		ivIcon.backgroundColor=[UIColor clearColor];
		
		[self.contentView addSubview:ivIcon];
		
		[self.contentView addSubview:ivBackImage];
		
		[self.contentView addSubview:lblFoodName];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)layoutSubviews 
{
	[super layoutSubviews];
	//CGRect contentRect = self.contentView.bounds;
	
	//CGFloat boundsX = contentRect.origin.x;
	//CGFloat contentWidth=contentRect.size.width;
	//CGFloat contentHeight=contentRect.size.height;
	//CGFloat contentWith=contentRect.size.width;
	
	
	CGRect frame;
	//frame = CGRectMake(60.0, (contentHeight-25)/2, 23.0, 25.0);
	//ivIcon.frame = frame;
	
	//x,y,w,h
    
	frame = CGRectMake( 0.0, 0.0, 320.0, 40.0);
	ivBackImage.frame = frame;
	
//	frame = CGRectMake(100,5.0, 250.0 , 26.0);
	frame = CGRectMake(53,5.0, 250.0 , 26.0);

	lblFoodName.frame = frame;
	
	
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




@end
