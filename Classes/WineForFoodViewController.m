//
//  WineForFoodViewController.m
//  FairPrice
//
//  Created by niladri.schatterjee on 10/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "WineForFoodViewController.h"
#import "SelectFoodViewController.h"
#import "AddDishWineViewController.h"
#import "FairPriceAppDelegate.h"
#import "CustomCell.h"
#import "Constants.h"
#import "FoodCell.h"
#import "DataModel.h"
#import "GroupDishItem.h"
#import "DishItem.h"
#import "TitleView.h"
#import "UIImage+Retina4.h"
@implementation WineForFoodViewController
@synthesize txtSearch,tblWineForFood,celIndex,arrGroupDish,arrTemp,searchResultLabel;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
//    [self setTitleText:@"PAIR FOOD WITH WINE"];
    self.title = @"PAIR FOOD WITH WINE";
    
    [super viewDidLoad];

    self.arrGroupDish = [appDelegate.dataModel arrGroupDish];
    
    NSLog(@"PAIR FOOD WITH group dish count %d",[self.arrGroupDish count]);
	self.arrTemp=[NSMutableArray array];
	[self.arrTemp addObjectsFromArray:self.arrGroupDish];
    
	self.searchResultLabel=[[UILabel alloc] init];
	[self.searchResultLabel setFrame:CGRectMake(110,172, 100,25)];
	[self.searchResultLabel setBackgroundColor:[UIColor clearColor]];
	[self.searchResultLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:24]];
	[self.searchResultLabel setTextColor:[UIColor redColor]];
	[self.searchResultLabel setTextAlignment:UITextAlignmentCenter];
	[self.view addSubview:self.searchResultLabel];
    
    //[self createSearchBar];
	[self createTable];
    
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTable) name:@"RELOAD" object:nil];
	
}

-(void)updateTable
{
	NSLog(@"Table Notified");

	[tblWineForFood reloadData];
}

-(void)viewWillAppear:(BOOL)animated{

    [super viewWillAppear:animated];
//    if ([[self.navigationController viewControllers] count]==1){
//		[self hideBackButton];
//	}
    
	GroupDishItem *groupDish=arrGroupDish[celIndex.row];
	FoodCell *cell=(FoodCell*)[tblWineForFood cellForRowAtIndexPath:celIndex];
    
	[cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
	
	if (groupDish.dishIcon1) 
	{
		UIImage *dishIcon=[UIImage imageWithData:groupDish.dishIcon1];
        
        CGRect contentRect = cell.contentView.bounds;
		CGFloat contentHeight=contentRect.size.height;
		CGFloat iconWidth=dishIcon.size.width;
		CGFloat iconHeight=dishIcon.size.height;
		
		[cell.ivIcon setFrame:CGRectMake(60,(contentHeight-iconHeight)/2,iconWidth,iconHeight)];
		[cell.ivIcon setImage:dishIcon];
	}
	cell.lblFoodName.textColor = [UIColor whiteColor];
	[tblWineForFood reloadData];
}

-(void)createNavigationMenu{    
    [self setTitleText:self.title];
    [super createNavigationMenu];
}

-(void)createSearchBar{
    
	UIImageView* aView = [[UIImageView alloc]initWithFrame:CGRectMake(10.0, START_Y+75.0, 300.0, 45.0)];
	[aView setUserInteractionEnabled:YES];
	
	UIImageView* srchLogo = [[UIImageView alloc]initWithFrame:CGRectMake(20.0, 14.0, 14.0, 15.0)];
	[srchLogo setImage:[UIImage imageNamed:@"icon_search.png"]];
	[aView addSubview:srchLogo];
	
	UITextField* searchText = [[UITextField alloc]initWithFrame:CGRectMake(40.0, 8.0, 210.0, 25.0)];
	//searchText.backgroundColor=[UIColor redColor];
    searchText.placeholder = @"SEARCH";
    [searchText setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:18]];
    [searchText setTextColor:[UIColor whiteColor]];
	[searchText addTarget:self action:@selector(doSearch:) forControlEvents:UIControlEventEditingChanged];
	[searchText setBorderStyle:UITextBorderStyleNone];
	searchText.returnKeyType = UIReturnKeySearch ;
	searchText.delegate = self;

	[self setTxtSearch:searchText];
	[aView addSubview:searchText];
	
	UIButton *srchButton = [[UIButton alloc] initWithFrame:CGRectMake(260.0, 11.0, 19.0, 19.0)];
	[srchButton setBackgroundImage:[UIImage imageNamed:@"icon_searchClose.png"] forState:UIControlStateNormal];
	[srchButton addTarget:self action:@selector(clickSBarButton:) forControlEvents:UIControlEventTouchUpInside];
	[aView addSubview:srchButton];
	
	[self.view addSubview:aView];
}	

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
	[textField resignFirstResponder];
	return YES;
}



-(void)createTable
{
	UITableView *aTable = [[UITableView alloc]initWithFrame:CGRectMake(0.0, START_Y+100.0, 320.0, 280.0)];
	
	[aTable setBackgroundColor:[UIColor clearColor]];
	[aTable setAllowsSelection:YES];
	[aTable setDelegate:self];
	[aTable setDataSource:self];
    aTable.showsVerticalScrollIndicator = NO;
	[self setTblWineForFood:aTable];
	[self.view addSubview:tblWineForFood];
    
    /*
     
     settings for button at the end of table
     
     */
	
//	UIButton *addDish = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 460.0-TABBARHEIGHT-45.0, 320.0, 45.0)];
//    
//    [addDish setBackgroundImage:[UIImage imageNamed:@"but_add_your_dish.png"] forState:UIControlStateNormal];
//	[addDish setBackgroundImage:[UIImage imageNamed:@"but_add_your_dish.png"] forState:UIControlStateHighlighted];
//    
//	[addDish addTarget:self action:@selector(clickAddDish:) forControlEvents:UIControlEventTouchUpInside];
//	
//	[self.view addSubview:addDish];
//	[addDish release];
	
}

-(void)clickAddDish:(id)sender
{
	AddDishWineViewController* addDishView= [[AddDishWineViewController alloc] init ];//WithNibName:@"AddDishViewController" bundle:[NSBundle mainBundle]];
	addDishView.requestType=FOOD;
	[self.navigationController pushViewController:addDishView	animated:YES];
	
}

#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 40.0;
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return 	[arrGroupDish count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
	
	FoodCell *cell = (FoodCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if(cell==nil) {
//		cell = [[[FoodCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        cell = [[FoodCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
	
   	GroupDishItem *groupDish=(self.arrGroupDish)[[indexPath row]];
	
    if (groupDish.dishIcon1) 
	{
		UIImage *dishIcon=[UIImage imageWithData:groupDish.dishIcon1];
		
		CGFloat iconWidth=dishIcon.size.width;
		CGFloat iconHeight=dishIcon.size.height;
		CGRect contentRect = cell.contentView.bounds;
		CGFloat contentHeight=contentRect.size.height;

//		[cell.ivIcon setFrame:CGRectMake(60,(contentHeight-iconHeight)/2,iconWidth,iconHeight)];
        [cell.ivIcon setFrame:CGRectMake(23.0,(contentHeight-iconHeight)/2,iconWidth,iconHeight)];
		[cell.ivIcon setImage:dishIcon];
	}
	
    [cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
	
	[cell.lblFoodName setText:[groupDish.dishTitle uppercaseString]];
    NSLog(@"cell lbl food name %@",groupDish.dishTitle);
    cell.lblFoodName.textColor = [UIColor whiteColor];
	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{  
	[self cellAdjustForIndexPath:indexPath isDeleteImage:YES];
	GroupDishItem *groupDish=arrGroupDish[[indexPath row]];
	NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
	paramDict[@"DishGroupID"] = groupDish.groupDishID;
	[self setCelIndex:(NSIndexPath *)indexPath];
	FoodCell *cell=(FoodCell*)[tblWineForFood cellForRowAtIndexPath:(NSIndexPath *)indexPath];
    
	[cell.lblFoodName setTextColor:[UIColor whiteColor]];
	
	[self startIndicator];
	[appDelegate.dataModel startRequestWithParameters:REQUEST_DISH_LIST WithParams:paramDict];
	[self stopIndicator];
	
	 if ([appDelegate.dataModel.arrDishItem count]>0) 
	{
		SelectFoodViewController* selFoodView = [[SelectFoodViewController alloc] init ];
		[selFoodView setTitle:cell.lblFoodName.text];
		[self.navigationController pushViewController:selFoodView	animated:YES];
	}
	else
	{
        [cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
        cell.lblFoodName.textColor = [UIColor whiteColor];
        [self cellAdjustForIndexPath:indexPath isDeleteImage:NO];
		
//		if (groupDish.dishIcon1) 
//		{
//			UIImage *dishIcon=[UIImage imageWithData:groupDish.dishIcon1];
//			
//			CGFloat iconWidth=dishIcon.size.width;
//			CGFloat iconHeight=dishIcon.size.height;
//			CGRect contentRect = cell.contentView.bounds;
//			CGFloat contentHeight=contentRect.size.height;
//			
//			[cell.ivIcon setFrame:CGRectMake(60,(contentHeight-iconHeight)/2,iconWidth,iconHeight)];
//			[cell.ivIcon setImage:dishIcon];
//		}	
	}
}


-(void)doSearch:(id) sender
{
	UITextField* textField = sender;
	if([textField.text isEqualToString:@""]||textField.text==nil)
	{
		[self.arrGroupDish removeAllObjects];
		[self.arrGroupDish addObjectsFromArray:self.arrTemp];
		[self.tblWineForFood reloadData];
		
		return;
	}
    [self.arrGroupDish removeAllObjects];
	NSInteger counter = 0;
    
	for(GroupDishItem *anItem in self.arrTemp)
	{
		@autoreleasepool {
			NSString* str  = [NSString stringWithFormat:@"%@",anItem.dishTitle];//,[anItem exName][anItem details],
			
			BOOL isFound = ([str rangeOfString:textField.text options:NSCaseInsensitiveSearch].location != NSNotFound);
			if(isFound)
			{
				[self.arrGroupDish addObject:anItem];
				counter++; 
			}
		}
		
    }
	
	NSLog(@"Searched %@ Found %d" , textField.text,counter);
	if(counter==0)
	{ 
		[searchResultLabel setText:[NSString stringWithFormat:@"No result!"]];
	}
	else {
		
		[searchResultLabel setText:@""];
	}
	[self.tblWineForFood reloadData];
}


-(void)clickSBarButton:(id)sender
{  
	[txtSearch setText:@""];
	[searchResultLabel setText:@""];
	[self doSearch:txtSearch];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
	
	NSLog(@"Searching %@ " , textField.text);
	NSRange rangeOfFirstCharacter=[string rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
	if([[textField text] length]==0 && rangeOfFirstCharacter.location!=NSNotFound)
		return NO;
	
	return YES;
	
}

-(void)cellAdjustForIndexPath:(NSIndexPath*)indexPath isDeleteImage:(BOOL)flag
{
	int row = indexPath.row;
	int sec = indexPath.section;
	if (row >0) {
		NSIndexPath* indexP = [NSIndexPath indexPathForRow:row-1 inSection:sec];
		FoodCell *aCell=(FoodCell*)[tblWineForFood cellForRowAtIndexPath:(NSIndexPath *)indexP];
		if(flag)
		  [aCell.ivBackImage setImage:nil];
		else 
			[aCell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
        
		[aCell.lblFoodName setTextColor:[UIColor whiteColor]];
	}
}


/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark - Methods not in use
-(void)createIntro
{
	UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(50.0, 20.0, 250.0, 100.0)];
	[lbl setText:@"Welcome to the FairPrice wine celler"];
	lbl.numberOfLines = 0 ;
	lbl.textColor = [UIColor blackColor];
	lbl.font = [UIFont boldSystemFontOfSize:25.0];
	[self.view addSubview:lbl];
}


@end
