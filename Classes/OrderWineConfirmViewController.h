//
//  OrderWineConfirmViewController.h
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "WineItem.h"

@interface OrderWineConfirmViewController : BaseViewController <UIScrollViewDelegate>

@property(nonatomic, strong) UIScrollView *svConfirm;

@property(nonatomic, strong) UILabel *lbl1;
@property(nonatomic, strong) UILabel *lbl2;
@property(nonatomic, strong) UILabel *lbl3;
@property(nonatomic, strong) UILabel *lbl4;

@property(nonatomic, strong) UILabel *lbl5;
@property(nonatomic, strong) UILabel *lbl6;

@property(nonatomic, strong) UILabel *lbl7;
@property(nonatomic, strong) UILabel *lbl8;

@property(nonatomic, strong) UILabel *lbl9;
@property(nonatomic, strong) UILabel *lbl10;

@property(nonatomic, strong) UILabel *lbl11;
@property(nonatomic, strong) UILabel *lbl12;

@property(nonatomic, strong) UILabel *lbl13;
@property(nonatomic, strong) UILabel *lbl14;

@property(nonatomic, strong) UILabel *lbl15;
@property(nonatomic, strong) UILabel *lbl16;
@property(nonatomic, strong) UILabel *lbl17;
@property(nonatomic, strong) UILabel *lbl18;

@property(nonatomic, strong) NSMutableDictionary *paramDict;

-(void)createNavigationMenu;
-(void)createScrollView;
-(void)createTextLabels;
-(void)createInfoLabels;
-(void)createLine;
-(void)createButtonConfirm;

-(BOOL)isStringValid:(NSString *)strCheck;
-(BOOL)isWineItemValid:(WineItem *)wiCheck;

-(void)showAlertWithMessage:(NSString*)strMessage;

-(float)getEstPrice;
-(float)getSavePrice;
-(float)getDiscPrice;

@end
