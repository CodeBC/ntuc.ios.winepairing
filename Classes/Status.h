//
//  Status.h
//  FairPrice
//
//  Created by ddseah on 14/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Status : NSObject

@property (nonatomic, strong) NSString *submitStatus;
@property (nonatomic, strong) NSString *submitMessage;

@end
