//
//  TipViewController.h
//  FairPrice
//
//  Created by Tridip Sarkar on 25/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface TipViewController : BaseViewController
{
	UIScrollView *tipScrollView;
	UIView *aView;
	UIView *aView2;
	UIButton *tip1button;
	NSInteger i;
}

@property(nonatomic,strong) UIView *aView;
@property(nonatomic,strong) UIView *aView2;

-(void)createTip1;
-(void)createTip2;
-(UILabel*)createLabelWithText:(NSString*)strText withYPos:(CGFloat)y withHeight:(CGFloat)h textIsBold:(BOOL)flg;


-(UIScrollView *)CreateScrollViewWithFrame:(CGRect)frame
                               ContentSize:(CGSize)size
						   VscrollVisibled:(BOOL)visible
						   HscrollVisibled:(BOOL)Visible
								WillBounce:(BOOL)no;
@end
