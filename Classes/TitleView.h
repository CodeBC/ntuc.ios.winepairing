//
//  TitleView.h
//  FairPrice
//
//  Created by ddseah on 15/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TitleView : UIView

@property (nonatomic, strong)UILabel *titleLabel;

@end
