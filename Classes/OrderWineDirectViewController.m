//
//  OrderWineDirectViewController.m
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FairPriceAppDelegate.h"
#import "DataModel.h"
#import "OrderWineDirectViewController.h"
#import "Constants.h"
#import "OrderWineFormViewController.h"

@interface OrderWineDirectViewController ()

@end

@implementation OrderWineDirectViewController

@synthesize DesWebView;
//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    self.title = @"ORDER IN BULK";
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    self.DesWebView = [[UIWebView alloc] init];
    
    [self createMainBody];
    [self createTextView];
    [self createButtons];
    
    for (id subview in DesWebView.subviews)
		if ([[subview class] isSubclassOfClass: [UIScrollView class]])
			((UIScrollView *)subview).bounces = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([[self.navigationController viewControllers] count]==1){
		[self hideBackButton];
	}
}

-(void)createNavigationMenu{
    [self setTitleText:self.title];
    [super createNavigationMenu];
    
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, START_Y+30.0, 300.0, 34.5)];
//	[titleLabel setText:@"ORDER IN BULK"];
//	titleLabel.textAlignment = UITextAlignmentCenter;
//	titleLabel.backgroundColor=[UIColor clearColor];
//	titleLabel.textColor = [UIColor redColor];
//    titleLabel.font = [UIFont fontWithName:@"Tommaso" size:26.0];
//	[self.view addSubview:titleLabel];
//	[titleLabel release];
//    
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.0, 300.0, 1.0)];
//    [imgView setImage:[UIImage imageNamed:@"nav_bar_line.png"]];
//    imgView.contentMode = UIViewContentModeScaleAspectFit;
//    [self.view bringSubviewToFront:imgView];
//    [self.view addSubview:imgView];
//    [imgView release];
}


-(void)createMainBody{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(174.0, START_Y+104.0, 146.0, 180.0)];
    [imgView setImage:[UIImage imageNamed:@"img_wine_crate.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view bringSubviewToFront:imgView];
    [self.view addSubview:imgView];
}

-(void)createTextView
{   
    
//    NSString *dataDescription1 = @"Save when you do. FairPrice accepts direct orders of 24 bottles or more. Once you place your order, expect delivery in 5 working days.";
//    NSString *dataDescription2 = @"At this stage FairPrice only accepts cash on delivery, no credit cards or cheques. Deliveries are made Monday to Saturday between 9am and 5 pm.";
    
//    NSString *dataDescription = @"<ul><li></li></ul>";
//    NSString *cssStyle = @"<head><style> .li{color: blue line-height: 20% border: solid}</style></head>";
//    NSString *dataDescription1 = @"<p style=\"line-height:20%;margin-left:20px;\"><ul><li>FairPrice accepts bulk orders - Minimum order of 5 cases and above</li></ul></p>";
   /* NSString *dataDescription1 = @"<ul><li>FairPrice accepts bulk orders - Minimum order of 5 cases and above</li>";
    NSString *dataDescription2 = @"<li>Delivery only available in mainland Singapore</li>";
    NSString *dataDescription3 = @"<li>Cash On Delivery</li>";
    NSString *dataDescription4 = @"<li>Please allow a minimum of 10 working days for delivery</li>";
    NSString *dataDescription5 = @"<li>Order placement by case only (individual bottle selection not available)</li>";
    NSString *dataDescription6 = @"<li>Orders placed will be confirmed by email within 5 working days</li>";
    NSString *dataDescription7 = @"<li>Payment must be made 3 days before delivery</li>";
    NSString *dataDescription8 = @"<li>No LinkPoint/Rebates will be given for order in bulk</li></ul>";*/
    NSString *dataDescription1 = @"<ul><li>Minimum order of 5 cases (12 bottles each) and above</li>";
    NSString *dataDescription2 = @"<li>Orders will be confirmed by email within 5 working days</li>";
    NSString *dataDescription3 = @"<li>Delivery is only available in mainland Singapore</li>";
    NSString *dataDescription4 = @"<li>Earliest delivery is 10 working days from date of order</li>";
    NSString *dataDescription5 = @"<li>Full payment must be made 5 days before delivery by cash or cheque only</li>";
    NSString *dataDescription6 = @"<li>No LinkPoint and rebate will be given</li>";
    /*NSString *dataDescription7 = @"<li>Payment must be made 3 days before delivery</li>";
    NSString *dataDescription8 = @"<li>No LinkPoint/Rebates will be given for order in bulk</li></ul>";*/
    
	NSString *strText= [NSString stringWithFormat:@"%@%@%@%@%@%@",dataDescription1,dataDescription2,dataDescription3,dataDescription4,dataDescription5,dataDescription6];
    
    NSLog(@"HTML code = %@",strText);
    
//	self.DesWebView.frame = CGRectMake(10.0, START_Y+85.0, 156.0, 220.0);
	self.DesWebView.frame = CGRectMake(-10.0, START_Y+65.0, 180.0, 245.0);
//    self.DesWebView.scrollView.frame = CGRectMake(20.0, START_Y+75.0, 146.0, 170.0);
//	self.DesWebView.frame = CGRectMake(20.0, START_Y+75.0, 290.0, 170.0);
	[self.DesWebView setOpaque:NO];
    [[self.DesWebView scrollView] setShowsVerticalScrollIndicator:NO];
	[self.DesWebView loadHTMLString:[NSString stringWithFormat:@"<html><body> <font face=\"EuphemiaUCAS\" size=\"1\" color='#ffffff'> %@ </font></body></html>",strText] baseURL:nil];
	self.DesWebView.backgroundColor = [UIColor clearColor];
	self.DesWebView.scalesPageToFit = NO;
	self.DesWebView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	self.DesWebView.delegate = self;
    
	[self.view addSubview: self.DesWebView];
}

-(void)createButtons{
    UIButton *fairPriceButton = [[UIButton alloc] initWithFrame:CGRectMake(20.0 , START_Y+375.0, 97.0, 40.0)];
	fairPriceButton.tag=1;
	//[fairPriceButton setTitle:@"Fairprice Best Buy" forState:UIControlStateNormal];
	//	[fairPriceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[fairPriceButton addTarget:self action:@selector(onClickPlaceOrder:) forControlEvents:UIControlEventTouchUpInside];
	[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"but_place_order.png"] forState:UIControlStateNormal];
	//[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
	[self.view addSubview:fairPriceButton];
    
    fairPriceButton = [[UIButton alloc] initWithFrame:CGRectMake(127.0 , START_Y+375.0, 180.0, 40.0)];
	fairPriceButton.tag=1;
	//[fairPriceButton setTitle:@"Fairprice Best Buy" forState:UIControlStateNormal];
	//	[fairPriceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[fairPriceButton addTarget:self action:@selector(onClickBecomeMember:) forControlEvents:UIControlEventTouchUpInside];
	[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"but_become_a_wine_club_member.png"] forState:UIControlStateNormal];
	//[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
	[self.view addSubview:fairPriceButton];
}

-(IBAction)onClickPlaceOrder:(id)sender{
    NSLog(@"GOT HERE!");
    [self startIndicator];
    [appDelegate.dataModel startRequestWithParameters:REQUEST_ALL_WINES WithParams:nil];
    [self stopIndicator];
    NSLog(@"appDelegate.dataModel = %d",[appDelegate.dataModel.arrWine count]);
    if ([appDelegate.dataModel.arrWine count] > 0){
        OrderWineFormViewController *ordWineFormView = [[OrderWineFormViewController alloc]init];
       [self.navigationController pushViewController:ordWineFormView animated:YES];
    }
}

-(IBAction)onClickBecomeMember:(id)sender{
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.fairprice.com.sg/webapp/wcs/stores/servlet/UserRegistrationForm?langId=-1&storeId=90001&catalogId=10052&new=Y&returnPage=&prevPage=&displayPromo=Y"]];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.fairprice.com.sg/webapp/wcs/stores/servlet/UserRegistrationForm?langId=-1&storeId=90001&catalogId=10052&new=Y&returnPage=&prevPage=&displayPromo=Y"]];
}

#pragma mark-
#pragma mark UIWebView delegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	// starting the load, show the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	// finished loading, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	// load error, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	// report the error inside the webview
	//NSString* errorString = [NSString stringWithFormat:
	//							 @"<html><center><font size=+5 color='red'>An error occurred:<br>%@</font></center></html>",
	//							 error.localizedDescription];
	//	[myWebView loadHTMLString:errorString baseURL:nil];
}



- (void)viewDidUnload
{
    [super viewDidUnload];
    self.DesWebView;
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
