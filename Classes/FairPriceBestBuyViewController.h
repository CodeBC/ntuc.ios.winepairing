//
//  FairPriceBestBuyViewController.h
//  FairPrice
//
//  Created by niladri.schatterjee on 10/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Constants.h"
#import "RateView.h"
#import "WineItem.h"

@class WineBuyInfo;
@class DishInfo;
@class GroupDishItem;
@class RateView;
@interface FairPriceBestBuyViewController : BaseViewController <RateViewDelegate>
{
	UILabel* lblWineName;
	UILabel* lblWinePrice;
	UIImageView* imgWineBottle;
	UIImageView* imgRibbon;
	NSInteger requestFromPage;
	NSString *ribbonImageStr;
	WineBuyInfo *wineBuydata;
    DishInfo *dishInfoData;
    GroupDishItem *groupDishData;
    WineItem *wineItemData;
	RateView  *ratingView;
	NSString *myRating;
	
}
@property(nonatomic,strong) UIImageView* imgWineBottle;
@property(nonatomic,strong) UIImageView* imgRibbon;
@property(nonatomic,strong) UILabel* lblWineName;
@property(nonatomic,strong) UILabel* lblWinePrice;
@property(nonatomic,assign) NSInteger requestFromPage;
@property(nonatomic,strong) NSString *ribbonImageStr;
@property(nonatomic,strong) WineBuyInfo *wineBuydata;
@property(nonatomic,strong) DishInfo *dishInfoData;
@property(nonatomic,strong) GroupDishItem *groupDishData;
@property(nonatomic,strong) WineItem *wineItemData;
@property(nonatomic,strong) NSString *myRating;

-(void)updateView;
-(void)createNavigationMenu;
-(void)createRatingPanelWithSelected:(int)noOfStar;
-(void)createPriceView;
-(void)createWineDetails;
-(void)createWineClubLabel;
@end
