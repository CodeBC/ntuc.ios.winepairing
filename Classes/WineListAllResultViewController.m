//
//  WineListResultViewController.m
//  FairPrice
//
//  Created by niladri.schatterjee on 10/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "WineListAllResultViewController.h"
#import "WineListSearchViewController.h"
#import "FoodResultViewController.h"
#import "AddDishWineViewController.h"
#import "FairPriceBestBuyViewController.h"
#import "FairPriceAppDelegate.h"
#import "Constants.h"
#import "CustomCell.h"
#import "WineGroupItem.h"
#import "DataModel.h"
#import "WineItem.h"
#import "UIImage+Retina4.h"

@implementation WineListAllResultViewController
@synthesize txtSearch,tblWineForFood,celIndex,requestFromPage,selectionStrip,arrWineList,arrTemp,searchResultLabel;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

//-(BOOL)startRequestWithParameters:(int)aRequest WithParams:(NSDictionary*) aParam	
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    self.title = @"ALL WINES";
    [super viewDidLoad];
    
    NSLog(@"WineListResultViewController");
	if(requestFromPage==BYWINE)
	{ self.selectionStrip=@"blueBg.png";
		//appDelegate=(FairPriceAppDelegate*)[[UIApplication sharedApplication] delegate];
        arrWineList = [NSMutableArray arrayWithArray:appDelegate.dataModel.arrGroupWine];
		
	}
	else if(requestFromPage==WINELIST)
	{ 
		self.selectionStrip=@"greenBg.png";
        arrWineList = [NSMutableArray arrayWithArray:appDelegate.dataModel.arrWine];
        [self setTitle:@"Result"];
		
	}
	else if(requestFromPage==WINE_INFO)
	{  
		self.selectionStrip=@"blueBg.png";
        arrWineList = [NSMutableArray arrayWithArray:appDelegate.dataModel.arrWine];
		[self setTitle:@"Result"];
        
	}
    else if(requestFromPage==DISH_INFO)
	{
		self.selectionStrip=@"redBg.png";
        arrWineList = [NSMutableArray arrayWithArray:appDelegate.dataModel.arrWine];
		[self setTitle:@"Result"];
	}
    
	self.arrTemp=[NSMutableArray array];
	[self.arrTemp addObjectsFromArray:arrWineList];
	
	self.searchResultLabel=[[UILabel alloc] init];
	[self.searchResultLabel setFrame:CGRectMake(110,172, 100,25)];
	[self.searchResultLabel setBackgroundColor:[UIColor clearColor]];
	[self.searchResultLabel setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:24]];
	[self.searchResultLabel setTextColor:[UIColor whiteColor]];
	[self.searchResultLabel setTextAlignment:UITextAlignmentCenter];
	[self.view addSubview:self.searchResultLabel];
	[self createSearchBar];
	[self createTable];
	
}

-(void)createNavigationMenu{    
    [self setTitleText:self.title];
    [super createNavigationMenu];
}


-(void)viewWillAppear:(BOOL)animated{
    
	CustomCell *cell=(CustomCell*)[tblWineForFood cellForRowAtIndexPath:celIndex];
	[cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
	cell.lblFoodName.textColor = [UIColor whiteColor];
	//////////
	[tblWineForFood reloadData];
	/////////
}

-(void)createSearchBar{
    
	UIImageView* aView = [[UIImageView alloc]initWithFrame:CGRectMake(10.0, START_Y+75.0, 300.0, 45.0)];
    //	[aView setImage:[UIImage imageNamed:@"searchBg.png"]];
	[aView setUserInteractionEnabled:YES];
	
	UIImageView* srchLogo = [[UIImageView alloc]initWithFrame:CGRectMake(20.0, 14.0, 14.0, 15.0)];
	[srchLogo setImage:[UIImage imageNamed:@"icon_search.png"]];
	[aView addSubview:srchLogo];
	
	UITextField* searchText = [[UITextField alloc]initWithFrame:CGRectMake(40.0, 8.0, 210.0, 25.0)];
	//searchText.backgroundColor=[UIColor redColor];
    searchText.placeholder = @"SEARCH";
    [searchText setFont:[UIFont fontWithName:@"Futura-CondensedMedium" size:18]];
    [searchText setTextColor:[UIColor whiteColor]];
	[searchText addTarget:self action:@selector(doSearch:) forControlEvents:UIControlEventEditingChanged];
	[searchText setBorderStyle:UITextBorderStyleNone];
	searchText.returnKeyType = UIReturnKeySearch ;
	searchText.delegate = self;
    
	[self setTxtSearch:searchText];
	[aView addSubview:searchText];
	
	UIButton *srchButton = [[UIButton alloc] initWithFrame:CGRectMake(260.0, 11.0, 19.0, 19.0)];
	[srchButton setBackgroundImage:[UIImage imageNamed:@"icon_searchClose.png"] forState:UIControlStateNormal];
	[srchButton addTarget:self action:@selector(clickSBarButton:) forControlEvents:UIControlEventTouchUpInside];
	[aView addSubview:srchButton];
	
	[self.view addSubview:aView];
}

-(void)createTable
{
	UITableView *aTable = [[UITableView alloc]initWithFrame:CGRectMake(0.0, START_Y+115.0, 320.0, 230.0)];
	
	[aTable setBackgroundColor:[UIColor clearColor]];
	[aTable setAllowsSelection:YES];
	[aTable setDelegate:self];
	[aTable setDataSource:self];
    aTable.showsVerticalScrollIndicator = NO;
	[self setTblWineForFood:aTable];
	[self.view addSubview:tblWineForFood];
    
    /*
     
     settings for button at the end of table
     
     */
	
	UIButton *addDish = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 460.0-TABBARHEIGHT-45.0, 320.0, 45.0)];
    
    [addDish setBackgroundImage:[UIImage imageNamed:@"but_filter.png"] forState:UIControlStateNormal];
//	[addDish setBackgroundImage:[UIImage imageNamed:@"but_add_your_wine.png"] forState:UIControlStateHighlighted];
    
	[addDish addTarget:self action:@selector(onClickFilter:) forControlEvents:UIControlEventTouchUpInside];
	
	[self.view addSubview:addDish];
	
}


-(void)addWine
{
	UIButton *addDish = [[UIButton alloc] initWithFrame:CGRectMake(10.0, START_Y+280.0, 300.0, 50.0)];
	[addDish setBackgroundImage:[UIImage imageNamed:@"addBg-Area.png"] forState:UIControlStateNormal];
	[addDish setBackgroundImage:[UIImage imageNamed:@"addBg-Area.png"] forState:UIControlStateHighlighted];
	[addDish addTarget:self action:@selector(clickAddWine:) forControlEvents:UIControlEventTouchUpInside];
	[addDish setTitle:@"  Add Your Wine" forState:UIControlStateNormal];
	[addDish.titleLabel setFont:[UIFont boldSystemFontOfSize:17]];
	[addDish setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
	
	UIImageView* imgAdd = [[UIImageView alloc]initWithFrame:CGRectMake(60.0, 16.0, 17.0, 17.0)];
	[imgAdd setImage:[UIImage imageNamed:@"icon_Add.png"]];
	[addDish addSubview:imgAdd];
	
	[self.view addSubview:addDish];
	
}
-(void)clickAddWine:(id)sender
{
	AddDishWineViewController* addDishView= [[AddDishWineViewController alloc] init ];//WithNibName:@"AddDishViewController" bundle:[NSBundle mainBundle]];
	addDishView.requestType=WINE;
	[self.navigationController pushViewController:addDishView	animated:YES];
	
}
-(void)onClickFilter:(id)sender{
    [self startIndicator];
    [appDelegate.dataModel loadSearchFilter];
//    [appDelegate.dataModel startRequestWithParameters:REQUEST_ALL_WINES WithParams:nil];
    //		[appDelegate.dataModel loadSearchFilter];
    [self stopIndicator];
    if ([appDelegate.dataModel.arrCountry count] > 0)
    {        
        WineListSearchViewController* wineSearchView= [[WineListSearchViewController alloc] init];
        wineSearchView.requestFromPage=WINELIST;
        [self.navigationController pushViewController:wineSearchView	animated:YES];
    }
}
#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 40.0;
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section 
{  
	return 	[arrWineList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *CellIdentifier = @"Cell";
	NSString *Title;
	CustomCell *cell = (CustomCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
	if(cell==nil)
	{
        //		cell = [[[CustomCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        cell = [[CustomCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
	}
	
	
    
	
	if(requestFromPage==BYWINE)	
	{  WineGroupItem *groupWine = arrWineList[indexPath.row];
		Title=groupWine.groupTitle;	
	}
	else if(requestFromPage==WINELIST)
	{
		WineItem *wineData = arrWineList[indexPath.row];
		Title=wineData.wineName;
	}
	else if(requestFromPage==WINE_INFO)
	{
		WineItem *wineData = arrWineList[indexPath.row];
		Title=wineData.wineName;
		
	}
	else if(requestFromPage==DISH_INFO)
	{
		WineItem *wineData = arrWineList[indexPath.row];
		Title=wineData.wineName;
		
	}
	
	[cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
	//[cell.ivIcon setImage:[UIImage imageNamed:@"icon_curries.png"]];
	cell.lblFoodName.frame = CGRectMake(0.0, 5.0, 300.0 , 26.0);
	[cell.lblFoodName setText:[Title uppercaseString]]; 
	cell.lblFoodName.textColor = [UIColor whiteColor];
	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.accessoryType = UITableViewCellAccessoryNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath 
{   
	if(requestFromPage==BYWINE)	
	{  	
        NSLog(@"from:BYWINE");
		[self cellAdjustForIndexPath:indexPath isDeleteImage:YES];
		WineGroupItem *groupWine = arrWineList[indexPath.row];
		NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
		paramDict[@"WineID"] = groupWine.wineID;
		
		CustomCell *cell=(CustomCell*)[tblWineForFood cellForRowAtIndexPath:(NSIndexPath *)indexPath];
        //		[cell.ivBackImage setImage:[UIImage imageNamed:selectionStrip]];
		[cell.lblFoodName setTextColor:[UIColor whiteColor]];
		
		[self startIndicator];
		[appDelegate.dataModel startRequestWithParameters:REQUEST_WINE_INFO WithParams:paramDict];
		[self stopIndicator];
		if([appDelegate.dataModel.arrWineInfo count]>0)
		{
            FoodResultViewController* foodResultView= [[FoodResultViewController alloc] init ];
            foodResultView.requestFromPage=self.requestFromPage;
            [foodResultView setTitle:cell.lblFoodName.text ];
            [self.navigationController pushViewController:foodResultView animated:YES];
		}
		else
		{
		  	[cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
			cell.lblFoodName.textColor = [UIColor whiteColor];	
			[self cellAdjustForIndexPath:indexPath isDeleteImage:NO];
		}
	}
	else if(requestFromPage==WINELIST)
	{
		NSLog(@"from:WINELIST");
		////////Cell Adjust////// Remove if crash found//////////
		[self cellAdjustForIndexPath:indexPath isDeleteImage:YES];
		///////////////////////////////////////////////////////////
		
		WineItem *wineData = arrWineList[indexPath.row];
        NSLog(@"wineData = %@",wineData.wineName);
		NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
		paramDict[@"WineID"] = wineData.wineID;
	    
		CustomCell *cell=(CustomCell*)[tblWineForFood cellForRowAtIndexPath:(NSIndexPath *)indexPath];
        //		[cell.ivBackImage setImage:[UIImage imageNamed:selectionStrip]];
		[cell.lblFoodName setTextColor:[UIColor whiteColor]];
		
		[self startIndicator];
		[appDelegate.dataModel startRequestWithParameters:REQUEST_ONE_WINE WithParams:paramDict];
		[self stopIndicator];
        if ([appDelegate.dataModel.arrWineInfo count]>0) 
		{
			FairPriceBestBuyViewController* wineResultView= [[FairPriceBestBuyViewController alloc]init];
			wineResultView.requestFromPage=WINELIST;       
//            wineResultView.wineItemData = wineData;
			[self.navigationController pushViewController:wineResultView	animated:YES];
		} 
            //		else
            //		{
            //			
            //		  	[cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
            //			cell.lblFoodName.textColor = [UIColor whiteColor];
            //			[self cellAdjustForIndexPath:indexPath isDeleteImage:NO];
            //			
            //		}
            //		
            
            
        }
        else if(requestFromPage==WINE_INFO)
        {
            NSLog(@"from:WINE_INFO");
            ////////Cell Adjust////// Remove if crash found//////////
            [self cellAdjustForIndexPath:indexPath isDeleteImage:YES];
            ///////////////////////////////////////////////////////////
            
            WineItem *wineData = arrWineList[indexPath.row];
            NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
            paramDict[@"WineID"] = wineData.wineID;
            
            CustomCell *cell=(CustomCell*)[tblWineForFood cellForRowAtIndexPath:(NSIndexPath *)indexPath];
            //		[cell.ivBackImage setImage:[UIImage imageNamed:selectionStrip]];
            [cell.lblFoodName setTextColor:[UIColor whiteColor]];
            
            [self startIndicator];
            [appDelegate.dataModel startRequestWithParameters:REQUEST_WINE_BUYINFO WithParams:paramDict];
            [self stopIndicator];
            if ([appDelegate.dataModel.arrWineBuyInfo count]>0) 
            {
                FairPriceBestBuyViewController* wineResultView= [[FairPriceBestBuyViewController alloc]init];
                wineResultView.requestFromPage=BYWINE;                //self.requestFromPage;       
                [self.navigationController pushViewController:wineResultView	animated:YES];
            }
            else
            {
                
                [cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
                cell.lblFoodName.textColor = [UIColor whiteColor];
                [self cellAdjustForIndexPath:indexPath isDeleteImage:NO];
                
            }
            
            
            
            
            
        }
        else if(requestFromPage==DISH_INFO)
        {
            ////////Cell Adjust////// Remove if crash found//////////
            [self cellAdjustForIndexPath:indexPath isDeleteImage:YES];
            ///////////////////////////////////////////////////////////
            
            WineItem *wineData = arrWineList[indexPath.row];
            NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
            paramDict[@"WineID"] = wineData.wineID;
            
            CustomCell *cell=(CustomCell*)[tblWineForFood cellForRowAtIndexPath:(NSIndexPath *)indexPath];
            //		[cell.ivBackImage setImage:[UIImage imageNamed:selectionStrip]];
            [cell.lblFoodName setTextColor:[UIColor whiteColor]];
            
            [self startIndicator];
            [appDelegate.dataModel startRequestWithParameters:REQUEST_WINE_BUYINFO WithParams:paramDict];
            [self stopIndicator];
            
            if ([appDelegate.dataModel.arrWineBuyInfo count]>0)
            {
                FairPriceBestBuyViewController* wineResultView= [[FairPriceBestBuyViewController alloc]init];
                wineResultView.requestFromPage=BYFOOD;                //self.requestFromPage;       
                [self.navigationController pushViewController:wineResultView	animated:YES];
            }
            else
            {
                [cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
                cell.lblFoodName.textColor = [UIColor whiteColor];
                [self cellAdjustForIndexPath:indexPath isDeleteImage:NO];
                
            }
        }
        
        
        
        
        
        [self setCelIndex:(NSIndexPath *)indexPath];
        //NewsData* anInfo = [[appDelegate.dataModel arrNews] objectAtIndex:[indexPath row]];
        
    }
    
    -(void)cellAdjustForIndexPath:(NSIndexPath*)indexPath isDeleteImage:(BOOL)flag
    {
        int row = indexPath.row;
        int sec = indexPath.section;
        if (row >0) {
            NSIndexPath* indexP = [NSIndexPath indexPathForRow:row-1 inSection:sec];
            CustomCell *aCell=(CustomCell*)[tblWineForFood cellForRowAtIndexPath:(NSIndexPath *)indexP];
            if(flag)
                [aCell.ivBackImage setImage:nil];
            else 
                [aCell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
            [aCell.lblFoodName setTextColor:[UIColor whiteColor]];
            
        }
        
    }
#pragma mark Text view Delegate
    -(void)doSearch:(id) sender
    {
        UITextField* textField = sender;
        if([textField.text isEqualToString:@""]||textField.text==nil)
        {
            [arrWineList removeAllObjects];
            [arrWineList addObjectsFromArray:self.arrTemp];
            [self.tblWineForFood reloadData];
            
            return;
        }
        [arrWineList removeAllObjects];
        NSInteger counter = 0;
        if(requestFromPage==BYWINE)
            for(WineGroupItem *anItem in self.arrTemp)
            {
                @autoreleasepool {
                
                    NSString* str  = [NSString stringWithFormat:@"%@",anItem.groupTitle];//,[anItem exName][anItem details],
                    
                    BOOL isFound = ([str rangeOfString:textField.text options:NSCaseInsensitiveSearch].location != NSNotFound);
                    if(isFound)
                    {
                        [arrWineList addObject:anItem];
                        counter++; 
                    }
                }
                
            }
        else
            for(WineItem *anItem in self.arrTemp)
            {
                @autoreleasepool {
                
                    NSString* str  = [NSString stringWithFormat:@"%@",anItem.wineName];//,[anItem exName][anItem details],
                    
                    BOOL isFound = ([str rangeOfString:textField.text options:NSCaseInsensitiveSearch].location != NSNotFound);
                    if(isFound)
                    {
                        [arrWineList addObject:anItem];
                        counter++; 
                    }
                }
                
            }
        
        
        NSLog(@"Searched %@ Found %d" , textField.text,counter);
        if(counter==0)
        { 
            [searchResultLabel setText:[NSString stringWithFormat:@"No result!"]];
        }
        else {
            
            [searchResultLabel setText:@""];
        }
        
        [self.tblWineForFood reloadData];
    }
    
    - (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
    {
        
        NSLog(@"Searching %@ " , textField.text);
        NSRange rangeOfFirstCharacter=[string rangeOfCharacterFromSet:[NSCharacterSet characterSetWithCharactersInString:@" "]];
        if([[textField text] length]==0 && rangeOfFirstCharacter.location!=NSNotFound)
            return NO;
        
        
        return YES;
        
    }
    -(BOOL)textFieldShouldReturn:(UITextField *)textField
    {
        [textField resignFirstResponder];
        return YES;
    }
    
    -(void)clickSBarButton:(id)sender
    {
        [txtSearch setText:@""];
        [searchResultLabel setText:@""];
        [self doSearch:txtSearch];
    }
    
    - (void)didReceiveMemoryWarning {
        // Releases the view if it doesn't have a superview.
        [super didReceiveMemoryWarning];
        
        // Release any cached data, images, etc that aren't in use.
    }
    
    - (void)viewDidUnload {
        [super viewDidUnload];
        // Release any retained subviews of the main view.
        // e.g. self.myOutlet = nil;
    }
    
    
    
    
    @end
