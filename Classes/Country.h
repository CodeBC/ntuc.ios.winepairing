//
//  Country.h
//  FairPrice
//
//  Created by Tridip Sarkar on 08/11/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface Country : NSObject 
{
	NSString *countryCode;
	NSString *countryName;
}

@property(nonatomic,strong) NSString *countryCode;
@property(nonatomic,strong) NSString *countryName;
-(id)init;
@end
