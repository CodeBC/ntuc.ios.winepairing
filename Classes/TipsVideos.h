//
//  TipsVideos.h
//  FairPrice
//
//  Created by ddseah on 12/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TipsVideos : NSObject

@property(nonatomic, strong) NSString *contentID;
@property(nonatomic, strong) NSString *contentTitle;

@end
