//
//  WineListResultViewController.h
//  FairPrice
//
//  Created by niladri.schatterjee on 10/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Constants.h"

@class FairPriceAppDelegate;
@interface WineListAllResultViewController : BaseViewController <UITextFieldDelegate,UITableViewDelegate,UITableViewDataSource>
{
    UITextField* txtSearch;
	UITableView* tblWineForFood;
	NSIndexPath* celIndex;
    NSInteger requestFromPage; 
	NSString *selectionStrip;
	NSMutableArray *arrWineList;
	NSMutableArray *arrTemp;
	UILabel *searchResultLabel;
	//FairPriceAppDelegate *appDelegate;
}
@property(nonatomic,strong) UITextField* txtSearch;
@property(nonatomic,strong) UITableView* tblWineForFood;
@property(nonatomic,strong) NSIndexPath* celIndex;
@property(nonatomic,assign) NSInteger requestFromPage;
@property(nonatomic,strong) NSString *selectionStrip;
@property(nonatomic,strong) NSMutableArray *arrWineList;
@property(nonatomic,strong) NSMutableArray *arrTemp;
@property(nonatomic,strong) UILabel *searchResultLabel;

-(void)createNavigationMenu;
-(void)createSearchBar;
-(void)addWine;
-(void)createTable;
-(void)cellAdjustForIndexPath:(NSIndexPath*)indexPath isDeleteImage:(BOOL)flag;

@end
