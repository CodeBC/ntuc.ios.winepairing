//
//  FairPriceAppDelegate.h
//  FairPrice
//
//  Created by Satish Kumar on 10/7/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FBConnect.h"
#define kAppID @"477539718930150"
//#define kAppID @"288516743258"
//#define kAppID @"288516743258"

#define kSecretID @"b3476587f772e089761c7f7454a1ba2e"
//#define kSecretID @"a2d1ffa149bb05878051b438f6375952"
//#define kSecretID @"6a8409ddc5603a348f65282795c1a917"
@class HomeViewController,DataModel;
@class WineForFoodViewController;
@class WineListSearchViewController;
@class WineListAllResultViewController;
@class FoodForWineViewController;
@class TipViewController;
@class JustWineClubViewController;
@class AboutViewController;
@class TipsAndVidViewController;
@class OrderWineDirectViewController;

@interface FairPriceAppDelegate : NSObject <UIApplicationDelegate , UINavigationControllerDelegate,UITabBarControllerDelegate,
    FBSessionDelegate, FBDialogDelegate, FBRequestDelegate> {
    UIWindow *window;
    UINavigationController* homeViewNavController;
	UINavigationController *foodNavController;
	UINavigationController *wineNavController;
	UINavigationController *wineListNavController;
	UINavigationController *TipNavController;
    UINavigationController *justWineClubNavController;
    UINavigationController *orderWineClubNavController;
    
	//UINavigationController *aboutNavController;
	
	
    HomeViewController *homeController;
	WineForFoodViewController *wineForFoodController;	
	FoodForWineViewController *foodForWineController;
//	WineListSearchViewController *wineListSearchController;
    WineListAllResultViewController *wineListSearchController;
	TipViewController *tipController;
    JustWineClubViewController *justWineClubController;
    OrderWineDirectViewController *orderWineClubController;
	///AboutViewController *aboutController;
	
    DataModel* dataModel;
    Facebook *facebook;
    NSString * strDevToken;

	
	//UITabBarController *tabBarController; ///////////////////
	
}

@property (nonatomic, strong) UINavigationController* homeViewNavController;
@property (nonatomic, strong) UINavigationController *foodNavController;
@property (nonatomic, strong) UINavigationController *wineNavController;
@property (nonatomic, strong) UINavigationController *wineListNavController;
@property (nonatomic, strong) UINavigationController *TipNavController;
@property (nonatomic, strong) UINavigationController *justWineClubNavController;
@property (nonatomic, strong) UINavigationController *tipsAndVidNavController;
@property (nonatomic, strong) UINavigationController *orderWineClubNavController;
//@property (nonatomic, retain) UINavigationController *aboutNavController;



//////////////////////////

@property (nonatomic, strong)  UIWindow *window;
@property (nonatomic, strong)  HomeViewController *homeController;
@property (nonatomic, strong)  WineForFoodViewController *wineForFoodController;
@property (nonatomic, strong)  FoodForWineViewController *foodForWineController;
@property (nonatomic, strong)  WineListAllResultViewController *wineListSearchController;
@property (nonatomic, strong)  TipViewController *tipController;
@property (nonatomic, strong)  JustWineClubViewController *justWineClubController;
@property (nonatomic, strong)  TipsAndVidViewController *tipsAndVidController;
@property (nonatomic, strong)  OrderWineDirectViewController *orderWineClubController;
//@property (nonatomic,retain)   AboutViewController *aboutController;
@property (nonatomic, strong)  DataModel* dataModel;
@property(nonatomic,strong) Facebook *facebook;

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

- (void)saveContext;

-(void)showViewController:(NSInteger)index;
-(void)animateView:(id)FlipView WithTransition:(UIViewAnimationTransition)transition;
-(void)createFacebookPost;
- (void) showFacebookDialog:(NSString *)strProduct ;
- (void) showOrderForm:(UIViewController *)viewController;

@end

