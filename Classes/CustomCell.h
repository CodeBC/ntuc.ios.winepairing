//
//  WineForFoodCell.h
//  FairPrice
//
//  Created by niladri.schatterjee on 10/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface CustomCell : UITableViewCell {
   	UILabel* lblFoodName;
	UIImageView* ivBackImage;
	//UIImageView* ivIcon;

	
}
@property (nonatomic,strong)UILabel* lblFoodName;
@property (nonatomic,strong)UIImageView* ivBackImage;
//@property (nonatomic,retain)UIImageView* ivIcon;

@end
