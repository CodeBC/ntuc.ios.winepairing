//
//  OrderWineFormViewController.m
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//
#import "FairPriceAppDelegate.h"
#import "DataModel.h"
#import "OrderWineFormViewController.h"
#import "Constants.h"
#import "OrderWineConfirmViewController.h"
#import "WineItem.h"


@interface OrderWineFormViewController ()

@end

@implementation OrderWineFormViewController

@synthesize svMain;
@synthesize pvMain;

@synthesize btDone;
@synthesize asPicker;

@synthesize txtField1;
@synthesize txtField2;
@synthesize txtField3;
@synthesize txtField4;
@synthesize txtField5;
@synthesize txtField6;

@synthesize arrWine;
@synthesize arrNoOfCrates;
@synthesize dictAllInfo;

@synthesize pickerID;
@synthesize selIndex;
@synthesize wineIndex;
@synthesize crateIndex;

@synthesize btnSender;

CGFloat animatedDistance;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

-(void)viewDidLoad{
    self.title = @"ORDER WINE DIRECT";
    [super viewDidLoad];
    
    if (!appDelegate.dataModel.arrWine) {
        appDelegate.dataModel.arrWine = [[NSMutableArray alloc] init];
    }
    self.arrWine = appDelegate.dataModel.arrWine;
    NSLog(@"The number os wines in arr is %i",[arrWine count]);
    [self addNoOfCrates];
    
    WineItem *wineItem = [WineItem createEntity];
    wineItem.wineName = @"Select Wine";
    
   // //[self.arrWine insertObject:wineItem atIndex:0];
   // [self.arrWine addObject:wineItem];
    
    self.dictAllInfo =[NSMutableDictionary dictionary];
    
    [self createScrollView];    
    [self createTextLabels];
    [self createSubmitButton];
    [self createBlanks];
    [self createTextFields];
    [self createWineOrderMenu];
    [self createActionSheet];
}

-(void)createScrollView{
    CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
    svMain = [[UIScrollView alloc] initWithFrame:CGRectMake(0, START_Y+80.0, appFrame.size.width, appFrame.size.height - START_Y + 80)];
    svMain.contentSize = CGSizeMake(320.0,410.0);
	svMain.clipsToBounds = YES;
    svMain.showsVerticalScrollIndicator = NO;
	svMain.delegate = self;
    
    [self.view addSubview:svMain];
}

-(void)createSubmitButton{
    UIButton *fairPriceButton = [[UIButton alloc] initWithFrame:CGRectMake(111.0 , 360.0, 97.0, 40.0)];
	fairPriceButton.tag=1;
	[fairPriceButton addTarget:self action:@selector(onClickSubmit:) forControlEvents:UIControlEventTouchUpInside];
	[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"but_submit.png"] forState:UIControlStateNormal];
	[self.svMain addSubview:fairPriceButton];
}

-(void)createNavigationMenu{    
    [self setTitleText:self.title];
    [super createNavigationMenu];
    
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, START_Y+30.0, 300.0, 34.5)];
//	[titleLabel setText:@"ORDER WINE DIRECT"];
//	titleLabel.textAlignment = UITextAlignmentCenter;
//	titleLabel.backgroundColor=[UIColor clearColor];
//	titleLabel.textColor = [UIColor redColor];
//    titleLabel.font = [UIFont fontWithName:@"Tommaso" size:26.0];
//	[self.view addSubview:titleLabel];
//	[titleLabel release];
//    
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.0, 300.0, 1.0)];
//    [imgView setImage:[UIImage imageNamed:@"nav_bar_line.png"]];
//    imgView.contentMode = UIViewContentModeScaleAspectFit;
//    [self.view bringSubviewToFront:imgView];
//    [self.view addSubview:imgView];
//    [imgView release];
}

-(void)createWineOrderMenu{
    //     alloc] initWithFrame:CGRectMake(111.0, 330.0, 103.0, 23.0)];
    UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(111.0,180.0,115.0, 23.0)];
    btn.tag=1;
    btn.titleLabel.textAlignment = UITextAlignmentLeft;
    btn.titleLabel.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
    btn.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;
    [btn setTitle:@"Select Wine" forState:UIControlStateNormal];
    
	[btn addTarget:self action:@selector(onClickGetWine:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[UIImage imageNamed:@"blank_med.png"] forState:UIControlStateNormal];
    [self.svMain addSubview:btn];
    
    btn = [[UIButton alloc] initWithFrame:CGRectMake(235.0 , 180.0, 65.0, 23.0)];
    btn.tag=1;
    btn.titleLabel.textAlignment = UITextAlignmentLeft;
    btn.titleLabel.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
    btn.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;
    [btn setTitle:@"No of case" forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(onClickCrateCount:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[UIImage imageNamed:@"blank_small.png"] forState:UIControlStateNormal];
    [self.svMain addSubview:btn];
    
    btn = [[UIButton alloc] initWithFrame:CGRectMake(111.0,210.0,115.0, 23.0)];
    btn.tag=2;
    btn.titleLabel.textAlignment = UITextAlignmentLeft;
    btn.titleLabel.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
    btn.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;
    [btn setTitle:@"Select Wine" forState:UIControlStateNormal];
    
	[btn addTarget:self action:@selector(onClickGetWine:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[UIImage imageNamed:@"blank_med.png"] forState:UIControlStateNormal];
    [self.svMain addSubview:btn];
    
    btn = [[UIButton alloc] initWithFrame:CGRectMake(235.0 , 210.0, 65.0, 23.0)];
    btn.tag=2;
    btn.titleLabel.textAlignment = UITextAlignmentLeft;
    btn.titleLabel.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
    btn.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;
    [btn setTitle:@"No of case" forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(onClickCrateCount:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[UIImage imageNamed:@"blank_small.png"] forState:UIControlStateNormal];
    [self.svMain addSubview:btn];
    
    btn = [[UIButton alloc] initWithFrame:CGRectMake(111.0,240.0,115.0, 23.0)];
    btn.tag=3;
    btn.titleLabel.textAlignment = UITextAlignmentLeft;
    btn.titleLabel.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
    btn.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;
    [btn setTitle:@"Select Wine" forState:UIControlStateNormal];
    
	[btn addTarget:self action:@selector(onClickGetWine:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[UIImage imageNamed:@"blank_med.png"] forState:UIControlStateNormal];
    [self.svMain addSubview:btn];
    
    btn = [[UIButton alloc] initWithFrame:CGRectMake(235.0 , 240.0, 65.0, 23.0)];
    btn.tag=3;
    btn.titleLabel.textAlignment = UITextAlignmentLeft;
    btn.titleLabel.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
    btn.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;
    [btn setTitle:@"No of case" forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(onClickCrateCount:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[UIImage imageNamed:@"blank_small.png"] forState:UIControlStateNormal];
    [self.svMain addSubview:btn];
    
    
    
    
    
    
    btn = [[UIButton alloc] initWithFrame:CGRectMake(111.0,270.0,115.0, 23.0)];
    btn.tag=4;
    btn.titleLabel.textAlignment = UITextAlignmentLeft;
    btn.titleLabel.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
    btn.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;
    [btn setTitle:@"Select Wine" forState:UIControlStateNormal];
    
	[btn addTarget:self action:@selector(onClickGetWine:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[UIImage imageNamed:@"blank_med.png"] forState:UIControlStateNormal];
    [self.svMain addSubview:btn];
    
    btn = [[UIButton alloc] initWithFrame:CGRectMake(235.0 , 270.0, 65.0, 23.0)];
    btn.tag=4;
    btn.titleLabel.textAlignment = UITextAlignmentLeft;
    btn.titleLabel.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
    btn.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;
    [btn setTitle:@"No of case" forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(onClickCrateCount:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[UIImage imageNamed:@"blank_small.png"] forState:UIControlStateNormal];
    [self.svMain addSubview:btn];
    
    btn = [[UIButton alloc] initWithFrame:CGRectMake(111.0,300.0,115.0, 23.0)];
    btn.tag=5;
    btn.titleLabel.textAlignment = UITextAlignmentLeft;
    btn.titleLabel.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
    btn.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;
    [btn setTitle:@"Select Wine" forState:UIControlStateNormal];
    
	[btn addTarget:self action:@selector(onClickGetWine:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[UIImage imageNamed:@"blank_med.png"] forState:UIControlStateNormal];
    [self.svMain addSubview:btn];
    
    btn = [[UIButton alloc] initWithFrame:CGRectMake(235.0 , 300.0, 65.0, 23.0)];
    btn.tag=5;
    btn.titleLabel.textAlignment = UITextAlignmentLeft;
    btn.titleLabel.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
    btn.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;
    [btn setTitle:@"No of case" forState:UIControlStateNormal];
    
    [btn addTarget:self action:@selector(onClickCrateCount:) forControlEvents:UIControlEventTouchUpInside];
    [btn setBackgroundImage:[UIImage imageNamed:@"blank_small.png"] forState:UIControlStateNormal];
    [self.svMain addSubview:btn];
}

-(void)createBlanks{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, 0.0, 189.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_long.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.svMain bringSubviewToFront:imgView];
    [self.svMain addSubview:imgView];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, 30.0, 189.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_long.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.svMain bringSubviewToFront:imgView];
    [self.svMain addSubview:imgView];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, 60.0, 189.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_long.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.svMain bringSubviewToFront:imgView];
    [self.svMain addSubview:imgView];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, 90.0, 103.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_med.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.svMain bringSubviewToFront:imgView];
    [self.svMain addSubview:imgView];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, 120.0, 189.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_long.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.svMain bringSubviewToFront:imgView];
    [self.svMain addSubview:imgView];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, 330.0, 103.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_med.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.svMain bringSubviewToFront:imgView];
    [self.svMain addSubview:imgView];
}

-(void)createBlanks2{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, START_Y+80.0, 189.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_long.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view bringSubviewToFront:imgView];
    [self.view addSubview:imgView];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, START_Y+110.0, 189.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_long.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view bringSubviewToFront:imgView];
    [self.view addSubview:imgView];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, START_Y+140.0, 189.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_long.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view bringSubviewToFront:imgView];
    [self.view addSubview:imgView];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, START_Y+170.0, 103.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_med.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view bringSubviewToFront:imgView];
    [self.view addSubview:imgView];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, START_Y+200.0, 189.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_long.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view bringSubviewToFront:imgView];
    [self.view addSubview:imgView];
    
    //    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, START_Y+230.0, 189.0, 23.0)];
    //    [imgView setImage:[UIImage imageNamed:@"blank_long.png"]];
    //    imgView.contentMode = UIViewContentModeScaleAspectFit;
    //    [self.view bringSubviewToFront:imgView];
    //    [self.view addSubview:imgView];
    //    [imgView release];
    
    //    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, START_Y+260.0, 57.0, 23.0)];
    //    [imgView setImage:[UIImage imageNamed:@"blank_small.png"]];
    //    imgView.contentMode = UIViewContentModeScaleAspectFit;
    //    [self.view bringSubviewToFront:imgView];
    //    [self.view addSubview:imgView];
    //    [imgView release];
    
    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(111.0, START_Y+315.0, 103.0, 23.0)];
    [imgView setImage:[UIImage imageNamed:@"blank_med.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view bringSubviewToFront:imgView];
    [self.view addSubview:imgView];
}

-(void)createTextLabels{
	UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 0.0, 89.0, 20.0)];
	[lbl setText:@"First Name"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 30.0, 89.0, 20.0)];
	[lbl setText:@"Surname"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 60.0, 89.0, 20.0)];
	[lbl setText:@"Email"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 90.0, 89.0, 20.0)];
	[lbl setText:@"Mobile"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 120.0, 89.0, 20.0)];
	[lbl setText:@"Address"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 150.0, 89.0, 20.0)];
	[lbl setText:@"Wine order"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(85.0, 150.0, 225.0, 20.0)];
	[lbl setText:@"(Note: Minimum order of 5 cases and above)"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS-Italic" size:10];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 180.0, 89.0, 20.0)];
	[lbl setText:@"Order 1"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 210.0, 89.0, 20.0)];
	[lbl setText:@"Order 2"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 240.0, 89.0, 20.0)];
	[lbl setText:@"Order 3"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 270.0, 89.0, 20.0)];
	[lbl setText:@"Order 4"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 300.0, 89.0, 20.0)];
	[lbl setText:@"Order 5"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 330.0, 89.0, 20.0)];
	[lbl setText:@"Just Wine Club"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 345.0, 89.0, 20.0)];
	[lbl setText:@"Member No."];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svMain addSubview:lbl];
}

-(void)createTextLabels2{
	UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +80.0, 89.0, 20.0)];
	[lbl setText:@"Firstname"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +110.0, 89.0, 20.0)];
	[lbl setText:@"Surname"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +140.0, 89.0, 20.0)];
	[lbl setText:@"Email"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +170.0, 89.0, 20.0)];
	[lbl setText:@"Mobile"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +200.0, 89.0, 20.0)];
	[lbl setText:@"Address"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +230.0, 89.0, 20.0)];
	[lbl setText:@"Wine order"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(85.0, START_Y +230.0, 225.0, 20.0)];
	[lbl setText:@"(Note: Minimum order is $300 and above)"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS-Italic" size:11];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +260.0, 89.0, 20.0)];
	[lbl setText:@"No. of bottles"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +310.0, 89.0, 20.0)];
	[lbl setText:@"Just Wine Club"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +325.0, 89.0, 20.0)];
	[lbl setText:@"Member No."];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
}

-(void)createTextFields{
	txtField1 = [[UITextField alloc]initWithFrame:CGRectMake(113.0, 2.0, 186.0, 23.0)];
    //    txtField1.placeholder = @"Placeholding";
    [txtField1 setText:@""];
    [txtField1 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField1 setTextColor:[UIColor whiteColor]];
	[txtField1 setBorderStyle:UITextBorderStyleNone];
	txtField1.returnKeyType = UIReturnKeyNext;
	txtField1.delegate = self;
    
    [self.svMain addSubview:txtField1];
    
    txtField2 = [[UITextField alloc]initWithFrame:CGRectMake(113.0, 32.0, 186.0, 23.0)];
    //    txtField2.placeholder = @"Placeholding";
    [txtField2 setText:@""];
    [txtField2 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField2 setTextColor:[UIColor whiteColor]];
	[txtField2 setBorderStyle:UITextBorderStyleNone];
	txtField2.returnKeyType = UIReturnKeyNext ;
	txtField2.delegate = self;
    
    [self.svMain addSubview:txtField2];
    
    txtField3 = [[UITextField alloc]initWithFrame:CGRectMake(113.0, 62.0, 186.0, 23.0)];
    //    txtField3.placeholder = @"Placeholding";
    [txtField3 setText:@""];
    [txtField3 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField3 setTextColor:[UIColor whiteColor]];
	[txtField3 setBorderStyle:UITextBorderStyleNone];
	txtField3.returnKeyType = UIReturnKeyNext ;
	txtField3.delegate = self;
    
    [self.svMain addSubview:txtField3];
    
    txtField4 = [[UITextField alloc]initWithFrame:CGRectMake(113.0, 92.0, 100.0, 23.0)];
    //    txtField4.placeholder = @"Placeholding";
    [txtField4 setText:@""];
    [txtField4 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField4 setTextColor:[UIColor whiteColor]];
	[txtField4 setBorderStyle:UITextBorderStyleNone];
	txtField4.returnKeyType = UIReturnKeyNext ;
	txtField4.delegate = self;
    
    [self.svMain addSubview:txtField4];
    
    txtField5 = [[UITextField alloc]initWithFrame:CGRectMake(113.0, 122.0, 186.0, 23.0)];
    //    txtField5.placeholder = @"Placeholding";
    [txtField5 setText:@""];
    [txtField5 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField5 setTextColor:[UIColor whiteColor]];
	[txtField5 setBorderStyle:UITextBorderStyleNone];
	txtField5.returnKeyType = UIReturnKeyNext ;
	txtField5.delegate = self;
    
    [self.svMain addSubview:txtField5];
    
    txtField6 = [[UITextField alloc]initWithFrame:CGRectMake(113.0, 332.0, 100.0, 23.0)];
    //    txtField6.placeholder = @"Placeholding";
    [txtField6 setText:@""];
    [txtField6 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField6 setTextColor:[UIColor whiteColor]];
	[txtField6 setBorderStyle:UITextBorderStyleNone];
	txtField6.returnKeyType = UIReturnKeyDone;
	txtField6.delegate = self;
    
    [self.svMain addSubview:txtField6];
}

-(void)createTextFields2{
	txtField1 = [[UITextField alloc]initWithFrame:CGRectMake(111.0, START_Y+80.0, 189.0, 23.0)];
    //    txtField1.placeholder = @"Placeholding";
    [txtField1 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField1 setTextColor:[UIColor whiteColor]];
	[txtField1 setBorderStyle:UITextBorderStyleNone];
	txtField1.returnKeyType = UIReturnKeyNext ;
	txtField1.delegate = self;
    
    [self.view addSubview:txtField1];
    
    txtField2 = [[UITextField alloc]initWithFrame:CGRectMake(111.0, START_Y+110.0, 189.0, 23.0)];
    //    txtField2.placeholder = @"Placeholding";
    [txtField2 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField2 setTextColor:[UIColor whiteColor]];
	[txtField2 setBorderStyle:UITextBorderStyleNone];
	txtField2.returnKeyType = UIReturnKeyNext ;
	txtField2.delegate = self;
    
    [self.view addSubview:txtField2];
    
    txtField3 = [[UITextField alloc]initWithFrame:CGRectMake(111.0, START_Y+140.0, 189.0, 23.0)];
    //    txtField3.placeholder = @"Placeholding";
    [txtField3 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField3 setTextColor:[UIColor whiteColor]];
	[txtField3 setBorderStyle:UITextBorderStyleNone];
	txtField3.returnKeyType = UIReturnKeyNext ;
	txtField3.delegate = self;
    
    [self.view addSubview:txtField3];
    
    txtField4 = [[UITextField alloc]initWithFrame:CGRectMake(111.0, START_Y+170.0, 103.0, 23.0)];
    //    txtField4.placeholder = @"Placeholding";
    [txtField4 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField4 setTextColor:[UIColor whiteColor]];
	[txtField4 setBorderStyle:UITextBorderStyleNone];
	txtField4.returnKeyType = UIReturnKeyNext ;
	txtField4.delegate = self;
    
    [self.view addSubview:txtField4];
    
    txtField5 = [[UITextField alloc]initWithFrame:CGRectMake(111.0, START_Y+200.0, 189.0, 23.0)];
    //    txtField5.placeholder = @"Placeholding";
    [txtField5 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField5 setTextColor:[UIColor whiteColor]];
	[txtField5 setBorderStyle:UITextBorderStyleNone];
	txtField5.returnKeyType = UIReturnKeyNext ;
	txtField5.delegate = self;
    
    [self.view addSubview:txtField5];
    
    //    txtField6 = [[UITextField alloc]initWithFrame:CGRectMake(111.0, START_Y+230.0, 189.0, 23.0)];
    //    txtField6.placeholder = @"Placeholding";
    //    [txtField6 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    //    [txtField6 setTextColor:[UIColor whiteColor]];
    //	[txtField6 setBorderStyle:UITextBorderStyleNone];
    //	txtField6.returnKeyType = UIReturnKeyNext ;
    //	txtField6.delegate = self;
    //    
    //    [self.view addSubview:txtField6];
    
    //    txtField7 = [[UITextField alloc]initWithFrame:CGRectMake(111.0, START_Y+260.0, 57.0, 23.0)];
    ////    txtField7.placeholder = @"Placeholding";
    //    [txtField7 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    //    [txtField7 setTextColor:[UIColor whiteColor]];
    //	[txtField7 setBorderStyle:UITextBorderStyleNone];
    //	txtField7.returnKeyType = UIReturnKeyNext ;
    //	txtField7.delegate = self;
    //    
    //    [self.view addSubview:txtField7];
    
    txtField6 = [[UITextField alloc]initWithFrame:CGRectMake(111.0, START_Y+315.0, 103.0, 23.0)];
    //    txtField6.placeholder = @"Placeholding";
    [txtField6 setFont:[UIFont fontWithName:@"EuphemiaUCAS" size:12]];
    [txtField6 setTextColor:[UIColor whiteColor]];
	[txtField6 setBorderStyle:UITextBorderStyleNone];
	txtField6.returnKeyType = UIReturnKeyDone;
	txtField6.delegate = self;
    
    [self.view addSubview:txtField6];
}

-(void)createPickerView:(int)index{
	UIView* parentView = [[UIView alloc]initWithFrame:CGRectMake(0.0, 0.0, 320, 480)];
	UIView* aView=[[UIView alloc]initWithFrame:CGRectMake(0.0, 180.0, 320,500 )];//250
	[aView setBackgroundColor:[UIColor blackColor]];
	
	UIPickerView *aPickerView=[[UIPickerView alloc] initWithFrame:CGRectMake(0, 35.0, 100, 235)];
	aPickerView.autoresizingMask = UIViewAutoresizingFlexibleWidth;
	
	//arrayNo = [[NSMutableArray alloc] initWithObjects:@" Speaker",@" Title",@" Topic",nil];
	
	
	aPickerView.delegate=self;
    aPickerView.dataSource=self;
	aPickerView.showsSelectionIndicator = YES;
	aPickerView.tag=index;
	if (pvMain){
		[pvMain.superview removeFromSuperview];
	}
	[self setPvMain:aPickerView];
	
	self.btDone = [UIButton buttonWithType:UIButtonTypeCustom];
	btDone.frame = CGRectMake(240, 3.0, 80.0, 30.0);
	btDone.tag=index;
	[btDone  setImage:[UIImage imageNamed:@"doneBtn.png"] forState:UIControlStateNormal];
	[btDone  setImage:[UIImage imageNamed:@"doneBtn.png"] forState:UIControlStateHighlighted];
	//[btDone setTitle:@"Done" forState:UIControlStateNormal];
	//[btDone setBackgroundColor:[UIColor blackColor]];//WithPatternImage:[UIImage imageNamed:@"bottom_bg.png"]]];
	[aView addSubview:btDone];
	[btDone addTarget:self action:@selector(handelDoneEvent:) forControlEvents:UIControlEventTouchUpInside];
	[btDone setShowsTouchWhenHighlighted:YES];
	
	[pvMain selectRow:1 inComponent:0 animated:NO];
	[aView addSubview:pvMain];
	[parentView addSubview:aView];
	[self.view addSubview:parentView];
	//[self.view addSubview:pickerView];
	
}

-(void)createActionSheet{
    NSString* titleActionSheet=@"";
    titleActionSheet=[titleActionSheet stringByAppendingString:@"\n\n\n\n\n\n\n\n\n\n\n\n\n"];
    
    asPicker=[[UIActionSheet alloc] initWithTitle:titleActionSheet delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    asPicker.actionSheetStyle=UIActionSheetStyleBlackOpaque;
    
    UIButton *saveButton=[[UIButton alloc]init];
	saveButton.frame=CGRectMake(250,5,62, 30);
	[saveButton setBackgroundColor:[UIColor clearColor]];
	[saveButton setBackgroundImage:[UIImage imageNamed:@"doneBtn.png"] forState:UIControlStateNormal];
	saveButton.tag = 1;
	saveButton.showsTouchWhenHighlighted=YES;
	[saveButton addTarget:self action:@selector(onClickDone:) forControlEvents:UIControlEventTouchUpInside];
	[asPicker addSubview:saveButton];
}

-(NSMutableArray *)getTxtFieldInfo{
    
    NSLog(@"wowow");
    
    NSMutableArray *txtFieldArray = [[NSMutableArray alloc]init];
    
    [txtFieldArray addObject:txtField1.text];
    [txtFieldArray addObject:txtField2.text];
    [txtFieldArray addObject:txtField3.text];
    [txtFieldArray addObject:txtField4.text];
    [txtFieldArray addObject:txtField5.text];
    [txtFieldArray addObject:txtField6.text];
    
    return txtFieldArray;
}

-(void)getDictTextFieldInfo{
    //    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
	(self.dictAllInfo)[@"Firstname"] = txtField1;
    (self.dictAllInfo)[@"Surname"] = txtField2;
    (self.dictAllInfo)[@"Email"] = txtField3;
    (self.dictAllInfo)[@"Mobile"] = txtField4;
    (self.dictAllInfo)[@"Address"] = txtField5;
    (self.dictAllInfo)[@"JustWineMemberNo"] = txtField6;
    
    NSLog(@"paramDict = %@",self.dictAllInfo);
}

-(BOOL)validateDict:(NSDictionary *)dict{
    for(UITextField *lbl in [dict allValues]){
        NSLog(@"lbl = %@",lbl.text);
        if(lbl.text == nil || [lbl.text isEqualToString:@""]){
            return NO;
        }
    }
    return YES;
}

-(BOOL)validateDict{
    
    if([[(self.dictAllInfo)[@"Firstname"]text] isEqualToString:@""]){
        return NO;
    }else if([[(self.dictAllInfo)[@"Surname"]text] isEqualToString:@""]){
        return NO;
    }else if([[(self.dictAllInfo)[@"Email"]text] isEqualToString:@""]){
        return NO;
    }else if([[(self.dictAllInfo)[@"Mobile"]text] isEqualToString:@""]){
        return NO;
    }
//    else if([[[self.dictAllInfo objectForKey:@"JustWineMemberNo"]text] isEqualToString:@""]){
//        return NO;
//    }
    

    BOOL isWineValidated = NO;
    BOOL isCrateValidated = NO;
//    BOOL isAllValidated = NO;
    
    for(int i=1; i<=5;i++){
        NSLog(@"loop i = %d",i);
        NSString *wineItemKey = [NSString stringWithFormat:@"WineItem%d",i];
        NSString *noCreateKey = [NSString stringWithFormat:@"CrateNo%d",i];
        
        if((self.dictAllInfo)[wineItemKey] == nil ||
           [[(self.dictAllInfo)[wineItemKey]wineName] isEqualToString:@"Select Wine"]){
            isWineValidated = NO;
        }else{
            isWineValidated = YES;
        }
        
        if((self.dictAllInfo)[noCreateKey] == nil ||
           [(self.dictAllInfo)[noCreateKey] isEqualToString:@"0"]){
            isCrateValidated = NO;
        }else{
            isCrateValidated = YES;
        }
        
        if(isWineValidated && !isCrateValidated){
            return NO;
        }else if(!isWineValidated && isCrateValidated){
            return NO;
        }
    }
    
    isWineValidated = NO;
    isCrateValidated = NO;
    
    for(int i=1; i<=5;i++){
        NSLog(@"loop i = %d",i);
        NSString *wineItemKey = [NSString stringWithFormat:@"WineItem%d",i];
        NSString *noCreateKey = [NSString stringWithFormat:@"CrateNo%d",i];
        
        if((self.dictAllInfo)[wineItemKey] == nil ||
           [[(self.dictAllInfo)[wineItemKey]wineName] isEqualToString:@"Select Wine"]){
            isWineValidated = NO;
        }else{
            isWineValidated = YES;
        }
        
        if((self.dictAllInfo)[noCreateKey] == nil ||
           [(self.dictAllInfo)[noCreateKey] isEqualToString:@"0"]){
            isCrateValidated = NO;
        }else{
            isCrateValidated = YES;
        }
        
        if(isWineValidated && isCrateValidated){
            return YES;
        }
    }
    return NO;
    
//    return oneValidated;
//    if([self.dictAllInfo objectForKey:@"WineItem1"] == nil || 
//       [[[self.dictAllInfo objectForKey:@"WineItem1"]wineName] isEqualToString:@"Select Wine"]){
//        NSLog(@"wineString = nil");
//        return NO;
//    }
//    
//    if([self.dictAllInfo objectForKey:@"CrateNo1"] == nil || 
//       [[[self.dictAllInfo objectForKey:@"WineItem1"]wineName] isEqualToString:@"Select Wine"]){
//        NSLog(@"wineString = nil");
//        return NO;
//    }
    
//    return YES;
}

-(BOOL)validateTxtFields:(NSMutableArray *)txtFieldArray{
    for(NSString *s in txtFieldArray){
        if([s isEqualToString:@""] || s == nil){
            return NO;
        }
    }
    return YES;
}

-(BOOL)validateEmail:(NSString *)candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}"; 
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex]; 
	
    return [emailTest evaluateWithObject:candidate];
}

-(BOOL)validateNoWineCases{
    int wineCrateNo = 0;
    for(int i=1; i<=5;i++){
        NSLog(@"loop i = %d",i);
        NSString *noCreateKey = [NSString stringWithFormat:@"CrateNo%d",i];
        
        if((self.dictAllInfo)[noCreateKey] != nil ||
           ![(self.dictAllInfo)[noCreateKey] isEqualToString:@"0"]){
            int thisCrate = [(self.dictAllInfo)[noCreateKey]intValue];
            wineCrateNo += thisCrate;
        }
    }
    if(wineCrateNo >= 5){
        return YES;
    }

    return NO;
}

-(void)displayError:(NSString *)displayError{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Oops" message:displayError
												   delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
	[alert show];
}

-(void)onClickDone:(id)sender{     
    if(pickerID == 0){
        
        WineItem *wineItem = (self.arrWine)[self.wineIndex];
        
        NSString *btnTitle = wineItem.wineName;
        
        [(UIButton *)self.btnSender setTitle:btnTitle forState:UIControlStateNormal]; 
        
        if([self.btnSender tag] == 1){
            (self.dictAllInfo)[@"WineItem1"] = wineItem;
        }else if([self.btnSender tag] == 2){
            (self.dictAllInfo)[@"WineItem2"] = wineItem;
        }else if([self.btnSender tag] == 3){
            (self.dictAllInfo)[@"WineItem3"] = wineItem;
        }else if([self.btnSender tag] == 4){
            (self.dictAllInfo)[@"WineItem4"] = wineItem;
        }else if([self.btnSender tag] == 5){
            (self.dictAllInfo)[@"WineItem5"] = wineItem;
        }
        
        [(UIButton *)self.btnSender setTitle:btnTitle forState:UIControlStateNormal];    
    }else if(pickerID == 1){
        
        NSString *btnTitle = (self.arrNoOfCrates)[crateIndex];
        
        if([self.btnSender tag] == 1){
            (self.dictAllInfo)[@"CrateNo1"] = btnTitle;
        }else if([self.btnSender tag] == 2){
            (self.dictAllInfo)[@"CrateNo2"] = btnTitle;
        }else if([self.btnSender tag] == 3){
            (self.dictAllInfo)[@"CrateNo3"] = btnTitle;
        }else if([self.btnSender tag] == 4){
            (self.dictAllInfo)[@"CrateNo4"] = btnTitle;
        }else if([self.btnSender tag] == 5){
            (self.dictAllInfo)[@"CrateNo5"] = btnTitle;
        }
        
        [(UIButton *)self.btnSender setTitle:btnTitle forState:UIControlStateNormal];    
    }
    
    
	[asPicker dismissWithClickedButtonIndex:0 animated:YES];
    //	//[[[sender superview] superview] removeFromSuperview];
}

- (void) checkerForWineDict{
    /*
     @property (nonatomic, retain) NSString * wineID;
     @property (nonatomic, retain) NSString * wineName;
     @property (nonatomic, retain) NSString * winePrice;
     @property (nonatomic, retain) NSString * winePriceValue;
     @property (nonatomic, retain) NSString * wineTaste;
     @property (nonatomic, retain) NSString * wineDesc;
     @property (nonatomic, retain) NSString * wineExpert;
     @property (nonatomic, retain) NSString * wineType;
     @property (nonatomic, retain) NSString * wineTypeValue;
     @property (nonatomic, retain) NSData * wineImage;
     @property (nonatomic, retain) NSString * wineRating;
     @property (nonatomic, retain) NSString * wineCountry;
     @property (nonatomic, retain) NSString * wineCountryValue;
     @property (nonatomic, retain) NSString * wineTypeID;
     @property (nonatomic, retain) NSString * wineTypeName;
*/
    
    WineItem *wineItm = self.arrWine[0];

    NSLog(@"got to checker");
    if (!(self.dictAllInfo)[@"WineItem1"])
        (self.dictAllInfo)[@"WineItem1"] = wineItm;
    if (!(self.dictAllInfo)[@"WineItem2"])
        (self.dictAllInfo)[@"WineItem2"] = wineItm;
    if (!(self.dictAllInfo)[@"WineItem3"])
        (self.dictAllInfo)[@"WineItem3"] =  wineItm;
    if (!(self.dictAllInfo)[@"WineItem4"])
        (self.dictAllInfo)[@"WineItem4"] =  wineItm;
    if (!(self.dictAllInfo)[@"WineItem5"])
        (self.dictAllInfo)[@"WineItem5"] =  wineItm;

    
    if (!(self.dictAllInfo)[@"CrateNo1"])
        (self.dictAllInfo)[@"CrateNo1"] = @"0";
    
    if (!(self.dictAllInfo)[@"CrateNo2"])
        (self.dictAllInfo)[@"CrateNo2"] = @"0";

    if (!(self.dictAllInfo)[@"CrateNo3"])
        (self.dictAllInfo)[@"CrateNo3"] = @"0";
    
    if (!(self.dictAllInfo)[@"CrateNo4"])
        (self.dictAllInfo)[@"CrateNo4"] = @"0";
    
    if (!(self.dictAllInfo)[@"CrateNo5"])
        (self.dictAllInfo)[@"CrateNo5"] = @"0";

}

-(IBAction)onClickSubmit:(id)sender{
    //go to confirmation page. send the following fields along with it.
    //do validation for empty fields and throw UIAlertViews
    
    [self checkerForWineDict];
    NSLog(@"HERE IS THE DICT %@",dictAllInfo);
     [self getDictTextFieldInfo];
    if([self validateDict]){
        [self resignAllResponders];
        //validate if email is acceptable
        if(![self validateEmail:txtField3.text]){
            [self displayError:@"Please enter valid email id"];
            return;
        }
        
        //validate if wine crate amount is acceptable
        //validate if wine crate amount is acceptable
        if(![self validateNoWineCases]){
            [self displayError:@"Minimum order of 5 cases and above"];
            return;
        }

        
        OrderWineConfirmViewController *orderConfirmView = [[OrderWineConfirmViewController alloc]init];
        [orderConfirmView setParamDict:self.dictAllInfo];
        [self.navigationController pushViewController:orderConfirmView animated:YES];
    }else{
        [self displayError:@"Please fill in the blanks, then submit the form again."];
    }
}

-(IBAction)onClickGetWine:(id)sender{
    NSLog(@"onClickGetWine");
    NSLog(@"sender.tag = %d",[sender tag]);
    
	[self showPickerWithTag:[sender tag]];
	
	[self setSelIndex:[sender tag]];
    
    pickerID = 0;
    
    wineIndex = 1;
    
    self.btnSender = sender;
    
    NSLog(@"btnSender = %@",self.btnSender);
}

-(IBAction)onClickCrateCount:(id)sender{
    NSLog(@"onClickCrateCount");
    NSLog(@"sender.tag = %d",[sender tag]);
    
	[self showPickerWithTag:[sender tag]];
	
	[self setSelIndex:[sender tag]];
    
    pickerID = 1;
    
    crateIndex = 1;
    
    self.btnSender = sender;
}

-(void)showPickerWithTag:(int)index{
    
    NSLog(@"showPickerWithTag:%d",index);
    
//	[self setPickerID:index];
	self.pvMain= [[UIPickerView alloc] initWithFrame:CGRectZero];
	self.pvMain.delegate = self;
	//picker_View.tag=index;
	self.pvMain.frame =CGRectMake(5,40 ,310,216);
	[self.pvMain selectRow:1 inComponent:0 animated:YES];
	
	self.pvMain.showsSelectionIndicator =YES;
	[asPicker addSubview:self.pvMain];
	[self.pvMain setDataSource:self];
    [asPicker showInView:self.view];
}

-(void)resignAllResponders{
    if ([txtField1 isFirstResponder]) {
        [txtField1 resignFirstResponder];
    }else if ([txtField2 isFirstResponder]) {
        [txtField2 resignFirstResponder];
    }else if ([txtField3 isFirstResponder]) {
        [txtField3 resignFirstResponder];
    }else if ([txtField4 isFirstResponder]) {
        [txtField4 resignFirstResponder];
    }else if ([txtField5 isFirstResponder]) {
        [txtField5 resignFirstResponder];
    }else if ([txtField6 isFirstResponder]) {
        [txtField6 resignFirstResponder];
    }
    //    else if ([txtField7 isFirstResponder]) {
    //        [txtField7 resignFirstResponder];
    //    }else if ([txtField8 isFirstResponder]) {
    //        [txtField8 resignFirstResponder];
    //    }
}

#pragma mark - UITextFieldDelegate Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if ([txtField1 isFirstResponder]) {
        [txtField2 becomeFirstResponder];
    }else if ([txtField2 isFirstResponder]) {
        [txtField3 becomeFirstResponder];
    }else if ([txtField3 isFirstResponder]) {
        [txtField4 becomeFirstResponder];
    }else if ([txtField4 isFirstResponder]) {
        [txtField5 becomeFirstResponder];
    }else if ([txtField5 isFirstResponder]) {
        [txtField6 becomeFirstResponder];
    }
    //    else if ([txtField6 isFirstResponder]) {
    //        [txtField7 becomeFirstResponder];
    //    }else if ([txtField7 isFirstResponder]) {
    //        [txtField8 becomeFirstResponder];
    //    }
    else{
        [textField resignFirstResponder];
    }
	
	return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField{
    svMain.contentSize = CGSizeMake(320.0,570.0);
    //    CGRect textFieldRect =
    //    [self.view.window convertRect:textField.bounds fromView:textField];
    //    CGRect viewRect =
    //    [self.view.window convertRect:self.view.bounds fromView:self.view];
    //    
    //    CGFloat midline = textFieldRect.origin.y + 0.5 * textFieldRect.size.height;
    //    CGFloat numerator =
    //    midline - viewRect.origin.y
    //    - MINIMUM_SCROLL_FRACTION * viewRect.size.height;
    //    CGFloat denominator =
    //    (MAXIMUM_SCROLL_FRACTION - MINIMUM_SCROLL_FRACTION)
    //    * viewRect.size.height;
    //    CGFloat heightFraction = numerator / denominator;
    //    
    //    if (heightFraction < 0.0)
    //    {
    //        heightFraction = 0.0;
    //    }
    //    else if (heightFraction > 1.0)
    //    {
    //        heightFraction = 1.0;
    //    }
    //    
    //    UIInterfaceOrientation orientation =
    //    [[UIApplication sharedApplication] statusBarOrientation];
    //    if (orientation == UIInterfaceOrientationPortrait ||
    //        orientation == UIInterfaceOrientationPortraitUpsideDown)
    //    {
    //        animatedDistance = floor(PORTRAIT_KEYBOARD_HEIGHT * heightFraction);
    //    }
    //    else
    //    {
    //        animatedDistance = floor(LANDSCAPE_KEYBOARD_HEIGHT * heightFraction);
    //    }
    //    
    //    CGRect viewFrame = self.view.frame;
    //    viewFrame.origin.y -= animatedDistance;
    //    
    //    [UIView beginAnimations:nil context:NULL];
    //    [UIView setAnimationBeginsFromCurrentState:YES];
    //    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    //    
    //    [self.view setFrame:viewFrame];
    //    
    //    [UIView commitAnimations];
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    svMain.contentSize = CGSizeMake(320.0,410.0);
    //    CGRect viewFrame = self.view.frame;
    //    viewFrame.origin.y += animatedDistance;
    //    
    //    [UIView beginAnimations:nil context:NULL];
    //    [UIView setAnimationBeginsFromCurrentState:YES];
    //    [UIView setAnimationDuration:KEYBOARD_ANIMATION_DURATION];
    //    
    //    [self.view setFrame:viewFrame];
    //    
    //    [UIView commitAnimations];
}

#pragma mark-
#pragma mark Picker View delegates
-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView;{
	return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
	NSInteger rowCount=0;
    
    if(pickerID == 0){
        rowCount = [self.arrWine count];
    }else if(pickerID == 1){
        rowCount = [self.arrNoOfCrates count];    
    }
    
    NSLog(@"rowCount = %d",rowCount);
    
	return rowCount;			 
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{ 
    NSString *item=@"";
    
    if(pickerID == 0){
        WineItem *wineData = (self.arrWine)[row];
        item = wineData.wineName;
    }else if(pickerID == 1){
        item = (self.arrNoOfCrates)[row];
    }
    
	return item;
}



-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    
    if(pickerID == 0){
        self.wineIndex = row;
        NSLog(@"wineIndex = %d",self.wineIndex);
        //        WineItem *wineData = [self.arrWine objectAtIndex:row];
        //        item = wineData.wineName;
    }else if(pickerID == 1){
        self.crateIndex = row;
        //        item = [self.arrNoOfCrates objectAtIndex:row];
    }
    
    //	NSString* valueForArray=@"";
    //	//if(picker_View.tag==1)
    //	if(pickerid==1)	{
    //        WineGroupType *wineGroupData =[arrWineType objectAtIndex:row];
    //		valueForArray=wineGroupData.wineConfigValue;
    //        self.wineIndex = row;
    //    }else if(pickerid==2){
    //		Country *countryData=[arrCountry objectAtIndex:row];
    //		valueForArray=countryData.countryName;
    //        self.countryIndex = row;
    //	}else if(pickerid==3)	
    //		valueForArray=[arrRating objectAtIndex:row];
    //	else  if(pickerid==4){
    //        PriceRange *priceData =[arrPrice objectAtIndex:row];
    //		valueForArray=priceData.textValue;
    //        self.priceIndex = row;
    //    }
    //	
    //	NSLog(@"Selected %@",valueForArray);
    //	//mlabel.text= [arrayNo objectAtIndex:row];
    //	[self setStrPickerValue:valueForArray];
    //	//[btShow setTitle:valueForArray forState:UIControlStateNormal];
	
}

-(void)addNoOfCrates{
    self.arrNoOfCrates = [NSMutableArray arrayWithCapacity:20];
    for(int i = 0;i<=20;i++){
        [arrNoOfCrates addObject:[NSString stringWithFormat:@"%d",i]];
    }
}

-(void)viewDidUnload{
    
    [super viewDidUnload];
    self.txtField1 = nil;
    self.txtField2 = nil;
    self.txtField3 = nil;
    self.txtField4 = nil;
    self.txtField5 = nil;
    self.txtField6 = nil;
    self.svMain = nil;
    self.pvMain = nil;
    //    self.pickerID = nil;
    self.asPicker = nil;
    self.arrWine = nil;
    self.arrNoOfCrates = nil;
    self.btnSender = nil;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
