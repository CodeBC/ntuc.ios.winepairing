//
//  FoodResultViewController.h
//  FairPrice
//
//  Created by Tridip Sarkar on 23/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Constants.h"

@class FairPriceAppDelegate;
@interface FoodResultViewController : BaseViewController <UIWebViewDelegate>
{
	UILabel* lblWineSelection;
	UILabel* lblFoodList;
	UITextView* txtDescription;
	NSInteger requestFromPage;
	//FairPriceAppDelegate  *appDelegate;
	NSMutableArray *arrWineInfo;
	UIWebView *DesWebView;
	
  
}


@property(nonatomic,strong) UILabel* lblWineSelection;
@property(nonatomic,strong) UILabel* lblFoodList;
@property(nonatomic,strong) UITextView* txtDescription;
@property(nonatomic,assign) NSInteger requestFromPage;
@property(nonatomic,strong) NSMutableArray *arrWineInfo;

-(void)createNavigationMenu;
-(void)createFoodSugession;
-(void)createLowerPanel;
-(void)createTextView;

@end
