//
//  Braches.m
//  FairPrice
//
//  Created by Zayar on 11/17/13.
//
//

#import "Braches.h"
#import "BranchCategories.h"


@implementation Braches

@dynamic b_id;
@dynamic b_cate_id;
@dynamic address;
@dynamic lat;
@dynamic log;
@dynamic category;

@end
