//
//  ShareViewController.m
//  FairPrice
//
//  Created by David Seah on 16/4/12.
//  Copyright (c) 2012 ddseah@gmail.com. All rights reserved.
//

#import "ShareViewController.h"
#import "Constants.h"

@implementation ShareViewController

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}



#pragma mark - View lifecycle

- (void)viewDidLoad{
    [super viewDidLoad];
    

}

-(void)createNavigationMenu{
    [super createNavigationMenu];
    
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, START_Y+30.0, 300.0, 34.5)];
	[titleLabel setText:@"SHARE"];
	titleLabel.textAlignment = UITextAlignmentCenter;
	titleLabel.backgroundColor=[UIColor clearColor];
	titleLabel.textColor = [UIColor redColor];
    titleLabel.font = [UIFont fontWithName:@"Tommaso" size:26.0];
	[self.view addSubview:titleLabel];
    
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.0, 300.0, 1.0)];
    [imgView setImage:[UIImage imageNamed:@"nav_bar_line.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view bringSubviewToFront:imgView];
    [self.view addSubview:imgView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
