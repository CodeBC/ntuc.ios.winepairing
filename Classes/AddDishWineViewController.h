//
//  AddDishViewController.h
//  FairPrice
//
//  Created by niladri.schatterjee on 10/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Constants.h"


@interface AddDishWineViewController : BaseViewController<UITextFieldDelegate> 
{
	UITextField* txtName;
	UITextField* txtEmail;
	UITextField* txtFoodSugession;
	NSInteger requestType;
	NSString *titleStr;
	NSString *descriptionStr;
	
	NSString *suggestionStr;
	NSString *suggestionPrompt;
	
}
@property(nonatomic,strong) UITextField* txtName;
@property(nonatomic,strong) UITextField* txtEmail;
@property(nonatomic,strong) UITextField* txtFoodSugession;
@property(nonatomic,assign) NSInteger requestType;
@property(nonatomic,strong) NSString *titleStr;
@property(nonatomic,strong) NSString *descriptionStr;
@property(nonatomic,strong) NSString *suggestionStr;
@property(nonatomic,strong) NSString *suggestionPrompt;
-(void)createAddView;
-(void)clickSubmit:(id)sender;
@end
