//
//  JustWineEmailViewController.h
//  FairPrice
//
//  Created by David Seah on 18/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseViewController.h"

@interface JustWineEmailViewController : BaseViewController<UITextFieldDelegate>

@property (nonatomic, strong) UITextField *txtField;

@end
