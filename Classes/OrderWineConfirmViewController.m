//
//  OrderWineConfirmViewController.m
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FairPriceAppDelegate.h"
#import "DataModel.h"
#import "OrderWineConfirmViewController.h"
#import "Constants.h"
#import "WineItem.h"

@interface OrderWineConfirmViewController ()

@end

@implementation OrderWineConfirmViewController

@synthesize svConfirm;

@synthesize lbl1;
@synthesize lbl2;
@synthesize lbl3;
@synthesize lbl4;
@synthesize lbl5;
@synthesize lbl6;
@synthesize lbl7;
@synthesize lbl8;
@synthesize lbl9;
@synthesize lbl10;
@synthesize lbl11;
@synthesize lbl12;
@synthesize lbl13;
@synthesize lbl14;
@synthesize lbl15;
@synthesize lbl16;
@synthesize lbl17;
@synthesize lbl18;

@synthesize paramDict;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

-(void)viewDidLoad{

    self.title = @"ORDER WINE DIRECT";
    [super viewDidLoad];

//    infoArray = [[NSMutableArray alloc] init];
    
    [self createScrollView];
    [self createLine];
    [self createTextLabels];
    [self createInfoLabels];
    [self createButtonConfirm];
    [self getEstPrice];
    
    NSLog(@"paramCount = %d",paramDict.count);
    
    WineItem *test = paramDict[@"WineItem1"];

    NSLog(@"WineItem1 = %@",test.wineName);
    
    WineItem *test2 = paramDict[@"WineItem5"];
    
    NSLog(@"WineItem5 = %@",test2.wineName);
    
//    NSLog(@"dictionary Content = %@",self.paramDict);
//    [self createLine];
}

-(void)createScrollView{
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, START_Y+30.0, 300.0, 34.5)];
//	[titleLabel setText:@"ORDER WINE DIRECT"];
//	titleLabel.textAlignment = UITextAlignmentCenter;
//	titleLabel.backgroundColor=[UIColor clearColor];
//	titleLabel.textColor = [UIColor whiteColor];
//    titleLabel.font = [UIFont fontWithName:@"Tommaso" size:26.0];


    
    svConfirm = [[UIScrollView alloc] initWithFrame:CGRectMake(0, START_Y+80.0, 320.0, 255.0)];
    svConfirm.contentSize = CGSizeMake(320.0,410.0);
	svConfirm.clipsToBounds = YES;
    svConfirm.backgroundColor = [UIColor clearColor];
    svConfirm.showsVerticalScrollIndicator = NO;
	svConfirm.delegate = self;
    
//    [self.svConfirm addSubview:titleLabel];
//    [titleLabel release];
    [self.view addSubview:svConfirm];

}

-(void)createNavigationMenu{   
    [self setTitleText:self.title];
    [super createNavigationMenu];
    
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, START_Y+30.0, 300.0, 34.5)];
//	[titleLabel setText:@"ORDER WINE DIRECT"];
//	titleLabel.textAlignment = UITextAlignmentCenter;
//	titleLabel.backgroundColor=[UIColor clearColor];
//	titleLabel.textColor = [UIColor redColor];
//    titleLabel.font = [UIFont fontWithName:@"Tommaso" size:26.0];
//	[self.view addSubview:titleLabel];
//	[titleLabel release];
//    
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.0, 300.0, 1.0)];
//    [imgView setImage:[UIImage imageNamed:@"nav_bar_line.png"]];
//    imgView.contentMode = UIViewContentModeScaleAspectFit;
//    [self.view bringSubviewToFront:imgView];
//    [self.view addSubview:imgView];
//    [imgView release];
}

-(void)createLine{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 280.0, 300.0, 40.0)];
    [imgView setImage:[UIImage imageNamed:@"cell_bg_small.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
//    [self.svConfirm bringSubviewToFront:imgView];
    [self.svConfirm addSubview:imgView];
}

-(void)createTextLabels{
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 0.0, 89.0, 20.0)];
	[lbl setText:@"Name"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 25.0, 89.0, 20.0)];
	[lbl setText:@"Email"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 50.0, 89.0, 20.0)];
	[lbl setText:@"Mobile"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 75.0, 89.0, 20.0)];
	[lbl setText:@"Address"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 100.0, 89.0, 20.0)];
	[lbl setText:@"Wine order"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 125.0, 89.0, 20.0)];
	[lbl setText:@"Order 1"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 150.0, 89.0, 20.0)];
	[lbl setText:@"Order 2"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 175.0, 89.0, 20.0)];
	[lbl setText:@"Order 3"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 200.0, 89.0, 20.0)];
	[lbl setText:@"Order 4"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 225.0, 89.0, 20.0)];
	[lbl setText:@"Order 5"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 250.0, 89.0, 20.0)];
	[lbl setText:@"Just Wine Club"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 264.0, 89.0, 20.0)];
	[lbl setText:@"Member No."];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 320.0, 89.0, 20.0)];
	[lbl setText:@"Est. Price"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 346.0, 89.0, 20.0)];
	[lbl setText:@"Just Wine"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 359.0, 89.0, 20.0)];
	[lbl setText:@"Club Saving"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, 385.0, 89.0, 20.0)];
	[lbl setText:@"Est. Total Price"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl];
    
//    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +205.0, 89.0, 20.0)];
//	[lbl setText:@"No. of Bottles"];
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor = [UIColor whiteColor];
//	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
//    lbl.backgroundColor = [UIColor clearColor];
//	[self.view addSubview:lbl];
//	[lbl release];
//    
//    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +240.0, 89.0, 20.0)];
//	[lbl setText:@"Price"];
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor = [UIColor whiteColor];
//	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
//    lbl.backgroundColor = [UIColor clearColor];
//	[self.view addSubview:lbl];
//	[lbl release];
//    
//    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +265.0, 89.0, 20.0)];
//	[lbl setText:@"Total Wine"];
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor = [UIColor whiteColor];
//	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
//    lbl.backgroundColor = [UIColor clearColor];
//	[self.view addSubview:lbl];
//	[lbl release];
//    
//    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +280.0, 89.0, 20.0)];
//	[lbl setText:@"Club Saving"];
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor = [UIColor whiteColor];
//	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
//    lbl.backgroundColor = [UIColor clearColor];
//	[self.view addSubview:lbl];
//	[lbl release];
//    
//    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +305.0, 89.0, 20.0)];
//	[lbl setText:@"Total Price"];
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor = [UIColor whiteColor];
//	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
//    lbl.backgroundColor = [UIColor clearColor];
//	[self.view addSubview:lbl];
//	[lbl release];
}

-(void)createTextLabels2{
    UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +80.0, 89.0, 20.0)];
	[lbl setText:@"Name"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +105.0, 89.0, 20.0)];
	[lbl setText:@"Email"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +130.0, 89.0, 20.0)];
	[lbl setText:@"Mobile"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +155.0, 89.0, 20.0)];
	[lbl setText:@"Address"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +180.0, 89.0, 20.0)];
	[lbl setText:@"Wine order"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +205.0, 89.0, 20.0)];
	[lbl setText:@"No. of Bottles"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +240.0, 89.0, 20.0)];
	[lbl setText:@"Price"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +265.0, 89.0, 20.0)];
	[lbl setText:@"Total Wine"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +280.0, 89.0, 20.0)];
	[lbl setText:@"Club Saving"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +305.0, 89.0, 20.0)];
	[lbl setText:@"Total Price"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
}

-(void)createInfoLabels{
    lbl1 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 0.0, 189.0, 20.0)];
    UITextField *lblVar = paramDict[@"Firstname"];
    UITextField *lblVar2 = paramDict[@"Surname"];
    lbl1.text = [NSString stringWithFormat:@"%@ %@",lblVar.text,lblVar2.text];
	lbl1.textAlignment = UITextAlignmentLeft;
	lbl1.textColor = [UIColor lightGrayColor];
	lbl1.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl1.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl1];
    
    lbl2 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 25.0, 189.0, 20.0)];
    lblVar = paramDict[@"Email"];
    lbl2.text = lblVar.text;
	lbl2.textAlignment = UITextAlignmentLeft;
	lbl2.textColor = [UIColor lightGrayColor];
	lbl2.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl2.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl2];
    
    lbl3 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 50.0, 189.0, 20.0)];
    lblVar = paramDict[@"Mobile"];
    lbl3.text = lblVar.text;
	lbl3.textAlignment = UITextAlignmentLeft;
	lbl3.textColor = [UIColor lightGrayColor];
	lbl3.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl3.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl3];
    
    lbl4 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 75.0, 189.0, 20.0)];
    lblVar = paramDict[@"Address"];
    lbl4.text = lblVar.text;
	lbl4.textAlignment = UITextAlignmentLeft;
	lbl4.textColor = [UIColor lightGrayColor];
	lbl4.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl4.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl4];
    
    
    
    
    
    
    
    
    
    lbl5 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 125.0, 135.0, 20.0)];
    WineItem *wineVar = paramDict[@"WineItem1"];
    lbl5.text = wineVar.wineName;
	lbl5.textAlignment = UITextAlignmentLeft;
	lbl5.textColor = [UIColor lightGrayColor];
	lbl5.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl5.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl5];
    
    lbl6 = [[UILabel alloc]initWithFrame:CGRectMake(255.0, 125.0, 65.0, 23.0)];
//    wineVar = [paramDict objectForKey:@"WineItem1"];
    NSString *stringVar = paramDict[@"CrateNo1"];
    if([self isStringValid:stringVar]){
        lbl6.text = [NSString stringWithFormat:@"%@ case(s)",stringVar];
    }
	lbl6.textAlignment = UITextAlignmentLeft;
	lbl6.textColor = [UIColor lightGrayColor];
	lbl6.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl6.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl6];
    
    
    
    
    
    
    
    
    lbl7 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 150.0, 135.0, 20.0)];
    WineItem *wineVar2 = paramDict[@"WineItem2"];
    if([self isWineItemValid:wineVar2]){
    lbl7.text = wineVar2.wineName;    
    }
	lbl7.textAlignment = UITextAlignmentLeft;
	lbl7.textColor = [UIColor lightGrayColor];
	lbl7.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl7.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl7];
    
    lbl8 = [[UILabel alloc]initWithFrame:CGRectMake(255.0, 150.0, 65.0, 23.0)];
    //    wineVar = [paramDict objectForKey:@"WineItem1"];
    NSString *stringVar2 = paramDict[@"CrateNo2"];
    if([self isStringValid:stringVar2]){
        lbl8.text = [NSString stringWithFormat:@"%@ case(s)",stringVar2];
    }
	lbl8.textAlignment = UITextAlignmentLeft;
	lbl8.textColor = [UIColor lightGrayColor];
	lbl8.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl8.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl8];
    
    
    
    
    
    
    
    
    lbl9 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 175.0, 135.0, 20.0)];
    WineItem *wineVar3 = paramDict[@"WineItem3"];
    if([self isWineItemValid:wineVar3]){
        lbl9.text = wineVar3.wineName;    
    }
	lbl9.textAlignment = UITextAlignmentLeft;
	lbl9.textColor = [UIColor lightGrayColor];
	lbl9.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl9.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl9];
    
    lbl10 = [[UILabel alloc]initWithFrame:CGRectMake(255.0, 175.0, 65.0, 23.0)];
    //    wineVar = [paramDict objectForKey:@"WineItem1"];
    NSString *stringVar3 = paramDict[@"CrateNo3"];
    if([self isStringValid:stringVar3]){
        lbl10.text = [NSString stringWithFormat:@"%@ case(s)",stringVar3];
    }
	lbl10.textAlignment = UITextAlignmentLeft;
	lbl10.textColor = [UIColor lightGrayColor];
	lbl10.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl10.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl10];
    
    
    
    
    
    
    
    lbl11 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 200.0, 135.0, 20.0)];
    WineItem *wineVar4 = paramDict[@"WineItem4"];
    if([self isWineItemValid:wineVar4]){
        lbl11.text = wineVar4.wineName;    
    }
	lbl11.textAlignment = UITextAlignmentLeft;
	lbl11.textColor = [UIColor lightGrayColor];
	lbl11.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl11.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl11];
    
    lbl12 = [[UILabel alloc]initWithFrame:CGRectMake(255.0, 200.0, 65.0, 23.0)];
    //    wineVar = [paramDict objectForKey:@"WineItem1"];
    NSString *stringVar4 = paramDict[@"CrateNo4"];
    if([self isStringValid:stringVar4]){
        lbl12.text = [NSString stringWithFormat:@"%@ case(s)",stringVar4];
    }
	lbl12.textAlignment = UITextAlignmentLeft;
	lbl12.textColor = [UIColor lightGrayColor];
	lbl12.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl12.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl12];
    
    
    
    
    
    
    
    lbl13 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 225.0, 135.0, 20.0)];
    WineItem *wineVar5 = paramDict[@"WineItem5"];
    if([self isWineItemValid:wineVar5]){
        lbl13.text = wineVar5.wineName;    
    }
	lbl13.textAlignment = UITextAlignmentLeft;
	lbl13.textColor = [UIColor lightGrayColor];
	lbl13.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl13.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl13];
    
    lbl14 = [[UILabel alloc]initWithFrame:CGRectMake(255.0, 225.0, 65.0, 23.0)];
    //    wineVar = [paramDict objectForKey:@"WineItem1"];
    NSString *stringVar5 = paramDict[@"CrateNo5"];
    if([self isStringValid:stringVar5]){
        lbl14.text = [NSString stringWithFormat:@"%@ case(s)",stringVar5];
    }
	lbl14.textAlignment = UITextAlignmentLeft;
	lbl14.textColor = [UIColor lightGrayColor];
	lbl14.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl14.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl14];
    
    
    
    
    
    
    
    lbl15 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 250.0, 135.0, 20.0)];
    //    wineVar = [paramDict objectForKey:@"WineItem1"];
    UITextField *stringVar6 = paramDict[@"JustWineMemberNo"];
//    if([self isStringValid:stringVar6]){
    lbl15.text = stringVar6.text;
//    }
	lbl15.textAlignment = UITextAlignmentLeft;
	lbl15.textColor = [UIColor lightGrayColor];
	lbl15.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl15.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl15];
    
    
    
    
    
    
    lbl16 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 320.0, 135.0, 20.0)];
    NSString *strEstPrice = [NSString stringWithFormat:@"%.02f", [self getEstPrice]];
    (self.paramDict)[@"EstPrice"] = [NSString stringWithFormat:@"%.02f", [self getEstPrice]];
    lbl16.text = [NSString stringWithFormat:@"$%@",strEstPrice];
	lbl16.textAlignment = UITextAlignmentLeft;
	lbl16.textColor = [UIColor lightGrayColor];
	lbl16.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl16.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl16];
    
    
    
    
    lbl17 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 353.0, 135.0, 20.0)];
    NSString *strSavePrice = [NSString stringWithFormat:@"%.02f", [self getSavePrice]];
    (self.paramDict)[@"SavePrice"] = [NSString stringWithFormat:@"%.02f", [self getSavePrice]];
    lbl17.text = [NSString stringWithFormat:@"$%@",strSavePrice];
	lbl17.textAlignment = UITextAlignmentLeft;
	lbl17.textColor = [UIColor lightGrayColor];
	lbl17.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl17.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl17];
    
    
    
    
    lbl18 = [[UILabel alloc]initWithFrame:CGRectMake(111.0, 387.0, 135.0, 20.0)];
    NSString *strDiscPrice = [NSString stringWithFormat:@"%.02f", [self getDiscPrice]];
    (self.paramDict)[@"DiscPrice"] = [NSString stringWithFormat:@"%.02f", [self getDiscPrice]];    
    lbl18.text = [NSString stringWithFormat:@"$%@",strDiscPrice];
	lbl18.textAlignment = UITextAlignmentLeft;
	lbl18.textColor = [UIColor lightGrayColor];
	lbl18.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
    lbl18.backgroundColor = [UIColor clearColor];
	[self.svConfirm addSubview:lbl18];
}

-(void)createButtonConfirm{
    UIButton *fairPriceButton = [[UIButton alloc] initWithFrame:CGRectMake(111.0 , START_Y+345.0, 97.0, 40.0)];
	fairPriceButton.tag=1;
	//[fairPriceButton setTitle:@"Fairprice Best Buy" forState:UIControlStateNormal];
	//	[fairPriceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[fairPriceButton addTarget:self action:@selector(onClickConfirm:) forControlEvents:UIControlEventTouchUpInside];
	[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"but_confirm_order.png"] forState:UIControlStateNormal];
	[self.view addSubview:fairPriceButton];
}

-(IBAction)onClickConfirm:(id)sender{
    //gather all lables and
    //send to server from here.
    //display uialertview with success or failure
    
    [self startIndicator];
    [appDelegate.dataModel startRequestWithParameters:REQUEST_SUBMIT_ORDER WithParams:self.paramDict];
    [self stopIndicator];
    
//    NSLog(@"appDelegate.dataModel = %d",[appDelegate.dataModel.arrWine count]);
    
    if ([appDelegate.dataModel.arrStatus count] > 0){
        [self showAlertWithMessage:@"Submission Successful"];
//        OrderWineFormViewController *ordWineFormView = [[OrderWineFormViewController alloc]init];
//        [self.navigationController pushViewController:ordWineFormView animated:YES];
//        [ordWineFormView release];
    }
    
}

-(void)showAlertWithMessage:(NSString*)strMessage
{
	UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"FairPrice" message:strMessage delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
	[alert show];
}

-(BOOL)isWineItemValid:(WineItem *)wiCheck{
    if(wiCheck==nil){
        return NO;
    }else if([wiCheck.wineName isEqualToString:@""]){
        return NO;
    }
    return YES;
}

-(BOOL)isStringValid:(NSString *)strCheck{
    if(strCheck==nil){
        return NO;
    }else if([strCheck isEqualToString:@""]){
        return NO;
    }
    return YES;
}

-(float)getEstPrice{
    
    float totalCost = 0;
    
    for(int i=1; i<=5;i++){
        
        NSLog(@"loop i = %d",i);
        
        NSString *wineItemKey = [NSString stringWithFormat:@"WineItem%d",i];
        NSString *noCrateKey = [NSString stringWithFormat:@"CrateNo%d",i];

        NSLog(@"wineItemKey = %@ , noCrateKey = %@",wineItemKey,noCrateKey);
        
        float wineItemFloat = [[(self.paramDict)[wineItemKey] winePrice] floatValue];
        float noCrateFloat = [(self.paramDict)[noCrateKey] floatValue];
        
        NSLog(@"wineItemFloat = %f , noCreateFloat = %f",wineItemFloat,noCrateFloat);
        
        float sizeOfCrateFloat = 12.0;
        
        totalCost += (wineItemFloat*noCrateFloat*sizeOfCrateFloat);
    }
    NSLog(@"totalCost = %f",totalCost);
    return totalCost;
}

-(float)getSavePrice{
    UITextField *justWineMemNo = (self.paramDict)[@"JustWineMemberNo"];
    if([self isStringValid:justWineMemNo.text]){
        float discPercentage = 0.08;
        return [self getEstPrice]*discPercentage;
    }
    return 0.0;
}

-(float)getDiscPrice{
    return [self getEstPrice]-[self getSavePrice];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    self.svConfirm = nil;
    
    self.lbl1 = nil;
    self.lbl2 = nil;
    self.lbl3 = nil;
    self.lbl4 = nil;
    self.lbl5 = nil;
    self.lbl6 = nil;
    self.lbl7 = nil;
    self.lbl8 = nil;
    self.lbl9 = nil;
    self.lbl10 = nil;
    self.lbl11 = nil;
    self.lbl12 = nil;
    self.lbl13 = nil;
    self.lbl14 = nil;
    self.lbl15 = nil;
    self.lbl16 = nil;
    self.lbl17 = nil;
    self.lbl18 = nil;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
