//
//  GroupDishItem.m
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import "GroupDishItem.h"
#import "Dishitem.h"


@implementation GroupDishItem

@dynamic dishIcon1;
@dynamic dishIcon2;
@dynamic dishImage;
@dynamic dishTitle;
@dynamic groupDishID;
@dynamic dishItems;

- (void)requestFinished:(ASIHTTPRequest *)request
{
    //    NSLog(@"requestResults = %@",[request responseData]);
    if ([request.username isEqualToString:@"GroupDishIcon_black"])
	{
        self.dishIcon1 = [request responseData];
    }
	else if ([request.username isEqualToString:@"GroupDishIcon_white"])
	{
        self.dishIcon2=[request responseData];
        
	}
	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"RELOAD" object:nil userInfo:nil];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
	if ([request.username isEqualToString:@"GroupDishIcon_black"])
	{
		self.dishIcon1 = nil;
    }
	else if ([request.username isEqualToString:@"GroupDishIcon_white"])
	{
		self.dishIcon2=nil;
		
	}
}


@end
