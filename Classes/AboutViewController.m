//
//  AboutViewController.m
//  FairPrice
//
//  Created by Tridip Sarkar on 26/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import "AboutViewController.h"
#import "Constants.h"
#import "UIImage+Retina4.h"
@implementation AboutViewController

@synthesize closeBtn;

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	[self setTitle:@"About"];
	self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.1/255 green:46.3/255 blue:28.2/255 alpha:0.2];
	[self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"app_bg.png"]]];
	[self createBody];
	[self createAboutText];
	
	
	closeBtn=[[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(clckCloseBtn:)];
	self.navigationItem.leftBarButtonItem=closeBtn;
	
	
}

-(void)createBody
{
	UIImageView* aView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 10.0, 300.0, 350.0)];
	[aView setImage:[UIImage imageNamed:@"contArea.png"]];
	[self.view addSubview:aView];
}
-(void)createAboutText
{   
	
	aboutScroll=[self CreateScrollViewWithFrame:CGRectMake(12,50,300,295) ContentSize:CGSizeMake(295,344) VscrollVisibled:YES HscrollVisibled:NO WillBounce:YES];
	[self.view addSubview:aboutScroll];
	
	UIView *aView=[[UIView alloc]initWithFrame:CGRectMake(0,10,300,344)];
	NSInteger yPos;
	NSString *tempStr=@"";
	yPos=20;
	tempStr=@"The FairPrice wine cellar";
	UILabel *lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:20 textIsBold:YES];
	//lbl.textAlignment=UITextAlignmentLeft;
	//lbl.backgroundColor=[UIColor greenColor];
	[aView addSubview:lbl];
	
	yPos=yPos+20;
	tempStr=@"Here, you'll find the largest range of wines from around the world, food and wine pairing recommendations, and wine tips from our wine educator Lim Hwee Peng, CSW, as well as suggestions from our fans, to match your every taste and budget. Simply select your preferences and you'll be toasting with your favourite wine in no time.";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:200 textIsBold:NO];
	//lbl.backgroundColor=[UIColor redColor];
	//lbl.textAlignment=UITextAlignmentLeft;
	[aView addSubview:lbl];
	
	[aboutScroll addSubview:aView];
	
}
	 
-(void)clckCloseBtn:(id)sender
{
	
	//UIWindow *w=[(id)[[UIApplication sharedApplication] delegate] window];
	//[self animateView:w WithTransition:UIViewAnimationTransitionCurlUp];

	
	[self.navigationController dismissModalViewControllerAnimated:YES];
	
	
}

//transition=UIViewAnimationTransitionFlipFromLeft ,UIViewAnimationTransitionFlipFromRight,UIViewAnimationTransitionCurlUp,UIViewAnimationTransitionCurlDown.
//FlipView=Any View object
-(void)animateView:(id)FlipView WithTransition:(UIViewAnimationTransition)transition
{  
	// setup the animation group
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.40];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationTransition:transition forView:FlipView cache:NO];
	[UIView commitAnimations];
}

-(UILabel*)createLabelWithText:(NSString*)strText withYPos:(CGFloat)y withHeight:(CGFloat)h textIsBold:(BOOL)flg
{
	UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(12.0,y,270.0,h)];
	lbl.numberOfLines=0;
	lbl.backgroundColor=[UIColor clearColor];
	[lbl setText:strText];
	lbl.textAlignment = UITextAlignmentCenter;
	lbl.textColor = UIColorFromHex(0x452801);
	if(flg)
		lbl.font = [UIFont boldSystemFontOfSize:17.0];
	else
		lbl.font = [UIFont systemFontOfSize:15.0];	
	
	return lbl;
	
}





-(UIScrollView *)CreateScrollViewWithFrame:(CGRect)frame
                               ContentSize:(CGSize)size
						   VscrollVisibled:(BOOL)visible
						   HscrollVisibled:(BOOL)Visible
								WillBounce:(BOOL)no
{
	
	UIScrollView *ScrollView=[[UIScrollView alloc] initWithFrame:frame];
	
    //Set the Properties of ListContainerView
	ScrollView.contentSize =size;
	ScrollView.scrollEnabled=YES;
	ScrollView.showsVerticalScrollIndicator=visible;
	ScrollView.showsHorizontalScrollIndicator=Visible;
	[ScrollView flashScrollIndicators];
	ScrollView.bounces =no;
	[ScrollView autoresizesSubviews];
	
	return ScrollView;
	
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}




@end
