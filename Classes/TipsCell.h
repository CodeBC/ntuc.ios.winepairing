//
//  WineForFoodCell.h
//  FairPrice
//
//  Created by niladri.schatterjee on 10/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface TipsCell : UITableViewCell

@property (nonatomic,strong)UILabel* lblTips;
@property (nonatomic,strong)UIImageView* ivBackImage;


@end
