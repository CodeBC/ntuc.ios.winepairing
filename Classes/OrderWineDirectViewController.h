//
//  OrderWineDirectViewController.h
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface OrderWineDirectViewController : BaseViewController<UIWebViewDelegate>

@property(nonatomic,strong) UIWebView *DesWebView;

-(void)createNavigationMenu;
-(void)createMainBody;
-(void)createTextView;
-(void)createButtons;

@end
