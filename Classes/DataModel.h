//
//  DataModel.h
//  FairPrice
//
//  Created by Satish Kumar on 10/7/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>

@class ASINetworkQueue;
@interface DataModel :NSObject 
{
	int selectedTabIndex;
	int REQUEST_ID;
	CXMLDocument*	    _xmlParser;
    NSString *networkErrMsg;
	
	NSMutableArray *ArrWine;
	NSMutableArray *arrGroupWine;
	NSMutableArray *arrGroupDish;
	NSMutableArray *arrDishItem;
	NSMutableArray *arrWineInfo;
	NSMutableArray *arrWineBuyInfo;
	NSMutableArray *arrDishInfo;
	NSMutableArray *arrWineType;
	NSMutableArray *arrPriceRange;
	NSMutableArray *arrCountry;
    NSMutableArray *arrWineGroupType;
	NSString *maxRating;
	NSString *responseStr;
	ASINetworkQueue* aQueue;
}

@property(nonatomic,strong) ASINetworkQueue* aQueue;
@property(nonatomic,strong) NSString *networkErrMsg;
@property(assign) int selectedTabIndex;
@property(nonatomic,assign) int REQUEST_ID;
@property(nonatomic,strong) NSMutableArray *arrWine;
@property(nonatomic,strong) NSMutableArray *arrGroupWine;
@property(nonatomic,strong) NSMutableArray *arrGroupDish;
@property(nonatomic,strong) NSMutableArray *arrDishItem;
@property(nonatomic,strong) NSMutableArray *arrWineInfo;
@property(nonatomic,strong) NSMutableArray *arrWineBuyInfo;
@property(nonatomic,strong) NSMutableArray *arrDishInfo;
@property(nonatomic,strong) NSMutableArray *arrWineType;
@property(nonatomic,strong) NSMutableArray *arrPriceRange;
@property(nonatomic,strong) NSMutableArray *arrCountry;
@property(nonatomic,strong) NSMutableArray *arrWineGroupType;
@property(nonatomic,strong) NSMutableArray *arrTipsArticles;
@property(nonatomic,strong) NSMutableArray *arrTipsVideos;
@property(nonatomic,strong) NSMutableArray *arrTipsArticleItem;
@property(nonatomic,strong) NSMutableArray *arrTipsVideoItem;
@property(nonatomic,strong) NSMutableArray *arrStatus;
@property(nonatomic,strong) NSString *maxRating;
@property(nonatomic,strong) NSString *responseStr;

-(BOOL)loadWineByType:(NSData*)aData;
-(NSData*)sendRequest:(NSString*) url;
-(BOOL)startRequestWithParameters:(int)aRequest WithParams:(NSMutableDictionary*) aParam;
-(BOOL)startRequest:(int) aRequest;
-(void)loadWineForFood:(NSData*) aData;
-(BOOL)loadWine:(NSData*)aData;
-(BOOL)loadWineBasedOnGroups:(NSData*)aData;
-(BOOL)loadGroupDishList:(NSData*)aData;
-(BOOL)loadDishList:(NSData*)aData withParent:(NSString *) groupDishItemId;
-(BOOL)loadWineGroupInfo:(NSData*)aData;
-(BOOL)loadWineBuyInfo:(NSData*)aData;
-(BOOL)loadDishInfo:(NSData*)aData;
-(BOOL)loadSearchFilter:(NSData*)aData;
-(BOOL)loadSearchFilter;
-(BOOL)loadTipsArticle:(NSData*)aData;
-(BOOL)loadTipsVideos:(NSData*)aData;
-(BOOL)loadTipsArticleItem:(NSData*)aData;
-(BOOL)loadTipsVideoItem:(NSData*)aData;
-(BOOL)loadSubmitOrder:(NSData*)aData;
-(BOOL)loadWineAllInfo:(NSData *)aData;
-(BOOL)getResponse:(NSData*)aData;
-(BOOL)getResponseRating:(NSData*)aData;
-(BOOL)getResponseEmail:(NSData*)aData;
-(void)showAlertWithMessage:(NSString*) strMessage;
//-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding;

@end
