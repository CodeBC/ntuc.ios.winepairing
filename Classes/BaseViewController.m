//
//  BaseViewController.m
//  FairPrice
//
//  Created by Satish Kumar on 10/7/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import "BaseViewController.h"
#import "OrderWineDirectViewController.h"
#import "WineForFoodViewController.h"
#import "WineListSearchViewController.h"
#import "DataModel.h"
#import "FairPriceAppDelegate.h"
#import "AboutViewController.h"
#import "Constants.h"
#import "TipsAndVidViewController.h"
#import "JustWineClubViewController.h"
#import "UIImage+Retina4.h"

@implementation BaseViewController
@synthesize mainView,tabBarView,tabIndex,actView,dataModel;
@synthesize but_order_direct;
@synthesize but_share;
@synthesize titleView;
@synthesize titleText;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
 if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
 // Custom initialization
 }
 return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */

-(void)viewWillAppear:(BOOL)animated{
    if(!self.navigationController.navigationBarHidden){
        //        [self.navigationController setNavigationBarHidden:YES];
    }
}


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];

	[self.view setMultipleTouchEnabled:NO];

    appDelegate=(FairPriceAppDelegate*)[[UIApplication sharedApplication] delegate];

    self.dataModel = ((FairPriceAppDelegate*)[[UIApplication sharedApplication] delegate]).dataModel;

    //    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"bc_app_bg"]]];
    //    [self.view setBackgroundColor:[UIColor clearColor]];
	[self.navigationController setNavigationBarHidden:YES];

	self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:67.1/255 green:46.3/255 blue:28.2/255 alpha:0.2];

    //    [self createButtonInfo];
    //    [self createBody];

    [self createBackground];
    [self createNavigationMenu];

	//[self setTabIndex:1];
	//[self addFooter];


    //	btnBack = [[UIButton alloc]initWithFrame:CGRectMake(0.0,0.0,51.0,30.0)];
    //	[btnBack setBackgroundImage:[UIImage imageNamed:@"back_Btn.png"] forState:UIControlStateNormal];
    //	[btnBack setExclusiveTouch:YES];
    //	[btnBack addTarget:self action:@selector(clickNavBtns:) forControlEvents:UIControlEventTouchUpInside];
    //	btnBack.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;
    //	UIBarButtonItem *backBarButton=[[UIBarButtonItem alloc]initWithCustomView:btnBack];
    //	[btnBack release];
    //	self.navigationItem.leftBarButtonItem=backBarButton;
    //	[backBarButton release];

}

-(void)createBackground{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    [imgView setImage: [UIImage imageNamed:@"bc_app_bg-568h@2x.png"]];
    [self.view addSubview:imgView];
}

-(void)createNavigationMenu{
    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+10, 300.0, 17.0)];
    [imgView setImage:[UIImage imageNamed: @"img_ntuc_logo_white.png"]];
    imgView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:imgView];

    btnBack = [[UIButton alloc] initWithFrame:CGRectMake(BACK_X, BACK_Y, 20.0, 20.0)];
    [btnBack setBackgroundImage:[UIImage imageNamed:@"but_back.png"] forState:UIControlStateNormal];
    NSLog(@"here is back btn added!!!!!");
    [btnBack addTarget:self action:@selector(clickNavBtns:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btnBack];

    but_share = [[UIButton alloc] initWithFrame:CGRectMake(320.0-BACK_X-19.0-10.0-20.0-10.0, BACK_Y, 22.0, 17.5)];
    [but_share setBackgroundImage:[UIImage imageNamed:@"ico_share_link.png"] forState:UIControlStateNormal];
    [but_share addTarget:self action:@selector(onClickShare:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:but_share];

    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(320.0-BACK_X-19.0-10.0, BACK_Y, 1.5, 26.0)];
    [imgView setImage:[UIImage imageNamed:@"small_line.png"]];
    [self.view addSubview:imgView];

    but_order_direct = [[UIButton alloc] initWithFrame:CGRectMake(320.0-BACK_X-19.0, BACK_Y, 20.0, 20.0)];
    [but_order_direct setBackgroundImage:[UIImage imageNamed:@"ico_order_wine.png"] forState:UIControlStateNormal];
    [but_order_direct addTarget:self action:@selector(onClickOrder:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:but_order_direct];

    titleView = [[TitleView alloc] initWithFrame:CGRectMake(0.0, START_Y+30, 320.0, 33.0)];
    titleView.titleLabel.text = titleText;
    titleView.backgroundColor = [UIColor clearColor];

    [self.view addSubview:titleView];
}

-(void)createButtonInfo{
	UIButton *btnInfo = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 0.0, 30.0, 18.0)];
	[btnInfo setExclusiveTouch:YES];
	[btnInfo setBackgroundImage:[UIImage imageNamed:@"infoBtn.png"] forState:UIControlStateNormal];

    btnInfo.tag=6;

	[btnInfo addTarget:self action:@selector(clickInfobtn:) forControlEvents:UIControlEventTouchUpInside];

	btnInfo.contentHorizontalAlignment=UIControlContentHorizontalAlignmentCenter;

	UIBarButtonItem *InfoBarButton=[[UIBarButtonItem alloc]initWithCustomView:btnInfo];
	self.navigationItem.rightBarButtonItem=InfoBarButton;

}

-(void)clickNavBtns: (id)sender{
//  	if ([appDelegate.dataModel aQueue] && [[appDelegate.dataModel aQueue] isNetworkActive]){
//		[[appDelegate.dataModel aQueue] cancelAllOperations];
//	}
	[self.navigationController popViewControllerAnimated:YES];
}

-(void)onClickOrder:(id)sender{
    if ([appDelegate.dataModel aQueue]){
		[[appDelegate.dataModel aQueue] cancelAllOperations];
	}
    //[appDelegate showViewController:ORDERWINE];
    [appDelegate showOrderForm:self];
    //    OrderWineDirectViewController *orderWineView = [[OrderWineDirectViewController alloc] init];
    //    [self.navigationController pushViewController:orderWineView	animated:YES];
    //    [orderWineView release];
}

-(void)onClickShare:(id)sender{
    // && [[appDelegate.dataModel aQueue] isNetworkActive]
    if ([appDelegate.dataModel aQueue]){
		[[appDelegate.dataModel aQueue] cancelAllOperations];
	}

    UIActionSheet *actionSheet = [[UIActionSheet alloc]initWithTitle:@"Share" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Twitter",@"Email", nil];

    [actionSheet showInView:self.view];
}

#pragma - Action Sheet Delegate Method
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"BaseViewController buttonIndex = %d",buttonIndex);
    if(buttonIndex == 0){
        //facebook
        [self createFacebookPost];
    }else if(buttonIndex == 1){
        //twitter
        [self createTweet];
    }else if(buttonIndex == 2){
        //email
        [self createMailLayout];
    }else{
        //cancel
    }
}

-(void)createFacebookPost{
    if(![appDelegate.facebook isSessionValid]) {
        NSArray *permissions = [[NSArray alloc] initWithObjects:@"publish_stream", nil];
        [appDelegate.facebook authorize:permissions];
    } else {
        // Create the parameters dictionary that will keep the data that will be posted.
        NSLog(@"BaseViewController createFacebookPost");
        [appDelegate showFacebookDialog:@"Create"];
    }
}

-(void)createTweet{
    if ([TWTweetComposeViewController canSendTweet]){
        TWTweetComposeViewController *tweetSheet = [[TWTweetComposeViewController alloc] init];
        [tweetSheet setInitialText:@"Have a local dish or wine in mind for pairing? Check out the recommendations provided by FairPrice Wine Pairing app! http://bit.ly/KrEnPw"];

	    [self presentModalViewController:tweetSheet animated:YES];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:@"Sorry"
                                                            message:@"You can't send a tweet right now, make sure your device has an internet connection and you have at least one Twitter account setup"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
        [alertView show];
    }
}

-(void)createMailLayout{

	MFMailComposeViewController *picker = [[MFMailComposeViewController alloc] init];
	picker.mailComposeDelegate = self;

	[picker setSubject:@"A world first application that pairs wine with Asian food and Western food"];


	// Set up recipients
	NSArray *toRecipients = @[@""];
	NSArray *ccRecipients = @[@"", @""];
	NSArray *bccRecipients = @[@""];

	[picker setToRecipients:toRecipients];
	[picker setCcRecipients:ccRecipients];
	[picker setBccRecipients:bccRecipients];

	// Attach an image to the email
    //	NSString *path = [[NSBundle mainBundle] pathForResource:@"rainy" ofType:@"png"];
    //    NSData *myData = [NSData dataWithContentsOfFile:path];
    //	[picker addAttachmentData:myData mimeType:@"image/png" fileName:@"rainy"];

	// Fill out the email body text
	NSString *emailBody = @"This app from FairPrice is cool. A world first, it pairs wine with Asian food and Western food. http://itunes.apple.com/sg/app/wine-pairing-app/id439254783?mt=8";
	[picker setMessageBody:emailBody isHTML:NO];

	[self presentModalViewController:picker animated:YES];
}

- (void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error{
    //	message.hidden = NO;
	// Notifies users about errors associated with the interface
	switch (result)
	{
		case MFMailComposeResultCancelled:
            //			message.text = @"Result: canceled";
			break;
		case MFMailComposeResultSaved:
            //			message.text = @"Result: saved";
			break;
		case MFMailComposeResultSent:
            //			message.text = @"Result: sent";
			break;
		case MFMailComposeResultFailed:
            //			message.text = @"Result: failed";
			break;
		default:
            //			message.text = @"Result: not sent";
			break;
	}
	[self dismissModalViewControllerAnimated:YES];
}

-(void)hideBackButton{
    NSLog(@"here is hide BackButton!!");
	[btnBack setHidden:YES];
}



-(void)clickInfobtn:(id)sender
{

	AboutViewController *aboutView=[[AboutViewController alloc]init];

	UINavigationController *nav_aboutView=[[UINavigationController alloc]initWithRootViewController:aboutView];

	[self presentModalViewController:nav_aboutView animated:YES];
}

//transition=UIViewAnimationTransitionFlipFromLeft ,UIViewAnimationTransitionFlipFromRight,UIViewAnimationTransitionCurlUp,UIViewAnimationTransitionCurlDown.
//FlipView=Any View object

-(void)animateView:(id)FlipView WithTransition:(UIViewAnimationTransition)transition
{
	// setup the animation group
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.40];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationTransition:transition forView:FlipView cache:NO];
	[UIView commitAnimations];
}

-(void)createBody
{
    UIImageView* aView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, 10.0, 300.0, 350.0)];
    [aView setImage:[UIImage imageNamed:@"contArea.png"]];
    [self.view addSubview:aView];
}
/*
 // Override to allow orientations other than the default portrait orientation.
 - (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
 // Return YES for supported orientations
 return (interfaceOrientation == UIInterfaceOrientationPortrait);
 }
 */

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];

    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

/*

 Custom Tab Bar Initiation

 */
-(void)addFooter{

    float width = 320/5.0;
	float height = 55.0;
	float sX = 0.0;

    CGRect appFrame = [[UIScreen mainScreen] applicationFrame];

	UIImageView* aView = [[UIImageView alloc] initWithFrame:CGRectMake(0.0, appFrame.size.height-height, appFrame.size.width, height)];
	[aView setUserInteractionEnabled:YES];

	NSString* aFileName = [NSString stringWithFormat:@"%d.png",dataModel.selectedTabIndex];
	[aView setImage:[UIImage imageNamed:aFileName]];

    [self setTabBarView:aView];

    for(int i = 1 ; i <= 5 ; i++)
	{
		UIButton* bt =  [[UIButton alloc] initWithFrame:CGRectMake(sX, 0.0, width, height)];
		[bt setExclusiveTouch:YES];
		[bt addTarget:self action:@selector(handleButton:) forControlEvents:UIControlEventTouchUpInside];
		[bt setBackgroundColor:[UIColor clearColor]];
		[bt setTag:i];

		[aView addSubview:bt];

        sX += (width+1);
	}

    [self.view addSubview:aView];
}


-(void)handleButton:(id)sender
{
	int aTag = [sender tag];

	[self.dataModel setSelectedTabIndex:aTag];

	switch (aTag)
	{


		case HOME:
        {
            [appDelegate showViewController:HOME];
            break;
        }

		case BYFOOD:
        {
            [self startIndicator];
            [appDelegate.dataModel startRequestWithParameters:REQUEST_GROUP_DISH WithParams:nil];
            [self stopIndicator];
            if ([appDelegate.dataModel.arrGroupDish count] > 0)
            {
                [appDelegate showViewController:BYFOOD];
            }

            break;
        }

		case BYWINE:
        {
            [self startIndicator];
            [appDelegate.dataModel startRequestWithParameters:REQUEST_WINETYPE WithParams:nil];
            [self stopIndicator];
            if ([appDelegate.dataModel.arrWineType count] > 0)
            {
                [appDelegate showViewController:BYWINE];
            }

            break;
        }


		case WINELIST:
        {


            //              [self startIndicator];
            //              //		[appDelegate.dataModel startRequestWithParameters:REQUEST_SEARCH_FILTER WithParams:nil];
            //              [appDelegate.dataModel loadSearchFilter];
            //              [self stopIndicator];
            //              NSLog(@"appDelegate.dataModel = %d",[appDelegate.dataModel.arrCountry count]);
            //              if ([appDelegate.dataModel.arrCountry count] > 0)
            //              {
            //                  WineListSearchViewController* wineSearchView= [[WineListSearchViewController alloc] init];
            //                  [self.navigationController pushViewController:wineSearchView	animated:YES];
            //                  [wineSearchView release];
            //              }
            //              else {
            //                  [imageLogo3  setImage:[UIImage imageNamed:@"ico_list_all_wines.png"]];
            //              }


            //            [self startIndicator];
            //            [appDelegate.dataModel loadSearchFilter];
            //            [self stopIndicator];

            [self startIndicator];
            [appDelegate.dataModel startRequestWithParameters:REQUEST_ALL_WINES WithParams:nil];
            [self stopIndicator];
            [self hideBackButton];
            if ([appDelegate.dataModel.arrWine count] > 0)
            {
                [appDelegate showViewController:WINELIST];
            }


            //            [self startIndicator];
            //            [appDelegate.dataModel startRequestWithParameters:REQUEST_ALL_WINES WithParams:nil];
            //            //		[appDelegate.dataModel loadSearchFilter];
            //            [self stopIndicator];
            //            if ([appDelegate.dataModel.arrWine count] > 0)
            //            {
            //
            //                WineListAllResultViewController* wineResultView= [[WineListAllResultViewController alloc]init];
            //                wineResultView.requestFromPage=WINELIST;
            //                [self.navigationController pushViewController:wineResultView	animated:YES];
            //                [wineResultView release];
            //
            //                //		  WineListSearchViewController* wineSearchView= [[WineListSearchViewController alloc] init];
            //                //		  [self.navigationController pushViewController:wineSearchView	animated:YES];
            //                //		  [wineSearchView release];
            //            }

            break;
        }


		case TIP:
		{

            [self startIndicator];
            [appDelegate.dataModel startRequestWithParameters:REQUEST_GET_ALL_TIPS WithParams:nil];
            [appDelegate.dataModel startRequestWithParameters:REQUEST_GET_ALL_TIPS_VIDEOS WithParams:nil];
            [self stopIndicator];

            if ([appDelegate.dataModel.arrTipsArticles count] > 0 && [appDelegate.dataModel.arrTipsVideos count])
            {
                [appDelegate showViewController:TIP];
            }


			break;

		}

        case WINECLUB:
		{
            NSLog(@"GO TO WINE CLUB");
            [appDelegate showViewController:WINECLUB];
			break;

		}

	}

	[self setTabIndex:aTag];
	NSLog(@"Button Clicked  %d" , aTag);

}

-(void)startIndicator
{
	//[self.view setUserInteractionEnabled:NO];
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	if (actView)
	{
		if (![actView isAnimating])
			[actView startAnimating];
	}
	else
	{
		UIView* aView = [[UIView alloc] initWithFrame:CGRectMake(130.0, 150.0, 60.0, 60.0)];
		CALayer * l = [aView layer];
		[l setMasksToBounds:YES];
		[l setCornerRadius:8.0];

		//[aView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"content_bg.png"]]];
		[aView setBackgroundColor:[UIColor blackColor]];
		[aView setAlpha:0.5];

		UIActivityIndicatorView* act = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
		[self setActView:act];
		[actView setFrame:CGRectMake(18.0, 12.0, 22.0, 25.0)];
		[actView setHidesWhenStopped:YES];
		[aView addSubview:actView];

		UILabel* loading = [[UILabel alloc]initWithFrame:CGRectMake(0.0, 45.0, 60.0, 12.0)];
		loading.font = [UIFont boldSystemFontOfSize:11.0];
		loading.textAlignment = UITextAlignmentCenter;
		loading.textColor = [UIColor whiteColor];
		[loading setBackgroundColor:[UIColor clearColor]];
		[loading setText:@"Loading..."];
		[aView addSubview:loading];
		[self.view addSubview:aView];
		[actView startAnimating];
		[self.view setUserInteractionEnabled:NO];
		btnBack.enabled=NO;
	}


}

-(void)stopIndicator
{
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	if(actView)
	{
		if([actView isAnimating])
		{
			[actView stopAnimating];
		}
		[actView.superview removeFromSuperview];
		self.actView = nil;
		[self.view setUserInteractionEnabled:YES ];
		btnBack.enabled=YES;
	}
}


-(void)showAlertWithMessage:(NSString*) strMessage
{
	UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"FairPrice" message:strMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
	[alert show];
}

-(NSString*)trim:(NSString*)string
{
  	string=[string stringByTrimmingCharactersInSet:[NSCharacterSet  whitespaceCharacterSet]];
	return string;
}

-(BOOL)validateEmail:(NSString *)candidate
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex];

    return [emailTest evaluateWithObject:candidate];
}



@end
