//
//  WineInfo.h
//  FairPrice
//
//  Created by Tridip Sarkar on 28/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface WineBuyInfo : NSObject 
{
	NSString *wineID;
	NSString *title;
	NSString *price;
	NSString *rating;
	NSString *description;
	NSData *wineImage;
}

@property(nonatomic,strong) NSString *wineID;
@property(nonatomic,strong) NSString *title;
@property(nonatomic,strong) NSString *price;
@property(nonatomic,strong) NSString *rating;
@property(nonatomic,strong) NSString *description;
@property(nonatomic,strong) NSData *wineImage;
-(id)init;
@end
