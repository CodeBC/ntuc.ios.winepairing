//
//  FoodResultViewController.m
//  FairPrice
//
//  Created by Tridip Sarkar on 23/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import "FoodResultViewController.h"
#import "FairPriceBestBuyViewController.h"
#import "WineListResultViewController.h"
#import "DataModel.h"
#import "FairPriceAppDelegate.h"
#import "WineInfo.h"
#import "WineGroupItem.h"
#import "UIImage+Retina4.h"

@implementation FoodResultViewController
@synthesize lblWineSelection,lblFoodList,txtDescription,requestFromPage,arrWineInfo;
/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView {
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad
{   
    [super viewDidLoad];
	[self setArrWineInfo:appDelegate.dataModel.arrGroupWine];
	DesWebView = [[UIWebView alloc] init];
	[self createFoodSugession];
//    [self createLowerPanel];
    [self createTextView];
    [self setTitle:@"Result"];
	
	for (id subview in DesWebView.subviews)
		if ([[subview class] isSubclassOfClass: [UIScrollView class]])
			((UIScrollView *)subview).bounces = YES;
	
}

-(void)createNavigationMenu{  
    [self setTitleText:self.title];
    [super createNavigationMenu];
    
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, START_Y+30.0, 300.0, 34.5)];
//	[titleLabel setText:self.title];
//	titleLabel.textAlignment = UITextAlignmentCenter;
//	titleLabel.backgroundColor=[UIColor clearColor];
//	titleLabel.textColor = [UIColor redColor];
//    titleLabel.font = [UIFont fontWithName:@"Tommaso" size:26.0];
//	[self.view addSubview:titleLabel];
//	[titleLabel release];
//    
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.0, 300.0, 1.0)];
//    [imgView setImage:[UIImage imageNamed:@"nav_bar_line.png"]];
//    imgView.contentMode = UIViewContentModeScaleAspectFit;
//    [self.view bringSubviewToFront:imgView];
//    [self.view addSubview:imgView];
//    [imgView release];
    
}

-(void)createFoodSugession
{   
	
	if([self.arrWineInfo count] == 0)
		return;
//	WineInfo *wineInfoData=[self.arrWineInfo objectAtIndex:0];
	WineGroupItem *wineInfoData=(self.arrWineInfo)[0];
	
//	UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y+50.0 , 290.0, 20.0)];
//	[lbl setText:@"Your wine selection"];
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor = UIColorFromHex(0x452801);
//	lbl.font = [UIFont italicSystemFontOfSize:18.0];
//	[self.view addSubview:lbl];
//	[lbl release];
	
//	UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +85.0, 250.0, 20.0)];
//	[lbl setText:wineInfoData.wineTitle];
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor =[UIColor whiteColor];
//	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:18];
//    lbl.backgroundColor = [UIColor clearColor];
//	[self setLblWineSelection:lbl];
//	[self.view addSubview:lbl];
//	[lbl release];
	
	UILabel *lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +85.0, 290.0, 20.0)];
	[lbl setText:@"YOUR FOOD TO GO WITH THIS:"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:18];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    
//	NSMutableString *foodList = [[NSMutableString alloc] init];
//	int len  = [wineInfoData.arrFoodToGo count];
//	for (int i = 0 ; i < len ; i++)
//	{   
//		NSString *tempStr = [wineInfoData.arrFoodToGo objectAtIndex:i];
//		[foodList appendString:[NSString stringWithFormat:@"%@",tempStr]];
//		if (i+1 < len)
//		{
//			[foodList appendString:@", "];
//		}
//	}
//	if([foodList length]==0)
//		[foodList setString:@"No result!"];
	
						
//	lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.00, START_Y+70 , 285.0, 40.0)];
//	lbl.numberOfLines=0;
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor = UIColorFromHex(0x452801);
//	lbl.font = [UIFont boldSystemFontOfSize:15.0];
//	[lbl setText:foodList];
//	[self setLblFoodList:lbl];
//	[self.view addSubview:lbl];
//	[lbl release];
	
	
	UITextView* txtField = [[UITextView alloc] initWithFrame:CGRectMake(20.0, START_Y+105.0 , 285.0, 50.0)];
	[txtField setText:[NSString stringWithFormat:@" %@",wineInfoData.foodNames]];              //@"  Chardonnay, Muscat, Pinor Noir"];
	txtField.contentInset = UIEdgeInsetsMake(-4,-8,0,0);
	txtField.editable=NO;
	txtField.textAlignment = UITextAlignmentLeft;
	txtField.textColor = [UIColor whiteColor];
	txtField.font = [UIFont fontWithName:@"EuphemiaUCAS" size:14];
    txtField.backgroundColor = [UIColor clearColor];
	[self.view addSubview:txtField];
	
	
//	[foodList release];
	
	UIButton* fairPriceButton = [[UIButton alloc] initWithFrame:CGRectMake(20.0 , START_Y+160.0, 144.0, 40.0)];
	fairPriceButton.tag=1;
	//[fairPriceButton setTitle:@"Fairprice Best Buy" forState:UIControlStateNormal];
	//	[fairPriceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
	[fairPriceButton addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
	[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"but_fairprice_recommends.png"] forState:UIControlStateNormal];
	//[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"redBg.png"] forState:UIControlStateHighlighted];
	[self.view addSubview:fairPriceButton];
	
}

-(void)createTextView
{   
	CGRect appFrame = [[UIScreen mainScreen] applicationFrame];
	DesWebView.frame = CGRectMake(20.0, START_Y+210.0, 290.0, appFrame.size.height - 290.0);
	[DesWebView setOpaque:NO];
	DesWebView.backgroundColor = [UIColor clearColor];
	DesWebView.scalesPageToFit = YES;
	DesWebView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	DesWebView.delegate = self;
	[self.view addSubview: DesWebView];
	//color='red'
	//@"Merlot dominates the blend of wines made in the Right Bank of Bordeaux,through statically,they remain the most widely-planted red grape in Bourdeaux";
	//UITextView* aTextView = [[UITextView alloc]initWithFrame:CGRectMake(10.0, START_Y+150.0, 300.0, 130.0) ];
//	[aTextView setBackgroundColor:[UIColor clearColor]];
//	aTextView.font = [UIFont systemFontOfSize:15.0];
//	[self setTxtDescription:aTextView];
//	[aTextView setEditable:NO];
//	[self.view addSubview:aTextView];
//	
//	WineInfo *wineInfoData;
    WineGroupItem *wineInfoData;
	if([self.arrWineInfo count]>0)
	{
		wineInfoData=(self.arrWineInfo)[0];
		NSString *strText=wineInfoData.wineTypeDesc; //bgcolor=\"#00afc0\"
		[DesWebView loadHTMLString:[NSString stringWithFormat:@"<html><body > <font face=\"EuphemiaUCAS\" size=\"6\" color='#ffffff'> %@ </font></body></html>",strText] baseURL:nil];

		//NSLog(@"DESC %@ ",strText);
	}
	
}
-(void)createLowerPanel
{
	UIImageView* imgLowerPanel = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+280.0, 300.0, 50.0)];
	[imgLowerPanel setImage:[UIImage imageNamed:@"addBg-Area.png"]];
	
	imgLowerPanel.userInteractionEnabled=YES;
	
	//UIImageView* imgAdd = [[UIImageView alloc]initWithFrame:CGRectMake(20.0, 22.0, 5.0, 9.0)];
//	[imgAdd setImage:[UIImage imageNamed:@"icon_leftArrow.png"]];
//	[imgLowerPanel addSubview:imgAdd];
//	[imgAdd release];
	
	WineInfo *wineInfoData;
	if([self.arrWineInfo count]>0)
	{
		wineInfoData=(self.arrWineInfo)[0];
		
	}
	UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(30.0, 0.0, 130.0, 50.0)];
	[btn setBackgroundColor:[UIColor clearColor]];
	btn.showsTouchWhenHighlighted=YES;
	btn.titleLabel.lineBreakMode=UILineBreakModeWordWrap;
	NSString *strTitle=[NSString stringWithFormat:@"View all %@",wineInfoData.wineTitle];////////////nedd to change
	
	[btn setTitle:strTitle forState:UIControlStateNormal];
	[btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
	[btn setTag:2];
	[btn setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
	btn.titleLabel.font = [UIFont systemFontOfSize:13.0];
	[imgLowerPanel addSubview:btn];
	
	
	//imgAdd = [[UIImageView alloc]initWithFrame:CGRectMake(265.0, 22.0, 5.0, 9.0)];
//	[imgAdd setImage:[UIImage imageNamed:@"icon_rightArrow.png"]];
//	[imgLowerPanel addSubview:imgAdd];
//	[imgAdd release];
	
	
	btn = [[UIButton alloc] initWithFrame:CGRectMake(170.0, 0.0, 100.0, 50.0)];
	[btn setBackgroundColor:[UIColor clearColor]];
	btn.showsTouchWhenHighlighted=YES;
	[btn setTitle:@"View full list" forState:UIControlStateNormal];
	[btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
	[btn setTag:3];
	[btn setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
	btn.titleLabel.font = [UIFont systemFontOfSize:13.0];
	[imgLowerPanel addSubview:btn];
	
	
	[self.view addSubview:imgLowerPanel];
	
}

-(void)clickBtn:(id)sender
{   
	//NSInteger aTag=(UIButton*)sender.tag;
	if ([sender tag]==1) 
	{
		
//	    WineInfo *wineInfoData=[self.arrWineInfo objectAtIndex:0];
	    WineGroupItem *wineInfoData=(self.arrWineInfo)[0];
	
	    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
		paramDict[@"FirstWineID"] = wineInfoData.wineID;
	
//		[self startIndicator];
//		[appDelegate.dataModel startRequestWithParameters:REQUEST_WINE_BUYINFO WithParams:paramDict];
//		[self stopIndicator];
//		if ([appDelegate.dataModel.arrWineBuyInfo count]>0)
//		{
			FairPriceBestBuyViewController* wineResultView= [[FairPriceBestBuyViewController alloc]init];
			wineResultView.requestFromPage=self.requestFromPage;       
			[self.navigationController pushViewController:wineResultView	animated:YES];
//		}
	}
	else if([sender tag]==2)
	{
		WineInfo *wineInfoData=(self.arrWineInfo)[0];
	    NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
		paramDict[@"wineGroupID"] = wineInfoData.wineGroupID;
		
		[self startIndicator];

		[appDelegate.dataModel startRequestWithParameters:REQUEST_SELECTED_GROUPWINE WithParams:paramDict];
		[self stopIndicator];
		if([appDelegate.dataModel.arrWine count]>0)
		{
			WineListResultViewController* wineResultView= [[WineListResultViewController alloc]init];
			wineResultView.requestFromPage=WINE_INFO;
			[self.navigationController pushViewController:wineResultView	animated:YES];
		}
	}
	else if([sender tag]==3)
	{
		[self startIndicator];
		[appDelegate.dataModel startRequestWithParameters:REQUEST_FULLWINE_LIST WithParams:nil];
		[self stopIndicator];
		if ([appDelegate.dataModel.arrWine count]>0) 
		{
			WineListResultViewController* wineResultView= [[WineListResultViewController alloc]init];
			wineResultView.requestFromPage=WINE_INFO;
			[self.navigationController pushViewController:wineResultView	animated:YES];
		}
	}
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

#pragma mark-
#pragma mark UIWebView delegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	// starting the load, show the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	// finished loading, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	// load error, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	// report the error inside the webview
	//NSString* errorString = [NSString stringWithFormat:
	//							 @"<html><center><font size=+5 color='red'>An error occurred:<br>%@</font></center></html>",
	//							 error.localizedDescription];
	//	[myWebView loadHTMLString:errorString baseURL:nil];
}
#pragma mark-

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview
    // Release anything that's not essential, such as cached data
}




@end
