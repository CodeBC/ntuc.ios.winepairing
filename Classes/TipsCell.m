//
//  WineForFoodCell.m
//  FairPrice
//
//  Created by niladri.schatterjee on 10/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "TipsCell.h"
#import "Constants.h"

@implementation TipsCell
@synthesize lblTips,ivBackImage;//ivIcon;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
		lblTips = [[UILabel alloc] init];
		lblTips.textAlignment = UITextAlignmentLeft;
		lblTips.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:18];
		lblTips.textColor = [UIColor whiteColor];
		lblTips.numberOfLines = 0 ;
		lblTips.backgroundColor = [UIColor clearColor];
		
		ivBackImage = [[UIImageView alloc] init];
		//ivIcon = [[UIImageView alloc] init];
		[self.contentView addSubview:ivBackImage];
		//[self.contentView addSubview:ivIcon];
		[self.contentView addSubview:lblTips];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        
    }
    return self;
}

- (void)layoutSubviews 
{
	[super layoutSubviews];
    //CGRect contentRect = self.contentView.bounds;
	
	//CGFloat boundsX = contentRect.origin.x;
	//CGFloat contentWidth=contentRect.size.width;
	//CGFloat contentHeight=contentRect.size.height;
	
	
	CGRect frame;
	
	//frame = CGRectMake(60.0, (contentHeight-25)/2, 18.0, 27.0);
	//ivIcon.frame = frame;
	
	frame = CGRectMake( 0.0, 0.0, 320.0, 40.0);//40
	ivBackImage.frame = frame;
	
	frame = CGRectMake(20.0,5.0, 280.0 , 26.0);
	lblTips.frame = frame;
	
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated 
{
    
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}




@end
