//
//  TipsAndVidViewController.h
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface TipsAndVidViewController : BaseViewController <UITableViewDelegate,UITableViewDataSource>{
    BOOL videosUp;
}

@property (nonatomic, strong) UITableView *tipsNVidtbl;

@property (nonatomic, strong) UIButton *but_tips;
@property (nonatomic, strong) UIButton *but_videos;
@property (nonatomic, strong) NSMutableArray *arrTips;
@property (nonatomic, strong) NSMutableArray *arrVideos;


-(void)createIntroText;
-(void)createTable;
-(void)createButtons;

@end
