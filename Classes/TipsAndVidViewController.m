//
//  TipsAndVidViewController.m
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FairPriceAppDelegate.h"
#import "DataModel.h"
#import "TipsAndVidViewController.h"
#import "Constants.h"
#import "TipsArticles.h"
#import "TipsVideos.h"
#import "TipsCell.h"
#import "TAVVideoViewController.h"
#import "TAVArticleViewController.h"

@interface TipsAndVidViewController ()

@end

@implementation TipsAndVidViewController

@synthesize tipsNVidtbl;
@synthesize but_tips;
@synthesize but_videos;
@synthesize arrTips,arrVideos;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

-(void)viewDidLoad
{
    self.title = @"TIPS & VIDEOS";
    [super viewDidLoad];
    
    videosUp = YES;
    
    // Do any additional setup after loading the view from its nib.
    [self createIntroText];
    [self createTable];
    [self createButtons];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateTable) name:@"RELOAD" object:nil];
	
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    if ([[self.navigationController viewControllers] count]==1)
	{
		[self hideBackButton];
	}
    [self setArrTips:appDelegate.dataModel.arrTipsArticles];
    [self setArrVideos:appDelegate.dataModel.arrTipsVideos];
}

-(void)updateTable
{
	NSLog(@"Table Notified");
    
	[tipsNVidtbl reloadData];
}

-(void)createNavigationMenu{
    [self setTitleText:self.title];
    [super createNavigationMenu];
    
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, START_Y+30.0, 300.0, 34.5)];
//	[titleLabel setText:@"TIPS & VIDEOS"];
//	titleLabel.textAlignment = UITextAlignmentCenter;
//	titleLabel.backgroundColor=[UIColor clearColor];
//	titleLabel.textColor = [UIColor redColor];
//    titleLabel.font = [UIFont fontWithName:@"Tommaso" size:26];
//	[self.view addSubview:titleLabel];
//	[titleLabel release];
//    
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.0, 300.0, 1.0)];
//    [imgView setImage:[UIImage imageNamed:@"nav_bar_line.png"]];
//    imgView.contentMode = UIViewContentModeScaleAspectFit;
//    [self.view bringSubviewToFront:imgView];
//    [self.view addSubview:imgView];
//    [imgView release];
}

-(void)createIntroText{
    NSString *lblContent = @"There is a special pleasure that comes from knowing your wines. And know them you will, when you go through the comprehensive list of tips and videos we’ve put together specially for you.";
    
	UITextView* txtField = [[UITextView alloc] initWithFrame:CGRectMake(20.0, START_Y+65.0 , 290.0, 70.0)];
	[txtField setText:lblContent];
	txtField.contentInset = UIEdgeInsetsMake(-4,-8,0,0);
    txtField.scrollEnabled = NO;
	txtField.editable=NO;
	txtField.textAlignment = UITextAlignmentLeft;
	txtField.textColor = [UIColor whiteColor];
	txtField.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
    txtField.backgroundColor = [UIColor clearColor];
	[self.view addSubview:txtField];
}

-(void)createTable{
    UITableView *aTable = [[UITableView alloc]initWithFrame:CGRectMake(0.0, START_Y+195.0, 320.0, 200.0)];
	
	[aTable setBackgroundColor:[UIColor clearColor]];
	[aTable setAllowsSelection:YES];
	[aTable setDelegate:self];
	[aTable setDataSource:self];
    aTable.showsVerticalScrollIndicator = NO;
    
	[self setTipsNVidtbl:aTable];
	[self.view addSubview:tipsNVidtbl];
    
}

-(void)createButtons{
    but_tips = [[UIButton alloc] initWithFrame:CGRectMake(0.0, START_Y+150.0, 160.0, 45.0)];
    
    [but_tips setBackgroundImage:[UIImage imageNamed:@"but_tips_down.png"] forState:UIControlStateNormal];
    
	[but_tips addTarget:self action:@selector(onClickTips:) forControlEvents:UIControlEventTouchUpInside];
	
	[self.view addSubview:but_tips];
    
    
    but_videos = [[UIButton alloc] initWithFrame:CGRectMake(160.0, START_Y+150.0, 160.0, 45.0)];
    
    [but_videos setBackgroundImage:[UIImage imageNamed:@"but_videos_up.png"] forState:UIControlStateNormal];
    
	[but_videos addTarget:self action:@selector(onClickVideos:) forControlEvents:UIControlEventTouchUpInside];
	
	[self.view addSubview:but_videos];
}

-(IBAction)onClickTipsNVideos:(id)sender{
    NSLog(@"onClickTipsNVideos");
    if(videosUp){
        [but_videos setBackgroundImage:[UIImage imageNamed:@"but_videos_down.png"] forState:UIControlStateNormal];
        [but_tips setBackgroundImage:[UIImage imageNamed:@"but_tips_up.png"] forState:UIControlStateNormal];
        videosUp = NO;
    }else{
        [but_videos setBackgroundImage:[UIImage imageNamed:@"but_videos_up.png"] forState:UIControlStateNormal];
        [but_tips setBackgroundImage:[UIImage imageNamed:@"but_tips_down.png"] forState:UIControlStateNormal];
        videosUp = YES;
    }
    [self.tipsNVidtbl reloadData];
}

-(IBAction)onClickTips:(id)sender{
    NSLog(@"onClickTipsNVideos");
    if(!videosUp){
        [but_tips setBackgroundImage:[UIImage imageNamed:@"but_tips_down.png"] forState:UIControlStateNormal];
        [but_videos setBackgroundImage:[UIImage imageNamed:@"but_videos_up.png"] forState:
         UIControlStateNormal];
        videosUp = YES;
        [self.tipsNVidtbl reloadData];
    }
}

-(IBAction)onClickVideos:(id)sender{
    NSLog(@"onClickVideos");
    if(videosUp) {
        [but_videos setBackgroundImage:[UIImage imageNamed:@"but_videos_down.png"] forState:
         UIControlStateNormal];
        [but_tips setBackgroundImage:[UIImage imageNamed:@"but_tips_up.png"] forState:UIControlStateNormal];
        videosUp = NO;
        [self.tipsNVidtbl reloadData];
    }
}



#pragma mark Table view methods
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	return 40.0;
}
// Customize the number of rows in the table view.
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(videosUp)
        return [arrTips count];
        return [arrVideos count];
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
//    static NSString *CellIdentifier = @"Cell";
//	UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier]; 
//	if(nil == cell){
//		cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier]autorelease];
//	}
    
    static NSString *CellIdentifier = @"Cell";
	
	TipsCell *cell = (TipsCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
	if(cell==nil) {
        //		cell = [[[FoodCell alloc] initWithFrame:CGRectZero reuseIdentifier:CellIdentifier] autorelease];
        cell = [[TipsCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
	}
    
    cell.lblTips.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:18];
    cell.lblTips.textColor = [UIColor whiteColor];
    if(videosUp){
        cell.lblTips.text = [[(TipsArticles *)arrTips[indexPath.row] contentTitle]uppercaseString];
    }else{
        cell.lblTips.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:15];
        cell.lblTips.text = [[(TipsVideos *)arrVideos[indexPath.row] contentTitle]uppercaseString];
    }

    [cell.ivBackImage setImage:[UIImage imageNamed:@"cell_bg.png"]];
	tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
	cell.selectionStyle = UITableViewCellSelectionStyleNone;
	cell.accessoryType = UITableViewCellAccessoryNone;
    
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(videosUp){
        //Handle article
        
        TipsArticles *tipsArticle = arrTips[indexPath.row];
        
        NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
        paramDict[@"TipsContentID"] = [tipsArticle contentID];
        
        [self startIndicator];
        [appDelegate.dataModel startRequestWithParameters:REQUEST_GET_INDIV_TIPS WithParams:paramDict];
        [self stopIndicator];
        
        if([appDelegate.dataModel.arrTipsArticleItem count]>0) 
        {
            TAVArticleViewController *vcArticle = [[TAVArticleViewController alloc]init];
            vcArticle.title = [tipsArticle contentTitle];
            [self.navigationController pushViewController:vcArticle animated:YES];
        }

    }else{
        //Handle video
        TipsVideos *tipsVideos = arrVideos[indexPath.row];
        
        NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
        paramDict[@"TipsContentID"] = [tipsVideos contentID];
        
        [self startIndicator];
        [appDelegate.dataModel startRequestWithParameters:REQUEST_GET_INDIV_TIPS_VIDEOS WithParams:paramDict];
        [self stopIndicator];
        
        if([appDelegate.dataModel.arrTipsVideoItem count]>0) 
        {
            TAVVideoViewController *vcTAVVideo = [[TAVVideoViewController alloc]init];
            NSString *tipsTitle = [tipsVideos contentTitle];
            NSString *shortTipsTitle = [tipsTitle componentsSeparatedByString:@":"][1];
            vcTAVVideo.title = shortTipsTitle;
            [self.navigationController pushViewController:vcTAVVideo animated:YES];
        }
    }
}


- (void)viewDidUnload{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
