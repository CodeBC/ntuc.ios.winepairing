//
//  WineResultViewController.m
//  FairPrice
//
//  Created by niladri.schatterjee on 10/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "WineResultViewController.h"
#import "FairPriceBestBuyViewController.h"
#import "DishInfo.h"
#import "WineInfo.h"
#import "DataModel.h"
#import "FairPriceAppDelegate.h"
#import "WineListResultViewController.h"
#import "UIImage+Retina4.h"

@implementation WineResultViewController
@synthesize lblFoodSelection ,dishData,DesWebView;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	if([appDelegate.dataModel.arrDishInfo count]==0)
		return;
	
    self.dishData=(appDelegate.dataModel.arrDishInfo)[0];
	self.DesWebView = [[UIWebView alloc] init];
    
    [self createWineSugession];
//    [self createLowerPanel];
    [self createTextView];
//    [self setTitle:@"Result"];
	
	for (id subview in DesWebView.subviews)
		if ([[subview class] isSubclassOfClass: [UIScrollView class]])
			((UIScrollView *)subview).bounces = NO;
	
	
}

-(void)createNavigationMenu{    
    [self setTitleText:self.title];
    [super createNavigationMenu];
    
    
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, START_Y+30.0, 300.0, 34.5)];
//	[titleLabel setText: self.title];
//	titleLabel.textAlignment = UITextAlignmentCenter;
//	titleLabel.backgroundColor=[UIColor clearColor];
//	titleLabel.textColor = [UIColor redColor];
//    titleLabel.opaque = YES;
//    titleLabel.clearsContextBeforeDrawing = YES;
//    titleLabel.contentMode = UIViewContentModeTop;
//    titleLabel.font = [UIFont fontWithName:@"Tommaso" size:26.0];
//	[self.view addSubview:titleLabel];
//	[titleLabel release];
//    
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.0, 320.0, 1.0)];
//    [imgView setImage:[UIImage imageNamed:@"nav_bar_line.png"]];
//    imgView.contentMode = UIViewContentModeScaleAspectFit;
//    [self.view bringSubviewToFront:imgView];
//    [self.view addSubview:imgView];
//    [imgView release];
    
    //    //    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.25, 300.0, 1.0)];
    //    imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.0, 300.0, 1.0)];
    //    [imgView setImage:[UIImage imageNamed:@"nav_bar_line.png"]];
    //    imgView.contentMode = UIViewContentModeScaleAspectFit;
    //    [self.view bringSubviewToFront:imgView];
    //    [self.view addSubview:imgView];
    //    [imgView release];
}

-(void)createWineSugession
{
	UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y+50 , 285.0, 20.0)];
//	[lbl setText:@"Your food selection:"];
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor = [UIColor whiteColor];
//	lbl.font = [UIFont italicSystemFontOfSize:18.0];
//    lbl.backgroundColor = [UIColor clearColor];
//	[self.view addSubview:lbl];
//	[lbl release];
	
//	lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y+70, 285.0, 20.0)];
//	[lbl setText:dishData.dishTitle ];//       @"  Beep Hor fun"];
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor = [UIColor whiteColor];
//	lbl.font = [UIFont boldSystemFontOfSize:18.0];
//    lbl.backgroundColor = [UIColor clearColor];
//	[self setLblFoodSelection:lbl];
//	[self.view addSubview:lbl];
//	[lbl release];
	
	lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.0, START_Y +85, 285.0, 20.0)];
	[lbl setText:@"YOUR WINE TO GO WITH THIS:"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:18];
    lbl.backgroundColor = [UIColor clearColor];
	[self.view addSubview:lbl];
    	 
//	 NSMutableString *wineList = [[NSMutableString alloc] init];
//	 int len  = [dishData.arrWineTogo count];
//	 for (int i = 0 ; i < len ; i++)
//	 {   
//		 WineInfo  *wineData = [dishData.arrWineTogo objectAtIndex:i];
//		 [wineList appendString:[NSString stringWithFormat:@"%@",wineData.wineTitle]];
//		 if (i+1 < len)
//		 {
//			 [wineList appendString:@", "];
//		 }
//	 }
	
	UITextView* txtField = [[UITextView alloc] initWithFrame:CGRectMake(20.00, START_Y+105.0 , 285.0, 50.0)];
	[txtField setText:[NSString stringWithFormat:@" %@",dishData.wineTypeName]];              //@"  Chardonnay, Muscat, Pinor Noir"];
	txtField.contentInset = UIEdgeInsetsMake(-4,-8,0,0);
	txtField.editable=NO;
	txtField.textAlignment = UITextAlignmentLeft;
	txtField.textColor = [UIColor whiteColor];
	txtField.font = [UIFont fontWithName:@"EuphemiaUCAS" size:14];
    txtField.backgroundColor = [UIColor clearColor];
	[self.view addSubview:txtField];
	
	
//	lbl = [[UILabel alloc]initWithFrame:CGRectMake(20.00, START_Y+70 , 285.0, 40.0)];
//	lbl.numberOfLines=0; 
//	 [lbl setText:wineList];              //@"  Chardonnay, Muscat, Pinor Noir"];
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor = UIColorFromHex(0x452801);
//	lbl.font = [UIFont boldSystemFontOfSize:15.0];
//	[self setLblWineList:lbl];
//	[self.view addSubview:lbl];
//	[lbl release];
//	[wineList release];
	
	UIButton* fairPriceButton = [[UIButton alloc] initWithFrame:CGRectMake(20.0 , START_Y+160.0, 144.0, 40.0)];
	fairPriceButton.tag=1;
	[fairPriceButton addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
	[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"but_fairprice_recommends.png"] forState:UIControlStateNormal];
	[self.view addSubview:fairPriceButton];
	
}

-(void)createTextView
{   
	
	NSString *strText= [NSString stringWithFormat:@"%@",self.dishData.dishDescription1];
    
	self.DesWebView.frame = CGRectMake(20.0, START_Y+200.0, 290.0, 170.0);
	[self.DesWebView setOpaque:NO];
	[self.DesWebView loadHTMLString:[NSString stringWithFormat:@"<html><body> <font face=\"EuphemiaUCAS\" size=\"2\" color='#ffffff'> %@ </font></body></html>",strText] baseURL:nil];
	self.DesWebView.backgroundColor = [UIColor clearColor];
	self.DesWebView.scalesPageToFit = NO;
	self.DesWebView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	self.DesWebView.delegate = self;
	[self.view addSubview: self.DesWebView];
	
			
}

-(void)createLowerPanel
{
	UIImageView* imgLowerPanel = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+280.0, 300.0, 50.0)];
	[imgLowerPanel setImage:[UIImage imageNamed:@"addBg-Area.png"]];
	imgLowerPanel.userInteractionEnabled=YES;	
	
	WineInfo *wineData = (dishData.arrWineTogo)[0];
	NSString *wineStr=[NSString stringWithFormat:@"View all %@",wineData.wineTitle];
	
	UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(30.0, 0.0, 130.0, 50.0)];
	btn.titleLabel.lineBreakMode=UILineBreakModeWordWrap;
	btn.showsTouchWhenHighlighted=YES;
	[btn setBackgroundColor:[UIColor clearColor]];
	//[addDish addTarget:self action:@selector(clickSBarButton:) forControlEvents:UIControlEventTouchUpInside];
	[btn setTitle:wineStr forState:UIControlStateNormal];
	[btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
	[btn setTag:2];
	[btn setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
	btn.titleLabel.font = [UIFont systemFontOfSize:13.0];
	[imgLowerPanel addSubview:btn];
	
	
	//imgAdd = [[UIImageView alloc]initWithFrame:CGRectMake(265.0, 22.0, 5.0, 9.0)];
//	[imgAdd setImage:[UIImage imageNamed:@"icon_rightArrow.png"]];
//	[imgLowerPanel addSubview:imgAdd];
//	[imgAdd release];
	
	
	btn = [[UIButton alloc] initWithFrame:CGRectMake(170.0, 0.0, 100.0, 50.0)];
	[btn setBackgroundColor:[UIColor clearColor]];
	btn.showsTouchWhenHighlighted=YES;
	//[addDish addTarget:self action:@selector(clickSBarButton:) forControlEvents:UIControlEventTouchUpInside];
	[btn setTitle:@"View full list" forState:UIControlStateNormal];
	[btn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
	[btn setTag:3];
	[btn setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
	btn.titleLabel.font = [UIFont systemFontOfSize:13.0];
	[imgLowerPanel addSubview:btn];
	
	
	[self.view addSubview:imgLowerPanel];
	
}

-(void)clickBtn:(id)sender
{  
	NSInteger aTag=[sender tag];
	
	if(aTag==1)
	{	
//        WineInfo *wineData = [dishData.arrWineTogo objectAtIndex:0];   
//        DishInfo *dishInfo = [dishData.wineTypeID];
	
		NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
		paramDict[@"FirstWineID"] = dishData.wineID;
	
		[self startIndicator];
//		[appDelegate.dataModel startRequestWithParameters:REQUEST_WINE_BUYINFO WithParams:paramDict];
	    [self stopIndicator];
//	     if([appDelegate.dataModel.arrWineBuyInfo count]>0)
//		 {
		    FairPriceBestBuyViewController* wineResultView= [[FairPriceBestBuyViewController alloc] init ];//WithNibName:@"FairPriceBestBuyViewController" bundle:[NSBundle mainBundle]];
		    wineResultView.requestFromPage=BYFOOD;
		    [self.navigationController pushViewController:wineResultView	animated:YES];
//		 }
	}
	else if(aTag==2)
	{
        
		WineInfo *wineData = (dishData.arrWineTogo)[0];
		NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
		paramDict[@"WineGroupID"] = wineData.wineGroupID;
		[self startIndicator];
		
		[appDelegate.dataModel startRequestWithParameters:REQUEST_SELECTED_GROUPWINE WithParams:paramDict];
		[self stopIndicator];
		if([appDelegate.dataModel.arrWine count]>0)
		{
			WineListResultViewController* wineResultView= [[WineListResultViewController alloc]init];
		    wineResultView.requestFromPage=DISH_INFO;
		    [self.navigationController pushViewController:wineResultView	animated:YES];
		}
		
	}
	else if(aTag==3)
	{
		[self startIndicator];
		
		[appDelegate.dataModel startRequestWithParameters:REQUEST_FULLWINE_LIST WithParams:nil];
		
		[self stopIndicator];
		if ([appDelegate.dataModel.arrWine count]>0) 
		{
			WineListResultViewController* wineResultView= [[WineListResultViewController alloc]init];
			wineResultView.requestFromPage=DISH_INFO;
			[self.navigationController pushViewController:wineResultView	animated:YES];
		}
		
		
		
		
		
		
	}
	
}

/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

#pragma mark-
#pragma mark UIWebView delegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	// starting the load, show the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	// finished loading, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	// load error, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	// report the error inside the webview
	//NSString* errorString = [NSString stringWithFormat:
	//							 @"<html><center><font size=+5 color='red'>An error occurred:<br>%@</font></center></html>",
	//							 error.localizedDescription];
	//	[myWebView loadHTMLString:errorString baseURL:nil];
}
#pragma mark-


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload 
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}




@end
