//
//  HomeViewController.h
//  FairPrice
//
//  Created by Satish Kumar on 10/7/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface HomeViewController : BaseViewController {
	
//	UIButton* menuButton1;
//	UIButton* menuButton2;
//	UIButton* menuButton3;
//	UIImageView* imageLogo1;
//	UIImageView* imageLogo2;
//	UIImageView* imageLogo3;
    NSMutableData *receivedData;
    
    NSString *appLink;
    
    NSURLConnection *clientCheckConnection;


}
@property(nonatomic,strong) UIButton* menuButton1;
@property(nonatomic,strong) UIButton* menuButton2;
@property(nonatomic,strong) UIButton* menuButton3;
@property(nonatomic,strong) UIButton* menuButton4;
@property(nonatomic,strong) UIButton* menuButton5;
@property(nonatomic,strong) UIButton* menuButton6;
@property(nonatomic,strong) UIButton* menuButton7;

@property(nonatomic,strong) UIImageView* imageLogo1;
@property(nonatomic,strong) UIImageView* imageLogo2;
@property(nonatomic,strong) UIImageView* imageLogo3;
@property(nonatomic,strong) UIImageView* imageLogo4;
@property(nonatomic,strong) UIImageView* imageLogo5;
@property(nonatomic,strong) UIImageView* imageLogo6;
@property(nonatomic,strong) UIImageView* imageLogo7;

@property(nonatomic, strong) NSString *appLink;

-(void)createNavigationMenu;
-(void)createBackground;

-(void)createIntro;
-(void)createMenu;
-(void)createImage;
-(void)clickBtn:(id)sender;
@end
