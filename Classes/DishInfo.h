//
//  DishInfo.h
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface DishInfo : NSManagedObject

@property (nonatomic, retain) NSString * dishID;
@property (nonatomic, retain) NSString * dishTitle;
@property (nonatomic, retain) NSString * dishDescription1;
@property (nonatomic, retain) NSString * dishDescription2;
@property (nonatomic, retain) NSString * wineTypeName;
@property (nonatomic, retain) NSString * wineID;
@property (nonatomic, retain) NSString * wineTypeID;
@property (nonatomic, retain) NSString * wineName;
@property (nonatomic, retain) NSString * winePrice;
@property (nonatomic, retain) NSString * wineRating;
@property (nonatomic, retain) NSData * wineImage;
@property (nonatomic, retain) NSString * wineGroupID;

@property(nonatomic,strong) NSMutableArray *arrWineTogo;

@end
