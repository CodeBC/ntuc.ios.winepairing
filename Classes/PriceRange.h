//
//  PriceRange.h
//  FairPrice
//
//  Created by Tridip Sarkar on 08/11/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface PriceRange : NSObject 

@property(nonatomic,strong) NSString *textValue;
@property(nonatomic,strong) NSString *valueValue;

-(id)init;
@end
