//
//  WineGroupType.h
//  FairPrice
//
//  Created by ddseah on 12/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WineGroupType : NSObject

@property (nonatomic,strong) NSString *wineConfigKey;
@property (nonatomic,strong) NSString *wineConfigValue;
-(id)init;
@end
