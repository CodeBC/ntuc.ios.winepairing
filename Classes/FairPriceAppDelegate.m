//
//  FairPriceAppDelegate.m
//  FairPrice
//
//  Created by Satish Kumar on 10/7/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import "FairPriceAppDelegate.h"
#import "HomeViewController.h"
#import "WineForFoodViewController.h"
#import "WineListSearchViewController.h"
#import "WineListAllResultViewController.h"
#import "FoodForWineViewController.h"
#import "TipViewController.h"
#import "AboutViewController.h"
#import "JustWineClubViewController.h"
#import "TipsAndVidViewController.h"
#import "DataModel.h"
#import "OrderWineDirectViewController.h"
#import "Facebook.h"
#import "JSON.h"
#import "JSONKit.h"
#import "GroupDishItem.h";
@implementation FairPriceAppDelegate

#define INMOBI_ADTRACKER_APP_ID @"016ddac4-c1a9-4ef3-a5ef-7e3501669806"

@synthesize window;
@synthesize dataModel,homeController,wineForFoodController,foodForWineController,wineListSearchController,tipController,justWineClubController,tipsAndVidController,orderWineClubController;//aboutController;
/////////
@synthesize homeViewNavController,foodNavController,wineNavController,wineListNavController,TipNavController,justWineClubNavController,tipsAndVidNavController,orderWineClubNavController;//aboutNavController;
@synthesize facebook;
@synthesize managedObjectContext = __managedObjectContext;
@synthesize managedObjectModel = __managedObjectModel;
@synthesize persistentStoreCoordinator = __persistentStoreCoordinator;

#pragma mark -
#pragma mark Application lifecycle

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
    //[TestFlight takeOff:@"af621d8e-9a29-423c-93db-ce475cd5edea"];
    //[MagicalRecord setupCoreDataStackWithAutoMigratingSqliteStoreNamed:@"Finder.sqlite"];
    //[MagicalRecord setupCoreDataStackWithStoreNamed:@"Finder.sqlite"];
//    [self setUpDB];
    [self managedObjectContext];
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Finder.sqlite"];
    NSLog(@"Finish setting up Magical Record");
    
    // Override point for customization after application launch.
	window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
	
    self.homeController = [[HomeViewController alloc] init];
    self.homeViewNavController = [[UINavigationController alloc] initWithRootViewController:homeController];
    
    self.wineForFoodController=[[WineForFoodViewController alloc]init];
    self.foodNavController=[[UINavigationController alloc] initWithRootViewController:wineForFoodController];
    
    self.foodForWineController=[[FoodForWineViewController alloc] init];
    self.wineNavController=[[UINavigationController alloc] initWithRootViewController:foodForWineController];
    
    self.wineListSearchController=[[WineListAllResultViewController alloc]init];
    wineListSearchController.requestFromPage=WINELIST;
    self.wineListNavController=[[UINavigationController alloc] initWithRootViewController:wineListSearchController];
    
    self.tipsAndVidController =[[TipsAndVidViewController alloc]init];
    self.tipsAndVidNavController = [[UINavigationController alloc] initWithRootViewController:tipsAndVidController];
    
    self.justWineClubController = [[JustWineClubViewController alloc]init];
    self.justWineClubNavController = [[UINavigationController alloc] initWithRootViewController:justWineClubController];
    
    self.orderWineClubController = [[OrderWineDirectViewController alloc]init];
    self.orderWineClubNavController = [[UINavigationController alloc] initWithRootViewController:orderWineClubController];
    
    ///Start tracker///
//    [IMAdTracker setLogLevel:IMAdTrackerLogLevelDebug];
//    [[IMAdTracker sharedInstance] startSession:INMOBI_ADTRACKER_APP_ID];
	
	//Start the application with the old HomeViewController 
	[self showViewController:101];
    
    [window makeKeyAndVisible];
    
	DataModel* aDataModel = [[DataModel alloc] init];
    [self setDataModel:aDataModel];
    
    NSLog(@"Init facebook instance");
    facebook = [[Facebook alloc]initWithAppId:kAppID andDelegate:self];
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"] 
        && [defaults objectForKey:@"FBExpirationDateKey"]) {
        facebook.accessToken = [defaults objectForKey:@"FBAccessTokenKey"];
        facebook.expirationDate = [defaults objectForKey:@"FBExpirationDateKey"];
    }
    
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeSound ];

    return YES;
}

- (void) setUpDB{
    NSArray *paths = [[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask];
    NSURL *documentPath = [paths lastObject];
    
    NSURL *storeURL = [documentPath URLByAppendingPathComponent:@"Finder.sqlite"];
    NSLog(@"store url %@",storeURL);
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[storeURL path]]) {
        NSLog(@"db not file exit!");
        NSURL *preloadURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Finder" ofType:@"sqlite"]];
        NSError* err = nil;
        
        if (![[NSFileManager defaultManager] copyItemAtURL:preloadURL toURL:storeURL error:&err]) {
            NSLog(@"Error: Unable to copy preloaded database.");
        }
    }
    else{
        NSLog(@"db file exit!");
    }
    [MagicalRecord setupCoreDataStackWithStoreNamed:@"Finder.sqlite"];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil)
    {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error])
        {
            /*
             Replace this implementation with code to handle the error appropriately.
             
             abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
             */
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        }
    }
}

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (__managedObjectContext != nil) {
        return __managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        __managedObjectContext = [[NSManagedObjectContext alloc] init];
        [__managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return __managedObjectContext;
}

/**
 Returns the managed object model for the application.
 If the model doesn't already exist, it is created from the application's model.
 */
- (NSManagedObjectModel *)managedObjectModel
{
    if (__managedObjectModel != nil)
    {
        return __managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"FairPrice" withExtension:@"momd"];
    __managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    //__managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
    return __managedObjectModel;
}

/**
 Returns the persistent store coordinator for the application.
 If the coordinator doesn't already exist, it is created and the application's store added to it.
 */
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator {
    
    if (__persistentStoreCoordinator != nil) {
        return __persistentStoreCoordinator;
    }
    
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Finder.sqlite"];
    
    NSLog(@"%@", storeURL);
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:[storeURL path]]) {
        NSURL *preloadURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"Finder" ofType:@"sqlite"]];
        
        NSError* err = nil;
        
        if (![[NSFileManager defaultManager] copyItemAtURL:preloadURL toURL:storeURL error:&err]) {
            NSLog(@"Oops, could copy preloaded data");
        } else {
            NSLog(@"Copied");
        }
    }
    
    NSError *error = nil;
    __persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![__persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         [NSDictionary dictionaryWithObjectsAndKeys:[NSNumber numberWithBool:YES], NSMigratePersistentStoresAutomaticallyOption, [NSNumber numberWithBool:YES], NSInferMappingModelAutomaticallyOption, nil];
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return __persistentStoreCoordinator;
}

- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

-(void)showViewController:(NSInteger)index
{
	NSLog(@"index = %d",index);
    
	switch (index) 
	{
		case HOME:
        {  
            if( [[self.homeViewNavController viewControllers] count]>0)
                [self.homeViewNavController popToRootViewControllerAnimated:YES];
            [window addSubview:homeViewNavController.view];
        }
			break;
		case BYFOOD:{
			if( [[self.foodNavController viewControllers] count]>0)
				[self.foodNavController popToRootViewControllerAnimated:YES];
            [window addSubview:self.foodNavController.view];
			break;
        }
		case BYWINE:{
			if( [[self.wineNavController viewControllers] count]>0)
				[self.wineNavController popToRootViewControllerAnimated:YES];
			[window addSubview:self.wineNavController.view];
			break;
        }
		case WINELIST:{
			if( [[self.wineListNavController viewControllers] count]>0)
				[self.wineListNavController popToRootViewControllerAnimated:YES];
            
			[window addSubview:self.wineListNavController.view];
			break;
            
        }
		case TIP:{
            //			[window addSubview:self.TipNavController.view];
            if( [[self.tipsAndVidNavController viewControllers] count]>0)
				[self.tipsAndVidNavController popToRootViewControllerAnimated:YES];
            [window addSubview:self.tipsAndVidNavController.view];
            
			break;
        }
        case WINECLUB:{
			if( [[self.justWineClubNavController viewControllers] count]>0)
				[self.justWineClubNavController popToRootViewControllerAnimated:YES];
			[window addSubview:self.justWineClubNavController.view];
			break;
        }
        case ORDERWINE:
        {
            if( [[self.orderWineClubNavController viewControllers] count]>0)
				[self.orderWineClubNavController popToRootViewControllerAnimated:YES];
			[window addSubview:self.orderWineClubNavController.view];
			break;
        }
	}
	
	
}

//transition=UIViewAnimationTransitionFlipFromLeft ,UIViewAnimationTransitionFlipFromRight,UIViewAnimationTransitionCurlUp,UIViewAnimationTransitionCurlDown.
//FlipView=Any View object
-(void)animateView:(id)FlipView WithTransition:(UIViewAnimationTransition)transition
{  
	// setup the animation group
	[UIView beginAnimations:nil context:nil];
	[UIView setAnimationDuration:0.5];
	[UIView setAnimationDelegate:self];
	[UIView setAnimationTransition:transition forView:FlipView cache:NO];
	[UIView commitAnimations];
}

//// This method will be used from iOS 4.2 and prior.
//-(BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url{
//    return [[_viewController facebook] handleOpenURL:url];
//}
//
//
//// This method will be used for iOS versions greater than 4.2.
//- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
//    return [[_viewController facebook] handleOpenURL:url];
//}



- (void)applicationWillResignActive:(UIApplication *)application {
    /*
     Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
     Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
     */
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    /*
     Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
     If your application supports background execution, called instead of applicationWillTerminate: when the user quits.
     */
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    /*
     Called as part of  transition from the background to the inactive state: here you can undo many of the changes made on entering the background.
     */
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    /*
     Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
     */
}


- (void)applicationWillTerminate:(UIApplication *)application {
    /*
     Called when the application is about to terminate.
     See also applicationDidEnterBackground:.
     */
    [MagicalRecord cleanUp];
}


#pragma mark -
#pragma mark Memory management

- (void)applicationDidReceiveMemoryWarning:(UIApplication *)application {
    /*
     Free up as much memory as possible by purging cached data objects that can be recreated (or reloaded from disk) later.
     */
}


#pragma mark - Facebook Delegate Methods
-(void)fbDidLogin{
    
    //    // Create the parameters dictionary that will keep the data that will be posted.
    //    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
    //                                   @"Test App", @"name",
    //                                   @"", @"link",
    //                                   @"", @"caption",
    //                                   @"", @"description",
    //                                   @"This app from FairPrice is cool. A world first, it pairs wine with Asian food and Western food. http://itunes.apple.com/sg/app/wine-pairing-app/id439254783?mt=8", @"message",              
    //                                   nil];
    //    
    //    // Publish.
    //    // This is the most important method that you call. It does the actual job, the message posting.
    //    [facebook requestWithGraphPath:@"me/feed" andParams:params andHttpMethod:@"POST" andDelegate:self];
    //    
    //    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:@"Screenshot posted successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    //    [alert show];
    //    [alert release];
    NSLog(@"fbDidLogin accessToken=%@",[facebook accessToken]);
    NSLog(@"fbDidLogin expirationDate=%@",[facebook expirationDate]);
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:[facebook accessToken] forKey:@"FBAccessTokenKey"];
    [defaults setObject:[facebook expirationDate] forKey:@"FBExpirationDateKey"];
    [defaults synchronize];
    //[self createFacebookPost];
    [self showFacebookDialog:@"create!"];
}

- (void) createFacebookPost {
    NSLog(@"FairPriceAppDelegate createFacebookPost");
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"WinePairing", @"name",
                                   @"", @"link",
                                   @"", @"caption",
                                   @"", @"description",
                                   @"Have a local dish or wine in mind for pairing? Check out the recommendations provided by FairPrice Wine Pairing app! http://itunes.apple.com/sg/app/wine-pairing-app/id439254783?mt=8", @"message",
                                   nil];
    
    // Publish.
    // This is the most important method that you call. It does the actual job, the message posting.
    [facebook requestWithGraphPath:@"me/feed" andParams:params andHttpMethod:@"POST" andDelegate:self];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:@"Screenshot posted successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];
}

- (void) showFacebookDialog:(NSString *)strProduct {
    NSLog(@"FairPriceAppDelegate showFacebookPost");
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithObjectsAndKeys:
                                   @"WinePairing", @"name",
                                   @"http://itunes.apple.com/sg/app/wine-pairing-app/id439254783?mt=8", @"link",
                                   @"FairPrice", @"caption",
                                   @"Have a local dish or wine in mind for pairing? Check out the recommendations provided by FairPrice Wine Pairing app!", @"description",
                                   nil];
    
    /*// Publish.
    // This is the most important method that you call. It does the actual job, the message posting.
    [facebook requestWithGraphPath:@"me/feed" andParams:params andHttpMethod:@"POST" andDelegate:self];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Notification" message:@"Screenshot posted successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [alert show];*/
    [facebook dialog:@"feed" andParams:params andDelegate:self];
}

- (void)request:(FBRequest *)request didFailWithError:(NSError *)error { 
    NSLog(@"%@", [error localizedDescription]); 
    NSLog(@"Err details: %@", [error description]); 
};

// Pre iOS 4.2 support
- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    NSLog(@"application handleOpenURL");
    return [facebook handleOpenURL:url]; 
}

// For iOS 4.2+ support
- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    NSLog(@"application openURL");
    return [facebook handleOpenURL:url]; 
}

- (void)request:(FBRequest *)request didLoad:(id)result {
    NSLog(@"didLoad");
}
- (void)fbDidNotLogin:(BOOL)cancelled {
    NSLog(@"fbDidNotLogin");
}

/**
 * Called after the access token was extended. If your application has any
 * references to the previous access token (for example, if your application
 * stores the previous access token in persistent storage), your application
 * should overwrite the old access token with the new one in this method.
 * See extendAccessToken for more details.
 */
- (void)fbDidExtendToken:(NSString*)accessToken
               expiresAt:(NSDate*)expiresAt {
    NSLog(@"fbDidExtendToken");
    [self showFacebookDialog:@"create!"];
}

/**
 * Called when the user logged out.
 */
- (void)fbDidLogout{}

/**
 * Called when the current session has expired. This might happen when:
 *  - the access token expired
 *  - the app has been disabled
 *  - the user revoked the app's permissions
 *  - the user changed his or her password
 */
- (void)fbSessionInvalidated {
    NSLog(@"fbSessionInvalidated");
    facebook.accessToken = nil;
    facebook.expirationDate = nil;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:@"FBAccessTokenKey"]) {
        [defaults removeObjectForKey:@"FBAccessTokenKey"];
        [defaults removeObjectForKey:@"FBExpirationDateKey"];
        [defaults synchronize];
    }
    NSArray *permissions = [[NSArray alloc] initWithObjects:@"publish_stream", nil];
    [facebook authorize:permissions];
    
}

- (void)request:(FBRequest *)request didReceiveResponse:(NSURLResponse *)response {
    NSLog(@"received response %@", response);
}

- (void) showOrderForm:(UIViewController *)viewController{
    [viewController.navigationController pushViewController:self.orderWineClubController animated:YES];
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
	NSLog(@"My token is: %@", deviceToken);
    NSString *tokenString = [deviceToken description];
    tokenString = [tokenString stringByTrimmingCharactersInSet:[NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    tokenString = [tokenString stringByReplacingOccurrencesOfString:@" " withString:@""];
    [[NSUserDefaults standardUserDefaults] setObject:tokenString forKey:@"token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    
    [self syncGenerateKey:tokenString];
    
}

- (void)syncGenerateKey:(NSString *)strKey{
    NSString * generateKey = @"modules/client/_ext_generate_key.php";
    NSLog(@"base link %@%@ and token %@",BASEURL,generateKey,strKey);
    ASIFormDataRequest *request=[[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASEURL,generateKey]]];
    [request setPostValue:@"I" forKey:@"client_platform"];
    [request setPostValue:strKey forKey:@"device_token"];
    [request setDelegate:self];
    [request startAsynchronous];
    strDevToken = strKey;
}

- (void)syncDeviceToken:(NSString *)strToken{
    NSString * generateKey = @"modules/client/_ext_update_GCM_registration_token.php";
    
    __block ASIFormDataRequest *request=[[ASIFormDataRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASEURL,generateKey]]];
    NSUserDefaults * pref = [NSUserDefaults standardUserDefaults];
    NSString * str = [pref objectForKey:@"key"];
    [request setPostValue:str forKey:@"key"];
    [request setPostValue:strToken forKey:@"gcm"];
    NSLog(@"base link %@%@ and key %@",BASEURL,generateKey,str);
    [request setCompletionBlock:^{
        // Use when fetching text data
        NSString *responseString = [request responseString];
        
        // Use when fetching binary data
        NSData *responseData = [request responseData];
        NSLog(@"syncDeviceToken string %@",responseString);
    }];
    [request startAsynchronous];
}


- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    // Use when fetching text data
    NSString *responseString = [request responseString];
    
    // Use when fetching binary data
    NSData *responseData = [request responseData];
    NSData *soapData = (NSData *)responseData;
    NSDictionary *dictionary;
    JSONDecoder* decoder = [[JSONDecoder alloc]
                            initWithParseOptions:JKParseOptionNone];
    dictionary = [decoder objectWithData:soapData];
    NSString * strKey = [dictionary objectForKey:@"key"];
    //NSString * strMsg = [dictionary objectForKey:@"message"];
    
    NSLog(@"syncGenerateKey string %@",strKey);
    NSInteger status = [[dictionary objectForKey:@"status"] integerValue];
    if (status == 1) {
        NSUserDefaults * pref = [NSUserDefaults standardUserDefaults];
        [pref setObject:strKey forKey:@"key"];
        [pref synchronize];
        [self syncDeviceToken:strDevToken];
    }
}


@end
