//
//  WineInfo.h
//  FairPrice
//
//  Created by Tridip Sarkar on 29/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface WineInfo : NSObject 
{ 
	NSString *wineTitle;
	NSString *wineDescription;
	NSMutableArray *arrFoodToGo;
	NSString *wineGroupID;
	NSString *firstWineID;

}
@property(nonatomic,strong) NSString *wineTitle;
@property(nonatomic,strong) NSString *wineDescription;
@property(nonatomic,strong) NSMutableArray *arrFoodToGo;
@property(nonatomic,strong) NSString *wineGroupID;
@property(nonatomic,strong) NSString *firstWineID;
-(id)init;
@end
