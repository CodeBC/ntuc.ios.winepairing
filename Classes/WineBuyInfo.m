//
//  WineInfo.m
//  FairPrice
//
//  Created by Tridip Sarkar on 28/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import "WineBuyInfo.h"

@implementation WineBuyInfo
@synthesize wineID,title,price,rating,description,wineImage;

-(id)init
{
	if(self = [super init]) 
	{
		wineID=nil;
		title=nil;
		price=nil;
		rating=nil;
		description=nil;
//		wineImage=nil;

	}
	
	return self;
	
}


- (void)requestFinished:(ASIHTTPRequest *)request
{
	self.wineImage = [request responseData];	
	[[NSNotificationCenter defaultCenter] postNotificationName:@"RELOADIMAGE" object:nil userInfo:nil];
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
	self.wineImage = nil;
	
}




@end
