//
//  WineForFoodViewController.h
//  FairPrice
//
//  Created by niladri.schatterjee on 10/11/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"


@interface WineForFoodViewController : BaseViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
  
	UITextField* txtSearch;
	UITableView* tblWineForFood;
	NSIndexPath* celIndex;
	NSMutableArray *arrGroupDish;
	NSMutableArray *arrTemp;
	UILabel *searchResultLabel;
}

@property(nonatomic,strong) NSIndexPath* celIndex;
@property(nonatomic,strong) UITextField* txtSearch;
@property(nonatomic,strong) UITableView* tblWineForFood;
@property(nonatomic,strong) NSMutableArray *arrGroupDish;
@property(nonatomic,strong) NSMutableArray *arrTemp;
@property(nonatomic,strong) UILabel *searchResultLabel;

-(void)createIntro;
-(void)createSearchBar;
-(void)createTable;
//-(void)cellAdjustForIndexPath:(NSIndexPath*)indexPath;

-(void)cellAdjustForIndexPath:(NSIndexPath*)indexPath isDeleteImage:(BOOL)flag;
@end
