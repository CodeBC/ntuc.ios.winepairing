/*
 *  Constants.h
 *  FairPrice
 *
 *  Created by Satish Kumar on 10/7/10.
 *  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
 *
 */

#ifndef __CONST_H_
#define __CONST_H_
#define UIColorFromHex(rgbValue) [UIColor \
colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 \
green:((float)((rgbValue & 0xFF00) >> 8))/255.0 \
blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

//#define BASEURL @"http://192.168.0.12/fairprice/"
//#define BASEURL   @"http://www.whatsupatfairprice.com.sg/wineapp/"       
#define BASEURL   @"https://www.balancedfbapps.com/ntuc/"
#define BASEIMAGEURL   @"https://www.balancedfbapps.com/ntuc/images/"
         

#define START_X 10.0
#define START_Y 10.0
#define BACK_X 20.0
#define BACK_Y 20.0
#define NAVBARHEIGHT 44.0
#define TABBARHEIGHT 55.0

#define HOME 101
#define BYFOOD 1
#define BYWINE 2
#define WINELIST 3
#define TIP 4
#define WINECLUB 5
#define ORDERWINE 6
#define FOODFORWINE 3
//#define ABOUT 6

#define WINE_INFO 100
#define DISH_INFO 200

#define FOOD 200
#define WINE 100

#define REQUEST_GROUP_DISH 1
#define REQUEST_WINETYPE 2
#define REQUEST_FULLWINE_LIST 3
#define REQUEST_GROUP_WINE 4
#define REQUEST_DISH_LIST 5
#define REQUEST_WINE_INFO 6
#define REQUEST_WINE_BUYINFO 7
#define REQUEST_DISH_INFO 8
#define REQUEST_SEARCH_FILTER 9
#define REQUEST_SEARCH_FILTER_2 14
#define REQUEST_SEARCH_RESULT 10
#define REQUEST_SELECTED_GROUPWINE 11
#define REQUEST_ADDDISH 12
#define REQUEST_SUBMITRATING 13
#define REQUEST_GET_ALL_TIPS 15
#define REQUEST_GET_ALL_TIPS_VIDEOS 16
#define REQUEST_GET_INDIV_TIPS 17
#define REQUEST_GET_INDIV_TIPS_VIDEOS 18
#define REQUEST_ALL_WINES 19
#define REQUEST_SUBMIT_ORDER 20
#define REQUEST_SEARCH_ALL 21
#define REQUEST_ONE_WINE 22
#define REQUEST_SEND_EMAIL 23

#endif