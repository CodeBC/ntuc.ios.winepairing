//
//  DishInfo.m
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import "DishInfo.h"


@implementation DishInfo

@dynamic dishID;
@dynamic dishTitle;
@dynamic dishDescription1;
@dynamic dishDescription2;
@dynamic wineTypeName;
@dynamic wineID;
@dynamic wineTypeID;
@dynamic wineName;
@dynamic winePrice;
@dynamic wineRating;
@dynamic wineImage;
@dynamic wineGroupID;

- (void)requestFinished:(ASIHTTPRequest *)request
{
    if ([request.username isEqualToString:@"WineImage"])
    {
        //        NSLog(@"requestDATA = %@",[request  responseData]);
        self.wineImage = [request responseData];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"RELOAD" object:nil userInfo:nil];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    if ([request.username isEqualToString:@"WineImage"])
    {
        self.wineImage = nil;
    }
}


@end
