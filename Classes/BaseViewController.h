//
//  BaseViewController.h
//  FairPrice
//
//  Created by Satish Kumar on 10/7/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuartzCore/QuartzCore.h"
#import <Twitter/Twitter.h>
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "TitleView.h"

@class DataModel,FairPriceAppDelegate;
@interface BaseViewController : UIViewController <UIActionSheetDelegate, MFMailComposeViewControllerDelegate> 
{
	UIImageView* tabBarView;
	int tabIndex;
	UIActivityIndicatorView *actView;
	DataModel* __weak dataModel;
	FairPriceAppDelegate* appDelegate;
	UIButton *btnBack;
	
}
@property(weak) DataModel* dataModel;
@property(nonatomic,assign) int tabIndex;
@property(nonatomic,strong) UIActivityIndicatorView *actView;
@property(nonatomic,strong) NSString *titleText;
@property(nonatomic,strong) TitleView *titleView;
@property(nonatomic,strong) UIImageView* tabBarView;
@property(nonatomic,strong) UIView* mainView ;
@property(nonatomic,strong) UIButton* but_order_direct;
@property(nonatomic,strong) UIButton* but_share;

-(void)addFooter;
-(void)createBody;
-(void)createButtonInfo;
-(void)createNavigationMenu;
-(void)createBackground;
-(void)createFacebookPost;
-(void)createTweet;
-(void)createMailLayout;


-(void)startIndicator;
-(void)stopIndicator;
-(void)showAlertWithMessage:(NSString*) strMessage;
-(NSString*)trim:(NSString*)string;
-(BOOL)validateEmail:(NSString *)candidate;
-(void)hideBackButton;

@end
