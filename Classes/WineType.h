//
//  WineType.h
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface WineType : NSManagedObject

@property (nonatomic, retain) NSString * wineTitle;
@property (nonatomic, retain) NSString * wineTypeId;
@property (nonatomic, retain) NSData * wineIcon1;
@property (nonatomic, retain) NSData * wineIcon2;

@end
