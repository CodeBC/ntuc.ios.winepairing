//
//  FairPriceBestBuyViewController.m
//  FairPrice
//
//  Created by niladri.schatterjee on 10/13/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "FairPriceBestBuyViewController.h"
#import "WineBuyInfo.h"
#import "DishInfo.h"
#import "WineGroupItem.h"
#import "DataModel.h"
#import "FairPriceAppDelegate.h"
#import "MapViewController.h"
#import "RateView.h"

@implementation FairPriceBestBuyViewController
@synthesize  lblWineName,lblWinePrice,imgWineBottle,imgRibbon,requestFromPage,ribbonImageStr,wineBuydata,myRating,dishInfoData,groupDishData,wineItemData;

/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
        // Custom initialization
    }
    return self;
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    self.title = @"FAIRPRICE RECOMMENDS";
    [super viewDidLoad];
	
	if(requestFromPage==BYFOOD)
    {  
        ribbonImageStr=@"fairPriceRed_food_btn.png";
//        self.wineBuydata=[appDelegate.dataModel.arrWineBuyInfo objectAtIndex:0];
        self.dishInfoData = (appDelegate.dataModel.arrDishInfo)[0];
        self.myRating=[NSString stringWithFormat:@"%f",[dishInfoData.wineRating floatValue]];
        //[self createRatingPanelWithSelected:[dishInfoData.wineRating intValue]];
	}
	else if(requestFromPage==BYWINE)
	{
        ribbonImageStr=@"fairPriceBlue_btn_wine.png";
//        self.wineBuydata=[appDelegate.dataModel.arrWineBuyInfo objectAtIndex:0];
        self.dishInfoData = (appDelegate.dataModel.arrGroupWine)[0];
//        self.groupDishData = [appDelegate.dataModel.arrGroupWine objectAtIndex:0];
        self.myRating=[NSString stringWithFormat:@"%f",[dishInfoData.wineRating floatValue]];		
        //[self createRatingPanelWithSelected:[dishInfoData.wineRating intValue]];
	}
	else if(requestFromPage==WINELIST)
	{
        ribbonImageStr=@"fairPriceGreen_btn_wine.png";
//        self.wineBuydata=[appDelegate.dataModel.arrWineBuyInfo objectAtIndex:0];	
//        self.dishInfoData = [appDelegate.dataModel.arrDishInfo objectAtIndex:0];
        self.dishInfoData = (appDelegate.dataModel.arrWineInfo)[0];
//        self.dishInfoData = (DishInfo *) wineItemData;
        self.myRating=[NSString stringWithFormat:@"%f",[dishInfoData.wineRating floatValue]];	
        //[self createRatingPanelWithSelected:[dishInfoData.wineRating intValue]];
//        [self createRatingPanelWithSelected:0];	
	}
    //[self createMapButton];
	[self createPriceView];
	[self createWineDetails];
    [self createWineClubLabel];


	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateView) name:@"RELOAD" object:nil];
}

-(void)createNavigationMenu{    
    [self setTitleText:self.title];
    [super createNavigationMenu];
    
//    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectMake(10.0, START_Y+30.0, 300.0, 34.5)];
//	[titleLabel setText:@"FAIRPRICE RECOMMENDS"];
//	titleLabel.textAlignment = UITextAlignmentCenter;
//	titleLabel.backgroundColor=[UIColor clearColor];
//	titleLabel.textColor = [UIColor redColor];
//    titleLabel.font = [UIFont fontWithName:@"Tommaso" size:26.0];
//	[self.view addSubview:titleLabel];
//	[titleLabel release];
//    
//    UIImageView *imgView = [[UIImageView alloc] initWithFrame:CGRectMake(10.0, START_Y+44.0, 300.0, 1.0)];
//    [imgView setImage:[UIImage imageNamed:@"nav_bar_line.png"]];
//    imgView.contentMode = UIViewContentModeScaleAspectFit;
//    [self.view bringSubviewToFront:imgView];
//    [self.view addSubview:imgView];
//    [imgView release];
}

-(void)createMapButton{
    UIButton *butMap = [[UIButton alloc] initWithFrame:CGRectMake(0.0, 460.0-TABBARHEIGHT-45.0 +30, 320.0, 45.0)];
    
    [butMap setBackgroundImage:[UIImage imageNamed:@"but_locate.png"] forState:UIControlStateNormal];
    //	[addDish setBackgroundImage:[UIImage imageNamed:@"but_add_your_wine.png"] forState:UIControlStateHighlighted];
    
	[butMap addTarget:self action:@selector(onClickMap:) forControlEvents:UIControlEventTouchUpInside];
	
	[self.view addSubview:butMap];
}

-(void)updateView{
    //image of wine bottle
	UIImageView* aView = [[UIImageView alloc]initWithFrame:CGRectMake(190.0+20, START_Y+140.0, 100.0, 289.0)];
	[aView setImage:[UIImage imageWithData:dishInfoData.wineImage]];
	self.imgWineBottle=aView;
	[self.view addSubview:imgWineBottle];
}

-(void)createPriceView
{
    NSLog(@"dishInfoData = %@",dishInfoData.wineName);
	UILabel* lbl = [[UILabel alloc]initWithFrame:CGRectMake(25.0, START_Y+95.0 , 285.0, 40.0)];
//	[lbl setText:wineBuydata.title];                     //@"   Alice White Chardonnay"];
	[lbl setText:[NSString stringWithFormat:@" %@",[dishInfoData.wineName uppercaseString]]];
	lbl.numberOfLines = 0;
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = [UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:18.0];
    lbl.backgroundColor =[UIColor clearColor];
	[self setLblWineName:lbl];
	[self.view addSubview:lbl];
	
	lbl = [[UILabel alloc]initWithFrame:CGRectMake(35.0, START_Y+213.0,80, 20.0)];
//	[lbl setText:wineBuydata.price];
	[lbl setText:[NSString stringWithFormat:@"$ %@",dishInfoData.winePrice]];
	lbl.textAlignment = NSTextAlignmentCenter;
	lbl.textColor =[UIColor whiteColor];
	lbl.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:22.0];
    lbl.backgroundColor =[UIColor clearColor];
	[self setLblWinePrice:lbl];
	[self.view addSubview:lbl];
    
//    lbl = [[UILabel alloc]initWithFrame:CGRectMake(25.0, START_Y+210.0, 285.0, 20.0)];
//    //	[lbl setText:wineBuydata.price];
//	[lbl setText:@"$"];
//	lbl.textAlignment = UITextAlignmentLeft;
//	lbl.textColor =[UIColor whiteColor];
//	lbl.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:12.0];
//    lbl.backgroundColor =[UIColor clearColor];
//	[self setLblWinePrice:lbl];
//	[self.view addSubview:lbl];
    
    UIImageView* squareView = [[UIImageView alloc]initWithFrame:CGRectMake(20.0, START_Y+200.0, 110, 48.0)];
	[squareView setImage:[UIImage imageNamed:@"img_wine_price.png"]];
	[self.view addSubview:squareView];
}

-(void)createRatingPanelWithSelected:(int)noOfStar
{   
	ratingView=[[RateView alloc]init];
	ratingView.editable=YES;
	ratingView.delegate=self;
	ratingView.frame=CGRectMake(97.0, START_Y+213.0, 100.0, 20.0);
	ratingView.fullSelectedImage=[UIImage imageNamed:@"ico_star_silver.png"];
	ratingView.notSelectedImage=[UIImage imageNamed:@"ico_star_empty.png"];
	ratingView.maxRating=5;
	ratingView.rating=noOfStar;
	[self.view addSubview:ratingView];
}

-(void)createWineDetails{
    
    //	UIButton* btnRateWine = [[UIButton alloc] initWithFrame:CGRectMake(17.0 , START_Y+310.0, 133.0, 37.0)];
    //	[btnRateWine addTarget:self action:@selector(clickRateBtn:) forControlEvents:UIControlEventTouchUpInside];
    //	[btnRateWine setBackgroundImage:[UIImage imageNamed:@"homeBtn2.png"] forState:UIControlStateNormal];
    //	[self.view addSubview:btnRateWine];
    //	[btnRateWine release];
	
	if(requestFromPage!=WINELIST){
        UIImageView* aView = [[UIImageView alloc]initWithFrame:CGRectMake(190.0+20, START_Y+140.0, 100.0, 289.0)];
        
        [aView setImage:[UIImage imageWithData:dishInfoData.wineImage]];
        self.imgWineBottle=aView;
        [self.view addSubview:imgWineBottle];
    }
	
	
	UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(20.0, START_Y+105.0,200.0, 125.0)];
    
    //	[lbl setText:@"Buy this wine at your nearest FairPrice Xtra or FairPrice Finest. While stocks last."];
	[lbl setText:@"This wine available at [FairPrice Finest and FairPrice Xtra]. \nStocks available while stocks last."];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.backgroundColor = [UIColor clearColor];
	lbl.textColor = [UIColor whiteColor];
	lbl.numberOfLines = 0;
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
	[self.view addSubview:lbl];
	
    
    
    //	aView = [[UIImageView alloc]initWithFrame:CGRectMake(5.0, START_Y+235.0, 217.0, 64.0)];
    //	[aView setImage:[UIImage imageNamed:ribbonImageStr]];
    //	[self setImgRibbon:aView];
    //	[self.view addSubview:aView];
    //	[aView release];
}

-(void)createWineClubLabel{
    UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(20.0, START_Y+250.0,200.0, 20.0)];
	//[lbl setText:[NSString stringWithFormat:@"%@\r\n%@", @"Welcome to the", @"FairPrice wine celler"]];
	[lbl setText:@"Join the Just Wine Club"];
	lbl.textAlignment = UITextAlignmentLeft;
	//lbl.textColor = [UIColor whiteColor];
	lbl.backgroundColor = [UIColor clearColor];
	lbl.textColor = [UIColor redColor];
	lbl.numberOfLines = 0;
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc] initWithFrame:CGRectMake(165.0, START_Y+250.0,200.0, 20.0)];
	//[lbl setText:[NSString stringWithFormat:@"%@\r\n%@", @"Welcome to the", @"FairPrice wine celler"]];
	[lbl setText:@"and"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.backgroundColor = [UIColor clearColor];
	lbl.textColor = [UIColor whiteColor];
	lbl.numberOfLines = 2;
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:12];
	[self.view addSubview:lbl];
    
    lbl = [[UILabel alloc] initWithFrame:CGRectMake(20.0, START_Y+265.0,190.0, 20.0)];
	//[lbl setText:[NSString stringWithFormat:@"%@\r\n%@", @"Welcome to the", @"FairPrice wine celler"]];
	[lbl setText:@"save 8% on your wine purchases"];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.backgroundColor = [UIColor clearColor];
	lbl.textColor = [UIColor whiteColor];
    lbl.lineBreakMode = UILineBreakModeCharacterWrap;
	lbl.numberOfLines = 1;
	lbl.font = [UIFont fontWithName:@"EuphemiaUCAS" size:11];
	[self.view addSubview:lbl];
}

-(void)clickRateBtn:(id)sender
{ 
//	NSLog(@"Rate this ");
//	
//	NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
//	[paramDict setObject:myRating forKey:@"Rating"];
////	[paramDict setObject:wineBuydata.wineID forKey:@"WineID"];
//	[paramDict setObject:dishInfoData.wineID forKey:@"WineID"];
//
//	[self startIndicator];
//	[appDelegate.dataModel startRequestWithParameters:REQUEST_SUBMITRATING WithParams:paramDict];
//	[self stopIndicator];
//	
//	[self showAlertWithMessage:appDelegate.dataModel.responseStr];
	 
}


- (void)rateView:(RateView *)rateView ratingDidChange:(float)rating
{
	//self.myRating=wineBuydata.rating;
	self.myRating=[NSString stringWithFormat:@"%f",rating];
	NSLog(@"myRating: %@ ",myRating);
    
    NSLog(@"Rate this ");
	
	NSMutableDictionary *paramDict=[NSMutableDictionary dictionary];
	paramDict[@"Rating"] = myRating;
	paramDict[@"WineID"] = dishInfoData.wineID;
    NSLog(@"dishInfoData.wineName = %@",dishInfoData.wineName);
    
	[self startIndicator];
	[appDelegate.dataModel startRequestWithParameters:REQUEST_SUBMITRATING WithParams:paramDict];
	[self stopIndicator];
    
    /*
     Change following method to display alertMessage when stars are clicked
     */
	
//	[self showAlertWithMessage:appDelegate.dataModel.responseStr];
	
}


-(void)clickBtn:(id)sender
{
	//int tag = [sender tag];
	//[[sender superview] removeFromSuperview];
	//[self createRatingPanelWithSelected:[sender tag]];
	//[sender setBackgroundImage:[UIImage imageNamed:@"icon_redStar.png"] forState:UIControlStateHighlighted];
}

-(IBAction)onClickMap:(id)sender{
    MapViewController *vcMap = [[MapViewController alloc]init];
    [self.navigationController pushViewController:vcMap animated:YES];
}

- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}



@end
