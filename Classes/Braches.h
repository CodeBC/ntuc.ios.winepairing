//
//  Braches.h
//  FairPrice
//
//  Created by Zayar on 11/17/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class BranchCategories;

@interface Braches : NSManagedObject

@property (nonatomic, retain) NSString * b_id;
@property (nonatomic, retain) NSString * b_cate_id;
@property (nonatomic, retain) NSString * address;
@property (nonatomic, retain) NSDecimalNumber * lat;
@property (nonatomic, retain) NSDecimalNumber * log;
@property (nonatomic, retain) BranchCategories *category;

@end
