//
//  WineResultViewController.h
//  FairPrice
//
//  Created by niladri.schatterjee on 10/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "DishInfo.h"
#import "Constants.h"

@interface WineResultViewController : BaseViewController<UIWebViewDelegate>
{
	UILabel* lblFoodSelection;
	DishInfo *dishData;
	UIWebView *DesWebView;
	 
}

@property(nonatomic,strong) UILabel* lblFoodSelection;
@property(nonatomic,strong) DishInfo *dishData;
@property(nonatomic,strong) UIWebView *DesWebView;

-(void)createNavigationMenu;
-(void)createWineSugession;
-(void)createLowerPanel;
-(void)createTextView;
@end
