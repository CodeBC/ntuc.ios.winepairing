//
//  MapViewController.m
//  FairPrice
//
//  Created by David Seah on 22/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "MapViewController.h"
#import "BranchCategories.h"
#import "Braches.h"
#import "JSONKit.h"
#import "DisplayMap.h"
#import "ZSPinAnnotation.h"
#import "ZSAnnotation.h"
#import "Utility.h"
@interface MapViewController ()
{
    NSString * strPinId;
    NSArray * arrB;
}
@end

@implementation MapViewController
#define MINIMUM_ZOOM_ARC 0.014 //approximately 1 miles (1 degree of arc ~= 69 miles)
#define ANNOTATION_REGION_PAD_FACTOR 1.15
#define MAX_DEGREES_ARC 360
@synthesize wvMap,wfMap;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self createWebView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.wvMap = nil;
}

-(void)createWebView{
    /*wvMap = [[UIWebView alloc] initWithFrame:CGRectMake(0, 60, 320, 340)];
    wvMap.delegate = self;
    wvMap.autoresizesSubviews = YES;
    wvMap.autoresizingMask=(UIViewAutoresizingFlexibleHeight | 
                                  UIViewAutoresizingFlexibleWidth);
    wvMap.backgroundColor = [UIColor blackColor];
    wvMap.scrollView.bounces = NO;
    wvMap.scrollView.showsVerticalScrollIndicator = NO;
    
    NSString *mapURL = @"https://www.balancedfbapps.com/ntuc/map/map.html";
    
    [wvMap loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:mapURL]]];
    
    [self.view addSubview:wvMap];*/
    if ([Utility isGreaterOREqualOSVersion:@"7"]) {
        self.wfMap = [[MKMapView alloc] initWithFrame:CGRectMake(0, 60, 320, 568-60)];
    }
    
    [self.wfMap setDelegate:self];
    [self.view addSubview:self.wfMap];
    

}

- (void)viewWillAppear:(BOOL)animated{
    [self syncProducts];
    
}

- (void) viewDidAppear:(BOOL)animated{
    [self reloadTheMapPin:[Braches findAll]];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (void)removeAllAnnotations {
    
    NSMutableArray *annotationsToRemove = [NSMutableArray arrayWithCapacity:[self.wfMap.annotations count]];
    NSLog(@"self mapview count %d",[self.wfMap.annotations count]);
    
    for (int i = 0; i < [self.wfMap.annotations count]; i++) {
        NSLog(@"removepin......");
        //if ([[self.mapView.annotations objectAtIndex:i] isKindOfClass:[DisplayMap class]]) {
        NSLog(@"removepin2......");
        [annotationsToRemove addObject:[self.wfMap.annotations objectAtIndex:i]];
        //}
    }
    
    [self.wfMap removeAnnotations:annotationsToRemove];
}

- (void) reloadTheMapPin:(NSArray *)arrBranches{
    arrB = arrBranches;
    int i =0;
    for(Braches * obj in arrBranches){
        
        //NSLog(@"id %d lat %f log %f",obj.idx,obj.latitue,obj.longtitue);
        strPinId=obj.b_id;
        
        [self.wfMap setMapType:MKMapTypeStandard];
        [self.wfMap setZoomEnabled:YES];
        [self.wfMap setScrollEnabled:YES];
        MKCoordinateRegion region = { {0.0, 0.0 }, { 0.0, 0.0 } };
        region.center.latitude = [obj.lat floatValue];
        region.center.longitude = [obj.log floatValue];
        region.span.longitudeDelta = 0.01f;
        region.span.latitudeDelta = 0.01f;
        [self.wfMap setRegion:region animated:YES];
        
        [self.wfMap setDelegate:self];
        
        //DisplayMap *ann = [[DisplayMap alloc] init];
        
        //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"b_cate_id == %@",obj.b_cate_id];
        //BranchCategories * cate = [BranchCategories findFirstWithPredicate:predicate];
        ZSAnnotation * ann = [[ZSAnnotation alloc] init];
        ann.title =@" ";
        ann.cate_id = obj.b_cate_id;
        
        //ann.subtitle=obj.address;
        ann.coordinate = region.center;
    
        ann.merchantId=i;
        [self.wfMap addAnnotation:ann];
        i ++;
    }
    [self zoomMapViewToFitAnnotations:self.wfMap animated:YES];
}

//size the mapView region to fit its annotations
- (void)zoomMapViewToFitAnnotations:(MKMapView *)mapView1 animated:(BOOL)animated
{
    NSArray *annotations = mapView1.annotations;
    int count = [mapView1.annotations count];
    
    //NSLog(@"zoomMapViewToFitAnnotations count %d ",count);
    if ( count == 0) { return; } //bail if no annotations
    
    //convert NSArray of id <MKAnnotation> into an MKCoordinateRegion that can be used to set the map size
    //can't use NSArray with MKMapPoint because MKMapPoint is not an id
    MKMapPoint points[count]; //C array of MKMapPoint struct
    for( int i=0; i<count; i++ ) //load points C array by converting coordinates to points
    {
        CLLocationCoordinate2D coordinate = [(id <MKAnnotation>)[annotations objectAtIndex:i] coordinate];
        points[i] = MKMapPointForCoordinate(coordinate);
        //NSLog(@"index %d:::: %f :%f",i,coordinate.latitude,coordinate.longitude);
    }
    //create MKMapRect from array of MKMapPoint
    MKMapRect mapRect = [[MKPolygon polygonWithPoints:points count:count] boundingMapRect];
    //convert MKCoordinateRegion from MKMapRect
    MKCoordinateRegion region = MKCoordinateRegionForMapRect(mapRect);
    
    //add padding so pins aren't scrunched on the edges
    region.span.latitudeDelta  *= ANNOTATION_REGION_PAD_FACTOR;
    region.span.longitudeDelta *= ANNOTATION_REGION_PAD_FACTOR;
    //but padding can't be bigger than the world
    //NSLog(@"region...%f :%f",region.span.latitudeDelta,region.span.longitudeDelta);
    if( region.span.latitudeDelta > MAX_DEGREES_ARC ) { region.span.latitudeDelta  = MAX_DEGREES_ARC; }
    if( region.span.longitudeDelta > MAX_DEGREES_ARC ){ region.span.longitudeDelta = MAX_DEGREES_ARC; }
    
    //and don't zoom in stupid-close on small samples
    if( region.span.latitudeDelta  < MINIMUM_ZOOM_ARC ) { region.span.latitudeDelta  = MINIMUM_ZOOM_ARC; }
    if( region.span.longitudeDelta < MINIMUM_ZOOM_ARC ) { region.span.longitudeDelta = MINIMUM_ZOOM_ARC; }
    //and if there is a sample of 1 we want the max zoom-in instead of max zoom-out
    if( count == 1 )
    {
        region.span.latitudeDelta = MINIMUM_ZOOM_ARC;
        region.span.longitudeDelta = MINIMUM_ZOOM_ARC;
    }
    [mapView1 setRegion:region animated:animated];
}

-(MKAnnotationView *)mapView:(MKMapView *)mV viewForAnnotation:(id <MKAnnotation>)annotation {
    NSLog(@"view for annotation!!");
    static NSString *defaultPinID = @"com.invasivecode.pin";
    ZSPinAnnotation *pinView =  nil;
	if(annotation != mV.userLocation)
	{
        ZSAnnotation *ann =(ZSAnnotation*)annotation;
        Braches * obje = [arrB objectAtIndex:ann.merchantId];
        
        pinView = (ZSPinAnnotation *)[self.wfMap dequeueReusableAnnotationViewWithIdentifier:defaultPinID];
        if ( pinView == nil ) pinView = [[ZSPinAnnotation alloc] initWithAnnotation:annotation reuseIdentifier:defaultPinID];
        //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"b_cate_id == %@",ann.cate_id];
        //BranchCategories * cate = [BranchCategories findFirstWithPredicate:predicate];
        NSLog(@"pin cate %@",ann.cate_id);
        if ([ann.cate_id isEqualToString:@"1"]) {
            //pinView.pinColor = MKPinAnnotationColorGreen;
            pinView.annotationColor = [UIColor blueColor];
        }
        else if([ann.cate_id isEqualToString:@"2"]){
            pinView.annotationColor = [UIColor orangeColor];
        }
        else if([ann.cate_id isEqualToString:@"3"]){
            pinView.annotationColor = [UIColor redColor];
        }
        else if([ann.cate_id isEqualToString:@"4"]){
            pinView.annotationColor = [UIColor greenColor];
        }
        else if([ann.cate_id isEqualToString:@"5"]){
            pinView.annotationColor = [UIColor yellowColor];
        }
        
        pinView.annotationType = ZSPinAnnotationTypeStandard;
        
        pinView.canShowCallout = YES;
        
        UIView *leftCAV = [[UIView alloc] initWithFrame:CGRectMake(4,0,250,60)];
        UILabel *l1=[[UILabel alloc] init];
        l1.frame=CGRectMake(4, 0, 250, 50);
        l1.text= obje.address;
        l1.font=[UIFont fontWithName:@"EuphemiaUCAS" size:(10.0)];
        l1.numberOfLines = 0;
        [leftCAV addSubview:l1];
        pinView.leftCalloutAccessoryView = leftCAV;
        
        //UIButton *infoButton = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        //pinView.rightCalloutAccessoryView = infoButton;
        
        return pinView;
    }
    
	return nil;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control {
    
    
    /*MCIDDelegate * delegate = [[UIApplication sharedApplication]delegate];
    ObjectMerchant * objMer =[delegate.db getMerchantById:view.tag];
    NSLog(@"go detail id %d lat %f long %f",view.tag,objMer.latitue,objMer.longtitue);
    MerchantsDetailViewController * merchantDetailViewController = [[MerchantsDetailViewController alloc] initWithNibName:@"MerchantsDetailViewController" bundle:[NSBundle mainBundle]];
    
    merchantDetailViewController.objSelectedMerchant = objMer;
    [self.navigationController pushViewController:merchantDetailViewController animated:YES];
    [merchantDetailViewController release];
    merchantDetailViewController = nil;
    [objMer release];*/
}


#pragma Stores Sync Methods
- (void) syncProducts{
    NSError *error;
    NSBundle *mainBundle = [NSBundle mainBundle];
    NSString *myFile = [mainBundle pathForResource: @"products" ofType: @"json"];
    NSString *response=[NSString stringWithContentsOfFile:myFile
                                                 encoding:NSUTF8StringEncoding
                                                    error:&error];
    
    if (error)
    {
        NSLog(@"error %@", error);
    }
    
    //NSString *validJSON = [self convertResponseIntoValidJSON:response];
    NSData *soapData = [response dataUsingEncoding:NSUTF8StringEncoding];
    
    JSONDecoder* decoder = [[JSONDecoder alloc]
                            initWithParseOptions:JKParseOptionNone];
    NSDictionary* myDict = [decoder objectWithData:soapData];
    NSString * strServerId = @"";
    NSString * strLocalId = @"";
    NSArray * arrNews = [myDict objectForKey:@"Branch_categories"];
    NSLog(@"arr news count %d",[arrNews count]);
    for(NSInteger i=0;i<[arrNews count];i++){
        NSDictionary * dicEmp = (NSDictionary *)[arrNews objectAtIndex:i];
        strServerId = [dicEmp objectForKey:@"cateID"];
        //strLocalId = [dicEmp objectForKey:@"local_id"];
        strLocalId = @" ";
        //[self saveORupdateNewsWithServerId:strServerId andLocalId:strLocalId andDics:dicEmp];
        
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"b_cate_id == %@",strServerId];
        BranchCategories * emp = [BranchCategories findFirstWithPredicate:predicate inContext:localContext];
        if (emp != nil) {
            
            //emp.server_id = [dics objectForKey:@"newsID"];
            /* 
             "cateID": 1,
             "cateName": "Xtra",
             "cateColor": "Blue"
             */
            emp.b_cate_id = [dicEmp objectForKey:@"cateID"];
            emp.name = [dicEmp objectForKey:@"cateName"];
            emp.color = [dicEmp objectForKey:@"cateColor"];
            [self updateProductCate:emp withLocalContext:localContext];
        }
        else{
            BranchCategories * objEmp2 = [BranchCategories createEntity];
            
            objEmp2.b_cate_id = [dicEmp objectForKey:@"cateID"];
            objEmp2.name = [dicEmp objectForKey:@"cateName"];
            objEmp2.color = [dicEmp objectForKey:@"cateColor"];
            
            [self insertProductCate:objEmp2 withLocalContext:localContext];
        }
    }
    
    
    NSArray * arrProducts = [myDict objectForKey:@"Branches"];
    NSLog(@"arr products count %d",[arrNews count]);
    for(NSInteger i=0;i<[arrProducts count];i++){
        NSDictionary * dicEmp = (NSDictionary *)[arrProducts objectAtIndex:i];
        strServerId = [dicEmp objectForKey:@"productID"];
        //strLocalId = [dicEmp objectForKey:@"local_id"];
        strLocalId = @" ";
        //[self saveORupdateNewsWithServerId:strServerId andLocalId:strLocalId andDics:dicEmp];
        
        NSManagedObjectContext *localContext    = [NSManagedObjectContext MR_contextForCurrentThread];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"b_id == %@",strServerId];
        Braches * emp = [Braches findFirstWithPredicate:predicate inContext:localContext];
        if (emp != nil) {
            /* 
             {
             "id": 7,
             "branchAddress": "No. 6 Marine Parade Central <br />#01-670 S(449411)",
             "lat": "1.301943",
             "long": "103.907595",
             "catId": "2"
             }
             */
            
            
            emp.b_id = [dicEmp objectForKey:@"id"];
            emp.address = [dicEmp objectForKey:@"branchAddress"];
            emp.lat = [NSDecimalNumber decimalNumberWithString:[dicEmp objectForKey:@"lat"]];
            emp.log = [NSDecimalNumber decimalNumberWithString:[dicEmp objectForKey:@"long"]];
            emp.b_cate_id = [dicEmp objectForKey:@"catId"];

            [self updateProduct:emp withLocalContext:localContext];
        }
        else{
            Braches * objEmp2 = [Braches createEntity];
            objEmp2.b_id = [dicEmp objectForKey:@"id"];
            objEmp2.address = [dicEmp objectForKey:@"branchAddress"];
            objEmp2.lat = [NSDecimalNumber decimalNumberWithString:[dicEmp objectForKey:@"lat"]];
            objEmp2.log = [NSDecimalNumber decimalNumberWithString:[dicEmp objectForKey:@"long"]];
            objEmp2.b_cate_id = [dicEmp objectForKey:@"catId"];
            
            [self insertProducts:objEmp2 withLocalContext:localContext];
        }
    }
}

- (void) updateProductCate:(BranchCategories *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    
    // Create a new Person in the current thread context
    //obj                          = [Employee MR_createInContext:localContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"b_cate_id ==[c] %@ ", obj.b_cate_id];
    BranchCategories * employeeFounded = [BranchCategories MR_findFirstWithPredicate:predicate inContext:localContext];
    if (employeeFounded) {
        /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         Employee * objEmp = [Employee MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
         NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
         }];*/
        employeeFounded = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            BranchCategories * objEmp = [BranchCategories MR_findFirstByAttribute:@"b_cate_id" withValue:obj.b_cate_id inContext:localContext];
           // NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
        }];
    }
}

- (void) insertProductCate:(BranchCategories *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    // Get the local context
    // Create a new Person in the current thread context
    //Employee *person                          = [Employee MR_createInContext:localContext];
    //person = obj;
    [obj MR_inContext:localContext];
    
    // Save the modification in the local context
    [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"Product Category saved");
    }];
}

- (void) updateProduct:(Braches *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    
    // Create a new Person in the current thread context
    //obj                          = [Employee MR_createInContext:localContext];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"b_id ==[c] %@ ", obj.b_id];
    Braches * employeeFounded = [Braches MR_findFirstWithPredicate:predicate inContext:localContext];
    if (employeeFounded) {
        /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
         Employee * objEmp = [Employee MR_findFirstByAttribute:@"server_id" withValue:obj.server_id inContext:localContext];
         NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
         }];*/
        employeeFounded = obj;
        [localContext MR_saveOnlySelfWithCompletion:^(BOOL success, NSError *error) {
            Braches * objEmp = [Braches MR_findFirstByAttribute:@"b_id" withValue:obj.b_id inContext:localContext];
            //NSLog(@"is successed updated ! %d and updated %@ and active %d",success,objEmp.local_id,[obj.is_active integerValue]);
        }];
    }
}

- (void) insertProducts:(Braches *)obj withLocalContext:(NSManagedObjectContext *)localContext{
    // Get the local context
    // Create a new Person in the current thread context
    //Employee *person                          = [Employee MR_createInContext:localContext];
    //person = obj;
    [obj MR_inContext:localContext];
    
    // Save the modification in the local context
    [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
        NSLog(@"Products saved");
    }];
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
