//
//  DishItem.m
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import "DishItem.h"
#import "GroupDishItem.h"


@implementation DishItem

@dynamic dishID;
@dynamic dishTitle;
@dynamic groupId;
@dynamic groupDishItem;

@end
