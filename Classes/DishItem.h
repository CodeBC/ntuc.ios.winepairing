//
//  DishItem.h
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class GroupDishItem;

@interface DishItem : NSManagedObject

@property (nonatomic, retain) NSString * dishID;
@property (nonatomic, retain) NSString * dishTitle;
@property (nonatomic, retain) NSString * groupId;
@property (nonatomic, retain) GroupDishItem *groupDishItem;

@end
