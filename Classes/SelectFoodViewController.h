//
//  SelectFoodViewController.h
//  FairPrice
//
//  Created by niladri.schatterjee on 10/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface SelectFoodViewController : BaseViewController <UITableViewDelegate , UITableViewDataSource , UITextFieldDelegate> {
	UITextField* txtSearch;
	UITableView* tblWineForFood;
	NSIndexPath* celIndex;
	NSMutableArray *arrDishItem;
	NSMutableArray *arrTemp;
	UILabel *searchResultLabel;
	
}
@property(nonatomic,strong) UITextField* txtSearch;
@property(nonatomic,strong) UITableView* tblWineForFood;
@property(nonatomic,strong) NSIndexPath* celIndex;
@property(nonatomic,strong) NSMutableArray *arrDishItem;
@property(nonatomic,strong) NSMutableArray *arrTemp;
@property(nonatomic,strong) UILabel *searchResultLabel;
-(void)createNavigationMenu;
-(void)createSearchBar;
-(void)createTable;
//-(void)cellAdjustForIndexPath:(NSIndexPath*)indexPath;
-(void)cellAdjustForIndexPath:(NSIndexPath*)indexPath isDeleteImage:(BOOL)flag;

@end
