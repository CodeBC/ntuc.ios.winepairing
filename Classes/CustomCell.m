//
//  WineForFoodCell.m
//  FairPrice
//
//  Created by niladri.schatterjee on 10/12/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import "CustomCell.h"
#import "Constants.h"

@implementation CustomCell
@synthesize lblFoodName,ivBackImage;//ivIcon;



- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if ((self = [super initWithStyle:style reuseIdentifier:reuseIdentifier])) {
        // Initialization code
		lblFoodName = [[UILabel alloc] init];
		lblFoodName.textAlignment = UITextAlignmentLeft;
		lblFoodName.font = [UIFont fontWithName:@"Futura-CondensedMedium" size:18];
		lblFoodName.textColor = [UIColor whiteColor];
		lblFoodName.numberOfLines = 0 ;
		lblFoodName.backgroundColor = [UIColor clearColor];
		
		ivBackImage = [[UIImageView alloc] init];
		//ivIcon = [[UIImageView alloc] init];
		[self.contentView addSubview:ivBackImage];
		//[self.contentView addSubview:ivIcon];
		[self.contentView addSubview:lblFoodName];
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
    }
    return self;
}

- (void)layoutSubviews 
{
	[super layoutSubviews];
//CGRect contentRect = self.contentView.bounds;
	
	//CGFloat boundsX = contentRect.origin.x;
	//CGFloat contentWidth=contentRect.size.width;
	//CGFloat contentHeight=contentRect.size.height;
	
	
	CGRect frame;
	
	//frame = CGRectMake(60.0, (contentHeight-25)/2, 18.0, 27.0);
	//ivIcon.frame = frame;
	
	frame = CGRectMake( 0.0, 0.0, 320.0, 40.0);//40
	ivBackImage.frame = frame;
	
	frame = CGRectMake(53,5.0, 250.0 , 26.0);
	lblFoodName.frame = frame;
	
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated 
{

    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




@end
