//
//  GroupDishItem.h
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Dishitem;

@interface GroupDishItem : NSManagedObject

@property (nonatomic, retain) NSData * dishIcon1;
@property (nonatomic, retain) NSData * dishIcon2;
@property (nonatomic, retain) NSString * dishImage;
@property (nonatomic, retain) NSString * dishTitle;
@property (nonatomic, retain) NSString * groupDishID;
@property (nonatomic, retain) Dishitem *dishItems;

@end
