//
//  WineGroupType.m
//  FairPrice
//
//  Created by ddseah on 12/05/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "WineGroupType.h"

@implementation WineGroupType

@synthesize wineConfigKey,wineConfigValue;
-(id)init
{
	if(self = [super init]) 
	{
		wineConfigKey=nil;
		wineConfigValue=nil;
        
	}
	
	return self;
	
}

@end
