//
//  WineGroupItem.h
//  FairPrice
//
//  Created by Zayar on 11/15/13.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface WineGroupItem : NSManagedObject

@property (nonatomic, retain) NSString * wineID;
@property (nonatomic, retain) NSString * groupTitle;
@property (nonatomic, retain) NSString * wineTypeDesc;
@property (nonatomic, retain) NSString * foodNames;
@property (nonatomic, retain) NSString * winePrice;
@property (nonatomic, retain) NSString * wineName;
@property (nonatomic, retain) NSString * wineRating;
@property (nonatomic, retain) NSString * wineCountry;
@property (nonatomic, retain) NSData * wineImage;
@property (nonatomic, retain) NSString * groupID;

@end
