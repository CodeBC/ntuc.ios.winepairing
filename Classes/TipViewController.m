//
//  TipViewController.m
//  FairPrice
//
//  Created by Tridip Sarkar on 25/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import "TipViewController.h"
#import "Constants.h"
#import "UIImage+Retina4.h"

@implementation TipViewController
@synthesize aView,aView2;

/*
// The designated initializer. Override to perform setup that is required before the view is loaded.
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
    if (self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]) {
        // Custom initialization
    }
    return self;
}
*/

/*
// Implement loadView to create a view hierarchy programmatically, without using a nib.
- (void)loadView{
}
*/


// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad 
{
    [super viewDidLoad];
	[self setTitle:@"Tips"];
	if ([[self.navigationController viewControllers] count]==1)
	{
		[self hideBackButton];
	}
	tipScrollView=[self CreateScrollViewWithFrame:CGRectMake(10,15,300,295) ContentSize:CGSizeMake(300,1500) VscrollVisibled:YES HscrollVisibled:NO WillBounce:YES];
	//tipScrollView.backgroundColor=[UIColor redColor];
	[self.view addSubview:tipScrollView];
	[self createTip1];
	[self createTip2];
				   
	
	tip1button = [[UIButton alloc] initWithFrame:CGRectMake(10.0, START_Y+275.0, 300.0, 45.0)];
	tip1button.titleLabel.lineBreakMode = UILineBreakModeTailTruncation;
	[tip1button.titleLabel setFont:[UIFont boldSystemFontOfSize:15]];
	[tip1button setBackgroundImage:[UIImage imageNamed:@"addBg-Area.png"] forState:UIControlStateNormal];
	[tip1button setBackgroundImage:[UIImage imageNamed:@"addBg-Area.png"] forState:UIControlStateHighlighted];
	[tip1button addTarget:self action:@selector(tip1button:) forControlEvents:UIControlEventTouchUpInside];
	[tip1button setTitle:@"How to match soup with wines?" forState:UIControlStateNormal];
	[tip1button setTitleColor:UIColorFromHex(0x452801) forState:UIControlStateNormal];
	[self.view addSubview:tip1button];
	
}

-(void)tip1button:(id)sender
{   
	
	if(i%2==0)
	{ 
	  [sender setTitle:@"How to speak intelligently on..." forState:UIControlStateNormal];//How to speak intelligently on wine and food pairing?
	  [self.aView removeFromSuperview];
	  [tipScrollView addSubview:self.aView2];
	  [tipScrollView setContentSize:CGSizeMake(300,440)];	
	}
	else 
	{
		[sender setTitle:@"How to match soup with wines?" forState:UIControlStateNormal];
		[self.aView2 removeFromSuperview];
		[tipScrollView addSubview:self.aView];//tipImage1
		[tipScrollView setContentSize:CGSizeMake(300,1500)];
		
	}
   i++;
	
		
}
/*
// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}
*/

-(UIScrollView *)CreateScrollViewWithFrame:(CGRect)frame
                               ContentSize:(CGSize)size
						   VscrollVisibled:(BOOL)visible
						   HscrollVisibled:(BOOL)Visible
								WillBounce:(BOOL)no
{
	
	UIScrollView *ScrollView=[[UIScrollView alloc] initWithFrame:frame];
	
    //Set the Properties of ListContainerView
	ScrollView.contentSize =size;
	ScrollView.scrollEnabled=YES;
	ScrollView.showsVerticalScrollIndicator=visible;
	ScrollView.showsHorizontalScrollIndicator=Visible;
	[ScrollView flashScrollIndicators];
	ScrollView.bounces =no;
	[ScrollView autoresizesSubviews];
	
	return ScrollView;
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning]; // Releases the view if it doesn't have a superview 
}

-(void)createTip1
{
	self.aView=[[UIView alloc]initWithFrame:CGRectMake(0,10,300,1000)];
	NSInteger yPos;
	NSString *tempStr=@"";
	yPos=0;
	tempStr=@"How to speak intelligently on wine and food pairing?";
	UILabel *lbl=[self createLabelWithText:tempStr withYPos:0 withHeight:45 textIsBold:YES];
	//lbl.backgroundColor=[UIColor greenColor];
	[self.aView addSubview:lbl];
	
    yPos=yPos+45.0;
	
	tempStr=@"Matching food and wine is an attempt to put flavours of wine and food together. Therefore, the most important consideration is simply, a familiarity with flavours. The following is a step-by-step guide to begin your own wine and food pairing adventure.";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:140 textIsBold:NO];
	//lbl.backgroundColor=[UIColor redColor];
	[self.aView addSubview:lbl];
	
	yPos=yPos+140.0;
	tempStr=@"Step 1";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:20 textIsBold:YES];
	//lbl.backgroundColor=[UIColor greenColor];
	[self.aView addSubview:lbl];
	
	 
	yPos=yPos+20.0;
	tempStr=@"Consider the overall and dominant flavours of food. The information will help to determine wines with certain comparable or complementary flavours and structure with the dish. Wine Tip: What are the dominant flavours (sweet, spicy, salty, etc.) of the dish;and what is the texture like (crispy, delicate, etc.)?";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:160 textIsBold:NO];
	//lbl.backgroundColor=[UIColor redColor];
	[self.aView addSubview:lbl];
		
	yPos=yPos+160.0;
	tempStr=@"Step 2";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:20 textIsBold:YES];
	//lbl.backgroundColor=[UIColor greenColor];
	[self.aView addSubview:lbl];
	
	yPos=yPos+20.0;
	tempStr=@"Consider the ingredients used, as well as how they are being prepared before cooking. For example, beef is different in texture and taste as compared to fish. For instance, sliced beef is lighter in body and structure than a cut of the same meat.";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:140 textIsBold:NO];
	//lbl.backgroundColor=[UIColor redColor];
	[self.aView addSubview:lbl];
	
	yPos=yPos+140.0;
	tempStr=@"Step 3";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:20 textIsBold:YES];
	//lbl.backgroundColor=[UIColor greenColor];
	[self.aView addSubview:lbl];
	
	yPos=yPos+20.0;
	tempStr=@"Consider the cooking method of the dish. For example, a deep fried fish and a steamed dish will have different texture and taste.";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:80 textIsBold:NO];
	//lbl.backgroundColor=[UIColor redColor];
	[self.aView addSubview:lbl];
	
	yPos=yPos+80.0;
	tempStr=@"Step 4";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:20 textIsBold:YES];
	//lbl.backgroundColor=[UIColor greenColor];
	[self.aView addSubview:lbl];
	
	yPos=yPos+20.0;
	tempStr=@"Consider the sauces or condiments that come with the dish. Sauces with strong taste can mask the true taste of the dish and become the dominant flavour e.g. chilli or black pepper crab. Some strong condiments like garlic, cinnamon, onion, chilli, etc. when used directly or indirectly on the dishes can dominate the dish’s flavours. Hence, these taste elements should be considered when choosing wines to match.";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:220 textIsBold:NO];
	//lbl.backgroundColor=[UIColor redColor];
	[self.aView addSubview:lbl];
	
	yPos=yPos+220.0;
	tempStr=@"Step 5";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:20 textIsBold:YES];
	//lbl.backgroundColor=[UIColor greenColor];
	[self.aView addSubview:lbl];
	
	yPos=yPos+20.0;
	tempStr=@"Consider the range of wines that you can select. Look at the entire wine group, and not just white or red wines. In wine classification, there are dry white, aromatic white, sweet white, dry red (light to full bodied types), sweet red, sparkling wines (from light and easy to full and prestigious Champagne), and even fortified wines such as Port (a sweet red wine) and Sherry (a dry white wine).";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:220 textIsBold:NO];
	//lbl.backgroundColor=[UIColor redColor];
	[self.aView addSubview:lbl];
	
	yPos=yPos+220.0;
	tempStr=@"Step 6";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:20 textIsBold:YES];
	//lbl.backgroundColor=[UIColor greenColor];
	[self.aView addSubview:lbl];
	
	yPos=yPos+20.0;
	tempStr=@"Consider wine types that suit the dish that you want to pair. For example, in complementing a lightly prepared (steamed method) seafood dish with salty sauces and delicate texture; a white wine with equally light intensity in flavour and body, with a little fruit note will be a good match (such as the Sauvignon Blanc or Riesling). If a red wine is preferred, try a Pinot Noir or a light and easy Sparkling wine. Wine Tip: As flavours can be subjective and differing preferences can be expected, it is best to consider 3 different wine types to offer the best matching choices (2 types do not a choice make, and anything more than 3 will be confusing).";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:330 textIsBold:NO];
	//lbl.backgroundColor=[UIColor redColor];
	[self.aView addSubview:lbl];
	
	 [tipScrollView addSubview:self.aView];
	
		
}

-(void)createTip2
{
	self.aView2=[[UIView alloc]initWithFrame:CGRectMake(0,10,300,440)];
	NSInteger yPos;
	NSString *tempStr=@"";
	yPos=0;
	tempStr=@"How to match soup with wines?";
	UILabel *lbl=[self createLabelWithText:tempStr withYPos:5 withHeight:20 textIsBold:YES];
	//lbl.backgroundColor=[UIColor greenColor];
	[self.aView2 addSubview:lbl];
	
	yPos=yPos+25.0;
	tempStr=@"The most important principle to matching soup and wine is to visualise the soup dish as a food item, then the approach will be much similar to food matching and become less daunting.\n\nTo test this method out, allow flavours to develop on your palate e.g. try a sufficient portion of soup, then sip your preferred wine to determine if it matches well with it.\n\nWine Tip: The hotter the temperature of the soup, the more time you’ll need before serving a suitable wine. Therefore, allow time for flavours to develop and then, serve the wines (especially white or sparkling wines) later.";
	lbl=[self createLabelWithText:tempStr withYPos:yPos withHeight:380 textIsBold:NO];
	//lbl.backgroundColor=[UIColor greenColor];
	[self.aView2 addSubview:lbl];
	
	
	
	//[tipScrollView addSubview:self.aView2];
	
	
}


-(UILabel*)createLabelWithText:(NSString*)strText withYPos:(CGFloat)y withHeight:(CGFloat)h textIsBold:(BOOL)flg
{
	UILabel* lbl = [[UILabel alloc] initWithFrame:CGRectMake(15.0,y,270.0,h)];
	lbl.numberOfLines=0;
	lbl.backgroundColor=[UIColor clearColor];
	[lbl setText:strText];
	lbl.textAlignment = UITextAlignmentLeft;
	lbl.textColor = UIColorFromHex(0x452801);
	if(flg)
	  lbl.font = [UIFont boldSystemFontOfSize:17.0];
	else
	  lbl.font = [UIFont systemFontOfSize:15.0];	
	
	return lbl;
	
}






@end
