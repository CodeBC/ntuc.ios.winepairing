//
//  ViewController.h
//  FairPrice
//
//  Created by ddseah on 7/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@interface JustWineClubViewController : BaseViewController<UIWebViewDelegate>

@property(nonatomic,strong) UIWebView *DesWebView;

-(void)createButtons;
-(void)createImages;
-(void)createText;
@end
