//
//  ViewController.m
//  FairPrice
//
//  Created by ddseah on 7/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JustWineClubViewController.h"
#import "JustWineEmailViewController.h"
#import "Constants.h"


@interface JustWineClubViewController ()

@end

@implementation JustWineClubViewController

@synthesize DesWebView;

//- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
//{
//    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
//    if (self) {
//        // Custom initialization
//    }
//    return self;
//}

- (void)viewDidLoad
{
    self.title = @"JUST WINE CLUB";
    [super viewDidLoad];
    
    self.DesWebView = [[UIWebView alloc] init];
    
    [self createImages];
    [self createText];
    [self createButtons];
}

-(void)viewWillAppear:(BOOL)animated{
    NSLog(@"navigationControl count = %d",[[self.navigationController viewControllers] count]);
    [super viewWillAppear:animated];
    if ([[self.navigationController viewControllers] count]==1){
		[self hideBackButton];
	}
}

-(void)createNavigationMenu{
    [self setTitleText:self.title];
    [super createNavigationMenu];
}

-(void)createImages{
    UIImageView* mainImg = [[UIImageView alloc]initWithFrame:CGRectMake(70.0, START_Y+85.0, 256.0, 120.0)];
	[mainImg setImage:[UIImage imageNamed:@"img_just_wine_club_card.png"]];
	[self.view addSubview:mainImg];
}

-(void)createText{
    /*NSString *dataDescription1 = @"<ul><li>Sign up and get 8% discount on wine*</li>";
    NSString *dataDescription2 = @"<li>News, updates and a personalised Just Wine Club membership card</li>";
    NSString *dataDescription3 = @"<p>*Exclude Japanese and Chinese wines, liquor, beer and other alcoholic drinks.</p>";*/
    NSString *dataDescription1 = @"<ul><li>Exclusive 8%* discount on wines** at FairPrice supermarkets, FairPrice. Finest, FairPrice Xtra and FairPrice Online</li>";
    NSString *dataDescription2 = @"<li>Invitations to private wine sales and wine dinners</li>";
    NSString *dataDescription3 = @"<p>*Exclude Japanese and Chinese wines, liquor, beer and other alcoholic drinks.</p>";
	
	NSString *strText= [NSString stringWithFormat:@"%@%@",dataDescription1,dataDescription2];
    
    NSLog(@"HTML code = %@",strText);
    
    //	self.DesWebView.frame = CGRectMake(10.0, START_Y+85.0, 156.0, 220.0);
	self.DesWebView.frame = CGRectMake(-10.0, START_Y+205.0, 310.0, 110.0);
    //    self.DesWebView.scrollView.frame = CGRectMake(20.0, START_Y+75.0, 146.0, 170.0);
    //	self.DesWebView.frame = CGRectMake(20.0, START_Y+75.0, 290.0, 170.0);
	[self.DesWebView setOpaque:NO];
    [[self.DesWebView scrollView]setBounces:NO];
    [[self.DesWebView scrollView]setScrollEnabled:NO];
    [[self.DesWebView scrollView] setShowsVerticalScrollIndicator:NO];
	[self.DesWebView loadHTMLString:[NSString stringWithFormat:@"<html><body><font face=\"EuphemiaUCAS\" size=\"2\" color='#ffffff'> %@ </font><font face=\"EuphemiaUCAS\" size=\"1\" color='#ffffff'> %@ </font></body></html>",strText,dataDescription3] baseURL:nil];
	self.DesWebView.backgroundColor = [UIColor clearColor];
	self.DesWebView.scalesPageToFit = NO;
	self.DesWebView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
	self.DesWebView.delegate = self;
    
	[self.view addSubview: self.DesWebView];
}

-(void)createButtons{
//    UIButton *fairPriceButton = [[UIButton alloc] initWithFrame:CGRectMake(10.0 , START_Y+300.0, 65.0, 40.0)];
//	fairPriceButton.tag=1;
//	//[fairPriceButton setTitle:@"Fairprice Best Buy" forState:UIControlStateNormal];
//	//	[fairPriceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//	[fairPriceButton addTarget:self action:@selector(onClickSignIn:) forControlEvents:UIControlEventTouchUpInside];
//	[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"but_signin.png"] forState:UIControlStateNormal];
//	[self.view addSubview:fairPriceButton];
//	[fairPriceButton release];
        
    //UIButton *fairPriceButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width/2 , START_Y+320.0, 65.0, 40.0)];
    UIButton *fairPriceButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [fairPriceButton setFrame:CGRectMake(self.view.frame.size.width/2 - fairPriceButton.frame.size.width/2 , START_Y+320.0+50, 65.0, 40.0)];
	fairPriceButton.tag=1;
	[fairPriceButton addTarget:self action:@selector(onClickRegister:) forControlEvents:UIControlEventTouchUpInside];
	//[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"but_register.png"] forState:UIControlStateNormal];
    [fairPriceButton setImage:[UIImage imageNamed:@"but_register.png"]  forState:UIControlStateNormal];
	[self.view addSubview:fairPriceButton];
    [self.view bringSubviewToFront:fairPriceButton];
    
    /*fairPriceButton = [[UIButton alloc] initWithFrame:CGRectMake(85.0 , START_Y+320.0, 150.0, 40.0)];
	fairPriceButton.tag=1;
	[fairPriceButton addTarget:self action:@selector(onClickMailForm:) forControlEvents:UIControlEventTouchUpInside];
	[fairPriceButton setBackgroundImage:[UIImage imageNamed:@"but_email_membership.png"] forState:UIControlStateNormal];
	[self.view addSubview:fairPriceButton];*/
}

-(void)onClickRegister:(id)sender{
    NSLog(@"on register!!!");
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"https://www.fairprice.com.sg/webapp/wcs/stores/servlet/LogonForm?langId=-1&storeId=90001&catalogId=10052&krypto=TwbyPAPczMVWVYQiQaAEkG4ZxCDNXHQrvrkQHrOqSiM%3D"]];
}

-(IBAction)onClickMailForm:(id)sender{
    JustWineEmailViewController *vcJustWineEmail = [[JustWineEmailViewController alloc]init];
    [self.navigationController pushViewController:vcJustWineEmail animated:YES];
//    [[UIApplication sharedApplication] openURL:[NSURL URLWithString: @"http://www.google.co.uk"]];
}

#pragma mark-
#pragma mark UIWebView delegate methods

- (void)webViewDidStartLoad:(UIWebView *)webView
{
	// starting the load, show the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
	// finished loading, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
	// load error, hide the activity indicator in the status bar
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	// report the error inside the webview
	//NSString* errorString = [NSString stringWithFormat:
	//							 @"<html><center><font size=+5 color='red'>An error occurred:<br>%@</font></center></html>",
	//							 error.localizedDescription];
	//	[myWebView loadHTMLString:errorString baseURL:nil];
}


- (void)viewDidUnload
{
    [super viewDidUnload];
    self.DesWebView = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
