//
//  WineListSearchViewController.h
//  FairPrice
//
//  Created by niladri.schatterjee on 10/18/10.
//  Copyright 2010 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"
#import "Constants.h"


@interface WineListSearchViewController : BaseViewController <UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource,UIActionSheetDelegate>
{
         
	//UITextField* txtSearch;
	NSMutableArray* arrWineType;
	NSMutableArray* arrCountry;
	NSMutableArray* arrRating;
	NSMutableArray* arrPrice;
	
	NSString* strPickerValue;
	
    NSInteger requestFromPage;
    
	UIPickerView*  picker_View;
	UILabel* lbl1;
	NSMutableArray* arrLbl;
	int selIndex;
	UIButton* btDone;
	NSInteger priceIndex;
	UIActionSheet *pickerActionsheet;
	//UIImageView* aView;
	int pickerid;
	
}
@property(nonatomic,assign) int selIndex;
@property(nonatomic,assign) NSInteger requestFromPage;
@property(nonatomic,strong) NSMutableArray* arrLbl;
@property(nonatomic,strong) UILabel* lbl1;
@property(nonatomic,strong) UIPickerView* picker_View;
//@property(nonatomic,retain) UITextField* txtSearch;
@property(nonatomic,strong) NSMutableArray* arrWineType;
@property(nonatomic,strong) NSMutableArray* arrCountry;
@property(nonatomic,strong) NSMutableArray* arrRating;
@property(nonatomic,strong) NSMutableArray* arrPrice;
@property(nonatomic,strong) NSString* strPickerValue;
@property(nonatomic,strong) UIButton* btDone;
@property(nonatomic,assign) NSInteger priceIndex;
@property(nonatomic,assign) NSInteger countryIndex;
@property(nonatomic,assign) NSInteger wineIndex;
@property(nonatomic,assign) int pickerid;

//@property(nonatomic,assign) UIImageView* aView;

//-(void)createSearchBar;
-(void)createDoneButton;
-(void)clickSBarButton:(id)sender;
-(void)createPickerView:(int)index;
-(void)createMenu;
-(void)addWine;
-(void)addPrice;
-(void)showPickerWithTag:(int)index;
@end
