//
//  FoodForWineViewController.h
//  FairPrice
//
//  Created by Tridip Sarkar on 21/10/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

@class FairPriceAppDelegate;
@interface FoodForWineViewController : BaseViewController<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>
{
	UITextField* txtSearch;
	UITableView* tblWineForFood;
	NSIndexPath* celIndex;
	NSMutableArray *arrWineType;
	NSMutableArray *arrTemp;
	UILabel *searchResultLabel;
}

@property(nonatomic,strong) NSIndexPath* celIndex;
@property(nonatomic,strong) UITextField* txtSearch;
@property(nonatomic,strong) UITableView* tblWineForFood;
@property(nonatomic,strong) NSMutableArray *arrWineType;
@property(nonatomic,strong) NSMutableArray *arrTemp;
@property(nonatomic,strong) UILabel *searchResultLabel;
-(void)createNavigationMenu;
-(void)createIntro;
-(void)createSearchBar;
-(void)createTable;

-(void)cellAdjustForIndexPath:(NSIndexPath*)indexPath isDeleteImage:(BOOL)flag;

@end
