//
//  MapViewController.h
//  FairPrice
//
//  Created by David Seah on 22/5/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "BaseViewController.h"
#import <MapKit/MapKit.h>
@interface MapViewController : BaseViewController<UIWebViewDelegate>

@property (nonatomic,strong)UIWebView *wvMap;

@property (nonatomic,strong)MKMapView * wfMap;

@end
