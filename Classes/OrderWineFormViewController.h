//
//  OrderWineFormViewController.h
//  FairPrice
//
//  Created by ddseah on 8/04/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseViewController.h"

#define KEYBOARD_ANIMATION_DURATION 0.3
//static const CGFloat MINIMUM_SCROLL_FRACTION = 0.2;
//static const CGFloat MAXIMUM_SCROLL_FRACTION = 0.8;
#define MINIMUM_SCROLL_FRACTION 0.2
#define MAXIMUM_SCROLL_FRACTION 0.8
#define PORTRAIT_KEYBOARD_HEIGHT 216
#define LANDSCAPE_KEYBOARD_HEIGHT 162

@interface OrderWineFormViewController : BaseViewController<UITextFieldDelegate,UIScrollViewDelegate,UIPickerViewDelegate,UIPickerViewDataSource>

@property(nonatomic, strong) UIScrollView *svMain;
@property(nonatomic, strong) UIPickerView *pvMain;

@property(nonatomic, strong) UIButton *btDone;
@property(nonatomic, strong) UIActionSheet *asPicker;

@property(nonatomic, strong) UITextField *txtField1;
@property(nonatomic, strong) UITextField *txtField2;
@property(nonatomic, strong) UITextField *txtField3;
@property(nonatomic, strong) UITextField *txtField4;
@property(nonatomic, strong) UITextField *txtField5;
@property(nonatomic, strong) UITextField *txtField6;

@property(nonatomic, strong) NSMutableArray *arrWine;
@property(nonatomic, strong) NSMutableArray *arrNoOfCrates;
@property(nonatomic, strong) NSMutableDictionary *dictAllInfo;

@property(nonatomic, assign) int pickerID;
@property(nonatomic, assign) int selIndex;
@property(nonatomic, assign) int wineIndex;
@property(nonatomic, assign) int crateIndex;

@property(nonatomic, strong) id btnSender;
//@property(nonatomic, retain) UITextField *txtField7;
//@property(nonatomic, retain) UITextField *txtField8;

-(void)createScrollView;
-(void)createNavigationMenu;
-(void)createSubmitButton;
-(void)createWineOrderMenu;
-(void)createTextLabels;
-(void)createTextLabels2;
-(void)createBlanks;
-(void)createBlanks2;
-(void)createTextFields;
-(void)createTextFields2;
-(void)createActionSheet;
-(void)addNoOfCrates;

-(void)showPickerWithTag:(int)index;

//-(NSDictionary *)getDictTextFieldInfo;
-(void)getDictTextFieldInfo;

-(void)resignAllResponders;

@end
