//
//  DataModel.m
//  FairPrice
//
//  Created by Satish Kumar on 10/7/10.
//  Copyright 2010 Web Spiders (India) Pvt. Ltd. All rights reserved.
//

#import "DataModel.h"
#import "Constants.h"
#import "WineItem.h"
#import "WineGroupItem.h"
#import "GroupDishItem.h"
#import "DishItem.h"
#import "WineInfo.h"
#import "WineBuyInfo.h"
#import "DishInfo.h"
#import "WineType.h"
#import "PriceRange.h"
#import "Country.h"
#import "WineGroupType.h"
#import "TipsVideos.h"
#import "TipsArticles.h"
#import "TipsArticleItem.h"
#import "TipsVideoItem.h"
#import "Status.h"
#import "FairPriceAppDelegate.h"

@implementation DataModel

@synthesize aQueue, networkErrMsg,selectedTabIndex,REQUEST_ID,arrWine,arrGroupWine,arrGroupDish,arrDishItem,arrWineInfo,arrWineBuyInfo,arrDishInfo,arrWineType,arrPriceRange,arrCountry,arrWineGroupType,arrTipsArticles,arrTipsVideos,arrTipsArticleItem,arrTipsVideoItem,arrStatus,maxRating,responseStr;
-(id)init
{
	if(self = [super init])
	{
		selectedTabIndex = 1;
        
	}
	return self;
}

-(BOOL)startRequest:(int) aRequest
{
	REQUEST_ID = aRequest;
    
	switch (aRequest)
	{
		case REQUEST_GROUP_DISH:
            return [self loadGroupDishList:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_food_category.php?mode=M",BASEURL]]];
			break;
		default:
			break;
	}
	
	return YES;
}


-(BOOL)startRequestWithParameters:(int)aRequest WithParams:(NSMutableDictionary*) aParam
{
    NSLog(@"base url!!! %@",BASEURL);
	switch(aRequest)
	{
		case REQUEST_GROUP_DISH:
		{   //NSArray* keys = [aParam allKeys];
			//NSString *resultObj=[aParam objectForKey:[keys objectAtIndex:0]];
			//http://192.168.0.12/fairprice/GetGroupDishListXML.php
            
            //			return [self loadGroupDishList:[self sendRequest:[NSString stringWithFormat:@"%@GetGroupDishListXML.php",BASEURL]]];
            
            return [self loadGroupDishList:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_food_category.php?mode=M",BASEURL]]];
            
			break;
		}
		case REQUEST_WINETYPE:
		{
            //http://192.168.0.12/fairprice/GetWineTypeXML.php
			
            //			return [self loadWineByType:[self sendRequest:[NSString stringWithFormat:@"%@%@",BASEURL,@"GetWineTypeXML.php"]]];
            
            return [self loadWineByType:[self sendRequest:[NSString stringWithFormat:@"%@%@",BASEURL,@"modules/foodwine/_read_wine_type.php?mode=M"]]];
			break;
			
			
		}
		case REQUEST_FULLWINE_LIST:
		{ //http://192.168.0.12/fairprice/GetWineListXML.php
			return [self loadWine:[self sendRequest:[NSString stringWithFormat:@"%@%@",BASEURL,@"GetWineListXML.php"]]];
			break;
		}
		case REQUEST_GROUP_WINE:
		{   
			NSArray* keys = [aParam allKeys];
			NSString *resultObj=aParam[keys[0]];
			//http://192.168.0.12/fairprice/GetGroupWineListXML.php?WineType=Red
            //			return [self loadWineBasedOnGroups:[self sendRequest:[NSString stringWithFormat:@"%@GetGroupWineListXML.php?WineType=%@",BASEURL,resultObj]]];
            return [self loadWineBasedOnGroups:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_winefood.php?winetype_id=%@&mode=M",BASEURL,resultObj]] withParent:resultObj];
		    resultObj=nil;
			break;
		}
            
		case REQUEST_DISH_LIST:
		{   
			NSArray* keys = [aParam allKeys];
			NSString *resultObj=aParam[keys[0]];
			//http://192.168.0.12/fairprice/GetDishListXML.php?DishGroupID=5
            //			return [self loadDishList:[self sendRequest:[NSString stringWithFormat:@"%@GetDishListXML.php?DishGroupID=%@",BASEURL,resultObj]]];
            return [self loadDishList:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_foodwine.php?mode=M&foodcategory_id=%@",BASEURL,resultObj]] withParent:resultObj];
			resultObj=nil;
			break;
		}
		case REQUEST_WINE_INFO:
		{   
			NSArray* keys = [aParam allKeys];
			NSString *resultObj=aParam[keys[0]];
			//http://192.168.0.12/fairprice/GetWineGroupInfoXML.php?WineGroupID=10
			return [self loadWineGroupInfo:[self sendRequest:[NSString stringWithFormat:@"%@GetWineGroupInfoXML.php?WineGroupID=%@",BASEURL,resultObj]]];
		    resultObj=nil;
			break;
		}
		case REQUEST_WINE_BUYINFO:
		{   
			NSArray* keys = [aParam allKeys];
			NSString *resultObj=aParam[keys[0]];
			//http://192.168.0.12/fairprice/GetWineBuyInfoXML.php?WineID=11023494
			return [self loadWineBuyInfo:[self sendRequest:[NSString stringWithFormat:@"%@GetWineBuyInfoXML.php?WineID=%@",BASEURL,resultObj]]];
            //            return [self loadWineBasedOnGroups:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_winefood.php?winetype_id=%@&mode=M",BASEURL,resultObj]]];
		    resultObj=nil;
			break;
		}
		case REQUEST_DISH_INFO:
		{   
			NSArray* keys = [aParam allKeys];
			NSString *resultObj=aParam[keys[0]];
			//http://192.168.0.12/fairprice/GetDishInfoXML.php?DishID=23
            //			return [self loadDishInfo:[self sendRequest:[NSString stringWithFormat:@"%@GetDishInfoXML.php?DishID=%@",BASEURL,resultObj]]];
			return [self loadDishInfo:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_food.php?food_id=%@&mode=M",BASEURL,resultObj]] withParent:resultObj];
            
		    resultObj=nil;
			break;
		}
	    case REQUEST_SEARCH_FILTER:	
		{   
			//NSArray* keys = [aParam allKeys];
			//NSString *resultObj=[aParam objectForKey:[keys objectAtIndex:0]];
			//http://192.168.0.12/fairprice/GetSearchFilterXML.php
            //			return [self loadSearchFilter:[self sendRequest:[NSString stringWithFormat:@"%@GetSearchFilterXML.php",BASEURL]]];
            return [self loadSearchFilter:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_country.php?mode=M",BASEURL]]];
		    //resultObj=nil;
			break;
		}
        case REQUEST_SEARCH_FILTER_2:	
		{   
            return [self loadSearchFilter:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_wine_filter.php?mode=M",BASEURL]]];
			break;
		}
		case REQUEST_SEARCH_RESULT:
		{  
			//NSArray* keys = [aParam allKeys];
            
			NSString *wineType=aParam[@"WineType"];
			NSString *priceRange=aParam[@"PriceRange"];
			NSString *rating=aParam[@"Rating"];
			NSString *countryTitle=aParam[@"CountryTitle"];
            
            if([wineType isEqualToString: @"All"]){
                wineType=@"";
            }
            if([priceRange isEqualToString: @"All"]){
                priceRange=@"";
            }
            if([rating isEqualToString: @"All"]){
                rating=@"";
            }
            if([countryTitle isEqualToString: @"All"]){
                countryTitle=@"";
            }
            
			countryTitle=(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)countryTitle,NULL,(CFStringRef)@"!*’();:@&=+$,/?%#[]",kCFStringEncodingUTF8 ));
			//http://192.168.0.12/fairprice/GetWineListXML.php?Type=Red&Min=10&Max=41&Rating=4&Country=Australia
            //			return [self loadWine:[self sendRequest:[NSString stringWithFormat:@"%@GetWineListXML.php?Type=%@&Min=%@&Max=%@&Rating=%@&Country=%@",BASEURL,wineType,minPrice,maxPrice,rating,countryTitle]]];
            //param to handle 6 
            //            return [self loadWine:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_wine.php?mode=M&wine_id=&winetype_id=& wine_country=%@&wine_type=%@&wine_rating=%@&price_range=%@&all=Y",BASEURL,countryTitle,wineType,rating,priceRange]]];
            
            return [self loadWine:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_wine.php?mode=M&wine_id=&winetype_id=&wine_country=%@&wine_type=%@&wine_rating=%@&price_range=%@&all=Y",BASEURL,countryTitle,wineType,rating,priceRange]]];
            
		    wineType=nil;
			rating=nil;
            priceRange=nil;
			countryTitle=nil;
			break;
	   	}
		case REQUEST_SELECTED_GROUPWINE:
        {
			
			NSArray* keys = [aParam allKeys];
			NSString *resultObj=aParam[keys[0]];
			//http://192.168.0.12/fairprice/GetWineListXML.php?WineGroup=1
			return [self loadWine:[self sendRequest:[NSString stringWithFormat:@"%@GetWineListXML.php?WineGroup=%@",BASEURL,resultObj]]];
		    resultObj=nil;
			break;	
			
		}
		case REQUEST_ADDDISH:
		{
			//NSArray* keys = [aParam allKeys];
			NSString *name=aParam[@"Name"];
			name=(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)name,NULL,(CFStringRef)@"!*’();:@&=+$,/?%#[]",kCFStringEncodingUTF8 ));
			NSString *email=aParam[@"Email"];
			NSString *suggestion=aParam[@"Suggestion"];
			suggestion=(NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)suggestion,NULL,(CFStringRef)@"!*’();:@&=+$,/?%#[]",kCFStringEncodingUTF8));
			NSString *type=aParam[@"Type"];
		    
			//http://192.168.0.12/fairprice/AddSuggestionXML.php?vName=debu&vEmail=debottam.bhatt@webspiders.com&vSuggestion=test%20suggestion&vType=dish
//			return [self getResponse:[self sendRequest:[NSString stringWithFormat:@"%@AddSuggestionXML.php?vName=%@&vEmail=%@&vSuggestion=%@&vType=%@",BASEURL,name,email,suggestion,type]]];
            return [self getResponse:[self sendRequest:[NSString stringWithFormat:@"%@modules/suggestion/_insert_suggestwine.php?suggestwine_name=%@&suggestwine_email=%@&suggestwine_wine=%@&suggestwine_type=%@&mode=M",BASEURL,name,email,suggestion,type]]];

		    name=nil;
			email=nil;
			suggestion=nil;
			type=nil;
			break;
            
		}
		case REQUEST_SUBMITRATING:{
			NSString *rating=aParam[@"Rating"];
			NSString *wineID=aParam[@"WineID"];
			//http://mars.webspiders.com/faireprice/AddWineRatingXML.php?Rating=3&WineID=11023494
            //			return [self getResponse:[self sendRequest:[NSString stringWithFormat:@"%@AddWineRatingXML.php?Rating=%@&WineID=%@",BASEURL,rating,wineID]]];
            return [self getResponseRating:[self sendRequest:[NSString stringWithFormat:@"%@modules/rating/_update_rating.php?wine_id=%@&rating=%@",BASEURL,rating,wineID]]];
            
            
		    rating=nil;
			wineID=nil;
			break;
		}
        case REQUEST_GET_ALL_TIPS:{
            return [self loadTipsArticle:[self sendRequest:[NSString stringWithFormat:@"%@modules/tipsvideos/_read_tips.php?mode=M",BASEURL]]];
            break;
        }
        case REQUEST_GET_ALL_TIPS_VIDEOS:{
            return [self loadTipsVideos:[self sendRequest:[NSString stringWithFormat:@"%@modules/tipsvideos/_read_videos.php?mode=M",BASEURL]]];
            break;
        }
        case REQUEST_GET_INDIV_TIPS:{
            NSString *tipsID = aParam[@"TipsContentID"];
            return [self loadTipsArticleItem:[self sendRequest:[NSString stringWithFormat:@"%@modules/tipsvideos/_read_tips_article.php?content_id=%@&mode=M",BASEURL,tipsID]]];
            break;
        }
        case REQUEST_GET_INDIV_TIPS_VIDEOS:{
            NSString *tipsID = aParam[@"TipsContentID"];
            return [self loadTipsVideoItem:[self sendRequest:[NSString stringWithFormat:@"%@modules/tipsvideos/_read_video_content.php?content_id=%@&mode=M",BASEURL,tipsID]]];
            break;
        }
        case REQUEST_ALL_WINES:
		{  
            return [self loadWine:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_wine.php?mode=M&wine_id=&winetype_id=&wine_country=&wine_type=&wine_rating=&price_range=&all=Y",BASEURL]]];
            
			break;
	   	}
        case REQUEST_SUBMIT_ORDER:
		{  
            //            NSArray* keys = [aParam allKeys];
            
            UITextField *firstname=aParam[@"Firstname"];
            UITextField *surname=aParam[@"Surname"];
            UITextField *email=aParam[@"Email"];
            UITextField *mobile=aParam[@"Mobile"];
            UITextField *address=aParam[@"Address"];
            UITextField *justWineMemberNo=aParam[@"JustWineMemberNo"];
//            WineItem *wineItem1=[aParam objectForKey:@"WineItem1"];
//            WineItem *wineItem2=[aParam objectForKey:@"WineItem2"];
//            WineItem *wineItem3=[aParam objectForKey:@"WineItem3"];
//            WineItem *wineItem4=[aParam objectForKey:@"WineItem4"];
//            WineItem *wineItem5=[aParam objectForKey:@"WineItem5"];
            NSString *estPrice=aParam[@"EstPrice"];
            NSString *savePrice=aParam[@"SavePrice"];
            NSString *discPrice=aParam[@"DiscPrice"];
            
            for(int i=1; i<=5;i++){
                NSLog(@"loop i = %d",i);
                NSString *wineItemKey = [NSString stringWithFormat:@"WineItem%d",i];
                NSString *noCrateKey = [NSString stringWithFormat:@"CrateNo%d",i];
                
                if(aParam[wineItemKey] == nil){
                    WineItem *wineData = [[WineItem alloc]init];
                    wineData.wineID = @"";
                    wineData.wineName = @"";
                    aParam[wineItemKey] = wineData;
                }
                     
                if(aParam[noCrateKey] == nil){
                    aParam[noCrateKey] = @"";
                }                     
            }
            
            WineItem *wineItem1=aParam[@"WineItem1"];
            WineItem *wineItem2=aParam[@"WineItem2"];
            WineItem *wineItem3=aParam[@"WineItem3"];
            WineItem *wineItem4=aParam[@"WineItem4"];
            WineItem *wineItem5=aParam[@"WineItem5"];
            
            NSString *crateNo1=aParam[@"CrateNo1"];
            NSString *crateNo2=aParam[@"CrateNo2"];
            NSString *crateNo3=aParam[@"CrateNo3"];
            NSString *crateNo4=aParam[@"CrateNo4"];
            NSString *crateNo5=aParam[@"CrateNo5"];

            //            if(justWineMemberNo.text == nil){
            //                justWineMemberNo.text=@"";
            //            }
            //            
            //            if([wineType isEqualToString: @"All"]){
            //                wineType=@"";
            //            }
            //            if([priceRange isEqualToString: @"All"]){
            //                priceRange=@"";
            //            }
            //            if([rating isEqualToString: @"All"]){
            //                rating=@"";
            //            }
            //            if([countryTitle isEqualToString: @"All"]){
            //                countryTitle=@"";
            //            }
            //            
            //			countryTitle=(NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,(CFStringRef)countryTitle,NULL,(CFStringRef)@"!*’();:@&=+$,/?%#[]",kCFStringEncodingUTF8 );
			//http://192.168.0.12/fairprice/GetWineListXML.php?Type=Red&Min=10&Max=41&Rating=4&Country=Australia
            //			return [self loadWine:[self sendRequest:[NSString stringWithFormat:@"%@GetWineListXML.php?Type=%@&Min=%@&Max=%@&Rating=%@&Country=%@",BASEURL,wineType,minPrice,maxPrice,rating,countryTitle]]];
            //param to handle 6 
            //            return [self loadWine:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_wine.php?mode=M&wine_id=&winetype_id=& wine_country=%@&wine_type=%@&wine_rating=%@&price_range=%@&all=Y",BASEURL,countryTitle,wineType,rating,priceRange]]];
            NSString *requestString = [NSString stringWithFormat:@"%@modules/foodwine/_submit_order.php?first_name=%@&surname=%@&email=%@&mobile=%@&address=%@&wine_club=%@&wine_order1=%@&wine_order2=%@&wine_order3=%@&wine_order4=%@&wine_order5=%@&wine_name1=%@&wine_name2=%@&wine_name3=%@&wine_name4=%@&wine_name5=%@&wine_qty1=%@&wine_qty2=%@&wine_qty3=%@&wine_qty4=%@&wine_qty5=%@&est_price=%@&wine_club_saving=%@&total_price=%@&mode=M",BASEURL,firstname.text,
                                       surname.text,
                                       email.text,
                                       mobile.text,
                                       address.text,
                                       justWineMemberNo.text,
                                       wineItem1.wineID,
                                       wineItem2.wineID,
                                       wineItem3.wineID,
                                       wineItem4.wineID,
                                       wineItem5.wineID,
                                       wineItem1.wineName,
                                       wineItem2.wineName,
                                       wineItem3.wineName,
                                       wineItem4.wineName,
                                       wineItem5.wineName,
                                       crateNo1,
                                       crateNo2,
                                       crateNo3,
                                       crateNo4,
                                       crateNo5,
                                       estPrice,
                                       savePrice,
                                       discPrice];
            
            NSString *encodedRequestString =
            [requestString stringByAddingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
            
            return [self loadSubmitOrder:[self sendRequest:encodedRequestString]];
            
            //		    wineType=nil;
            //			rating=nil;
            //            priceRange=nil;
            //			countryTitle=nil;
			break;
	   	}
            
        case REQUEST_ONE_WINE:
		{  
            NSString *wineID = aParam[@"WineID"];
            return [self loadWineAllInfo:[self sendRequest:[NSString stringWithFormat:@"%@modules/foodwine/_read_wine.php?mode=M&wine_id=%@",BASEURL,wineID]] withParent:wineID];
            
			break;
	   	}
        case REQUEST_SEND_EMAIL:
		{  
            NSString *email = aParam[@"Email"];
            return [self getResponseEmail:[self sendRequest:[NSString stringWithFormat:@"%@modules/email/_send_application_form.php?mode=M&strEmail=%@",BASEURL,email]]];
            
			break;
	   	}

            
            
		default:
			break;										
            
	}									
    
	
	return YES;
}

//-(NSString *)urlEncodeUsingEncoding:(NSStringEncoding)encoding {
//    return (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL,
//                                                               (CFStringRef)self,
//                                                               NULL,
//                                                               (CFStringRef)@"!*'\"();:@&=+$,/?%#[]% ",
//                                                               CFStringConvertNSStringEncodingToEncoding(encoding));
//}
-(NSData*)sendRequest:(NSString*) url
{
	NSLog(@"Request url : %@",url);
	NSData* aData = nil;
	self.networkErrMsg=@"";
    /*
     For
     */
	if(url)
	{
        
		ASIHTTPRequest *request=[[ASIHTTPRequest alloc] initWithURL:[NSURL URLWithString:url]];
		[request startSynchronous];
		if ([request error])
		{  
			self.networkErrMsg=[[request error] localizedDescription];
			return nil;
		}
		else{
			aData = [NSData dataWithData:[request responseData]];
        }
		
		
	}
	return aData;
}

-(void)loadWineForFood:(NSData*) aData{
	//parse xml data
	
}

-(BOOL)getResponse:(NSData*)aData
{
	NSLog(@"Response");	
	BOOL retResult=NO;
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
    NSLog(@"xmlParser = %@",_xmlParser);
    
	NSArray *resultNodes = NULL;
	resultNodes = [_xmlParser nodesForXPath:@"//Suggestion/message" error:nil];
	NSLog(@"TOT NODE COUNT: %d",[resultNodes count]);
	
	if([resultNodes count]>0)
	{ 
		NSString *strStatus;
		CXMLNode* nodeMsg = resultNodes[0];
		NSLog(@"Name: %@",[nodeMsg name]);
		NSLog(@"Value: %@",[nodeMsg stringValue]);
		strStatus = [nodeMsg stringValue];
		self.responseStr=strStatus;
		retResult=YES;
	}
	else{
		retResult=NO;
		[self showAlertWithMessage:@"Unable to send suggestion"];
		NSLog(@"Un Success");
		
		
	}
	
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
    //	return retResult;
    return YES;
    
}

-(BOOL)getResponseRating:(NSData*)aData
{
    [self showAlertWithMessage:@"Rating sent! Thank you for your feedback"];
//	NSLog(@"Response");	
//	BOOL retResult=NO;
//	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
//	_xmlParser = [[[CXMLDocument alloc] initWithData:aData options:0 error:nil] autorelease];
//	
//    NSLog(@"xmlParser = %@",_xmlParser);
//    
//	NSArray *resultNodes = NULL;
//	resultNodes = [_xmlParser nodesForXPath:@"" error:nil];
//	
//	if([resultNodes count]>0)
//	{ 
//		NSString *strStatus;
//		CXMLNode* nodeMsg = [resultNodes objectAtIndex:0];
//		NSLog(@"Name: %@",[nodeMsg name]);
//		NSLog(@"Value: %@",[nodeMsg stringValue]);
//		strStatus = [nodeMsg stringValue];
//		self.responseStr=strStatus;
//		retResult=YES;
//	}
//	else{
//		retResult=NO;
//		[self showAlertWithMessage:@"Unable to send suggestion"];
//		NSLog(@"Un Success");
//
//	}
	
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
    //	return retResult;
    return YES;
    
}

-(BOOL)getResponseEmail:(NSData*)aData
{
    [self showAlertWithMessage:@"The membership application form has been emailed to the email address that you provided."];
    //	NSLog(@"Response");	
    //	BOOL retResult=NO;
    //	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    //	_xmlParser = [[[CXMLDocument alloc] initWithData:aData options:0 error:nil] autorelease];
    //	
    //    NSLog(@"xmlParser = %@",_xmlParser);
    //    
    //	NSArray *resultNodes = NULL;
    //	resultNodes = [_xmlParser nodesForXPath:@"" error:nil];
    //	
    //	if([resultNodes count]>0)
    //	{ 
    //		NSString *strStatus;
    //		CXMLNode* nodeMsg = [resultNodes objectAtIndex:0];
    //		NSLog(@"Name: %@",[nodeMsg name]);
    //		NSLog(@"Value: %@",[nodeMsg stringValue]);
    //		strStatus = [nodeMsg stringValue];
    //		self.responseStr=strStatus;
    //		retResult=YES;
    //	}
    //	else{
    //		retResult=NO;
    //		[self showAlertWithMessage:@"Unable to send suggestion"];
    //		NSLog(@"Un Success");
    //
    //	}
	
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
    //	return retResult;
    return YES;
    
}


-(BOOL)loadWineAllInfo:(NSData *)aData withParent:(NSString *)wineId{
    
	BOOL retResult;    
	
    NSLog(@"Parsing wine");	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
//	self.arrWine=[NSMutableArray arrayWithCapacity:10];
    self.arrWineInfo=[NSMutableArray arrayWithCapacity:10];
//    self.arrWineInfo=[NSMutableArray array];
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
//	NSLog(@"xmlParser = %@",_xmlParser);
    
	NSArray *resultNodes = NULL;
	resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
    
	if([resultNodes count]>0)
	{ 
//        self.aQueue = [ASINetworkQueue queue];
//	    [aQueue setDelegate:self];
//		[aQueue setShouldCancelAllRequestsOnFailure:NO];
//		[aQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
        for(CXMLElement *resultElement in resultNodes)
        {
            int counter;
            NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"wineID == %@",wineId];
            //[DishItem truncateAll];
            
            WineItem *wineData;
            wineData = [WineItem MR_findFirstWithPredicate:predicate];
            if ([wineData.wineID isEqualToString:@""]) {
                    wineData = [WineItem createEntity];
            }
           
            for (counter = 0 ; counter < [resultElement childCount] ; counter++)
            {
                CXMLNode *node1 = [resultElement childAtIndex:counter];
                
                if ([[node1 name] isEqualToString:@"wine_id"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineID= [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_name"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineName = [node1 stringValue];
                }                
                if ([[node1 name] isEqualToString:@"wine_price"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.winePrice = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_priceValue"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.winePriceValue = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_taste"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineTaste = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_desc"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineDesc = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_expert"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineExpert = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_type"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineType = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_typevalue"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineTypeValue = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_typevalue"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineTypeValue = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_rating"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineRating = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_country"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineCountry = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_countryvalue"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineCountryValue = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"winetype_id"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineTypeID = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"winetype_name"])
                {
                    NSLog(@"%@:%@",[node1 name],[node1 stringValue]);
                    wineData.wineTypeName = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_image"])
				{
                    NSLog(@"%@:%@ ",[node1 name],[node1 stringValue]);
					ASIHTTPRequest* aRequest = [ASIHTTPRequest requestWithURL:
                                                [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASEIMAGEURL,[node1 stringValue]]]];
                    NSLog(@"aRequest = %@",aRequest.url);
					[aRequest setUsername:@"WineImage"];
					[aRequest setDelegate:wineData];
					[aQueue addOperation:aRequest];
				}
            }
            [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if  (success) NSLog(@"WineItemInfo saved");
            }];
            [self.arrWineInfo addObject:wineData];
            
        }		
		retResult=YES;
	}
	else
	{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"wineID == %@",wineId];
        WineItem *wineData = [WineItem MR_findFirstWithPredicate:predicate];
        if (![wineData.winePrice isEqualToString:@""]) {
            self.arrWineInfo = [WineItem MR_findAllWithPredicate:predicate];
            if (self.arrWineInfo == 0) {
                retResult=NO;
                NSLog(@"Un Success");
                NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No wine found",self.networkErrMsg];
                [self showAlertWithMessage:msgStr];
            }
        }
        else{
            retResult=NO;
            NSLog(@"Un Success");
            NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No wine found",self.networkErrMsg];
            [self showAlertWithMessage:msgStr];
        }
        
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	return retResult;

}

-(BOOL)loadWine:(NSData*)aData
{
	NSLog(@"Parsing wine");	
	//NSString* theString = [[NSString alloc] initWithData:dt encoding:NSUTF8StringEncoding];
	//NSLog(@"OUTPUT:%@",theString);
	BOOL retResult=NO;
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	
	self.arrWine=[NSMutableArray arrayWithCapacity:10];
	
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	NSLog(@"xmlParser = %@",_xmlParser);
	NSArray *resultNodes = NULL;
	resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
    
	if([resultNodes count]>0)
	{
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        [WineItem truncateAll];
        
        for(CXMLElement *resultElement in resultNodes)
        {
            int counter;
            WineItem *wineData = [WineItem MR_createEntity];
            
            for (counter = 0 ; counter < [resultElement childCount] ; counter++)
            {
                CXMLNode *node1 = [resultElement childAtIndex:counter];
                if ([[node1 name] isEqualToString:@"wine_id"])
                {
                    NSLog(@"%@",[node1 stringValue]);
                    wineData.wineID= [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_name"])
                {
                    NSLog(@"%@",[node1 stringValue]);
                    wineData.wineName = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_price"])
                {
                    NSLog(@"%@",[node1 stringValue]);
                    wineData.winePrice = [node1 stringValue];
                }
                if ([[node1 name] isEqualToString:@"wine_priceValue"])
                {
                    NSLog(@"%@",[node1 stringValue]);
                    wineData.winePriceValue = [node1 stringValue];
                }
            }
            [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if  (success) NSLog(@"wineData saved");
            }];
            [self.arrWine addObject:wineData];

        }
        self.arrWine = (NSMutableArray *)[WineItem findAllSortedBy:@"wineName" ascending:YES];
		retResult=YES;
	}
	else
	{
        self.arrWine = (NSMutableArray *)[WineItem findAllSortedBy:@"wineName" ascending:YES];
        if ([self.arrWine count] == 0) {
            retResult=NO;
            NSLog(@"Un Success");
            NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No wine found",self.networkErrMsg];
            [self showAlertWithMessage:msgStr];
        }
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	return retResult;
    
    
}

-(BOOL)loadWineByType:(NSData*)aData
{
	//http://192.168.0.12/fairprice/GetWineTypeXML.php
	BOOL retResult;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Loading WineByType...");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	self.arrWineType=[NSMutableArray array];
    
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
	resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
	NSLog(@"Total count WineType = %d",[resultNodes count]);
	
	if([resultNodes count]>0)
	{  
        
		self.aQueue = [ASINetworkQueue queue];
	    [aQueue setDelegate:self];
		[aQueue setShouldCancelAllRequestsOnFailure:NO];
		[aQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
		//[aQueue setQueueDidFinishSelector:@selector(queueFinished:)];
        NSManagedObjectContext *localContext   = [NSManagedObjectContext MR_contextForCurrentThread];
        [WineType truncateAll]; // Dump the database
		for(CXMLElement *resultElement in resultNodes)
		{
			int counter;
			WineType *wineTypeData = [WineType MR_createEntity];
			
			for (counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
				if ([[node name] isEqualToString:@"winetype_id"])
				{
					NSLog(@"%@: %@",[node name], [node stringValue] );
					wineTypeData.wineTypeId=[node stringValue];
				}
				if ([[node name] isEqualToString:@"winetype_name"])
				{   
					NSLog(@"%@: %@",[node name], [node stringValue] );
					wineTypeData.wineTitle=[node stringValue];
                }
			}
            [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if  (success) NSLog(@"wineTypeData saved");
            }];
			
			[self.arrWineType addObject:wineTypeData];
		}
        self.arrWineType = (NSMutableArray *)[WineType MR_findAllSortedBy:@"wineTitle" ascending:YES];
		retResult=YES;
		
	}
	else
	{
       
        self.arrWineType = (NSMutableArray *)[WineType MR_findAllSortedBy:@"wineTitle" ascending:YES];
        
        if ([self.arrWineType count]==0) {
        retResult=NO;
		NSLog(@"Un Success");
		NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No wine Item found",self.networkErrMsg];
		[self showAlertWithMessage:msgStr];
        }
		
	}
	
	if ([self.arrWineType count]>0) 
	{
		[aQueue go];
	}
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting WineByType");
	return retResult;
}

-(BOOL)loadWineBasedOnGroups:(NSData*)aData withParent:(NSString *)wineGroupId
{
    
	BOOL retResult;
	
	NSLog(@"Parsing wine");	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    self.arrGroupWine=[NSMutableArray array];
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
	NSArray *resultNodes = NULL;
	
	resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
	NSLog(@"Total node count: %d",[resultNodes count]);
	
	if([resultNodes count]>0)
	{ 
		for(CXMLElement *resultElement in resultNodes)
		{
			int counter;
            NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
            //[DishItem truncateAll];
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"groupID == %@",wineGroupId];
            [WineGroupItem MR_deleteAllMatchingPredicate:predicate];
            
			WineGroupItem *wineGroupData = [WineGroupItem MR_createEntity];
			
			for (counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node1 = [resultElement childAtIndex:counter];
                if ([[node1 name] isEqualToString:@"winetype_name"])
				{
                    NSLog(@"%@:%@ ",[node1 name],[node1 stringValue]);
					wineGroupData.groupTitle = [node1 stringValue];
				}
                if ([[node1 name] isEqualToString:@"winetype_desc"])
				{
                    NSLog(@"%@:%@ ",[node1 name],[node1 stringValue]);
					wineGroupData.wineTypeDesc = [node1 stringValue];
				}
                if ([[node1 name] isEqualToString:@"food_names"])
				{
                    NSLog(@"%@:%@ ",[node1 name],[node1 stringValue]);
					wineGroupData.foodNames = [node1 stringValue];
				}
                if ([[node1 name] isEqualToString:@"wine_id"])
				{
                    NSLog(@"%@:%@ ",[node1 name],[node1 stringValue]);
					wineGroupData.wineID = [node1 stringValue];
				}
                if ([[node1 name] isEqualToString:@"wine_name"])
				{
                    NSLog(@"%@:%@ ",[node1 name],[node1 stringValue]);
					wineGroupData.wineName = [node1 stringValue];
				}
                if ([[node1 name] isEqualToString:@"wine_price"])
				{
                    NSLog(@"%@:%@ ",[node1 name],[node1 stringValue]);
					wineGroupData.winePrice = [node1 stringValue];
				}
                if ([[node1 name] isEqualToString:@"wine_rating"])
				{
                    NSLog(@"%@:%@ ",[node1 name],[node1 stringValue]);
					wineGroupData.wineRating = [node1 stringValue];
				}
				if ([[node1 name] isEqualToString:@"wine_country"])
				{
                    NSLog(@"%@:%@ ",[node1 name],[node1 stringValue]);
					wineGroupData.wineCountry= [node1 stringValue];
				}
                if ([[node1 name] isEqualToString:@"wine_image"])
				{
                    NSLog(@"%@:%@ ",[node1 name],[node1 stringValue]);
					ASIHTTPRequest* aRequest = [ASIHTTPRequest requestWithURL:
                                                [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASEIMAGEURL,[node1 stringValue]]]];
					[aRequest setUsername:@"WineImage"];
					[aRequest setDelegate:wineGroupData];
					[aQueue addOperation:aRequest];
                    
				}
                wineGroupData.groupID = wineGroupId;
			}
			NSLog(@"%@: %@", wineGroupData.wineID, wineGroupData.groupTitle);
            [localContext saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if  (success) NSLog(@"wineGroupData saved");
            }];
			[self.arrGroupWine addObject:wineGroupData];
			
		}
        self.arrGroupWine = (NSMutableArray *)[WineGroupItem MR_findAllSortedBy:@"wineName" ascending:YES];
		retResult=YES;
	}
	else
	{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"groupID == %@",wineGroupId];
        self.arrGroupWine = (NSMutableArray *)[WineGroupItem MR_findAllSortedBy:@"wineName" ascending:YES withPredicate:predicate];
        if (self.arrGroupWine.count == 0) {
            retResult=NO;
            NSLog(@"Un Success");
            NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No Wine found",self.networkErrMsg];
            [self showAlertWithMessage:msgStr];
        }
	}
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting loadWineBasedOnGroups");
	
	return retResult;
}

-(void)queueFinished:(id) aQueue{
	
}

-(BOOL)loadGroupDishList:(NSData*)aData
{
	//http://192.168.0.12/fairprice/GetGroupDishListXML.php
	BOOL retResult=NO;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Loading _read_food_category");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
    self.arrGroupDish=[NSMutableArray array];
	
    FairPriceAppDelegate * delegate = [[UIApplication sharedApplication] delegate];
    
        _xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
        
        resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
        
        NSLog(@"Total count for GroupDishList = %d",[resultNodes count]);
        
        if([resultNodes count]>0)
        {
            self.aQueue = [ASINetworkQueue queue];
            [aQueue setDelegate:self];
            [aQueue setShouldCancelAllRequestsOnFailure:NO];
            [aQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
            //[aQueue setQueueDidFinishSelector:@selector(queueFinished:)];
            
            NSManagedObjectContext *localContext   = [NSManagedObjectContext MR_contextForCurrentThread];
            [GroupDishItem truncateAll]; // Dump the database
            
            for(CXMLElement *resultElement in resultNodes)
            {
                int counter;
                
                GroupDishItem * groupDishData = [GroupDishItem createEntity];
                NSLog(@"resultElementid = %@",resultElement);
                for (counter = 0 ; counter < [resultElement childCount] ; counter++)
                {
                    NSLog(@"childCount = %d",[resultElement childCount]);
                    CXMLNode *node = [resultElement childAtIndex:counter];
                    NSLog(@"node values = %@",[node stringValue]);
                    
                    //return value1
                    if ([[node name] isEqualToString:@"foodcategory_id"])
                    {
                        NSLog(@"foodcategory_id: %@", [node stringValue] );
                        groupDishData.groupDishID=[node stringValue];
                    }
                    //return value2
                    if ([[node name] isEqualToString:@"foodcategory_name"])
                    {
                        NSLog(@"foodcategory_name: %@", [node stringValue] );
                        groupDishData.dishTitle=[node stringValue];
                        //update into data structure
                    }
                    //return value3
                    if ([[node name] isEqualToString:@"foodcategory_image"])
                    {
                        NSLog(@"foodcategory_image: %@", [node stringValue] );
                        
                        groupDishData.dishImage = [node stringValue];
                        NSLog(@"%@:%@ ",[node name],[node stringValue]);
                        ASIHTTPRequest* aRequest = [ASIHTTPRequest requestWithURL:
                                                    [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASEIMAGEURL,[node stringValue]]]];
                        
                        [aRequest setUsername:@"GroupDishIcon_black"];
                        [aRequest setDelegate:groupDishData];
                        [aQueue addOperation:aRequest];
                        //update into data structure
                    }
                }
                /*[localContext MR_saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                    NSLog(@"Saved %@", groupDishData.dishTitle);
                }];*/
                //[groupDishData MR_inContext:localContext];
                // Save the modification in the local context
                [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                    if  (success) NSLog(@"groupDishData saved");
                }];
                [self.arrGroupDish addObject:groupDishData];
            }
            self.arrGroupDish = (NSMutableArray *)[GroupDishItem MR_findAllSortedBy:@"dishTitle" ascending:YES];
            retResult=YES;
        }
        else
        {
            self.arrGroupDish = (NSMutableArray *)[GroupDishItem MR_findAllSortedBy:@"dishTitle" ascending:YES];
            for (GroupDishItem * dishItem in self.arrGroupDish) {
                NSLog(@"dish item %@",dishItem.dishTitle);
            }
            
            if ([self.arrGroupDish count]==0) {
                NSLog(@"Un Success");
                 NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No Dish Item found",self.networkErrMsg];
                 [self showAlertWithMessage:msgStr];
                 
                 retResult=NO;
            }
        }
    
        if([self.arrGroupDish count] >0)
        { 
            [aQueue go];
            
        }
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
    NSLog(@"Exiting GroupDishList");
	return retResult;
}

-(BOOL)loadDishList:(NSData*)aData withParent:(NSString *)groupDishItemId
{
	BOOL retResult;
	NSArray *resultNodes = NULL;
    
	
	NSLog(@"Loading DishList... and group dish Item id %@",groupDishItemId);
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	[self setArrDishItem:[NSMutableArray arrayWithCapacity:10]];
	
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
	resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
	NSLog(@"Total count for DishList = %d",[resultNodes count]);
	
	if([resultNodes count]>0)
	{
        
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        //[DishItem truncateAll];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"groupId == %@",groupDishItemId];
        [DishItem MR_deleteAllMatchingPredicate:predicate];
        
        // Look for parent item first
        NSArray* parentGroupDishItemArr = [GroupDishItem MR_findByAttribute:@"groupDishID" withValue:groupDishItemId];
        GroupDishItem *parentGroupDishItem = [parentGroupDishItemArr objectAtIndex:0];
        
		for(CXMLElement *resultElement in resultNodes)
		{
			int counter;
			DishItem *dishData = [DishItem MR_createEntity];
			
			for (counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
				if ([[node name] isEqualToString:@"food_id"])
				{
					NSLog(@"DishID: %@", [node stringValue] );
					dishData.dishID=[node stringValue];
				}
				if ([[node name] isEqualToString:@"food_name"])
				{   
					dishData.dishTitle=[node stringValue];
					//update into data structure
				}
			}
            dishData.groupDishItem = parentGroupDishItem;
            dishData.groupId = groupDishItemId;
            
            [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if  (success) NSLog(@"DishItem saved");
            }];
            
			[self.arrDishItem addObject:dishData];
		}		
		retResult=YES;
	}
	else
	{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"groupId == %@",groupDishItemId];
        
        self.arrDishItem = (NSMutableArray *)[DishItem MR_findAllWithPredicate:predicate];;
        for (DishItem * dishItem in self.arrDishItem) {
            NSLog(@"dish item %@",dishItem.dishTitle);
        }
        
        if ([self.arrDishItem count]==0) {
            NSLog(@"Un Success");
            NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No dish found",self.networkErrMsg];
            [self showAlertWithMessage:msgStr];
            retResult=NO;
        }
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting DishList");
	
	return retResult;
}


-(BOOL)loadWineGroupInfo:(NSData*)aData
{
	//http://192.168.0.12/fairprice/GetWineGroupInfoXML.php
	BOOL retResult = NO;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Loading WineGroupInfo...");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	//NSMutableArray* ar  = [NSMutableArray arrayWithCapacity:10];
	self.arrWineInfo=[NSMutableArray arrayWithCapacity:10];
	
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
	resultNodes = [_xmlParser nodesForXPath:@"//WineGroupInfo" error:nil];
	NSLog(@"Total count for WineGroupInfo = %d",[resultNodes count]);
	
	if([resultNodes count]>0)
	{ 
		for(CXMLElement *resultElement in resultNodes)
		{
			int counter;
            WineInfo *wineInfoData = [[WineInfo alloc] init];
			
			for (counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
				if ([[node name] isEqualToString:@"WineGroupID"])
				{
					NSLog(@"WineGroupID: %@", [node stringValue] );
					wineInfoData.wineGroupID=[node stringValue];
				}
				if ([[node name] isEqualToString:@"WineGroupTitle"])
				{   
					NSLog(@"Value:%@", [node stringValue] );
					wineInfoData.wineTitle=[node stringValue];
					//update into data structure
				}
				if ([[node name] isEqualToString:@"WineGroupDesc"])
				{   
					NSLog(@"Value:%@", [node stringValue] );
					[wineInfoData setWineDescription:[node stringValue]];
					//update into data structure
				}
				if ([[node name] isEqualToString:@"FirstWineID"])
				{  
					NSLog(@"Value:%@", [node stringValue] );
					wineInfoData.firstWineID=[node stringValue];
					//update into data structure
				}
				if ([[node name] isEqualToString:@"FoodToGo"])
				{
					NSArray *resultFoodNodes = NULL;
					resultFoodNodes = [_xmlParser nodesForXPath:@"//WineGroupInfo/FoodToGo/FoodItem" error:nil];
					//NSLog(@"Total count for FoodToGo = %d",[resultNodes count]);
					
					if([resultFoodNodes count]>0)
					{ 
						for(CXMLElement *resultFoodElement in resultFoodNodes)
						{
							int counterFood;
							
							for (counterFood = 0 ; counterFood < [resultFoodElement childCount] ; counterFood++)
							{
								CXMLNode *nodeFood = [resultFoodElement childAtIndex:counterFood];
								
								if ([[nodeFood name] isEqualToString:@"FoodID"])
								{
									NSLog(@"Value:%@", [nodeFood stringValue] );
								}
								if ([[nodeFood name] isEqualToString:@"FoodName"])
								{   
									NSLog(@"Value:%@", [nodeFood stringValue] );
									[wineInfoData.arrFoodToGo addObject:[nodeFood stringValue]];
									//update data structure
								}	
								//NSLog(@"%@ = %@", [nodeFood name] , [nodeFood stringValue] );
							}			 
						}
					}	
				}
			}
			
			//[arrWineInfo addObject:wineInfoData];
			
		}	
		retResult=YES;
		
	}
	else 
	{
	    NSLog(@"Un Success");
		NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No wine info found",self.networkErrMsg];
		[self showAlertWithMessage:msgStr];
		
		retResult=NO;
	}
	
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting WineGroupInfo %d ",[arrWineInfo count]);
	
	return retResult;
}

-(BOOL)loadWineBuyInfo:(NSData*)aData
{
	//http://192.168.0.12/fairprice/GetWineBuyInfoXML.php
	BOOL retResult;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Loading WineBuyInfo...");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	[self setArrWineBuyInfo:[NSMutableArray array]];
	
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
	resultNodes = [_xmlParser nodesForXPath:@"//WineList/WineInformation" error:nil];
	NSLog(@"Total count for WineBuyInfo = %d",[resultNodes count]);
	
	if([resultNodes count]>0)
	{  
		self.aQueue = [ASINetworkQueue queue];
	    [aQueue setDelegate:self];
		[aQueue setShouldCancelAllRequestsOnFailure:NO];
		[aQueue setMaxConcurrentOperationCount:NSOperationQueueDefaultMaxConcurrentOperationCount];
		//[aQueue setQueueDidFinishSelector:@selector(queueFinished:)];
		for(CXMLElement *resultElement in resultNodes)
		{
			int counter;
            WineBuyInfo *wineBuyInfo = [[WineBuyInfo alloc] init];
			
			for (counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
				if ([[node name] isEqualToString:@"WineID"])
				{   
					wineBuyInfo.wineID=[node stringValue];
					NSLog(@"WineID %@", [node stringValue] );
				}
				if ([[node name] isEqualToString:@"Title"])
				{  
					NSLog(@"Title %@", [node stringValue] );
					wineBuyInfo.title=[node stringValue];
					//update into data structure
				}
				if ([[node name] isEqualToString:@"Price"])
				{   
					NSLog(@"Price %@", [node stringValue] );
					wineBuyInfo.price=[node stringValue];
					//update into data structure
				}
				if ([[node name] isEqualToString:@"AverageRating"])
				{    
					NSLog(@"AverageRating %@", [node stringValue] );
					wineBuyInfo.rating=[node stringValue];
					//update into data structure
				}
				if ([[node name] isEqualToString:@"Description"])
				{  
					NSLog(@"Description %@", [node stringValue] );
					wineBuyInfo.description=[node stringValue];
					//update into data structure
				}
				if ([[node name] isEqualToString:@"wineImage"])
				{  
					NSLog(@"WineImage: %@", [node stringValue] );
					//NSData* imageData = [[NSData alloc] initWithContentsOfURL:[NSURL URLWithString:[node stringValue]]];
					//wineBuyInfo.wineImage=imageData;
					//@"http://www.whatsupatfairprice.com.sg/winefinder/image/Wyndham-bin333-Pinot-Noir.png"
					NSLog(@"%@:%@ ",[node name],[node stringValue]);
					ASIHTTPRequest* aRequest = [ASIHTTPRequest requestWithURL:[NSURL URLWithString:[node stringValue]]];
					[aRequest setDelegate:wineBuyInfo];
					[aQueue addOperation:aRequest];
					
					//update into data structure					
				}
			}
			
            [self.arrWineBuyInfo addObject:wineBuyInfo];
		}		
		retResult=YES;
	}
	else
	{  
		NSLog(@"Un Success");
		NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No wine information found",self.networkErrMsg];
		[self showAlertWithMessage:msgStr];
		
        
		retResult=NO;
	}
	if ([self.arrWineBuyInfo count]>0) 
	{
		[aQueue go];
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting WineBuyInfo");
	
	return retResult;
}

-(BOOL)loadDishInfo:(NSData*)aData withParent:(NSString *)dishItemId
{
	//http://192.168.0.12/fairprice/GetDishInfoXML.php?DishID=23
	BOOL retResult;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Loading DishInfo... and dish id %@",dishItemId);
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	[self setArrDishInfo:[NSMutableArray arrayWithCapacity:10]];
	
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
	resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
	NSLog(@"Total count for DishInfo = %d",[resultNodes count]);
	
	if([resultNodes count]>0)
	{
        NSManagedObjectContext *localContext = [NSManagedObjectContext MR_contextForCurrentThread];
        //[DishItem truncateAll];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dishID == %@",dishItemId];
        [DishInfo MR_deleteAllMatchingPredicate:predicate];
		for(CXMLElement *resultElement in resultNodes)
		{
			int counter;
			DishInfo *dishData = [DishInfo MR_createEntity];
			
			for (counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
				if ([[node name] isEqualToString:@"DishID"])
				{   
					NSLog(@"%@ = %@", [node name] , [node stringValue] );
					dishData.dishID=[node stringValue];
                
				}
				if ([[node name] isEqualToString:@"food_name"])
				{  
					dishData.dishTitle=[node stringValue];
					//update into data structure
				}
				if ([[node name] isEqualToString:@"food_desc"])
				{   
					NSLog(@"%@ = %@", [node name] , [node stringValue] );
					dishData.dishDescription1=[node stringValue];
					//update into data structure
				}
                
				if ([[node name] isEqualToString:@"winetype_name"])
				{   
					NSLog(@"%@ = %@", [node name] , [node stringValue] );
					dishData.wineTypeName=[node stringValue];
					//update into data structure
				}
                if ([[node name] isEqualToString:@"wine_id"])
				{   
					NSLog(@"%@ = %@", [node name] , [node stringValue] );
					dishData.wineID=[node stringValue];
					//update into data structure
				}
                if ([[node name] isEqualToString:@"winetype_id"])
				{   
					NSLog(@"%@ = %@", [node name] , [node stringValue] );
					dishData.wineTypeID=[node stringValue];
					//update into data structure
				}
                if ([[node name] isEqualToString:@"wine_name"])
				{   
					NSLog(@"%@ = %@", [node name] , [node stringValue] );
					dishData.wineName=[node stringValue];
					//update into data structure
				}
                if ([[node name] isEqualToString:@"wine_price"])
				{   
					NSLog(@"%@ = %@", [node name] , [node stringValue] );
					dishData.winePrice=[node stringValue];
					//update into data structure
				}
                if ([[node name] isEqualToString:@"wine_rating"])
				{   
					NSLog(@"%@ = %@", [node name] , [node stringValue] );
					dishData.wineRating=[node stringValue];
					//update into data structure
				}
                if ([[node name] isEqualToString:@"wine_image"])
				{   
                    
                    NSLog(@"%@:%@ ",[node name],[node stringValue]);
					ASIHTTPRequest* aRequest = [ASIHTTPRequest requestWithURL:
                                                [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",BASEIMAGEURL,[node stringValue]]]];
                    
					[aRequest setUsername:@"WineImage"];
					[aRequest setDelegate:dishData];
					[aQueue addOperation:aRequest];
                    
					//update into data structure
				}
                dishData.dishID=dishItemId;
                //				if ([[node name] isEqualToString:@"WineToGo"])
                //				{   
                //					CXMLNode *resultWineNodes = NULL;
                //					
                //					for (int counterFood = 0 ; counterFood < [node childCount] ; counterFood++)
                //					{   
                //						resultWineNodes = [node childAtIndex:counterFood];
                //						WineInfo  *wineData = [[WineInfo alloc] init];
                //						for (int counterWineNode = 0 ; counterWineNode < [resultWineNodes childCount] ; counterWineNode++)
                //						{
                //							CXMLNode *nodeWine = [resultWineNodes childAtIndex:counterWineNode];
                //							
                //							if ([[nodeWine name] isEqualToString:@"ID"])
                //							{   
                //								NSLog(@"%@ = %@",[nodeWine name], [nodeWine stringValue] );
                //								wineData.wineGroupID=[nodeWine stringValue];
                //							}
                //							if ([[nodeWine name] isEqualToString:@"Title"])
                //							{
                //								NSLog(@"%@ = %@",[nodeWine name], [nodeWine stringValue] );
                //								wineData.wineTitle=[nodeWine stringValue];
                //							}
                //							if ([[nodeWine name] isEqualToString:@"WID"])
                //							{
                //								NSLog(@"%@ = %@",[nodeWine name], [nodeWine stringValue] );
                //								wineData.firstWineID=[nodeWine stringValue];
                //							}
                //							
                //						
                //						}
                //						
                //						[dishData.arrWineTogo addObject:wineData];
                //						[wineData release];
                //						
                //					}
                //				}
			}
            [[NSManagedObjectContext MR_defaultContext] saveToPersistentStoreWithCompletion:^(BOOL success, NSError *error) {
                if  (success) NSLog(@"DishItemInfo saved");
            }];
			
            [self.arrDishInfo addObject:dishData];
		}		
		retResult=YES;
	}
	else
	{
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"dishID == %@",dishItemId];
        self.arrDishInfo = [DishInfo MR_findAllWithPredicate:predicate];
        if (self.arrDishInfo == 0) {
            NSLog(@"Un Success");
            NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No dish information found",self.networkErrMsg];
            [self showAlertWithMessage:msgStr];
            
            retResult=NO;
        }
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting DishInfo");
	
	return retResult;
}

-(BOOL)loadSearchFilter{
    
	BOOL retResult=NO;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Loading SearchFilter...");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	self.arrWineType=[NSMutableArray array];
	self.arrPriceRange=[NSMutableArray array];
	self.arrCountry=[NSMutableArray array];
    self.arrWineGroupType = [NSMutableArray array];
	
    //Calling webservice for information
    NSString *urlString = [NSString stringWithFormat:@"%@modules/foodwine/_read_wine_filter.php?mode=M",BASEURL];    
    NSString *percentEscapedString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"urlString = %@",percentEscapedString);
    NSURL *urlLink = [NSURL URLWithString:percentEscapedString];
    
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:urlLink];
    
    NSURLResponse *UIDresponse;
    NSError *UIDError = nil;
    
    NSData *getTypeTitles = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&UIDresponse error:&UIDError];
    
    NSString *responseBody = [[NSString alloc] initWithData:getTypeTitles encoding:NSUTF8StringEncoding];
    
    if(UIDError){
        NSLog(@"Error in loadSearchFilter");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"You are not connected to the Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
	_xmlParser = [[CXMLDocument alloc] initWithData:getTypeTitles options:0 error:nil];
    
    //    NSLog(@"responseBody = %@",responseBody);
    
	//Parsing WineByType...
	resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
    
    
    
	if([resultNodes count]>0)
	{  
        
        [self.arrWineGroupType removeAllObjects];
		for(CXMLElement *resultElement in resultNodes)
		{
            WineGroupType *wineGroupData = [[WineGroupType alloc]init];
            
            
            
			for (int counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
                //				if ([[node name] isEqualToString:@"config_key"])
                //				{
                //					NSLog(@"%@ = %@", [node name], [node stringValue] );
                //					[self.arrWineType addObject:[node stringValue]];
                //				}
                if ([[node name] isEqualToString:@"config_key"])
				{
					NSLog(@"%@ = %@", [node name], [node stringValue] );
					wineGroupData.wineConfigKey = [node stringValue];
				}
                if ([[node name] isEqualToString:@"config_value"])
				{
					NSLog(@"%@ = %@", [node name], [node stringValue] );
					wineGroupData.wineConfigValue = [node stringValue];
				}
			}
            [self.arrWineGroupType addObject:wineGroupData];
		}	
		retResult=YES;
	}
    
    //    if(retResult==YES && [resultCountryNodes count]>0)
    //	{  [arrCountry removeAllObjects];
    //		for(CXMLElement *resultCountryElement in resultCountryNodes)
    //		{   
    //			Country *countryData=[[Country alloc]init];
    //			for (int counterCountry = 0;counterCountry < [resultCountryElement childCount];counterCountry++)
    //			{
    //				CXMLNode *nodeCountry = [resultCountryElement childAtIndex:counterCountry];
    //				if ([[nodeCountry name] isEqualToString:@"country_code"])
    //				{
    //					NSLog(@"%@ = %@", [nodeCountry name] , [nodeCountry stringValue] );
    //					countryData.countryCode=[nodeCountry stringValue];
    //				}
    //				if ([[nodeCountry name] isEqualToString:@"country_name"])
    //				{
    //					NSLog(@"%@ = %@", [nodeCountry name] , [nodeCountry stringValue] );
    //					countryData.countryName=[nodeCountry stringValue];
    //				}
    //			}
    //			[self.arrCountry addObject:countryData];
    //			[countryData release];
    //		}
    //		retResult=YES;
    //	}
	
	//Parsing PriceRange/Range...
    //	NSArray *resultRangeNodes = [_xmlParser nodesForXPath:@"//SearchFilter/PriceRange/Range" error:nil];
    //	
    //	
    //	if(retResult==YES && [resultRangeNodes count]>0)
    //	{  
    //		[arrPriceRange removeAllObjects];
    //		PriceRange *ran=[[PriceRange alloc]init];
    ////		[ran setMaxPrice:@"All"];
    ////		[ran setMinPrice:@"All"];
    //		[self.arrPriceRange addObject:ran];
    //		[ran release];
    //		
    //		for(CXMLElement *resultRangeElement in resultRangeNodes)
    //		{   
    //			PriceRange *range=[[PriceRange alloc]init];
    //			
    //			for (int counterRange = 0;counterRange < [resultRangeElement childCount];counterRange++)
    //			{
    //				CXMLNode *nodeRange = [resultRangeElement childAtIndex:counterRange];
    //				
    //				if ([[nodeRange name] isEqualToString:@"Min"])
    //				{  
    //					NSLog(@"%@ = %@", [nodeRange name] , [nodeRange stringValue] );
    ////					range.minPrice=[nodeRange stringValue];
    //					
    //				}
    //				if ([[nodeRange name] isEqualToString:@"Max"])
    //				{
    //					NSLog(@"%@ = %@", [nodeRange name] , [nodeRange stringValue] );
    ////					range.maxPrice= [nodeRange stringValue];
    //				}
    //			}
    //			
    //			[self.arrPriceRange addObject:range];
    //			[range release];
    //		}
    //		retResult=YES;
    //	}
	
	//Parsing Rating...
    //	resultNodes = [_xmlParser nodesForXPath:@"//SearchFilter/Rating" error:nil];
    //	
    //	if(retResult==YES && [resultNodes count]>0)
    //	{ 
    //		for(CXMLElement *resultElement in resultNodes)
    //		{
    //			//WineBuyInfo *wineBuyInfo = [[WineBuyInfo alloc] init];
    //			
    //			for (int counter = 0 ; counter < [resultElement childCount] ; counter++)
    //			{
    //				CXMLNode *node = [resultElement childAtIndex:counter];
    //				
    //				if ([[node name] isEqualToString:@"MaxStar"])
    //				{
    //					NSLog(@"%@ = %@", [node name], [node stringValue] );
    //					self.maxRating=[node stringValue];
    //				}
    //			}
    //			
    //			//[self.ArrRedWine addObject:wineBuyInfo];
    //			//[wineBuyInfo release];
    //		}		
    //		retResult=YES;
    //	}	
	
	//Parsing Countries/Country...
    //	NSArray *resultCountryNodes = [_xmlParser nodesForXPath:@"//SearchFilter/Countries/Country" error:nil];
    
    //Calling webservice for information
    urlString = [NSString stringWithFormat:@"%@modules/foodwine/_read_country.php?mode=M",BASEURL];    
    percentEscapedString = [urlString stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    
    NSLog(@"urlString = %@",percentEscapedString);
    urlLink = [NSURL URLWithString:percentEscapedString];
    
    urlRequest = [NSURLRequest requestWithURL:urlLink];
    
    UIDresponse = nil;
    UIDError = nil;
    
    NSData *getCountryTitles = [NSURLConnection sendSynchronousRequest:urlRequest returningResponse:&UIDresponse error:&UIDError];
    
    responseBody = [[NSString alloc] initWithData:getCountryTitles encoding:NSUTF8StringEncoding];
    
    if(UIDError){
        NSLog(@"Error in loadSearchFilter");
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry" message:@"You are not connected to the Internet" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return NO;
    }
    
	_xmlParser = [[CXMLDocument alloc] initWithData:getCountryTitles options:0 error:nil];
	
    NSArray *resultCountryNodes = [_xmlParser nodesForXPath:@"//Countries/items/item" error:nil];
    
	if(retResult==YES && [resultCountryNodes count]>0)
	{  
        [arrCountry removeAllObjects];
		for(CXMLElement *resultCountryElement in resultCountryNodes)
		{   
			Country *countryData=[[Country alloc]init];
			for (int counterCountry = 0;counterCountry < [resultCountryElement childCount];counterCountry++)
			{
				CXMLNode *nodeCountry = [resultCountryElement childAtIndex:counterCountry];
				if ([[nodeCountry name] isEqualToString:@"country_code"])
				{
					NSLog(@"%@ = %@", [nodeCountry name] , [nodeCountry stringValue] );
					countryData.countryCode=[nodeCountry stringValue];
				}
				if ([[nodeCountry name] isEqualToString:@"country_name"])
				{
					NSLog(@"%@ = %@", [nodeCountry name] , [nodeCountry stringValue] );
					countryData.countryName=[nodeCountry stringValue];
				}
			}
			[self.arrCountry addObject:countryData];
		}
		retResult=YES;
	}
	
	
	if (retResult != YES)
	{  
		NSLog(@"Un Success");
		NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No filter data found",self.networkErrMsg];
		[self showAlertWithMessage:msgStr];
		
		retResult=NO;
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting SearchFilter");
	
	return retResult;
}

//not in use[look at -BOOL loadSearchFilter]
-(BOOL)loadSearchFilter:(NSData*)aData
{
	//http://192.168.0.12/fairprice/GetSearchFilterXML.php
	BOOL retResult=NO;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Loading SearchFilter...");
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	self.arrWineType=[NSMutableArray array];
	self.arrPriceRange=[NSMutableArray array];
	self.arrCountry=[NSMutableArray array];
	
	
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
	//Parsing WineByType...
	resultNodes = [_xmlParser nodesForXPath:@"//SearchFilter/WineByType" error:nil];
    //	resultNodes = [_xmlParser nodesForXPath:@"//Countries/items/item" error:nil];
	
	if([resultNodes count]>0)
	{  
		for(CXMLElement *resultElement in resultNodes)
		{
			for (int counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
				if ([[node name] isEqualToString:@"TypeTitle"])
                    //				if ([[node name] isEqualToString:@"country_name"])
				{
					NSLog(@"%@ = %@", [node name], [node stringValue] );
					[self.arrWineType addObject:[node stringValue]];
				}
			}
			
			
		}		
		retResult=YES;
	}
	
	//Parsing PriceRange/Range...
	NSArray *resultRangeNodes = [_xmlParser nodesForXPath:@"//SearchFilter/PriceRange/Range" error:nil];
	
	
	if(retResult==YES && [resultRangeNodes count]>0)
	{  
		[arrPriceRange removeAllObjects];
		PriceRange *ran=[[PriceRange alloc]init];
        //		[ran setMaxPrice:@"All"];
        //		[ran setMinPrice:@"All"];
		[self.arrPriceRange addObject:ran];
		
		for(CXMLElement *resultRangeElement in resultRangeNodes)
		{   
			PriceRange *range=[[PriceRange alloc]init];
			
			for (int counterRange = 0;counterRange < [resultRangeElement childCount];counterRange++)
			{
				CXMLNode *nodeRange = [resultRangeElement childAtIndex:counterRange];
				
				if ([[nodeRange name] isEqualToString:@"Min"])
				{  
					NSLog(@"%@ = %@", [nodeRange name] , [nodeRange stringValue] );
                    //					range.minPrice=[nodeRange stringValue];
					
				}
				if ([[nodeRange name] isEqualToString:@"Max"])
				{
					NSLog(@"%@ = %@", [nodeRange name] , [nodeRange stringValue] );
                    //					range.maxPrice= [nodeRange stringValue];
				}
			}
			
			[self.arrPriceRange addObject:range];
		}
		retResult=YES;
	}
	
	//Parsing Rating...
	resultNodes = [_xmlParser nodesForXPath:@"//SearchFilter/Rating" error:nil];
	
	if(retResult==YES && [resultNodes count]>0)
	{ 
		for(CXMLElement *resultElement in resultNodes)
		{
			//WineBuyInfo *wineBuyInfo = [[WineBuyInfo alloc] init];
			
			for (int counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
				if ([[node name] isEqualToString:@"MaxStar"])
				{
					NSLog(@"%@ = %@", [node name], [node stringValue] );
					self.maxRating=[node stringValue];
				}
			}
			
			//[self.ArrRedWine addObject:wineBuyInfo];
			//[wineBuyInfo release];
		}		
		retResult=YES;
	}	
	
	//Parsing Countries/Country...
    //	NSArray *resultCountryNodes = [_xmlParser nodesForXPath:@"//SearchFilter/Countries/Country" error:nil];
	NSArray *resultCountryNodes = [_xmlParser nodesForXPath:@"//Countries/items/item" error:nil];
	
	if(retResult==YES && [resultCountryNodes count]>0)
        //    if([resultCountryNodes count]>0)
	{  [arrCountry removeAllObjects];
		for(CXMLElement *resultCountryElement in resultCountryNodes)
		{   
			Country *countryData=[[Country alloc]init];
			for (int counterCountry = 0;counterCountry < [resultCountryElement childCount];counterCountry++)
			{
				CXMLNode *nodeCountry = [resultCountryElement childAtIndex:counterCountry];
				
				if ([[nodeCountry name] isEqualToString:@"ID"])
				{
					NSLog(@"%@ = %@", [nodeCountry name] , [nodeCountry stringValue] );
					
				}
				if ([[nodeCountry name] isEqualToString:@"country_code"])
				{
					NSLog(@"%@ = %@", [nodeCountry name] , [nodeCountry stringValue] );
					countryData.countryCode=[nodeCountry stringValue];
				}
				if ([[nodeCountry name] isEqualToString:@"country_name"])
				{
					NSLog(@"%@ = %@", [nodeCountry name] , [nodeCountry stringValue] );
					countryData.countryName=[nodeCountry stringValue];
				}
			}
			[self.arrCountry addObject:countryData];
		}
		retResult=YES;
	}
	
	
	if (retResult != YES)
	{  
		NSLog(@"Un Success");
		NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No filter data found",self.networkErrMsg];
		[self showAlertWithMessage:msgStr];
		
		retResult=NO;
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting SearchFilter");
	
	return retResult;
}

-(BOOL)loadTipsArticle:(NSData*)aData{
	BOOL retResult;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Loading Tips Articles...");
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	[self setArrTipsArticles:[NSMutableArray arrayWithCapacity:10]];
	
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
	resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
	
	if([resultNodes count]>0)
	{ 
		for(CXMLElement *resultElement in resultNodes)
		{
			int counter;
			TipsArticles *tipsArticles = [[TipsArticles alloc] init];
			
			for (counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
				if ([[node name] isEqualToString:@"content_id"])
				{
					NSLog(@"ContentID: %@", [node stringValue] );
                    tipsArticles.contentID = [node stringValue];
				}
				if ([[node name] isEqualToString:@"content_title"])
				{   
					NSLog(@"ContentTitle: %@", [node stringValue] );
					tipsArticles.contentTitle = [node stringValue];
					//update into data structure
				}
			}
			
			[self.arrTipsArticles addObject:tipsArticles];
		}		
		retResult=YES;
	}
	else
	{  
		NSLog(@"Un Success");
		NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No Tips Articles found",self.networkErrMsg];
		[self showAlertWithMessage:msgStr];
		
		
		retResult=NO;
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting DishList");
	
	return retResult;
}

-(BOOL)loadTipsVideos:(NSData*)aData{
	BOOL retResult;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Loading Tips Videos...");
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	[self setArrTipsVideos:[NSMutableArray arrayWithCapacity:10]];
	
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
	resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
	
	if([resultNodes count]>0)
	{ 
		for(CXMLElement *resultElement in resultNodes)
		{
			int counter;
			TipsVideos *tipsVideos = [[TipsVideos alloc] init];
			
			for (counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
				if ([[node name] isEqualToString:@"content_id"])
				{
					NSLog(@"ContentID: %@", [node stringValue] );
                    tipsVideos.contentID = [node stringValue];
				}
				if ([[node name] isEqualToString:@"content_title"])
				{   
					NSLog(@"ContentTitle: %@", [node stringValue] );
					tipsVideos.contentTitle = [node stringValue];
					//update into data structure
				}
			}
			
			[self.arrTipsVideos addObject:tipsVideos];
		}		
		retResult=YES;
	}
	else
	{  
		NSLog(@"Un Success");
		NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No Tips Articles found",self.networkErrMsg];
		[self showAlertWithMessage:msgStr];
		
		
		retResult=NO;
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting DishList");
	
	return retResult;
}


-(BOOL)loadTipsArticleItem:(NSData*)aData{
    BOOL retResult;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Loading Tips Article Item...");
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	[self setArrTipsArticleItem:[NSMutableArray arrayWithCapacity:10]];
	
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
	resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
	
	if([resultNodes count]>0)
	{ 
		for(CXMLElement *resultElement in resultNodes)
		{
			int counter;
			TipsArticleItem *tipsArticleItem = [[TipsArticleItem alloc] init];
			
			for (counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
				if ([[node name] isEqualToString:@"content_title"])
				{
					NSLog(@"ContentTitle: %@", [node stringValue] );
                    tipsArticleItem.contentTitle = [node stringValue];
				}
				if ([[node name] isEqualToString:@"content_desc"])
				{   
					NSLog(@"ContentDesc: %@", [node stringValue] );
					tipsArticleItem.contentDesc = [node stringValue];
					//update into data structure
				}
			}
			
			[self.arrTipsArticleItem addObject:tipsArticleItem];
		}		
		retResult=YES;
	}
	else
	{  
		NSLog(@"Un Success");
		NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No Tips Articles found",self.networkErrMsg];
		[self showAlertWithMessage:msgStr];
		
		
		retResult=NO;
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting DishList");
	
	return retResult;
    
    
}
-(BOOL)loadTipsVideoItem:(NSData*)aData{
	BOOL retResult;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Loading Tips Video Item...");
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	[self setArrTipsVideoItem:[NSMutableArray arrayWithCapacity:10]];
	
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
	resultNodes = [_xmlParser nodesForXPath:@"//items/item" error:nil];
	
	if([resultNodes count]>0)
	{ 
		for(CXMLElement *resultElement in resultNodes)
		{
			int counter;
			TipsVideoItem *tipsVideoItem = [[TipsVideoItem alloc] init];
			
			for (counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				
				if ([[node name] isEqualToString:@"content_title"])
				{
					NSLog(@"ContentTitle: %@", [node stringValue] );
                    tipsVideoItem.contentTitle = [node stringValue];
				}
				if ([[node name] isEqualToString:@"content_desc"])
				{   
					NSLog(@"ContentDesc: %@", [node stringValue] );
					tipsVideoItem.contentDesc = [node stringValue];
					//update into data structure
				}
			}
			
			[self.arrTipsVideoItem addObject:tipsVideoItem];
		}		
		retResult=YES;
	}
	else
	{  
		NSLog(@"Un Success");
		NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"No Tips Articles found",self.networkErrMsg];
		[self showAlertWithMessage:msgStr];
		
		
		retResult=NO;
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting DishList");
	
	return retResult;
    
}

-(BOOL)loadSubmitOrder:(NSData*)aData{
	BOOL retResult;
	NSArray *resultNodes = NULL;
	
	NSLog(@"Submitting Order...");
    
	[UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
	
	[self setArrStatus:[NSMutableArray arrayWithCapacity:10]];
	
	_xmlParser = [[CXMLDocument alloc] initWithData:aData options:0 error:nil];
	
//    NSLog(@"_xmlParser = %@",_xmlParser);
    
	resultNodes = [_xmlParser nodesForXPath:@"//BulkOrder" error:nil];
	
	if([resultNodes count]>0)
	{ 
		for(CXMLElement *resultElement in resultNodes)
		{
			int counter;
			Status *statusData = [[Status alloc] init];
			
			for (counter = 0 ; counter < [resultElement childCount] ; counter++)
			{
				CXMLNode *node = [resultElement childAtIndex:counter];
				NSLog(@"node = %@:%@",[node name],[node stringValue]);
				if ([[node name] isEqualToString:@"status"])
				{
					NSLog(@"SubmitStatus: %@", [node stringValue] );
                    statusData.submitStatus = [node stringValue];
				}
				if ([[node name] isEqualToString:@"message"])
				{   
					NSLog(@"SubmitMessage: %@", [node stringValue] );
					statusData.submitMessage = [node stringValue];
					//update into data structure
				}
			}
			
			[self.arrStatus addObject:statusData];
		}		
		retResult=YES;
	}
	else
	{  
		NSLog(@"Un Success");
		NSString *msgStr=[NSString stringWithFormat:@"%@ \n %@",@"Submission Unsuccessful. Please resubmit your order",self.networkErrMsg];
		[self showAlertWithMessage:msgStr];
		
		retResult=NO;
	}
	
	[UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
	
	NSLog(@"Exiting Submission");
	
	return retResult;
    
}




-(void)showAlertWithMessage:(NSString*) strMessage
{
	UIAlertView *alert=[[UIAlertView alloc]initWithTitle:@"FairPrice" message:strMessage delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok",nil];
	[alert show];
}





@end
