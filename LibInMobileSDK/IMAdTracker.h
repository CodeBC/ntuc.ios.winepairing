///
///  IMAdTracker.h
///  InMobi Ad Tracker SDK
///
///  @author: InMobi
///  Copyright (c) 2012 InMobi Inc. All rights reserved.
///

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

///
/// Different logging levels for debugging purpose.
///
typedef enum IMAdTrackerLogLevel_ {
    ///
    /// Minimal Log Level.
    ///
    IMAdTrackerLogLevelMinimal = 1,
    ///
    /// Debugging Log Level.
    ///
    IMAdTrackerLogLevelDebug = 2,
    ///
    /// Critical Debugging Log Level.
    ///
    IMAdTrackerLogLevelCritical = 3,
    
} IMAdTrackerLogLevel;


///
/// Main class of InMobi Ad Tracker iOS SDK.
/// This contains the code required to track all different application goals.
/// It helps you track the post-click conversions of your iOS App campaigns
/// across all your advertising channels. There is no limitation to what can
/// be tracked: downloads, leads, registrations, purchases, and so on.
///
@interface IMAdTracker : NSObject

///
/// Get the shared instance of this class.
/// @returns Returns the shared instance of this class.
///
+ (id)sharedInstance;

///
/// This starts a session with InMobi Ad Tracker.
/// This is a non blocking API (returns immediately).
/// When you start a session, Installation of your App is reported immediately.
/// @Note: You need start a session with an InMobi Ad Tracker App ID before
/// you can report any custom goal.
/// @param appId InMobi Ad Tracker Application Id.
///
- (void)startSession:(NSString *)appId;

///
/// This reports your application goals to InMobi Ad Tracker.
/// This is a non blocking API (returns immediately), and performs the
/// reporting job in background. When a goal is reported, it is made sure that
/// it reaches InMobi Ad Tracker server whenever the device is connected
/// to internet. You can report different goals without worrying about the
/// internet connection. Note that before you report a goal name using this
/// API, Please make sure that you have registered the same goal name on the
/// InMobi Ad Tracker website first.
/// @param goalName Your custom goal name.
///
- (void)reportGoal:(NSString *)goalName;

///
/// Use this only for cleaning purpose.
/// This stops and cleans up the started session.
///
- (void)stopSession;


///============================================>
///--------- FOR DEBUGGING PURPOSE ------------>
///============================================>

///
/// Use this to set the logging level for debugging.
/// @param loggingLevel logging level for debugging.
///
+ (void)setLogLevel:(IMAdTrackerLogLevel)loggingLevel;

/// 
/// Get the logging level set for debugging.
///
+ (IMAdTrackerLogLevel)getLogLevel;

///
/// Get the current SDK version.
///
+ (NSString *)getSDKVersion;

@end
