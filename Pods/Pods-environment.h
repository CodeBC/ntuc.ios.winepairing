
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// ASIHTTPRequest
#define COCOAPODS_POD_AVAILABLE_ASIHTTPRequest
#define COCOAPODS_VERSION_MAJOR_ASIHTTPRequest 1
#define COCOAPODS_VERSION_MINOR_ASIHTTPRequest 8
#define COCOAPODS_VERSION_PATCH_ASIHTTPRequest 2

// ASIHTTPRequest/ASIWebPageRequest
#define COCOAPODS_POD_AVAILABLE_ASIHTTPRequest_ASIWebPageRequest
#define COCOAPODS_VERSION_MAJOR_ASIHTTPRequest_ASIWebPageRequest 1
#define COCOAPODS_VERSION_MINOR_ASIHTTPRequest_ASIWebPageRequest 8
#define COCOAPODS_VERSION_PATCH_ASIHTTPRequest_ASIWebPageRequest 2

// ASIHTTPRequest/CloudFiles
#define COCOAPODS_POD_AVAILABLE_ASIHTTPRequest_CloudFiles
#define COCOAPODS_VERSION_MAJOR_ASIHTTPRequest_CloudFiles 1
#define COCOAPODS_VERSION_MINOR_ASIHTTPRequest_CloudFiles 8
#define COCOAPODS_VERSION_PATCH_ASIHTTPRequest_CloudFiles 2

// ASIHTTPRequest/Core
#define COCOAPODS_POD_AVAILABLE_ASIHTTPRequest_Core
#define COCOAPODS_VERSION_MAJOR_ASIHTTPRequest_Core 1
#define COCOAPODS_VERSION_MINOR_ASIHTTPRequest_Core 8
#define COCOAPODS_VERSION_PATCH_ASIHTTPRequest_Core 2

// ASIHTTPRequest/S3
#define COCOAPODS_POD_AVAILABLE_ASIHTTPRequest_S3
#define COCOAPODS_VERSION_MAJOR_ASIHTTPRequest_S3 1
#define COCOAPODS_VERSION_MINOR_ASIHTTPRequest_S3 8
#define COCOAPODS_VERSION_PATCH_ASIHTTPRequest_S3 2

// JSONKit
#define COCOAPODS_POD_AVAILABLE_JSONKit
#define COCOAPODS_VERSION_MAJOR_JSONKit 1
#define COCOAPODS_VERSION_MINOR_JSONKit 6
#define COCOAPODS_VERSION_PATCH_JSONKit 0

// MagicalRecord/Shorthand
#define COCOAPODS_POD_AVAILABLE_MagicalRecord_Shorthand
#define COCOAPODS_VERSION_MAJOR_MagicalRecord_Shorthand 2
#define COCOAPODS_VERSION_MINOR_MagicalRecord_Shorthand 2
#define COCOAPODS_VERSION_PATCH_MagicalRecord_Shorthand 0

// Reachability
#define COCOAPODS_POD_AVAILABLE_Reachability
#define COCOAPODS_VERSION_MAJOR_Reachability 3
#define COCOAPODS_VERSION_MINOR_Reachability 1
#define COCOAPODS_VERSION_PATCH_Reachability 1

// SDWebImage
#define COCOAPODS_POD_AVAILABLE_SDWebImage
#define COCOAPODS_VERSION_MAJOR_SDWebImage 3
#define COCOAPODS_VERSION_MINOR_SDWebImage 6
#define COCOAPODS_VERSION_PATCH_SDWebImage 0

// SDWebImage/Core
#define COCOAPODS_POD_AVAILABLE_SDWebImage_Core
#define COCOAPODS_VERSION_MAJOR_SDWebImage_Core 3
#define COCOAPODS_VERSION_MINOR_SDWebImage_Core 6
#define COCOAPODS_VERSION_PATCH_SDWebImage_Core 0

// TouchXML
#define COCOAPODS_POD_AVAILABLE_TouchXML
#define COCOAPODS_VERSION_MAJOR_TouchXML 0
#define COCOAPODS_VERSION_MINOR_TouchXML 1
#define COCOAPODS_VERSION_PATCH_TouchXML 0

